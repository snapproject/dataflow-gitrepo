# $BEGIN_DATAFLOW_LICENSE$
# 
# This file is part of the DataFlow project, a software library for
# efficient asynchronous multi-stream inter-process communication.
# 
# Copyright (c) 2014-2021 Jeremy Hill
# 
# DataFlow is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program. If not, see http://www.gnu.org/licenses/ .
# 
# Some third-party source code is compiled and linked into the
# DataFlow binary, as follows: devel/src/sem_timedwait_apple.cpp
# is adapted from sem_timedwait.cpp by Keith Shortridge, copyright
# (c) 2013 Australian Astronomical Observatory (AAO), released under
# the MIT License; devel/src/dirent_win.h is copyright (c) 2006
# Toni Ronkko, and released under the MIT License. The DataFlow build
# system also uses some third-party code:
# devel/build/cmake-recipes/DefineFunction_TARGET_ARCHITECTURE.cmake
# which is copyright (c) 2012 Petroules Corporation, released under
# the 2-Clause BSD License. For more details, see the respective
# copyright, licensing and disclaimer information in each of these
# files.
# 
# $END_DATAFLOW_LICENSE$

import os
import re
import sys
import struct
import inspect
import platform
import ctypes, ctypes.util

def WhereAmI( nFileSystemLevelsUp=1, nStackLevelsBack=0 ):
	"""
	`WhereAmI( 0 )` is equivalent to `__file__`
	
	`WhereAmI()` or `WhereAmI(1)` gives you the current source file's
	parent directory.
	"""
	try:
		frame = inspect.currentframe()
		for i in range( abs( nStackLevelsBack ) + 1 ):
			frame = frame.f_back
		file = inspect.getfile( frame )
	finally:
		del frame  # https://docs.python.org/3/library/inspect.html#the-interpreter-stack
	return os.path.realpath( os.path.join( file, *[ '..' ] * abs( nFileSystemLevelsUp ) ) )

def LoadSharedLibrary(
	dllNameStem, usePlatformInflection=True,
	sourceDirectoryPriority=1, workingDirectoryPriority=2, systemPathPriority=3,
 ):
	"""
	Args:
		dllNameStem:
			If you pass a stem that does not start with 'lib', then on Windows the name
			of the dynamic library file will be assumed to start exactly as specified,
			whereas on other platforms a 'lib' prefix will always be added.
			If you specify a stem that starts with 'lib', then it will be assumed that
			the name of file starts with 'lib' on all platforms including Windows (an
			additional 'lib' will not be prepended, on other platforms). 
		
		usePlatformInflection:
			If this is False, the file extension is appended directly after the stem.
			If this is True, a suffix reflecting the platform and architecture on which
			we're currently running (of the form '-Windows-32bit, '-Darwin-64bit', etc.)
			is appended to the stem, before the extension.
			If you pass the special value of 'win64only', then the suffix '64' is used
			on 64-bit Windows, and no suffix at all is used on other platforms.
	
		sourceDirectoryPriority, workingDirectoryPriority, systemPathPriority:
			These specify the order in which to search different locations for the
			dynamic library file.  Setting any of them to `None` (or to 0) causes the
			corresponding location to be excluded from consideration. Positive numeric
			values indicate that the location should be considered (in ascending numeric
			order). The "source directory" means the parent directory of the Python file
			from which this function is being called. "System path" means the usual
			system search locations for dynamic libraries (`%PATH%` on Windows,
			`$DYLD_LIBRARY_PATH` on Darwin, `$LD_LIBRARY_PATH` on Linux). Note that on
			Windows, if you are allowing the system path to be searched, you will not
			be able to exclude (or assign a later priority to) the working directory.
	"""
	uname = platform.system()
	if   uname.lower().startswith( 'win'    ): dllPrefix, dllExtension = '',    '.dll'
	elif uname.lower().startswith( 'darwin' ): dllPrefix, dllExtension = 'lib', '.dylib'
	else:                                      dllPrefix, dllExtension = 'lib', '.so'
	
	if usePlatformInflection:
		machine = platform.machine().lower()
		if machine in [ '', 'i386', 'x86_64', 'amd64' ]:
			arch = '%dbit' % ( struct.calcsize( 'P' ) * 8 ) # remember this will depend on the Python executable, not just the machine
		else:
			arch = machine
			if arch.startswith( 'armv' ): arch = arch.rstrip( 'l' )
		platformInflection = '-' + uname + '-' + arch
		if isinstance( usePlatformInflection, str ) and usePlatformInflection.lower() == 'win64only':
			platformInflection = '64' if platformInflection.lower() == 'windows-64bit' else ''
		dllNameStem += platformInflection
		
	dllName = dllNameStem if dllNameStem.startswith( dllPrefix ) else dllPrefix + dllNameStem
	if dllNameStem.startswith( dllPrefix ): dllNameStem = dllNameStem[ len( dllPrefix ): ]
	dllName += dllExtension
	
	locations = []
	if sourceDirectoryPriority:
		locations.append( ( sourceDirectoryPriority, os.path.join( WhereAmI( nStackLevelsBack=1 ), dllName ) ) )
	if workingDirectoryPriority:
		locations.append( ( workingDirectoryPriority, os.path.join( os.getcwd(), dllName ) ) )
	if systemPathPriority:
		locations.append( ( systemPathPriority, ctypes.util.find_library( dllName ) ) )
	locations = [ filePath for priority, filePath in sorted( locations ) if filePath and os.path.isfile( filePath ) ]
	if not locations: raise ImportError( "failed to find dynamic library " + dllName )
	dllPath = os.path.realpath( locations[ 0 ] )
	whereIsDLL = os.path.dirname( dllPath )
	oldWorkingDirectory = os.getcwd()	
	try:
		os.chdir( whereIsDLL )
		return ctypes.CDLL( dllPath )
	finally:
		os.chdir( oldWorkingDirectory )
