/*
# $BEGIN_DATAFLOW_LICENSE$
# 
# This file is part of the DataFlow project, a software library for
# efficient asynchronous multi-stream inter-process communication.
# 
# Copyright (c) 2014-2021 Jeremy Hill
# 
# DataFlow is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program. If not, see http://www.gnu.org/licenses/ .
# 
# Some third-party source code is compiled and linked into the
# DataFlow binary, as follows: devel/src/sem_timedwait_apple.cpp
# is adapted from sem_timedwait.cpp by Keith Shortridge, copyright
# (c) 2013 Australian Astronomical Observatory (AAO), released under
# the MIT License; devel/src/dirent_win.h is copyright (c) 2006
# Toni Ronkko, and released under the MIT License. The DataFlow build
# system also uses some third-party code:
# devel/build/cmake-recipes/DefineFunction_TARGET_ARCHITECTURE.cmake
# which is copyright (c) 2012 Petroules Corporation, released under
# the 2-Clause BSD License. For more details, see the respective
# copyright, licensing and disclaimer information in each of these
# files.
# 
# $END_DATAFLOW_LICENSE$
*/

#include <iostream>
#include <string.h> // for strcmp()
#include <signal.h> // for signal(), sig_atomic_t

#include <Windows.h>

volatile ::sig_atomic_t gCaughtSignal = 0;
volatile HANDLE gAbortEvent = NULL; // NB: must be declared volatile

void CatchOnce( int signum )
{
	gCaughtSignal = signum;
	if( gAbortEvent ) ::SetEvent( gAbortEvent );
	::signal( signum, SIG_DFL ); // in case of deadlock, user can press ctrl-C a second time to terminate as usual 
}

int main( int argc, const char * argv[] )
{
	::signal( SIGINT, CatchOnce );
	HANDLE semaphore = ::CreateSemaphore( NULL, 1, 1, TEXT( "blah" ) );
	gAbortEvent = ::CreateEvent( NULL, FALSE, FALSE, NULL );
	//std::cout << gAbortEvent << "\n";
	
	if( argc > 1 && ::strcmp( argv[ 1 ], "send" ) == 0 )
	{
		::ReleaseSemaphore( semaphore, 1, NULL );
	}
	else if( argc > 1 && strcmp( argv[ 1 ], "wait" ) == 0 )
	{
		HANDLE events[ 2 ] = { semaphore, gAbortEvent };
		DWORD result = ::WaitForMultipleObjects( 2, events, FALSE, INFINITE ); // instead of WaitForSingleObject( semaphore, INFINITE );
		// result will be ( WAIT_OBJECT_0 + 1 ) if gAbortEvent was the one that tripped. In this simple case, gCaughtSignal will also be set:
		if( !gCaughtSignal ) result = ::WaitForMultipleObjects( 2, events, FALSE, INFINITE ); // instead of WaitForSingleObject( semaphore, INFINITE );
		if( !gCaughtSignal ) ::ReleaseSemaphore( semaphore, 1, NULL );
	}
	else do {} while( !gCaughtSignal );
	
	signal( SIGINT, SIG_DFL );
	std::cout << "Cleaning up...\n";
	return 0;
}
