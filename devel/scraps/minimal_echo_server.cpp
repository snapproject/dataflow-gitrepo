/*

The code below is my attempt at a minimal cross-platform echo server in C++.
It does what I expect, except for a platform-dependent asymmetry in the way
IP backward-compatibility is handled: there's a Windows-specific
failure of an IPv6 server to handle requests from IPv4 clients.<sup>*</sup>
It breaks down as follows:

Server uses IPv4 (`minimal_echo_server 8081 4`):
1. client uses IPv4 address:   server responds as expected
2. client uses IPv6 address:   connection fails<sup>**</sup> as expected

Server uses IPv6 on Linux/Darwin (`./minimal_echo_server 8081 6`):
3. client uses IPv4 address:   server responds just fine (renders the client's IP as `::ffff:` followed by its IPv4 address)
4. client uses IPv6 address:   server responds as expected

Server uses IPv6 on Windows 10 (`.\minimal_echo_server.exe 8081 6`)::
5. client uses IPv4 address:   connection fails<sup>**</sup>
6. client uses IPv6 address:   server responds as expected

To me result #3 was an unexpected but welcome win. My questions are: is
it expected that the same thing fails on Windows (compare results #3 and #5)?
And is there something I can do with the server code to get #5 to succeed?


<sup>*</sup> An example of what I mean by "IPv4 client" might be the following `netcat` call:

    nc WWW.XXX.YYY.ZZZ 8081 # target the server via its IPv4 address
	
in contrast to the following where the client uses IPv6:

    nc fe80::WWWW:XXXX:YYYY:ZZZZ%en0 8081  # target the server via its IPv6 address (with the client's local adapter name appended as scope)

<sup>**</sup> In both cases of "failure", the mode of failure seems to depend on the client OS: a Windows 10 client will pause for a few seconds and
then report "No connection could be made because the target machine actively refused it" whereas a Linux or Darwin client seems to hang indefinitely as far as I can tell.



*/

	// Welcome to  minimal_echo_server.cpp
	// This can be compiled with `cl.exe minimal_echo_server.cpp` on Windows 10 using Visual Studio 2019
	// or with `g++ -o minimal_echo_server minimal_echo_server.cpp` on something more GNUey.
	#ifdef _WIN32
	#	pragma comment(lib, "Ws2_32.lib")   // winsock2
	#	include <winsock2.h>
	#	include <ws2tcpip.h>   // for inet_pton() and inet_ntop()
	#	define SOCKET_IS_VALID(S)      (S != INVALID_SOCKET)
	#	define CLOSE_SOCKET closesocket
		typedef int socklen_t;
		bool InitializeSockets(void)
		{
			static bool initialized = false;
			WSADATA wsa;
			if(!initialized && WSAStartup(MAKEWORD(2, 2), &wsa) == 0) initialized = true;
			return initialized;
		}
	#else
	#	include <sys/socket.h> // for socket(), bind(), etc
	#	include <arpa/inet.h>  // for sockaddr_in and inet_ntop()
	#	include <unistd.h>     // close()
	#	define SOCKET_IS_VALID(S)     (S >= 0)
	#	define CLOSE_SOCKET close
		typedef int SOCKET;
		bool InitializeSockets(void) { return true; }
	#endif

	#include <string.h>	
	#include <string>	
	#include <sstream>	
	#include <iostream>	

	#define USAGE "Mandatory first argument: port number (decimal integer > 0)\n" \
				  "Optional second argument: 4 or 6 to denote IP version (default: 4)\n"
		
	int main(int argc, const char * argv[])
	{
		const int maxPendingConnections = 5;
		
		struct ::sockaddr_in  serverAddress4, remoteAddress4;
		struct ::sockaddr_in6 serverAddress6, remoteAddress6;
		struct ::sockaddr * addressPtr;
		::socklen_t addressSize;
		int domain;
		
		int port      = (argc > 1) ? ::atoi(argv[1]) : 0;
		int ipVersion = (argc > 2) ? ::atoi(argv[2]) : 4;	
		if(!port) { std::cerr << USAGE; return -1; }
		
		if(!InitializeSockets()) return -2;
		
		if(ipVersion == 4)
		{
			domain = PF_INET;
			addressPtr = (struct ::sockaddr *)&serverAddress4;
			addressSize = (::socklen_t)sizeof(serverAddress4);
			::memset(addressPtr, 0, addressSize);
			
			serverAddress4.sin_family = AF_INET;
			serverAddress4.sin_port = htons(port);
			serverAddress4.sin_addr.s_addr = htonl(INADDR_ANY);
		}
		else if(ipVersion == 6)
		{
			domain = PF_INET6;
			addressPtr = (struct ::sockaddr *)&serverAddress6;
			addressSize = (::socklen_t)sizeof(serverAddress6);
			::memset(addressPtr, 0, addressSize);
			
			serverAddress6.sin6_family = AF_INET6;
			serverAddress6.sin6_port = htons(port);
			serverAddress6.sin6_addr = in6addr_any;
			serverAddress6.sin6_scope_id = 0; // right?
		}
		else { std::cerr << USAGE; return -1; }
		
		SOCKET localServerSocket = ::socket(domain, SOCK_STREAM, IPPROTO_TCP);
		if(!SOCKET_IS_VALID(localServerSocket)) return -3;
		// to keep the example minimal, I will not be explicitly closing sockets on error
		if(::bind(localServerSocket, addressPtr, addressSize) != 0) return -4;
		if(::listen(localServerSocket, maxPendingConnections) != 0) return -5;
		std::cerr << "listening on port " << port << " using IPv" << ipVersion << std::endl;
		while(true)
		{
			if(ipVersion == 4)
			{
				addressPtr = (struct ::sockaddr *)&remoteAddress4;
				addressSize = (::socklen_t)sizeof(remoteAddress4);
			}
			else if(ipVersion == 6)
			{
				addressPtr = (struct ::sockaddr *)&remoteAddress6;
				addressSize = (::socklen_t)sizeof(remoteAddress6);
			}
			SOCKET remoteConnectionSocket = ::accept(localServerSocket, addressPtr, &addressSize); 
			if(!SOCKET_IS_VALID(remoteConnectionSocket)) return -6;
			
			char buffer[128];
			std::string remoteAddressString;
			if(ipVersion == 4)
			{
				remoteAddressString = ::inet_ntop(AF_INET,  &remoteAddress4.sin_addr,  buffer, (socklen_t)sizeof(buffer)) ? buffer : "???";
			}
			else if(ipVersion == 6)
			{
				remoteAddressString = ::inet_ntop(AF_INET6, &remoteAddress6.sin6_addr, buffer, (socklen_t)sizeof(buffer)) ? buffer : "???";
				if( IN6_IS_ADDR_LINKLOCAL( &remoteAddress6.sin6_addr ) )
				{
					std::stringstream ss;
					ss << "%" << remoteAddress6.sin6_scope_id;
					remoteAddressString += ss.str();
				}
			}
			std::cerr << "  accepted connection from " << remoteAddressString << std::endl;
			
			while(true)
			{
				char incomingData[32];
				int bytesReceived = ::recv(remoteConnectionSocket, incomingData, sizeof(incomingData), 0);
				if(bytesReceived < 0) return -7;
				if(bytesReceived == 0) break;
				std::cerr << "  received " << bytesReceived << " bytes from " << remoteAddressString << std::endl;
				int bytesSent = ::send(remoteConnectionSocket, incomingData, bytesReceived, 0); // echo
				if(bytesSent != bytesReceived) return -8;
			}
			CLOSE_SOCKET(remoteConnectionSocket);
			std::cerr << "  closed connection from " << remoteAddressString << std::endl;
		}
		return 0; // never reached, but let's suppress the compiler warning
	}
