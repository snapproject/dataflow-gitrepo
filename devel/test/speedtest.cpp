/*
# $BEGIN_DATAFLOW_LICENSE$
# 
# This file is part of the DataFlow project, a software library for
# efficient asynchronous multi-stream inter-process communication.
# 
# Copyright (c) 2014-2021 Jeremy Hill
# 
# DataFlow is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program. If not, see http://www.gnu.org/licenses/ .
# 
# Some third-party source code is compiled and linked into the
# DataFlow binary, as follows: devel/src/sem_timedwait_apple.cpp
# is adapted from sem_timedwait.cpp by Keith Shortridge, copyright
# (c) 2013 Australian Astronomical Observatory (AAO), released under
# the MIT License; devel/src/dirent_win.h is copyright (c) 2006
# Toni Ronkko, and released under the MIT License. The DataFlow build
# system also uses some third-party code:
# devel/build/cmake-recipes/DefineFunction_TARGET_ARCHITECTURE.cmake
# which is copyright (c) 2012 Petroules Corporation, released under
# the 2-Clause BSD License. For more details, see the respective
# copyright, licensing and disclaimer information in each of these
# files.
# 
# $END_DATAFLOW_LICENSE$
*/

/*
TODO:
	- need to convert to some kind of ring buffer

*/


#include "DataFlow.h"
#include "DataFlow.hpp"

#include "CommandLine.hpp"
#include "ExceptionUtils.hpp"
#include "DebuggingUtils.hpp"
#include "TimeUtils.hpp" // for FormatDateStamp
#include "StringUtils.hpp"

#include <stdio.h>

#include <cmath>
#include <ios>
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>

typedef struct
{
	double   fCycleDuration;
	double   fCycleIdleTime;
	uint64_t fExcessBytes;	
} CycleResult;

class SpeedTest
{
	private:		
		int                     mVerbosity;
		double                  mCycleDurationMsec;
		
		uint64_t                mInputBytesPerCycle; // lets assume our client requires this many input bytes for processing, on each cycle
		uint64_t                mNewInputBytesPerCycle; // ...but it may use overlapping windows, in which case it only requires this many *new* bytes per cycle
		std::string             mInputFileName;
		char *                  mInputBuffer;
		
		uint64_t                mOutputBytesPerCycle;
		std::string             mOutputFileName;
		char *                  mOutputBuffer;
		char                    mOutputByteValue;
		
		uint64_t                mInputFileOffset;  // precomputed offset for the input file    TODO: maybe abolish the offsets and instead use filenames every time when opening, and also inside the order 
		uint64_t                mOutputFileOffset; // precomputed offset for the output file         file. Rationale: full-blown DataFlow will eventually be using names rather than pre-cached offsets, so 
		uint64_t                mDeliveryFileOffset; // precomputed offset for the delivery file       it may be a fairer test to emulate the use of file names in the speed test
		std::string             mOrdersDir;        // path to a directory that will, globally across all clients of this archive, contain "orders" (requests for notification)
		std::string             mOrderPath;        // path to a file inside the orders directory, that will be used for the order of this particular client
		std::string             mDeliveriesDir;    // path to a directory that will, globally across all clients of this archive, contain "deliveries" (results of fulfilled orders)
		std::string             mDeliveryPath;     // path to a file inside the delivery directory, where this particular client will check its incoming mail
		
		DF::SharedDataArchive * mArchive;
		double                  mTimeZero;
		uint64_t                mCycleNumber;
		uint64_t                mRequiredInputFileSize;
		
		CycleResult *           mResults;
		size_t 	                mCyclesToRecord;
		std::string             mReportFileName;
		
		const std::string       DELIVERY_PREFIX = "_FULFILLED";
		
	public:
		SpeedTest( int argc, const char * argv[] )
		: mResults( NULL )
		{	
			// Grok the parameters with which to set up this client
			START_ARGS;
			
			uint64_t archiveSize = 100000000;
			GET_OPT( "--size", archiveSize, "an unsigned integer" );
			
			int debugLevel = 0;
			GET_OPT( "--debug", debugLevel, "an integer" );
			
			mVerbosity = -1;
			GET_OPT( "--verbosity", mVerbosity, "an integer" );
		
			mCycleDurationMsec = 50.0;
			GET_OPT( "--period", mCycleDurationMsec, "a floating-point number" );

			mNewInputBytesPerCycle = 0;
			GET_OPT( "--stride", mNewInputBytesPerCycle, "an unsigned integer" );

			mInputBytesPerCycle = 0;
			GET_OPT( "--in", mInputBytesPerCycle, "an unsigned integer" );
			if( mNewInputBytesPerCycle == 0 ) mNewInputBytesPerCycle = mInputBytesPerCycle;

			mOutputBytesPerCycle = 0;
			GET_OPT( "--out", mOutputBytesPerCycle, "an unsigned integer" );

			mInputFileName = "default";
			GET_OPT( "--from", mInputFileName, "a string" )
			
			mOutputFileName = "default";
			GET_OPT( "--to",   mOutputFileName, "a string" );
			
			mCyclesToRecord = 1000;
			GET_OPT( "--cycles", mCyclesToRecord, "an unsigned integer" )
						
			mReportFileName = "";			
			GET_OPT( "--report",   mReportFileName, "a string" );
			mReportFileName = StringUtils::RegexReplace( mReportFileName, "\\$YYYYMMDD", TimeUtils::FormatDateStamp( TimeUtils::NOW, true, "%Y%m%d" ) );
			mReportFileName = StringUtils::RegexReplace( mReportFileName, "\\$HHMMSS",   TimeUtils::FormatDateStamp( TimeUtils::NOW, true, "%H%M%S" ) );
			

			NO_MORE_OPTS( "--" );		
			NO_MORE_ARGS;
			
			if( mInputBytesPerCycle  && mInputFileName == ""  ) RAISE( "if you have input, the `--from` file cannot be blank" );
			if( mOutputBytesPerCycle && mOutputFileName == "" ) RAISE( "if you have output, the `--to` file cannot be blank" );
			if( mInputBytesPerCycle  && mOutputBytesPerCycle && mInputFileName == mOutputFileName ) RAISE( "if you have both input and output, you must change either or both of the `--from` and `--to` filename" );
		
			if( mVerbosity < 0 ) mVerbosity = mOutputBytesPerCycle ? 0 :  mInputBytesPerCycle ? 0 : 1;
			DebuggingUtils::SetDebugLevel( mVerbosity );
			
			DBREPORT( 0, mCyclesToRecord );
			DBREPORT( 0, mCycleDurationMsec );
			DBREPORT( 0, mInputBytesPerCycle );
			DBREPORT( 0, mOutputBytesPerCycle );
			DBREPORTQ( 0, mInputFileName );
			DBREPORTQ( 0, mOutputFileName );
			DEBUG( 0, "" );
			
			
			
			// Global config
			DF::CatchFirstInterrupt(); // TODO: might need a different handler that puts flags down...
			DF::SetDebugLevel( debugLevel );
			
			// Create an archive (or a client that connects to an existing archive)
			mArchive = new DF::SharedDataArchive( "sda", archiveSize );
			mArchive->SynchronizeTimer();
			
			// Ensure that the orders directory exists
			BUILD_STRING( mOrdersDir, "_ORDERS/" << mArchive->GetName() ); // NB: because the file paths of the order files will double as kernel-wide signal names, they should be scoped like this with the name of the SDA at the beginning, even though this seems redundant inside the SDA itself
			BUILD_STRING( mOrderPath, mOrdersDir << "/placeholder" );
			DF::SharedDataArchive::File ph1( mArchive, mOrderPath.c_str(), 0, 'o', 1, 'p' );   // 'p' for placeholder. All this file does is ensure that the parent directory doesn't keep getting auto-created and -destroyed in the case where there's just one client making orders
			// Generate a path to a file, inside the orders directory, that will be uniquely dedicated to *this* client's orders
			BUILD_STRING( mOrderPath, mOrdersDir << "/" << mArchive->GetClientID() );
			
			// Ensure that the Delivery directory exists
			BUILD_STRING( mDeliveriesDir, DELIVERY_PREFIX << mOrdersDir );
			BUILD_STRING( mDeliveryPath, mDeliveriesDir << "/placeholder" );
			DF::SharedDataArchive::File ph2( mArchive, mDeliveryPath.c_str(), 0, 'o', 1, 'p' );   // 'p' for placeholder. All this file does is ensure that the parent directory doesn't keep getting auto-created and -destroyed in the case where there's just one client making orders
			// Generate a path to a file, inside the delivery directory, that will be uniquely dedicated to deliveries for *this* client
			BUILD_STRING( mDeliveryPath, DELIVERY_PREFIX << mOrderPath );
			
			// Initialize local buffers for input and output
			mInputBuffer  = mInputBytesPerCycle  ? new char[ ( size_t )mInputBytesPerCycle  ] : NULL;
			mOutputBuffer = mOutputBytesPerCycle ? new char[ ( size_t )mOutputBytesPerCycle ] : NULL;
			
			mResults = mCyclesToRecord ? new CycleResult[ ++mCyclesToRecord ] : NULL;
		} // end SpeedTest() constructor
		
		~SpeedTest()
		{
			delete mArchive; mArchive = NULL;
			delete [] mInputBuffer;  mInputBuffer  = NULL;
			delete [] mOutputBuffer; mOutputBuffer = NULL;
			delete [] mResults; mResults = NULL;
		}

		void Reset( void )
		{
			mTimeZero = -1.0;
			mCycleNumber = 0;
			mRequiredInputFileSize = 0;

			mInputFileOffset = mOutputFileOffset = mDeliveryFileOffset = 0;
			
			if( mInputBytesPerCycle )
			{
				if( !mInputFileName.length() ) RAISE( "input file name unspecified" );
				DF::SharedDataArchive::File inputFile( mArchive, mInputFileName.c_str(), 0, 'o', 1 );
				mInputFileOffset = inputFile.GetFileOffset();
				DF::SharedDataArchive::File deliveryFile( mArchive, mDeliveryPath.c_str(), 0, 'o', mInputBytesPerCycle );
				mDeliveryFileOffset = deliveryFile.GetFileOffset();
				RequireInputFileSize( mInputBytesPerCycle ); // Writes this client's order for the input that the very first cycle will require
				DF::RemoveKernelResource( mOrderPath.c_str() ); // remove notification semaphore
			}
			if( mOutputBytesPerCycle )
			{
				if( !mOutputFileName.length() ) RAISE( "output file name unspecified" );
				DF::SharedDataArchive::File outputFile( mArchive, mOutputFileName.c_str(), 0, 'o', 1 );
				outputFile.AutoWrap();
				mOutputFileOffset = outputFile.GetFileOffset();
				mOutputByteValue = 'A';
			}
		}
		
		void RequireInputFileSize( uint64_t nBytes )
		{
			// TODO: in principle it's possible (although unlikely) to run out of archive space here
			mRequiredInputFileSize = nBytes;
			DF::SharedDataArchive::File order( mArchive, mOrderPath.c_str(), 0, 'o', sizeof( mRequiredInputFileSize ) + sizeof( mInputBytesPerCycle ) + sizeof( mInputFileOffset ) + sizeof( mDeliveryFileOffset ) );
			order.Write( ( const char * )&mRequiredInputFileSize, sizeof( mRequiredInputFileSize ) );
			order.Write( ( const char * )&mInputBytesPerCycle,    sizeof( mInputBytesPerCycle )    );
			order.Write( ( const char * )&mInputFileOffset,       sizeof( mInputFileOffset )       );
			order.Write( ( const char * )&mDeliveryFileOffset,    sizeof( mDeliveryFileOffset )    );
			// When the signal is sent, it will have the fully-canonicalized name of the file, starting with '$', so let's make sure that's what we're expecting:
			if( mOrderPath[ 0 ] != '$' ) mOrderPath = order.GetName(); // should happen only once
		}
		
		int Run( void )
		{
			Reset();
			double previousTimestamp = 0.0;
			std::string errorMessage;
			while( !DF::CaughtSignal() ) // main cycle loop
			{
				uint64_t excessBytes = 0;
				double waitTime = 0.0;
				
				// Consume
				if( mInputBytesPerCycle )
				{
					waitTime = DF::PrecisionTime();
					// TODO: cannot abort wait with ctrl-C on Windows (odd: `dft --Signal wait` can be aborted...)
					DEBUG( 2, "Waiting for signal \"" << mOrderPath << "\"" );
					try { DF::WaitForSignal( mOrderPath.c_str() ); }  // TODO: is there any gain in efficiency if we pre-initialize the NamedSemaphore instance? WaitForSignal() constructs and destructs a new one each time
					catch( std::string const & e ) { errorMessage = e; break; } // TODO: a bit too broad but it's there for the case where the wait is interrupted by ctrl-C 
					waitTime = DF::PrecisionTime() - waitTime;
					
					DF::SharedDataArchive::File inputFile( mArchive, NULL, mInputFileOffset, 'o' ); // we'll start by holding the writer's lock already, because whatever happens we'll also want to write an order file
					//if( inputFile.TellMax() < mRequiredInputFileSize ) RAISE( "broken promise: expected " << mRequiredInputFileSize << ", found " << inputFile.TellMax() );
					//inputFile.Seek( mRequiredInputFileSize - mInputBytesPerCycle );
					//inputFile.Read( mInputBuffer, mInputBytesPerCycle );
					
					// Above: the (now abandoned) model where we read the input file directly, after receiving the signal
					// The problem with that is that the archive may (in principle, in real DataFlow use cases) have changed
					// between the moment the signaller signals/releases the lock, and the moment when we acquire the lock,
					// and this may invalidate order fulfillment. Because of this general possibility, we must operate on
					// a model whereby the signaller also takes a snapshot of all requested data and posts it in the archive
					// for pickup. Although this brings no benefit in the toy architecture of speedtest, speedtest simulates
					// this extra copying operation to evaluate performance in the light of it. Below: the new model.

					DF::SharedDataArchive::File deliveryFile( mArchive, NULL, mDeliveryFileOffset, 'o' ); // we'll start by holding the writer's lock already, because whatever happens we'll also want to write an order file
					if( deliveryFile.AvailableContentLength() != mInputBytesPerCycle ) RAISE( "broken promise: expected " << mInputBytesPerCycle << ", got " << deliveryFile.AvailableContentLength() );
					deliveryFile.Read( mInputBuffer, mInputBytesPerCycle );
					excessBytes = inputFile.TellMax() - mRequiredInputFileSize; // TODO: log this
					
					uint64_t abbreviate = 100;
					DEBUG( 3, "Received: " << StringUtils::StringLiteral( std::string( mInputBuffer, ( unsigned int )( mInputBytesPerCycle <= abbreviate ? mInputBytesPerCycle : abbreviate ) ) + ( mInputBytesPerCycle <= abbreviate ? "" : "..." ) ) );
					
					// Consumption has now succeeded: the appropriate mInputBytesPerCycle bytes have been retrieved from the
					// archive and copied to local memory. Now we write a new order:
					RequireInputFileSize( mRequiredInputFileSize + mNewInputBytesPerCycle );
				}
				
				double betweenTime = DF::PrecisionTime();
				if( mTimeZero < 0.0 ) mTimeZero = betweenTime;
				double timeSlept = DF::SleepUntil( mTimeZero + mCycleNumber * mCycleDurationMsec / 1000.0 ) - betweenTime;
				if( mResults && mCycleNumber <= mCyclesToRecord && mCycleNumber > 0 )
				{
					CycleResult * result = mResults + mCycleNumber - 1;
					result->fCycleDuration = betweenTime - previousTimestamp;
					result->fCycleIdleTime = waitTime + timeSlept;
					result->fExcessBytes   = excessBytes;
				}
				previousTimestamp = betweenTime;
				mCycleNumber++;
				
				DEBUG( 1, betweenTime + timeSlept );
								
				if( mOutputBytesPerCycle )
				{
					// Produce!
					for( size_t i = 0; i < mOutputBytesPerCycle; i++ )
					{ // fill the local output buffer
						mOutputBuffer[ i ] = mOutputByteValue;
						if( mOutputByteValue++ == 'Z' ) mOutputByteValue = 'A';
					}
					DF::SharedDataArchive::File outputFile( mArchive, NULL, mOutputFileOffset, 'a', 1 );
					outputFile.Write( mOutputBuffer, mOutputBytesPerCycle ); // copies the local buffer contents to the output file in the shared archive
					// TODO: how to handle it gracefully if we run out of archive space here?
					// Production is done. But let's not let go of the lock just yet...
					
					// As a producer whose activity has changed the archive content: before releasing the lock,
					// we must check through all the orders to see whether they are fulfillable. Wherever the
					// answer is yes, fulfill and delete the order, and fire off a signal to the requesting client.
					DF::SharedDataArchive::File order( mArchive, mOrdersDir.c_str(), 0, 'o', 0, 'd' ); // this file instance will Walk(), pointing to each order file in turn - TODO: could presumably pre-find the offset for this file
					DF::SharedDataArchive::File target( mArchive, NULL, 0, 'o' );   // this file instance will point to the target of each order (the file that the requester wants to use as its input)
					DF::SharedDataArchive::File doomed( mArchive, NULL, 0, 'o' );   // this file instance will be used for removing fulfilled order files
					bool removePrevious = false;
					// iterate over all the $f-type files in the orders directory
					for( order.Walk( 0, false, "f" ); order.Exists(); order.Next() ) // restrictToTypes="f" causes the "$p/..." placeholder to be skipped
					{
						uint64_t requestedFileSize = 0, requestedFileOffset = 0, requestedChunkSize = 0, deliveryFileOffset = 0;
						order.Read( &requestedFileSize,   sizeof( requestedFileSize   ) );
						order.Read( &requestedChunkSize,  sizeof( requestedChunkSize  ) );
						order.Read( &requestedFileOffset, sizeof( requestedFileOffset ) );
						order.Read( &deliveryFileOffset,  sizeof( deliveryFileOffset  ) );
						if( removePrevious ) { doomed.Remove(); removePrevious = false; } // delete the file from the previous iteration, if appropriate
						target.Open( requestedFileOffset );
						DEBUG( 2, "checking \"" << order.GetName() << "\": Opening " << requestedFileOffset << "->" << target.GetFileOffset() << " and finding " << target.TellMax() << " bytes written (order: " << requestedChunkSize << " bytes starting at position " << requestedFileSize - requestedChunkSize << ")" );
						if( target.TellMax() >= requestedFileSize )
						{
							DEBUG( 2, "sending signal \"" << order.GetName() << "\"" );
							DF::SendSignal( order.GetName() ); // for efficiency, the order file path will do double duty as the signal name (this saves us from having to keep reading/writing a signal name to/from the file)
							doomed.Open( order.GetFileOffset() ); // we can't remove files while they are open and expect the walk to know which way to go Next(). Instead, we'll just mark the current file for deletion on the next iteration (or after the loop ends)
							removePrevious = true;
							
							char * copyBuffer = new char[ ( unsigned int )requestedChunkSize ];
							target.Seek( requestedFileSize - requestedChunkSize );
							target.Read( copyBuffer, requestedChunkSize );    //  }_ Collapse these two lines in a File::CopyContent method, across two instances?
							target.Open( deliveryFileOffset );
							target.Write( copyBuffer, requestedChunkSize );   //  }
							DEBUG( 2, "filled order \"" << target.GetName() << "\": " << requestedChunkSize << " bytes starting at payload position " << requestedFileSize - requestedChunkSize );
							delete [] copyBuffer;
						}
					}
					if( removePrevious ) doomed.Remove(); // delete the file from the very last iteration, if appropriate
				}
				if( mCyclesToRecord && mCycleNumber == mCyclesToRecord ) break;
			} // end main cycle loop
			
			if( mInputBytesPerCycle ) DF::RemoveKernelResource( mOrderPath.c_str() ); // remove notification semaphore
			
			if( mCyclesToRecord ) DEBUG( 0, mCycleNumber - 1 << " cycles recorded" )
#			define OUT ( report.is_open() ? report : std::cout ) 
			std::ofstream report;
			if( mReportFileName.length() )
			{
				report.open( mReportFileName );
				DEBUG( 0, "Writing results to \"" << mReportFileName << "\"\n" );
			}
			OUT << "{\n";
			OUT << "  'results' : [\n";
			for( uint64_t i = 0; mCycleNumber > 0; i++ )
			{
				bool last = ( i + 2 >= mCycleNumber || i + 1 >= mCyclesToRecord );
				OUT << "    [ " << std::fixed << std::setprecision( 6 ) << std::setw( 10 ) << mResults[ i ].fCycleDuration * 1000.0;
				OUT <<     ", " << std::fixed << std::setprecision( 6 ) << std::setw( 10 ) << mResults[ i ].fCycleIdleTime * 1000.0;
				OUT <<     ", " << std::setw( 5 ) << ( uint64_t )mResults[ i ].fExcessBytes;
				OUT <<     " ]" << ( last ? "" : "," ) << "\n";
				if( last ) break;
			}
			OUT << "  ],\n" << std::defaultfloat;
			OUT << "        'cycleDurationMsec' : "  << mCycleDurationMsec   <<  ",\n";
			OUT << "       'inputBytesPerCycle' : "  << mInputBytesPerCycle  <<  ",\n";
			OUT << "      'strideBytesPerCycle' : "  << mInputBytesPerCycle  <<  ",\n";
			OUT << "      'outputBytesPerCycle' : "  << mOutputBytesPerCycle <<  ",\n";
			OUT << "            'inputFileName' : '" << mInputFileName       <<  "',\n";
			OUT << "           'outputFileName' : '" << mOutputFileName      <<  "',\n";
			OUT << "           'reportFileName' : '" << mReportFileName      <<  "',\n";
			OUT << "      'inputBytesPerSecond' : "  << mInputBytesPerCycle  * 1000.0 / mCycleDurationMsec <<  ",\n";
			OUT << "     'outputBytesPerSecond' : "  << mOutputBytesPerCycle * 1000.0 / mCycleDurationMsec <<  ",\n";
			OUT << "              'libraryFile' : '" << DF::LibraryPath()            <<  "',\n";
			OUT << "          'libraryRevision' : '" << DF::GetRevision()            <<  "',\n";
			OUT << "    'libraryBuildDateStamp' : '" << DF::GetBuildDatestamp()      <<  "',\n";
			OUT << "         'testingDateStamp' : '" << TimeUtils::FormatDateStamp() <<  "'\n";
			OUT << "}\n";
			if( report.is_open() ) report.close();
			return 0;
		} // end Run() method definition		
}; // end SpeedTest class definition

int Main( int argc, const char * argv[] )
{
	if( Load_DF_API( NULL, 1 ) != 0 ) RAISE( Get_DF_Loader_Error() );
	DEBUG( 0, "Loaded DataFlow API:" );
	DEBUG( 0, "        Version: " << DF::GetVersion() );
	DEBUG( 0, "       Revision: " << DF::GetRevision() );
	DEBUG( 0, "     Build Date: " << DF::GetBuildDatestamp() );
	DEBUG( 0, "   Library File: " << DF::LibraryPath() );
	DEBUG( 0, "Executable File: " << DF::ExecutablePath() );
	DEBUG( 0, "" );
	
	SpeedTest t( argc, argv );
	return t.Run();
}
int main( int argc, const char * argv[] )
{
	try { return Main( argc, argv ); }
	catch( std::string const & error ) { std::cerr << error.c_str() << std::endl; }
	catch( std::exception const & ex ) { std::cerr << "std::exception caught: " << ex.what() << std::endl; }
	catch( ... ) { std::cerr << "uncaught exception" << std::endl; }
	return 1;
}

