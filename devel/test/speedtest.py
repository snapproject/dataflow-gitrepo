#!/usr/bin/env python

# $BEGIN_DATAFLOW_LICENSE$
# 
# This file is part of the DataFlow project, a software library for
# efficient asynchronous multi-stream inter-process communication.
# 
# Copyright (c) 2014-2021 Jeremy Hill
# 
# DataFlow is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program. If not, see http://www.gnu.org/licenses/ .
# 
# Some third-party source code is compiled and linked into the
# DataFlow binary, as follows: devel/src/sem_timedwait_apple.cpp
# is adapted from sem_timedwait.cpp by Keith Shortridge, copyright
# (c) 2013 Australian Astronomical Observatory (AAO), released under
# the MIT License; devel/src/dirent_win.h is copyright (c) 2006
# Toni Ronkko, and released under the MIT License. The DataFlow build
# system also uses some third-party code:
# devel/build/cmake-recipes/DefineFunction_TARGET_ARCHITECTURE.cmake
# which is copyright (c) 2012 Petroules Corporation, released under
# the 2-Clause BSD License. For more details, see the respective
# copyright, licensing and disclaimer information in each of these
# files.
# 
# $END_DATAFLOW_LICENSE$

import os
import sys
import ast
import glob
import argparse

import numpy, numpy as np
import matplotlib, matplotlib as mpl, matplotlib.pyplot as plt


def numpify( x ):
	if isinstance( x, dict ):
		for k, v in x.items(): x[ k ] = numpify( v )
	elif isinstance( x, ( list, tuple ) ):
		try: a = numpy.array( x )
		except: return x
		if a.dtype.name.startswith( ( 'uint', 'int', 'float' ) ): x = a
	return x

def Plot( *args ):
	parser = argparse.ArgumentParser()
	parser.add_argument( '-t', '--title', metavar='TITLE', action='store',      default=None,  type=str,   help='figure title' )
	parser.add_argument( '-s', '--sort',                   action='store_true', default=False,             help='sort input files by throughput' )
	parser.add_argument( '-d', '--dark',                   action='store_true', default=False,             help='plot in dark mode' )
	parser.add_argument(       '--ymin',  metavar='YMIN',  action='store',      default=None,  type=float, help='lower y-axis limit' )
	parser.add_argument(       '--ymax',  metavar='YMAX',  action='store',      default=None,  type=float, help='upper y-axis limit' )
	opts, args = parser.parse_known_args( args=args )
	
	d = {}
	for filename in args:
		rec = open( filename ).read()
		#rec = ast.literal_eval( rec )
		rec = eval( rec, { 'inf' : numpy.inf, 'nan' : numpy.nan } )
		if not isinstance( rec, dict ): rec = { 'results' : rec }
		d[ filename ] = numpify( rec )
	
	
	def SortKey( kv ):
		k, v = kv
		return (
			v.get( 'outputBytesPerSecond', 0 ) + v.get( 'inputBytesPerSecond', 0 ),
			v.get( 'outputBytesPerSecond', 0 ),
			v.get( 'inputBytesPerSecond', 0 ),
			v.get( 'testingDateStamp', '' ), # newer versions use this key
			v.get( 'dateStamp', '' ),        # older versions used this key
			k,
		)
	def SortedItems( d ):
		d = list( d.items() )
		if opts.sort: d.sort( key=SortKey )
		return d
	
	nCols = int( numpy.ceil( len( d ) ** 0.5 ) )
	nRows = int( numpy.ceil( len( d ) / float( nCols ) ) )
	
	if 'IPython' in sys.modules: plt.ion()
	if opts.dark: plt.style.use('dark_background')
	plt.clf()
	ylMin = numpy.inf
	ylMax = -numpy.inf
	for i, ( k, rec ) in enumerate( SortedItems( d ), 1 ): 
		plt.subplot( nRows, nCols, i )
		plt.plot( rec[ 'results' ][ 1:, :2 ], 'x-' )
		ylMin = min( ylMin, min( plt.ylim() ) )
		ylMax = max( ylMax, max( plt.ylim() ) )
		
		keys = []
		if 'cycleDurationMsec' in rec: keys.append( 'cycleDurationMsec' )
		if rec.get( 'outputBytesPerCycle', 0 ): keys.append( 'outputBytesPerSecond' if numpy.isfinite( rec[ 'outputBytesPerSecond' ] ) else 'outputBytesPerCycle' )
		if rec.get(  'inputBytesPerCycle', 0 ): keys.append(  'inputBytesPerSecond' if numpy.isfinite( rec[  'inputBytesPerSecond' ] ) else  'inputBytesPerCycle' )
		if rec.get(  'inputBytesPerCycle', 0 ) != rec.get( 'strideBytesPerCycle', 0 ): keys.append( 'strideBytesPerCycle' )		
		title = '\n'.join( k + '=' + ( '%5g' % rec[ k ] ).strip() for k in keys ).strip()
		plt.title( title if title else k ).set_fontsize( 7 )
		
	if opts.ymin is not None: ylMin = opts.ymin
	if opts.ymax is not None: ylMax = opts.ymax
	for ax in plt.gcf().axes:
		ax.set_ylim( [ ylMin, ylMax ] )
	plt.gcf().suptitle( opts.title )
	plt.show()

if __name__ == '__main__':
	Plot( *sys.argv[ 1: ] )
 