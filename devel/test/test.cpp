/*
# $BEGIN_DATAFLOW_LICENSE$
# 
# This file is part of the DataFlow project, a software library for
# efficient asynchronous multi-stream inter-process communication.
# 
# Copyright (c) 2014-2021 Jeremy Hill
# 
# DataFlow is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program. If not, see http://www.gnu.org/licenses/ .
# 
# Some third-party source code is compiled and linked into the
# DataFlow binary, as follows: devel/src/sem_timedwait_apple.cpp
# is adapted from sem_timedwait.cpp by Keith Shortridge, copyright
# (c) 2013 Australian Astronomical Observatory (AAO), released under
# the MIT License; devel/src/dirent_win.h is copyright (c) 2006
# Toni Ronkko, and released under the MIT License. The DataFlow build
# system also uses some third-party code:
# devel/build/cmake-recipes/DefineFunction_TARGET_ARCHITECTURE.cmake
# which is copyright (c) 2012 Petroules Corporation, released under
# the 2-Clause BSD License. For more details, see the respective
# copyright, licensing and disclaimer information in each of these
# files.
# 
# $END_DATAFLOW_LICENSE$
*/

//#include "DataFlow.hpp"

// TODO: devel/src/DataFlow.hpp is overshadowed by the autogenned release/DataFlow.hpp
//       and needs to be renamed (or the autogenned one needs to be filled out to support everything that we want to do here...)
//       For now, let's include by hand everything devel/src/DataFlow.hpp was intended to bundle

#include "BasicTypes.hpp"
#include "ExceptionUtils.hpp"
#include "DebuggingUtils.hpp"
#include "Introspection.hpp"
#include "StringUtils.hpp"
#include "ThreadGlobals.hpp"
#include "TimeUtils.hpp"
#include "FileUtils.hpp"
#include "ConsoleUtils.hpp"
#include "NetworkUtils.hpp"
#include "CommandLine.hpp"
#include "NamedSemaphore.hpp"
#include "SharedMemorySegment.hpp"
#include "ReadWriteMutex.hpp"
#include "SharedDataArchive.hpp"
#include "DataNode.hpp"

#include <iostream>
#include <iomanip>

using namespace std;

int Message( const char * msg, int debugLevel=3 )
/*
	Debug levels 1 or less:  described as an "error"
	      level 2: described as a "warning"
	      higher: just a "message"
*/
{
	//if( debugLevel > 10 ) return 0;
	//cerr << "DataFlow " << ( debugLevel < 2 ? "Error" : debugLevel < 3 ? "Warning" : "Message" ) << " (level " << debugLevel << "): " << msg << endl;
	cerr << msg << endl << flush;
	return 1;
}

int Run( const string & execName, const StringUtils::StringVector & args )
{
	std::cout << "Hello world" << std::endl;
	return 0;
}

int main( int argc, const char * argv[] )
{
	try
	{
		std::string execName = CommandLine::GetExecutableName( argc, argv );
		std::string mode, defaultMode = "SharedDataArchive";
		
		if( CommandLine::FilterOption( argc, argv, "--SharedMemorySegment" ) ) mode = "SharedMemorySegment";
		if( CommandLine::FilterOption( argc, argv, "--NamedSemaphore" ) ) mode = "NamedSemaphore";
		if( CommandLine::FilterOption( argc, argv, "--Signal" ) ) mode = "Signal";
		if( CommandLine::FilterOption( argc, argv, "--ReadWriteMutex" ) ) mode = "ReadWriteMutex";
		if( CommandLine::FilterOption( argc, argv, "--DataNode" ) ) mode = "DataNode";
		if( CommandLine::FilterOption( argc, argv, "--ThreadGlobals" ) ) mode = "ThreadGlobals";
		if( CommandLine::FilterOption( argc, argv, "--SharedDataArchive" ) ) mode = "SharedDataArchive";
		if( CommandLine::FilterOption( argc, argv, "--TimeUtils" ) ) mode = "TimeUtils";
		if( CommandLine::FilterOption( argc, argv, "--FileUtils" ) ) mode = "FileUtils";
		if( CommandLine::FilterOption( argc, argv, "--StringUtils" ) ) mode = "StringUtils";
		if( CommandLine::FilterOption( argc, argv, "--ConsoleUtils" ) ) mode = "ConsoleUtils";
		if( CommandLine::FilterOption( argc, argv, "--NetworkUtils" ) ) mode = "NetworkUtils";
		if( CommandLine::FilterOption( argc, argv, "--CleanUp" ) ) mode = "CleanUp";
		const char * revision = CommandLine::FilterOption( argc, argv, "--revision" );
		const char * version  = CommandLine::FilterOption( argc, argv, "--version" );
		const char * build    = CommandLine::FilterOption( argc, argv, "--build" );
		const char * help     = CommandLine::FilterOption( argc, argv, "--help" );
		
		if( mode == "" )
		{
			if( !revision && !version && !build && !help ) mode = defaultMode;
			if( help )
			{
				std::string usage = R"ZARP(
Usage modes::

    ${execName} [ --help | --version | --revision | --build ]* ...
				
    ${execName} --TimeUtils
    ${execName} --FileUtils
    ${execName} --ConsoleUtils
    ${execName} --NetworkUtils ( serve | send ) ...
    ${execName} --ThreadGlobals
    ${execName} --DataNode
    ${execName} --NamedSemaphore [NAME [TIMEOUT_SECONDS]]
    ${execName} --Signal ( wait | send ) NAME
    ${execName} --ReadWriteMutex  NAME ( r | w )
    ${execName} --CleanUp NAME [MORE_NAMES....]
    ${execName} --SharedMemorySegment NAME [SIZE [MSG [REPEATS]]]
    
    ${execName} [--SharedDataArchive] [ARCHIVE_NAME [SUBCOMMAND [ARGS...]]] [--OPTIONS]
    
Currently the default mode is `${defaultMode}`.

)ZARP"; // CPLUSPLUS11 R"()" literal
				usage = StringUtils::RegexReplace( usage, R"(\$\{execName\})", execName );
				usage = StringUtils::RegexReplace( usage, R"(\$\{defaultMode\})", defaultMode );
				std::cout << usage;
			}
		}
		if( version  ) std::cout << Introspection::GetVersion() << std::endl;
		if( revision ) std::cout << "Built from revision: " << Introspection::GetRevision() << std::endl;
		if( build    ) std::cout << "Build datestamp:     " << Introspection::GetBuildDatestamp() << std::endl;
		if( mode == "" ) return 0;
		
		if( mode != "SharedDataArchive" )
		{
			const char * unsupported = CommandLine::FirstOption( argc, argv );
			if( unsupported ) RAISE( "unknown mode \"" << unsupported << "\"" );
		}
		if( help ) argv[ argc++ ] = help - ( *help ? 7 : 6 );
		
		if( mode == "NamedSemaphore" ) return NamedSemaphore::Demo( argc, argv );
		if( mode == "Signal" ) return NamedSemaphore::SignalingDemo( argc, argv );
		if( mode == "SharedMemorySegment" ) return SharedMemorySegment::Demo( argc, argv );
		if( mode == "ReadWriteMutex" ) return ReadWriteMutex::Demo( argc, argv );
		if( mode == "ThreadGlobals" ) return ThreadGlobals::Demo( argc, argv );
		if( mode == "DataNode" ) return DataNode::Demo( argc, argv );
		if( mode == "SharedDataArchive" ) return SharedDataArchive::Demo( argc, argv );
		if( mode == "TimeUtils" ) return TimeUtils::Demo( argc, argv );
		if( mode == "FileUtils" ) return FileUtils::Demo( argc, argv );
		if( mode == "StringUtils" ) return StringUtils::Demo( argc, argv );
		if( mode == "ConsoleUtils" ) return ConsoleUtils::Demo( argc, argv );
		if( mode == "NetworkUtils" ) return NetworkUtils::Demo( argc, argv );
		if( mode == "CleanUp" ) return KernelResources::RemovalUtility( argc, argv );
		RAISE( "missed the mode" ); // shouldn't happen
	}
	catch( std::string const & error )
	{
		return Message( error.c_str(), 1 );
	}
	catch( std::exception const & ex )
	{
		std::string s = "std::exception caught: "; s += ex.what();
		return Message( s.c_str(), 1 );
	}
	catch( ... )
	{
		return Message( "uncaught exception", 1 );
	}
	return 0;
}
