/*
# $BEGIN_DATAFLOW_LICENSE$
# 
# This file is part of the DataFlow project, a software library for
# efficient asynchronous multi-stream inter-process communication.
# 
# Copyright (c) 2014-2021 Jeremy Hill
# 
# DataFlow is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program. If not, see http://www.gnu.org/licenses/ .
# 
# Some third-party source code is compiled and linked into the
# DataFlow binary, as follows: devel/src/sem_timedwait_apple.cpp
# is adapted from sem_timedwait.cpp by Keith Shortridge, copyright
# (c) 2013 Australian Astronomical Observatory (AAO), released under
# the MIT License; devel/src/dirent_win.h is copyright (c) 2006
# Toni Ronkko, and released under the MIT License. The DataFlow build
# system also uses some third-party code:
# devel/build/cmake-recipes/DefineFunction_TARGET_ARCHITECTURE.cmake
# which is copyright (c) 2012 Petroules Corporation, released under
# the 2-Clause BSD License. For more details, see the respective
# copyright, licensing and disclaimer information in each of these
# files.
# 
# $END_DATAFLOW_LICENSE$
*/

#include <stdio.h>
#include "DataFlow.h"

/*
From release directory:

	gcc -DDF_PLATFORM=-Darwin-64bit  -I. ../devel/test/minimaltest.c DataFlow_API_Loader.c           && ./a.out
	gcc -DDF_PLATFORM=-Linux-64bit   -I. ../devel/test/minimaltest.c DataFlow_API_Loader.c -ldl -lm  && ./a.out
	cl  /DDF_PLATFORM=-Windows-32bit /I. ..\devel\test\minimaltest.c DataFlow_API_Loader.c           && .\minimaltest.exe
	cl  /DDF_PLATFORM=-Windows-64bit /I. ..\devel\test\minimaltest.c DataFlow_API_Loader.c           && .\minimaltest.exe

On Windows, the `cl.exe` compiler will only be available if you have previously set up the
Visual Studio paths. One way to do this is by running::

	devel\build\VisualStudio.cmd Win32

or::

	devel\build\VisualStudio.cmd Win64

depending on which architecture you want to target.

*/

#define REPORT( X ) printf( "%s = %g\n", #X, ( double )( X ) )
#define REPORTS( X ) printf( "%s = \"%s\"\n", #X, X )
#define REPORTP( X ) printf( "%s = %p\n", #X, X )

int main( int argc, const char * argv[] )
{
	if( Load_DF_API( NULL, 1 ) != 0 ) return ( fprintf( stderr, "%s\n", Get_DF_Loader_Error() ), -1 );
	
	DF_SanityCheck();
	if( DF_Error() ) return ( fprintf( stderr, "%s\n", DF_ClearError() ), -1 );
	REPORTS( DF_GetVersion() );
	REPORTS( DF_GetRevision() );
	REPORTS( DF_GetBuildDatestamp() );
	REPORTS( DF_LibraryPath( "", NULL ) );
	REPORTS( DF_ExecutablePath( "" ) );
	
	DF_Thing thing = DF_Thing_New( "blah" );
	REPORT( DF_Thing_IsCalled( thing, "blah" ) );
	REPORT( DF_Thing_IsCalled( thing, "bloo" ) );
	
	const DF_Thing others[ 2 ] = {DF_Thing_New("bloo"), DF_Thing_New("blah")};
	
	REPORTP( DF_Thing_FirstNamesake( thing, 2, others ) );
	REPORTS( DF_Thing_GetName( thing ) );
	
	DF_Thing_Delete( others[ 1 ] );
	DF_Thing_Delete( others[ 0 ] );
	DF_Thing_Delete( thing );
	return 0;
}
