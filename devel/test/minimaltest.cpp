/*
# $BEGIN_DATAFLOW_LICENSE$
# 
# This file is part of the DataFlow project, a software library for
# efficient asynchronous multi-stream inter-process communication.
# 
# Copyright (c) 2014-2021 Jeremy Hill
# 
# DataFlow is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program. If not, see http://www.gnu.org/licenses/ .
# 
# Some third-party source code is compiled and linked into the
# DataFlow binary, as follows: devel/src/sem_timedwait_apple.cpp
# is adapted from sem_timedwait.cpp by Keith Shortridge, copyright
# (c) 2013 Australian Astronomical Observatory (AAO), released under
# the MIT License; devel/src/dirent_win.h is copyright (c) 2006
# Toni Ronkko, and released under the MIT License. The DataFlow build
# system also uses some third-party code:
# devel/build/cmake-recipes/DefineFunction_TARGET_ARCHITECTURE.cmake
# which is copyright (c) 2012 Petroules Corporation, released under
# the 2-Clause BSD License. For more details, see the respective
# copyright, licensing and disclaimer information in each of these
# files.
# 
# $END_DATAFLOW_LICENSE$
*/

#include <iostream>
#include <stdio.h>
#include "DataFlow.h" // NB: currently required purely for declarations of `Load_DF_API()` and `Get_DF_Loader_Error()` (whose definitions are in DataFlow_API_Loader.c)
#include "DataFlow.hpp"

/*
From release directory::

	g++ -xc++     -DDF_PLATFORM=-Darwin-64bit  -I. ../devel/test/minimaltest.cpp DataFlow_API_Loader.c           && ./a.out
	g++ -xc++     -DDF_PLATFORM=-Linux-64bit   -I. ../devel/test/minimaltest.cpp DataFlow_API_Loader.c -ldl -lm  && ./a.out
	cl  /TP /EHsc /DDF_PLATFORM=-Windows-32bit /I. ..\devel\test\minimaltest.cpp DataFlow_API_Loader.c           && .\minimaltest.exe
	cl  /TP /EHsc /DDF_PLATFORM=-Windows-64bit /I. ..\devel\test\minimaltest.cpp DataFlow_API_Loader.c           && .\minimaltest.exe

The explicit `-xc++` is required to avoid a deprecation warning arising from the fact that the
`DataFlow_API_Loader.c` has a `.c` file extension but is nonetheless being compiled as C++.
On Windows, the `/TP` plays the same role and is required in order to avoid linker errors.

On Windows, the `cl.exe` compiler will only be available if you have previously set up the
Visual Studio paths. One way to do this is by running::

	devel\build\VisualStudio.cmd Win32

or::

	devel\build\VisualStudio.cmd Win64

depending on which architecture you want to target.

*/

#define REPORT( X )  ( std::cout << #X << " = " << ( X ) << "\n" ) 
#define REPORTS( X ) ( std::cout << #X << " = \"" << ( X ) << "\"\n" ) 

int Main( int argc, const char * argv[] )
{
	if( Load_DF_API( NULL, 1 ) != 0 ) throw std::string( Get_DF_Loader_Error() );
	
	DF::SanityCheck();
	REPORTS( DF::GetVersion() );
	REPORTS( DF::GetRevision() );
	REPORTS( DF::GetBuildDatestamp() );
	REPORTS( DF::LibraryPath() );
	REPORTS( DF::ExecutablePath() );

	DF::Thing thing( "blah" );
	REPORT( thing.IsCalled( "blah" ) );
	REPORT( thing.IsCalled( "bloo" ) );
	REPORTS( thing.GetName() );

	return 0;
}
int main( int argc, const char * argv[] )
{
	try { return Main( argc, argv ); }
	catch( std::string const & x ) { std::cerr << x << std::endl; }
	catch( std::exception const & x ) { std::cerr << "std::exception caught: " << x.what() << std::endl; }
	catch( ... ) { std::cerr << "uncaught exception" << std::endl; }
	return 1;
}
