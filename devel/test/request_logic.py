# $BEGIN_DATAFLOW_LICENSE$
# 
# This file is part of the DataFlow project, a software library for
# efficient asynchronous multi-stream inter-process communication.
# 
# Copyright (c) 2014-2021 Jeremy Hill
# 
# DataFlow is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program. If not, see http://www.gnu.org/licenses/ .
# 
# Some third-party source code is compiled and linked into the
# DataFlow binary, as follows: devel/src/sem_timedwait_apple.cpp
# is adapted from sem_timedwait.cpp by Keith Shortridge, copyright
# (c) 2013 Australian Astronomical Observatory (AAO), released under
# the MIT License; devel/src/dirent_win.h is copyright (c) 2006
# Toni Ronkko, and released under the MIT License. The DataFlow build
# system also uses some third-party code:
# devel/build/cmake-recipes/DefineFunction_TARGET_ARCHITECTURE.cmake
# which is copyright (c) 2012 Petroules Corporation, released under
# the 2-Clause BSD License. For more details, see the respective
# copyright, licensing and disclaimer information in each of these
# files.
# 
# $END_DATAFLOW_LICENSE$
"""
DF::DataNode *
DF::Request::Pull( DF::Delta & d, double & timeTaken )
{
	double pullTime = d.Lock();
	bool success = false;
	
	

	timeTaken = d.Unlock() - pullTime;
	return success ? this : NULL;
}
"""

ANY = -1
SEP = '/'
WILD = '*'

import sys
import time
import weakref
import threading

class Event( object ):
	def __init__( self, value, timestamp ):
		self.value = value
		self.timestamp = timestamp
	def __repr__( self ):
		return '%s(timestamp=%s, value=%r)' % ( self.__class__.__name__, self.timestamp, self.value ) 

class Delta( object ):
	def __init__( self, name ):
		self.name = name
		self.timeCreated = time.time()
		self.events = {}
		self.lock = threading.Lock()
	def Lock( self ):
		self.lock.acquire()
		return time.time()
	def Unlock( self ):
		self.lock.release()
		return time.time()
	def AddEvent( self, path, value, timestamp=None ):
		if timestamp is None: timestamp = time.time()
		self.events.setdefault( path, [] ).append( Event( value=value, timestamp=timestamp ) ) 
	def StreamNames( self ):
		return sorted( self.events.keys() )

def PathMatch( pattern, path ):
	pattern = [ stripped for part in pattern.split( SEP ) for stripped in [ part.strip() ] if stripped ]
	path    = [ stripped for part in path.split(    SEP ) for stripped in [ part.strip() ] if stripped ]
	result = ( xpath == xpattern or xpattern == WILD for xpattern, xpath in zip( pattern, path ) )
	#result = list( result );print(pattern, path, result)
	return False not in result
	
class Rule( object ):
	def __init__( self, pattern, sinceTimeStamp=ANY, minSpan=ANY, maxSpan=ANY, minEvents=ANY, maxEvents=ANY ):
		self.pattern = pattern
		self.sinceTimeStamp = sinceTimeStamp
		self.minSpan = minSpan
		self.maxSpan = maxSpan
		self.minEvents = minEvents
		self.maxEvents = maxEvents
	def __repr__( self ):
		attrs = [ getattr( self, k ) for k in 'pattern sinceTimeStamp minSpan maxSpan minEvents maxEvents'.split() ]
		attrs = [ self.__class__.__name__ ] + [ 'ANY' if x == ANY else repr( x ) for x in attrs ]
		return '%s(pattern=%s, since=%s,  span=[%s,%s],  nEvents=[%s,%s])' % tuple( attrs )

class DataNode( object ):
	def __init__( self, name='', parent=None, pending=False ):
		self._name = name
		self._pending = pending
		self._parent = None
		if parent is not None: self._parent = parent if pending else weakref.ref( parent )
	def __repr__( self ):
		maxlen = max( len( k ) for k, v in self._FlatMap() )
		return '\n'.join( [ '%s(%r):' % ( self.__class__.__name__, self._name ) ] + [ '    %s%s  %r' % ( k, ' ' * ( maxlen - len( k ) ), v ) for k, v in self._FlatMap() ] )
	def __getattr__( self, name ):
		return DataNode( name=name, parent=self, pending=True )		
	def __setattr__( self, name, value ):
		object.__setattr__( self, name, value )
		if name.startswith( '_' ): return
		if isinstance( value, DataNode ):
			value._name = name
			value._parent = weakref.ref( self )
			value._pending = False
		parent = self._parent
		if parent is None: return
		if callable( parent ): parent = parent()
		setattr( parent, self._name, self )
		self._pending = False
	def _Clear( self ):
		[ self.__dict__.pop( k ) for k in self.__dict__.keys() if not k.startswith( '_' ) ]
	def _FlatMap( self, prefix=SEP ):
		m = []
		for k, v in sorted( self.__dict__.items() ):
			if isinstance( v, DataNode ): m += v._FlatMap( prefix=prefix + v._name + SEP )
			elif not k.startswith( '_' ):  m.append( ( prefix + k, v ) )
		return m
	def _Push( self, delta ):
		pushTime = delta.Lock()
		for k, v in self._FlatMap():
			delta.AddEvent( k, v, pushTime )
		time.sleep( 0.001 )
		timeTaken = delta.Unlock() - pushTime

def DictString( d, fmt='%s=%r', delim='\n' ):
	s = delim.join( fmt % ( k, v ) for k, v in sorted( d.items() ) )
	return s
	
class Request( DataNode ):
	
	def __init__( self, name='' ):
		self._rules = []
		DataNode.__init__( self, name=name )
		
	def _Scope( self, *pargs, **kwargs ):
		rule = Rule( *pargs, **kwargs )
		self._rules.append( rule )
		return rule
		
	def _UpdateTimeStamps( self, t ):
		for rule in self._rules:
			if rule.sinceTimeStamp >= 0: rule.sinceTimeStamp = t
	
	def _Pull( self, delta ):
		pulltime = delta.Lock()
		self._Clear()
		match = {}
		for streamName in delta.StreamNames():
			for rule in self._rules[ ::-1 ]:
				if PathMatch( rule.pattern, streamName ):
					match[ streamName ] = rule
					break
		# TODO: what now?
		print( 'match = {\n%s\n}' % DictString( match, '   %s  ---  %r' ) )
		timeTaken = delta.Unlock() - pulltime


d = Delta( "foo" )
r = Request()
last = 0
r._Scope( "/Eyetracker/*", ANY,   ANY, ANY,   ANY, 1 )
r._Scope( "/Eyetracker/Stream/*/Gaze/*",  last,    ANY, ANY,    1, ANY ) # later-defined rules are considered first
#         pattern, sinceTimeStamp=ANY, minSpan=ANY, maxSpan=ANY, minEvents=ANY, maxEvents=ANY


n = DataNode()
n.Eyetracker.Manufacturer = 'Tobii'
n.Eyetracker.Model = 'EyeX'
n.Eyetracker.SamplingRateInHz = 60.0
n.Eyetracker.Stream.LeftEye.Gaze.X        = 10
n.Eyetracker.Stream.LeftEye.Gaze.Y        = 100
n.Eyetracker.Stream.LeftEye.Position.X    = 30
n.Eyetracker.Stream.LeftEye.Position.Y    = 300
n.Eyetracker.Stream.LeftEye.Position.Z    = 3000
n.Eyetracker.Stream.RightEye.Gaze.X       = 20
n.Eyetracker.Stream.RightEye.Gaze.Y       = 200
n.Eyetracker.Stream.RightEye.Position.X   = 40
n.Eyetracker.Stream.RightEye.Position.Y   = 400
n.Eyetracker.Stream.RightEye.Position.Z   = 4000
n.Subject.Name.Last = 'Bond'
n.Subject.Name.First = 'James'
n.Subject.Code = '007'
n._Push( d )

n2 = DataNode()
n2.Eyetracker.Stream = n.Eyetracker.Stream
n2.Eyetracker.Stream.LeftEye.Gaze.X      += 1
n2.Eyetracker.Stream.LeftEye.Gaze.Y      += 1
n2.Eyetracker.Stream.LeftEye.Position.X  += 1
n2.Eyetracker.Stream.LeftEye.Position.Y  += 1
n2.Eyetracker.Stream.LeftEye.Position.Z  += 1
n2.Eyetracker.Stream.RightEye.Gaze.X     += 1
n2.Eyetracker.Stream.RightEye.Gaze.Y     += 1
n2.Eyetracker.Stream.RightEye.Position.X += 1
n2.Eyetracker.Stream.RightEye.Position.Y += 1
n2.Eyetracker.Stream.RightEye.Position.Z += 1
n2._Push( d )

r._Pull( d )


"""
$f/**
	latest:
		format_datestamp
		format
		body_datestamp (or event offset?)
		body blob
$e/**
	offset of corresponding $f-file
	offset of corresponding $b-file
	stream-specific ring buffer of event records, each record:
		timestamp(s)
		body offset
		offset of event record in master ring buffer?
		(empty the ring buffer if events become invalidated by format change)
		
$b/**
	stream-specific ring buffer of body blobs

$a/MasterRingBuffer
	ring buffer of chronologically-ordered event records for all streams - each record:
		timestamp(s)
		offset of first Lot of $e-file
		offset of body blob in $b-file (set to 0 if event becomes invalidated by format change)
		offset of event record in $e-file  (set to 0 if event becomes invalidated by format change)
		
$r/*
	request record
	
	
Consignment c;
c.SetTimestamp(t, clockName);	
c.Add();

"""


