/*
# $BEGIN_DATAFLOW_LICENSE$
# 
# This file is part of the DataFlow project, a software library for
# efficient asynchronous multi-stream inter-process communication.
# 
# Copyright (c) 2014-2021 Jeremy Hill
# 
# DataFlow is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program. If not, see http://www.gnu.org/licenses/ .
# 
# Some third-party source code is compiled and linked into the
# DataFlow binary, as follows: devel/src/sem_timedwait_apple.cpp
# is adapted from sem_timedwait.cpp by Keith Shortridge, copyright
# (c) 2013 Australian Astronomical Observatory (AAO), released under
# the MIT License; devel/src/dirent_win.h is copyright (c) 2006
# Toni Ronkko, and released under the MIT License. The DataFlow build
# system also uses some third-party code:
# devel/build/cmake-recipes/DefineFunction_TARGET_ARCHITECTURE.cmake
# which is copyright (c) 2012 Petroules Corporation, released under
# the 2-Clause BSD License. For more details, see the respective
# copyright, licensing and disclaimer information in each of these
# files.
# 
# $END_DATAFLOW_LICENSE$
*/
#ifndef    INCLUDED_Confluence_CPP
#define    INCLUDED_Confluence_CPP
//////////////////////////////////////////////////////////////////////

#include "Confluence.hpp"
#include "DebuggingUtils.hpp"


class Stream
{
	public:

		Stream( SharedDataArchive & archive, char mode, const std::string & name, DataNode * newValue )
		: mArchive( archive ), mMode( mode ), mLock( mArchive.Lock( mMode ) ), mName( name )
		{
			uint64_t minimumNumberOfBytes = 0, TODO_xx = 0, TODO_yy = 0;
	
			mOffsetOfPersistentValueFile = mArchive.FindFile( mName, minimumNumberOfBytes, 'f' );
			mOffsetOfEventHistoryFile = mArchive.FindFile( mName, TODO_xx, 'e' );
			mOffsetOfBodyStreamFile = mArchive.FindFile( mName, TODO_yy, 'b' );
		}

		Stream( const Stream & other )
		: mArchive( other.mArchive ), mMode( other.mMode ), mLock( mArchive.Lock( mMode ) ), mName( other.mName )
		{
			mOffsetOfPersistentValueFile = other.mOffsetOfPersistentValueFile;
			mOffsetOfEventHistoryFile = other.mOffsetOfEventHistoryFile;
			mOffsetOfBodyStreamFile = other.mOffsetOfBodyStreamFile;
		}

		~Stream()
		{
	
		}
		
	private:
		SharedDataArchive &        mArchive;
		char                       mMode;
		ReadWriteMutex::ScopedLock mLock;
		std::string                mName;
		
		uint64_t                   mOffsetOfPersistentValueFile;
		uint64_t                   mOffsetOfEventHistoryFile;
		uint64_t                   mOffsetOfBodyStreamFile;
};

Confluence::Confluence( const std::string & name, uint64_t segmentSize, bool forceReformat )
: mArchive( name, segmentSize, forceReformat )
{
	
}

Confluence::~Confluence()
{
	
}

#define LOCK( MODE )  ReadWriteMutex::ScopedLock _lock = mArchive.Lock( MODE );

void
Confluence::Push( DataNode * root, double & timeTaken )
{
	if( !root ) return;
	double pushTime = mArchive.Age();
		
	{
		LOCK( 'w' );
		for( DataNodePtr node = root, stop = root->Next( false ); node != stop; node = node->Next() )
		{
			if( !node->HasData() ) continue;
			DBREPORTQ( 0, node->GetPath() ); // TODO: remove
			std::string serialized = node->SerializeFormat();

			uint64_t nEvents = node->GetNumberOfEvents();
			uint64_t fixedEventLength = node->GetBytesPerEvent();
			/*
			char * eventStart = node->mData; // TODO: private mData
			
			EventPointer eventRecordAddress = NULL; // TODO: definition of EventPointer
			node->mPushTimeStamps.assign( as_size_t( nEvents ), pushTime ); // TODO: private mPushTimeStamps

			Stream * streamPtr = this->GetStream( node->GetPath() );
			if( !streamPtr ) streamPtr = this->GetStream( this->CreateStream( node->GetPath(), serialized.length(), fixedEventLength ? fixedEventLength : strlen( eventStart ) ) );
			if( !streamPtr ) RAISE( "failed to create new stream \"" << node->GetPath() << "\"" );
			streamPtr->PushFormat( serialized.c_str(), serialized.length() );
		
			for( uint64_t iEvent = 0; iEvent < nEvents; iEvent++ )
			{
				double nativeTime = ( as_size_t( iEvent ) < node->mNativeTimeStamps.size() ) ? node->mNativeTimeStamps[ as_size_t( iEvent ) ] : 0.0;
				uint64_t eventLength = fixedEventLength ? fixedEventLength : strlen( eventStart );
				streamPtr->PushEvent( pushTime, nativeTime, eventStart, eventLength );
				eventStart += eventLength;
			}
			*/
		}
	} // release LOCK
	timeTaken = mArchive.Age() - pushTime;
}

DataNode *
Confluence::Pull( Request * request, double & timeTaken )
{
	class Match
	{
		public:
			Match( Rule & rule, Stream & stream, const char * streamPath, uint64_t nEvents, uint64_t totalMemory )
			: mRule( rule ), mStream( stream ), mStreamPath( streamPath ), mNumberOfEvents( nEvents ), mTotalMemory( totalMemory ) {}
			~Match() {}
			Rule &           mRule;
			Stream           mStream;
			const char *     mStreamPath;
			uint64_t         mNumberOfEvents;
			uint64_t         mTotalMemory;
	};
	typedef std::vector< Match > MatchVector;
	MatchVector matches;
	DataNode * result = ( DataNode * )request;	
	
	double pullTime = mArchive.Age();
	
	{
		LOCK( 'r' );
/*		for( StreamHeader::ID streamID = 0; result != NULL; streamID++ )
		{
			Stream * streamPtr = NULL;
			streamPtr = delta.GetStream( streamID );
			if( !streamPtr ) break;
			const char * streamPath = streamPtr->GetPath();
			for( RuleList::iterator iRule = mRules.end(); iRule != mRules.begin() && result != NULL; )
			{
				iRule--;
				if( iRule->PathMatches( streamPath ) )
				{
					bool fulfilled = true;
					uint64_t nEvents = 0;
					uint64_t totalMemory = 0;
					// TODO: - compute whether fulfillment conditions (minimum span and minimum number of events) are met,
					//         and change the values of fulfilled, numberOfEvents, and totalMemory
					//         (remember to add 1 to totalMemory for null-termination if appropriate)
					//       - leave the corresponding Stream pointing at the (chronologically) first qualifying event
					if( !fulfilled ) result = NULL; // failed one fulfillment condition, fail the whole Pull() attempt
					else if( nEvents ) matches.emplace_back( *iRule, *streamPtr, streamPath, nEvents, totalMemory ); // CPLUSPLUS11
					break; // only one match per stream
				}
			}
		}
		if( result )
		{
			ClearData();
			RemoveDescendants();
			for( MatchVector::iterator iMatch = matches.begin(); iMatch != matches.end(); iMatch++ )
			{
				Stream & stream = iMatch->mStream;
				DataNode * node = Create( iMatch->mStreamPath, true );
				node->DecodeFormat( stream.GetFormat() );
				char * bodyData = ( char * )node->Allocate( iMatch->mTotalMemory );
				uint64_t eventBodySize = node->GetBytesPerEvent(); // NB: 0 for strings: must compute size on the fly in this case
				bool nullTerminate = ( eventBodySize == 0 );
				for( uint64_t iEvent = 0; iEvent < iMatch->mNumberOfEvents; iEvent++, stream.GoToNextEvent() )
				{
					uint64_t fragmentLength;
					for( const char * src = stream.GoToFirstFragment( fragmentLength ); src; src = stream.GoToNextFragment( fragmentLength ) )
					{
						memcpy( bodyData, src, as_size_t( fragmentLength ) );
						bodyData += fragmentLength;
					}
					if( nullTerminate ) *bodyData++ = 0;
					// TODO: record this event's two time-stamps
				}
			}
		}
*/
	} // release LOCK
	timeTaken = mArchive.Age() - pullTime;
	return result;
}

//////////////////////////////////////////////////////////////////////
#endif  // INCLUDED_Confluence_CPP
