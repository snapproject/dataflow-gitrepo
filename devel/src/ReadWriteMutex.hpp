/*
# $BEGIN_DATAFLOW_LICENSE$
# 
# This file is part of the DataFlow project, a software library for
# efficient asynchronous multi-stream inter-process communication.
# 
# Copyright (c) 2014-2021 Jeremy Hill
# 
# DataFlow is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program. If not, see http://www.gnu.org/licenses/ .
# 
# Some third-party source code is compiled and linked into the
# DataFlow binary, as follows: devel/src/sem_timedwait_apple.cpp
# is adapted from sem_timedwait.cpp by Keith Shortridge, copyright
# (c) 2013 Australian Astronomical Observatory (AAO), released under
# the MIT License; devel/src/dirent_win.h is copyright (c) 2006
# Toni Ronkko, and released under the MIT License. The DataFlow build
# system also uses some third-party code:
# devel/build/cmake-recipes/DefineFunction_TARGET_ARCHITECTURE.cmake
# which is copyright (c) 2012 Petroules Corporation, released under
# the 2-Clause BSD License. For more details, see the respective
# copyright, licensing and disclaimer information in each of these
# files.
# 
# $END_DATAFLOW_LICENSE$
*/
#ifndef   INCLUDE_ReadWriteMutex_HPP
#define   INCLUDE_ReadWriteMutex_HPP

#include "BasicTypes.hpp"
#include "NamedSemaphore.hpp"
#include "SharedMemorySegment.hpp"

#include <string>

class ReadWriteMutex // This uses NamedSemaphores and as such is designed to be used between *processes*. It *could* also be used between threads but each thread would have to have a separate instance: they do not share an instance like they would a normal mutex; instead they share a common set of *named* kernel resources
{
	public:
		ReadWriteMutex( std::string name, uint32_t * countPtr=NULL, bool verbose=false );
		~ReadWriteMutex();
		void Acquire( char mode, double timeoutSeconds=-1.0 );
		void Release( bool force=false, double timeoutSeconds=-1.0 );
		bool Downgrade( double timeoutSeconds=-1.0 );
		char Acquired( void ) const;
		void SetCountPointer( uint32_t * countPtr );

		int  Remove( void );
	
		bool                  mVerbose;
	
	private:
		std::string           mName;
		NamedSemaphore *      mReadingSemaphore;
		NamedSemaphore *      mWritingSemaphore;
		NamedSemaphore *      mCountingSemaphore;
		SharedMemorySegment * mDedicatedSharedMemory;
		uint32_t *            mCountPtr;  // points to number of readers across all thread/processes (count in shared memory)
		char                  mMode;
		bool                  mOpened;
		uint32_t              mLocalCount; // keeps a local (i.e. specific to this process and instance) count of how many times acquired (reacquisitions from the same instance do not affect the semaphores or shared count)
	
		void                  Open( uint32_t * countPtr );
		void                  Close( void );
			
	public:
		class ScopedLock
		{
			public:
				ScopedLock( ReadWriteMutex * base, char mode, double timeoutSeconds=-1.0 );
				ScopedLock( ReadWriteMutex & base, char mode, double timeoutSeconds=-1.0 );
				ScopedLock( ScopedLock && other ); // CPLUSPLUS11 move semantics, for returning one of these instances from a function without unlocking it
				~ScopedLock();
				void UnlockPrematurely( double timeoutSeconds=-1.0 );
				bool Downgrade( double timeoutSeconds=-1.0 );
				char Acquired( void ) const;
			private:
				ReadWriteMutex * mBase;
		};
		
		static std::string ResourceName( std::string baseName, char semaphoreType );
		static int         Remove( std::string baseName );
		
		static int         Demo( int argc, const char * argv[] );
};


namespace KernelResources
{
	// These live here in ReadWriteMutex for now, just because all the relevant headers are already included here
	int Remove( const std::string & name, const std::string & execname="" );
	int RemovalUtility( int argc, const char * argv[] );
}

#endif // INCLUDE_ReadWriteMutex_HPP
