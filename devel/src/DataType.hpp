/*
# $BEGIN_DATAFLOW_LICENSE$
# 
# This file is part of the DataFlow project, a software library for
# efficient asynchronous multi-stream inter-process communication.
# 
# Copyright (c) 2014-2021 Jeremy Hill
# 
# DataFlow is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program. If not, see http://www.gnu.org/licenses/ .
# 
# Some third-party source code is compiled and linked into the
# DataFlow binary, as follows: devel/src/sem_timedwait_apple.cpp
# is adapted from sem_timedwait.cpp by Keith Shortridge, copyright
# (c) 2013 Australian Astronomical Observatory (AAO), released under
# the MIT License; devel/src/dirent_win.h is copyright (c) 2006
# Toni Ronkko, and released under the MIT License. The DataFlow build
# system also uses some third-party code:
# devel/build/cmake-recipes/DefineFunction_TARGET_ARCHITECTURE.cmake
# which is copyright (c) 2012 Petroules Corporation, released under
# the 2-Clause BSD License. For more details, see the respective
# copyright, licensing and disclaimer information in each of these
# files.
# 
# $END_DATAFLOW_LICENSE$
*/
#ifndef   INCLUDED_DataType_HPP
#define   INCLUDED_DataType_HPP

#include <string>

namespace DataType
{
	typedef int Code;
	bool         IsFullyFormed( Code t );
	bool         IsNumeric( Code t );
	bool         IsFloatingPoint( Code t );
	bool         IsInteger( Code t );
	bool         IsSigned( Code t );
	bool         IsString( Code t );
	int          SizeInBits( Code t );  // returns 0 for strings
	int          SizeInBytes( Code t ); // returns 0 for strings
	const char * ToString( Code t );
	Code         FromString( std::string s );

	const Code   Unknown        =      -1;
	const Code   None           =       0;
	const Code   OneBit         =       1;
	const Code   Unsigned       =       2;
	const Code   Signed         =       4;
	const Code   EightBit       =       8;
	const Code   SixteenBit     =      16;
	const Code   ThirtyTwoBit   =      32;
	const Code   SixtyFourBit   =      64;
	const Code   Integer        =     128;
	const Code   FloatingPoint  =     256;
	const Code   String         =     512;
	
#	define DefineDataType( NAME, CODE ) const Code NAME = ( CODE );
#	include "DataType.hpp"	
	
} // end namespace DataType

#else // header being included for second or subsequent time...

DefineDataType( NONE           ,          None                 );
DefineDataType( STRING         ,        String                 );
DefineDataType( BOOL           ,       Integer |  1 | Unsigned );
DefineDataType( UINT8          ,       Integer |  8 | Unsigned );
DefineDataType( INT8           ,       Integer |  8 | Signed   );
DefineDataType( UINT16         ,       Integer | 16 | Unsigned );
DefineDataType( INT16          ,       Integer | 16 | Signed   );
DefineDataType( UINT32         ,       Integer | 32 | Unsigned );
DefineDataType( INT32          ,       Integer | 32 | Signed   );
DefineDataType( UINT64         ,       Integer | 64 | Unsigned );
DefineDataType( INT64          ,       Integer | 64 | Signed   );
DefineDataType( FLOAT32        , FloatingPoint | 32 | Signed   );
DefineDataType( FLOAT64        , FloatingPoint | 64 | Signed   );

#endif // INCLUDED_DataType_HPP

#undef  DefineDataType
#define DefineDataType( NAME, CODE )
