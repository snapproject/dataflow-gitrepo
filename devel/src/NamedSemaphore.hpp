/*
# $BEGIN_DATAFLOW_LICENSE$
# 
# This file is part of the DataFlow project, a software library for
# efficient asynchronous multi-stream inter-process communication.
# 
# Copyright (c) 2014-2021 Jeremy Hill
# 
# DataFlow is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program. If not, see http://www.gnu.org/licenses/ .
# 
# Some third-party source code is compiled and linked into the
# DataFlow binary, as follows: devel/src/sem_timedwait_apple.cpp
# is adapted from sem_timedwait.cpp by Keith Shortridge, copyright
# (c) 2013 Australian Astronomical Observatory (AAO), released under
# the MIT License; devel/src/dirent_win.h is copyright (c) 2006
# Toni Ronkko, and released under the MIT License. The DataFlow build
# system also uses some third-party code:
# devel/build/cmake-recipes/DefineFunction_TARGET_ARCHITECTURE.cmake
# which is copyright (c) 2012 Petroules Corporation, released under
# the 2-Clause BSD License. For more details, see the respective
# copyright, licensing and disclaimer information in each of these
# files.
# 
# $END_DATAFLOW_LICENSE$
*/
#ifndef INCLUDE_NamedSemaphore_HPP
#define INCLUDE_NamedSemaphore_HPP

#include "ExceptionUtils.hpp" // for STRINGSUBCLASS, RAISE,  RAISE_SUBCLASS


#ifdef _WIN32
#	include <windows.h>
#else
#	include <semaphore.h>
#endif

#include <string>

class NamedSemaphore
{
	public:
		NamedSemaphore( std::string name );
		~NamedSemaphore();
	
		std::string GetName( void ) const;
		void Acquire( double timeoutSeconds=-1.0 );
		void Release( void );
		bool Acquired( void );
	
		int  Remove( void );
		void SetCooperative( bool setting );
		// The default is to be not cooperative (i.e. a mutex):  the instance that acquired the lock must be the one to release it, and it does so automatically on instance destruction 
		// If you set cooperative mode to true, then, another instance (in another thread or process) is allowed to release it on your behalf, and releasing does not happen automatically
		
		void WaitForSignal( double timeoutSeconds=-1.0 ); // put the flag up and wait for someone else to put it down
		void SendSignal( void ); // put someone else's flag down
		
	private:
		std::string mName;
		bool        mAcquired;
		bool        mOpened;
		bool        mIsMutex;
#ifdef    _WIN32
		HANDLE      mHandle;
#else  // _WIN32
		sem_t *     mHandle;
		sem_t       mLocal;
#endif // _WIN32
	
		void        Open( void );
		void        Close( void );
	
	
	public:
		class ScopedLock
		{
			public:
				ScopedLock( NamedSemaphore & base, double timeoutSeconds=-1.0 );
				ScopedLock( NamedSemaphore * base, double timeoutSeconds=-1.0 );
				~ScopedLock();
			private:
				NamedSemaphore * mBase;
		};
		
		static std::string LegalizeName( std::string name );
		static int         Remove( std::string name );
		
		static int         Demo( int argc, const char * argv[] );
		static int         SignalingDemo( int argc, const char * argv[] );

		STRINGSUBCLASS( TimeoutError );
};

#endif // ifndef INCLUDE_NamedSemaphore_HPP
