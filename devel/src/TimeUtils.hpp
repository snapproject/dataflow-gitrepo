/*
# $BEGIN_DATAFLOW_LICENSE$
# 
# This file is part of the DataFlow project, a software library for
# efficient asynchronous multi-stream inter-process communication.
# 
# Copyright (c) 2014-2021 Jeremy Hill
# 
# DataFlow is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program. If not, see http://www.gnu.org/licenses/ .
# 
# Some third-party source code is compiled and linked into the
# DataFlow binary, as follows: devel/src/sem_timedwait_apple.cpp
# is adapted from sem_timedwait.cpp by Keith Shortridge, copyright
# (c) 2013 Australian Astronomical Observatory (AAO), released under
# the MIT License; devel/src/dirent_win.h is copyright (c) 2006
# Toni Ronkko, and released under the MIT License. The DataFlow build
# system also uses some third-party code:
# devel/build/cmake-recipes/DefineFunction_TARGET_ARCHITECTURE.cmake
# which is copyright (c) 2012 Petroules Corporation, released under
# the 2-Clause BSD License. For more details, see the respective
# copyright, licensing and disclaimer information in each of these
# files.
# 
# $END_DATAFLOW_LICENSE$
*/
#ifndef INCLUDE_TimeUtils_HPP
#define INCLUDE_TimeUtils_HPP

#include "BasicTypes.hpp"
#include "DebuggingUtils.hpp"

#include <chrono> // CPLUSPLUS11, for std::chrono::steady_clock and friends
#include <string>
#include <cmath> // for std::nan (which is CPLUSPLUS11)

namespace TimeUtils
{
	const double NOW = 1e99;
	typedef std::chrono::time_point< std::chrono::steady_clock > TimerOrigin;
	
	double      SecondsSinceEpoch( bool atTimerOrigin=false );  // uses system_clock, and so is vulnerable to system clock updates
	double      PrecisionTime( bool relativeToFirstCall=true ); // uses steady_clock.  On first call, steady_clock AND system_clock "origin" values will be recorded globally.  If relativeToFirstCall is true, elapsed steady_clock time is returned relative to this origin.
	double      PrecisionTime( TimerOrigin * origin ); // compute elapsed steady_clock time since specified origin (or raw absolute steady_clock value if origin is NULL)
	TimerOrigin GetTimerOrigin( void ); // returns the absolute steady_clock value that is being used as the origin for PrecisionTime
	void        SetTimerOrigin( TimerOrigin origin, double secondsSinceEpoch=NOW ); // change the global origin used for PrecisionTime (and the corresponding system_clock time, which will be read now if you use the default value NOW)
	void        SleepSeconds( double seconds );
	double      SleepUntil( double targetPrecisionTime, bool relativeToFirstCall=true ); // sleep until after a particular specified PrecisionTime() value (return ASAP after the target, but accuracy is not guaranteed)
	std::string FormatDateStamp( double secondsSinceEpoch=NOW, bool local=true, const char * fmt=NULL );
	double      ParseDateStamp( const std::string & s, bool local=true, const char *fmt=NULL );
	std::string FormatRelativeTimeStamp( double t, unsigned int precision=3 );
	
	
	class Timer
	{
		public:
			Timer( double durationSeconds, double startSecondsSinceEpoch=NOW );
			bool Ticking( void );
			void Tick( double sleepSeconds );
		private:
			double mDeadline;
	};
	
	class Welford
	{
		private:
			double mPreviousValue;
			double mMean;
			double mM2;
			uint64_t mCount;
			uint64_t mLimit;
			Welford * mDifferences;
			std::string mUnits;
			double mScaling;
			
		public:
			Welford( uint64_t limit=0xFFFFFFFFFFFFFFFF );
			~Welford();
			Welford & Differences( void );
			Welford & SetScaling( double scaling, const std::string & units );
			void      Reset( void );
			uint64_t  SetLimit( uint64_t limit );
			uint64_t  Limit( void ) const;
			uint64_t  Count( void ) const;
			double    Process( double x );
			bool      Finished( void ) const;
			double    Mean( void ) const;
			double    StandardDeviation( uint64_t sampleBiasCorrection=1 ) const;
			std::string Report( uint64_t sampleBiasCorrection=1, int precision=3 ) const ;
	};
	
	int         Demo( int argc, const char * argv[] );

}

#define MEASURE_TIME_TAKEN( N, WHAT, OUTPUT_VARIABLE_FOR_MEAN ) { \
	int _n = ( N ); \
	double _t0 = TimeUtils::PrecisionTime(); \
	for( int _count = 1; _count <= _n; _count++ ) { WHAT; } \
	OUTPUT_VARIABLE_FOR_MEAN = ( TimeUtils::PrecisionTime() - _t0 ) / ( double )_n; \
}
#define DBREPORT_TIME_TAKEN( LEVEL, N, WHAT ) { \
	int __n = ( N ); \
	double __avg; \
	MEASURE_TIME_TAKEN( __n, WHAT, __avg ); \
	DEBUG( LEVEL, "average time taken for " << #WHAT << " over " << __n << " repetitions: " << std::fixed << std::setprecision( 3 ) << 1e6 * __avg << " microseconds" ); \
}

#define MEASURE_TIME_TAKEN_WITH_VARIANCE( N, WHAT, OUTPUT_VARIABLE_FOR_MEAN, OUTPUT_VARIABLE_FOR_STD ) { \
	/* \
		Only use this one if you're measuring something that takes, say, 1 microsecond or more, \
		and you're interested in knowing the variance. This looks at the timer before & after   \
		each iteration, and the overhead of these measurements themselves may add an artefact   \
		to the estimate, on the order of 0.1 microseconds.  Otherwise, use MEASURE_TIME_TAKEN() \
		which takes a single time measurement before, and a single measurement after, a tight   \
		loop (so it gets a better mean estimate, but cannot measure variance).                  \
	*/ \
	TimeUtils::Welford _w( N ); \
	while( !_w.Finished() ) \
	{ \
		double _t0 = TimeUtils::PrecisionTime(); \
		{ WHAT; } \
		_w.Process( TimeUtils::PrecisionTime() - _t0 ); \
	} \
	OUTPUT_VARIABLE_FOR_MEAN = _w.Mean(); \
	OUTPUT_VARIABLE_FOR_STD  = _w.StandardDeviation(); \
}
#define DBREPORT_TIME_TAKEN_WITH_VARIANCE( LEVEL, N, WHAT ) { \
	/* \
		Only use this one if you're measuring something that takes, say, 1 microsecond or more, \
		and you're interested in knowing the variance. Otherwise use DBREPORT_TIME_TAKEN(). \
		(See notes above). \
	*/ \
	int __n = ( N ); \
	double __avg, __std; \
	MEASURE_TIME_TAKEN_WITH_VARIANCE( __n, WHAT, __avg, __std ); \
	DEBUG( LEVEL, "average time taken for " << #WHAT << " over " << __n << " repetitions: " << std::fixed << std::setprecision( 3 ) << 1e6 * __avg << " microseconds (+/- " << 1e6 * __std << " SD)" ); \
}


#define MEASURE_TIMER_RESOLUTION( N, EXPR, OUTPUT_VARIABLE_FOR_MEAN, OUTPUT_VARIABLE_FOR_STD ) { \
	TimeUtils::Welford _w( N ); \
	double _t0, _t1 = ( double )( EXPR ); \
	while( !_w.Finished() ) \
	{ \
		while( _t1 == ( _t0 = ( double )( EXPR ) ) ) {} \
		while( _t0 == ( _t1 = ( double )( EXPR ) ) ) {} \
		_w.Process( _t1 - _t0 ); \
	} \
	OUTPUT_VARIABLE_FOR_MEAN = _w.Mean(); \
	OUTPUT_VARIABLE_FOR_STD  = _w.StandardDeviation(); \
}
#define DBREPORT_TIMER_RESOLUTION( LEVEL, N, EXPR ) { \
	int __n = ( N ); \
	double __avg, __std; \
	MEASURE_TIMER_RESOLUTION( __n, EXPR, __avg, __std ); \
	DEBUG( LEVEL, "minimal increment over " << __n << " cycles of " << #EXPR << ": avg " << std::fixed << std::setprecision( 3 ) << 1e6 * __avg << " microseconds (+/- " << 1e6 * __std << " SD)" ); \
}

#endif // ifndef INCLUDE_TimeUtils_HPP
