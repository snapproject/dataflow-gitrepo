/*
# $BEGIN_DATAFLOW_LICENSE$
# 
# This file is part of the DataFlow project, a software library for
# efficient asynchronous multi-stream inter-process communication.
# 
# Copyright (c) 2014-2021 Jeremy Hill
# 
# DataFlow is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program. If not, see http://www.gnu.org/licenses/ .
# 
# Some third-party source code is compiled and linked into the
# DataFlow binary, as follows: devel/src/sem_timedwait_apple.cpp
# is adapted from sem_timedwait.cpp by Keith Shortridge, copyright
# (c) 2013 Australian Astronomical Observatory (AAO), released under
# the MIT License; devel/src/dirent_win.h is copyright (c) 2006
# Toni Ronkko, and released under the MIT License. The DataFlow build
# system also uses some third-party code:
# devel/build/cmake-recipes/DefineFunction_TARGET_ARCHITECTURE.cmake
# which is copyright (c) 2012 Petroules Corporation, released under
# the 2-Clause BSD License. For more details, see the respective
# copyright, licensing and disclaimer information in each of these
# files.
# 
# $END_DATAFLOW_LICENSE$
*/
#ifndef INCLUDED_StringUtils_HPP
#define INCLUDED_StringUtils_HPP

#include <string>
#include <ostream>
#include <sstream>
#include <vector>

namespace StringUtils
{
	typedef std::vector< std::string  > StringVector;
	typedef std::vector< StringVector > StringVectorVector;
	
	std::string  ToLower( const std::string & s );
	std::string  ToUpper( const std::string & s );
	std::string  ChompString( std::string & s, const std::string & delims, char groupOpener='\0', char groupCloser='\0', bool countEmpty=false );
	std::string  Join( const StringVector & v, const std::string & delim=" ", bool removeDelim=false, size_t startIndex=0, size_t numberOfElements=std::string::npos );
	void         Split( StringVector & output, std::string input, const std::string & delims=" \t\r\n", char groupOpener='\0', char groupCloser='\0', bool countEmpty=false ); // split by ANY ONE of the characters in `delims`
	bool         Match( const std::string & pattern, const std::string & candidate, const char *options="" );
	bool         StartsWith( const std::string & fullString, const std::string & prefix );
	bool         EndsWith( const std::string & fullString, const std::string & suffix );
	
	void         BuildStringVector( StringVector & sv, int argc, const char * argv[], bool append=false );
	StringVector BuildStringVector( int argc, const char * argv[] );
	
	std::string  StringLiteral( const std::string & s, const char * quote="\"" );
	std::string  StringLiteral( const char * s, const char * quote="\"" );
	std::string  StringLiteral( const char * s, size_t length, const char * quote="\"" );
	
	std::string  RegexReplace( const std::string & in, const std::string & pattern, const std::string & replacement );
	std::string  Strip( const std::string & in, const char * end="both", const std::string & pattern="\\s+" );
	
	int          Demo( int argc, const char * argv[] );
}
std::ostream & operator<<( std::ostream & os, const StringUtils::StringVector & x );	

#define BUILD_STRING( VARIABLE_NAME, THINGS ) { std::stringstream _buildss; _buildss << THINGS; VARIABLE_NAME = _buildss.str(); }
#define DECLARE_STRING( VARIABLE_NAME, THINGS ) std::string VARIABLE_NAME; BUILD_STRING( VARIABLE_NAME, THINGS )


#endif // ifndef INCLUDED_StringUtils_HPP
