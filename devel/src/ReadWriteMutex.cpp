/*
# $BEGIN_DATAFLOW_LICENSE$
# 
# This file is part of the DataFlow project, a software library for
# efficient asynchronous multi-stream inter-process communication.
# 
# Copyright (c) 2014-2021 Jeremy Hill
# 
# DataFlow is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program. If not, see http://www.gnu.org/licenses/ .
# 
# Some third-party source code is compiled and linked into the
# DataFlow binary, as follows: devel/src/sem_timedwait_apple.cpp
# is adapted from sem_timedwait.cpp by Keith Shortridge, copyright
# (c) 2013 Australian Astronomical Observatory (AAO), released under
# the MIT License; devel/src/dirent_win.h is copyright (c) 2006
# Toni Ronkko, and released under the MIT License. The DataFlow build
# system also uses some third-party code:
# devel/build/cmake-recipes/DefineFunction_TARGET_ARCHITECTURE.cmake
# which is copyright (c) 2012 Petroules Corporation, released under
# the 2-Clause BSD License. For more details, see the respective
# copyright, licensing and disclaimer information in each of these
# files.
# 
# $END_DATAFLOW_LICENSE$
*/
#ifndef   INCLUDE_ReadWriteMutex_CPP
#define   INCLUDE_ReadWriteMutex_CPP

#include "ReadWriteMutex.hpp"
#include "ExceptionUtils.hpp"
#include "DebuggingUtils.hpp"

#define DBRW  mVerbose ? 0 : 3 // debug level for reporting ReadWriteMutex state changes (do not parenthesize---it's abused at one point...)


ReadWriteMutex::ReadWriteMutex( std::string name, uint32_t * countPtr, bool verbose )
: mReadingSemaphore( NULL ),
  mWritingSemaphore( NULL ),
  mCountingSemaphore( NULL ),
  mDedicatedSharedMemory( NULL ),
  mCountPtr( NULL ),
  mMode( 0 ),
  mLocalCount( 0 ),
  mOpened( false ),
  mVerbose( verbose )
{
	mName = NamedSemaphore::LegalizeName( name );
	Open( countPtr );
}

ReadWriteMutex::~ReadWriteMutex()
{
	Close();
}

std::string
ReadWriteMutex::ResourceName( std::string baseName, char resourceType )
{
	std::string semName = NamedSemaphore::LegalizeName( baseName );
	std::string shmName = SharedMemorySegment::LegalizeName( baseName, false );
	switch( resourceType )
	{
		case 'r': return semName + ".reading";  break;
		case 'w': return semName + ".writing";  break;
		case 'c': return semName + ".counting"; break;
		case 'm': return shmName + ".count"; break;
	}
	return "";
}

void
ReadWriteMutex::Open( uint32_t * countPtr )
{
	if( mOpened ) return;
	mReadingSemaphore  = new NamedSemaphore( ResourceName( mName, 'r' ) );
	mWritingSemaphore  = new NamedSemaphore( ResourceName( mName, 'w' ) );
	mWritingSemaphore->SetCooperative( true );
	mCountingSemaphore = new NamedSemaphore( ResourceName( mName, 'c' ) );
	mCountPtr = countPtr;
	if( !mCountPtr )
	{
		mDedicatedSharedMemory = new SharedMemorySegment( ResourceName( mName, 'm' ), false, sizeof( *countPtr ) ); // NB: could in principle use a fourth semaphore (a cooperative one) for the shared count
		mCountPtr = ( uint32_t * )mDedicatedSharedMemory->Open();
	}
	mOpened = true;
}

void
ReadWriteMutex::Close( void )
{
	Release( true );
	delete mReadingSemaphore;  mReadingSemaphore  = NULL;
	delete mWritingSemaphore;  mWritingSemaphore  = NULL;
	delete mCountingSemaphore; mCountingSemaphore = NULL;
	delete mDedicatedSharedMemory; mDedicatedSharedMemory = NULL;
	mCountPtr = NULL;
	mMode = 0;
	mLocalCount = 0;
	mOpened = false;
}

int
ReadWriteMutex::Remove( void )
{
	Close();
	return Remove( mName );
}

int
ReadWriteMutex::Remove( std::string baseName )
{
	int result, returnCode = 0;
	if( ( result =      NamedSemaphore::Remove( ResourceName( baseName, 'r' ) ) ) != 0 ) returnCode++;
	if( ( result =      NamedSemaphore::Remove( ResourceName( baseName, 'w' ) ) ) != 0 ) returnCode++;
	if( ( result =      NamedSemaphore::Remove( ResourceName( baseName, 'c' ) ) ) != 0 ) returnCode++;
	if( ( result = SharedMemorySegment::Remove( ResourceName( baseName, 'm' ) ) ) != 0 ) returnCode++;
	return returnCode;
}

void
ReadWriteMutex::Acquire( char mode, double timeoutSeconds )
{
	if( mMode == mode ) { mLocalCount++; return; }
	if( mode == 'r' )
	{
		if( mMode == 'w' ) { mLocalCount++; return; } // it's OK to read if you yourself already have permission to write (NB: this doesn't actually downgrade the permission from write to read-only, it just acknowledges that you can do both)
		//if( mMode ) Release( true );
		DEBUG( DBRW, "ReadWriteMutex \"" << mName << "\" is being acquired in mode '" << mode << "'" );
		NamedSemaphore::ScopedLock readingLock( mReadingSemaphore, timeoutSeconds );
		if( mCountPtr )
		{
			NamedSemaphore::ScopedLock countingLock( mCountingSemaphore, timeoutSeconds );
			( *mCountPtr )++;
			DEBUG( DBRW, "Reader count for ReadWriteMutex \"" << mName << "\" has been incremented to " << *mCountPtr );
			if( *mCountPtr == 1 ) mWritingSemaphore->Acquire( timeoutSeconds ); // Nobody is allowed to write while there are any registered readers. (mWritingSemaphore is cooperative, so whoever leaves last will be the one to remove this restriction)
		}
	} // However, once I have acquired reader rights and this scope ends, others will also be allowed to register as readers. 
	else if( mode == 'w' )
	{
		// It's not OK to write if you only have permission to read, and unfortunately this is not an "upgradable" readers-writer 
		// lock (like boost's UpgradeLockable), i.e. you cannot simply promote from reader to writer. Upgradable RW locks are 
		// vulnerable to deadlocks.  It's better to foresee the potential need for write access from the beginning of your operation.
		if( mMode ) RAISE( "illegal attempt to upgrade ReadWriteMutex from r to w" );
		// if( mMode ) Release( true );
				
		DEBUG( DBRW, "ReadWriteMutex \"" << mName << "\" is being acquired in mode '" << mode << "'" );
		mReadingSemaphore->Acquire( timeoutSeconds ); // Nobody is allowed to register as a new reader or writer while I'm writing.
		mWritingSemaphore->Acquire( timeoutSeconds ); // I cannot start writing until the last reader has left.
	}
	else
	{
		RAISE( "unrecognized mode '" << mode << "' for ReadWriteMutex" );
	}
	mMode = mode;
	mLocalCount = 1;
}

bool
ReadWriteMutex::Downgrade( double timeoutSeconds )
{
	if( mMode != 'w' ) return true;
	if( mLocalCount > 1 ) return false;
	if( mCountPtr )
	{
		NamedSemaphore::ScopedLock countingLock( mCountingSemaphore, timeoutSeconds );
		( *mCountPtr )++; // there's one more reader
		DEBUG( DBRW, "Reader count for ReadWriteMutex \"" << mName << "\" has been incremented to " << *mCountPtr );
	}
	mReadingSemaphore->Release(); // we're now in reader mode, and reader locks only hold the writing semaphore (they only prevent writing)
	mMode = 'r';
	return true;
}

void
ReadWriteMutex::Release( bool force, double timeoutSeconds )
{
	if( mLocalCount > 1 && !force ) { mLocalCount--; return; }
	if( mMode == 'r' )
	{
		DEBUG( DBRW, "ReadWriteMutex \"" << mName << "\" is being released from mode '" << mMode << "'" );
		bool release = !mCountPtr;
		if( mCountPtr )
		{
			NamedSemaphore::ScopedLock countingLock( mCountingSemaphore, timeoutSeconds );
			release = !--( *mCountPtr );
			DEBUG( DBRW, "Reader count for ReadWriteMutex \"" << mName << "\" has been decremented to " << *mCountPtr );
		}
		if( release ) mWritingSemaphore->Release(); // reader locks only hold the writing semaphore (they only prevent writing)
	}
	if( mMode == 'w' )
	{
		DEBUG( DBRW, "ReadWriteMutex \"" << mName << "\" is being released from mode '" << mMode << "'" );
		mWritingSemaphore->Release(); // writer locks hold both semaphores
		mReadingSemaphore->Release();
	}
	mMode = 0;
	mLocalCount = 0;
}

char
ReadWriteMutex::Acquired( void ) const
{
	return mMode;
}

void
ReadWriteMutex::SetCountPointer( uint32_t * countPtr )
{
	mCountPtr = countPtr;
}

ReadWriteMutex::ScopedLock::ScopedLock( ReadWriteMutex * base, char mode, double timeoutSeconds ) : mBase(  base ) { if( mBase ) mBase->Acquire( mode, timeoutSeconds ); }
ReadWriteMutex::ScopedLock::ScopedLock( ReadWriteMutex & base, char mode, double timeoutSeconds ) : mBase( &base ) { if( mBase ) mBase->Acquire( mode, timeoutSeconds ); }
ReadWriteMutex::ScopedLock::ScopedLock( ScopedLock && other ) // CPLUSPLUS11
{ // This implements move semantics, for returning one of these instances from a function without unlocking it.
  // It will only be used if you disable copy elision (a.k.a NRVO), for example with g++ -fno-elide-constructors
	mBase = other.mBase;
	other.mBase = NULL;
	if( mBase ) DEBUG( mBase->DBRW, "moving ReadWriteMutex::ScopedLock " << &other << " -> " << this );
}
ReadWriteMutex::ScopedLock::~ScopedLock() { if( mBase ) mBase->Release(); } // TODO: what about timeout? always infinite, like this? or some long default? (and then if that fails, catch the exception here and abandon the attempt)
void ReadWriteMutex::ScopedLock::UnlockPrematurely( double timeoutSeconds ) { if( mBase ) mBase->Release( timeoutSeconds ); mBase = NULL; }
bool ReadWriteMutex::ScopedLock::Downgrade( double timeoutSeconds ) { return mBase ? mBase->Downgrade( timeoutSeconds ) : true; }
char ReadWriteMutex::ScopedLock::Acquired( void ) const { return mBase ? mBase->Acquired() : '\0'; }



#include "ConsoleUtils.hpp"
#include "TimeUtils.hpp"
#include <stdio.h>
#include <ctype.h>
#include <iostream>
int
ReadWriteMutex::Demo( int argc, const char * argv[] )
{
/*
	arguments: NAME MODE
	
	e.g.:
	
	$ ./bin/cpptest ZARP r
	# This acquires the mutex ZARP for either reading (r) or writing (w), then waits for
	# user to press return. In a different shell, you can see the shared resources it
	# creates:
	
	$ lsof | grep ZARP
	cpptest   32171  jez    3r  PSXSEM                          0t0          /ZARP.reading
	cpptest   32171  jez    4r  PSXSEM                          0t0          /ZARP.writing
	cpptest   32171  jez    5r  PSXSEM                          0t0          /ZARP.counting
	cpptest   32171  jez    6u  PSXSHM                         4096          /ZARP.count

	# Note that ZARP.count is a tiny piece of shared memory (PSXSHM) rather than a semaphore
	# (PSXSEM). It is created automatically because this Demo function constructs ReadWriteMutex
	# with the counter pointer unspecified (defaults to NULL).  In real-world usage you might
	# actually want to pass the constructor the address of 4 bytes that you have reserved inside
	# a previously-created shared memory segment (if the ReadWriteMutex is intended to protect
	# the segment itself, for example)
	
	# Run multiple instances from multiple shells to explore how readers and writers compete for
	# the mutex.
	
	# Not sure what the equivalent of lsof is in Windows - possibly accesschk
	#  ( https://technet.microsoft.com/en-us/sysinternals/bb664922.aspx )
	
*/
	const char * execname = *argv++; argc--;
	if( !argc ) return fprintf( stderr, "%s: must supply mutex name\n", execname );
	const char * name = *argv++; argc--;
	if( !argc ) return fprintf( stderr, "%s: must supply mode (R or W)\n", execname );
	char mode = ::tolower( ( *argv++ )[ 0 ] ); argc--;
	if( argc ) return fprintf( stderr, "%s: too many arguments\n", execname );
	ReadWriteMutex rw( name );
	ConsoleUtils::CatchFirstInterrupt();
	printf( "%16.6f   Waiting to %s...\n", TimeUtils::SecondsSinceEpoch(), ( mode == 'r' ? "read" : "write" ) );
	{
		ReadWriteMutex::ScopedLock lock( rw, mode );
		printf( "%16.6f   Acquired permission to %s.\n", TimeUtils::SecondsSinceEpoch(), ( mode == 'r' ? "read" : "write" ) );
		ConsoleUtils::GetUserInput( "Press return: " );
		//TimeUtils::SleepSeconds( 1.0 );
	}
	printf( "%16.6f   Finished %s.\n", TimeUtils::SecondsSinceEpoch(), ( mode == 'r' ? "reading" : "writing" ) );
	
	return 0;
}

#include "FileUtils.hpp"
int
KernelResources::Remove( const std::string & name, const std::string & execname )
{	
	if( !name.length() ) return 0;	
	int hits = 0;
#ifdef _WIN32
		DEBUG( 2, execname << ( execname.length() ? " will not remove \"" : "not removing \"" ) << name << "\": on Windows, resources will be removed automatically when the last process releases them." );
#else
		int rwmError = ReadWriteMutex::Remove( name );
		if( rwmError < 2 ) { hits++; DEBUG( 2, "removed readers/writer lock \"" << name << "\"" ); }
		int semError = NamedSemaphore::Remove( name );
		if( !semError ) { hits++; DEBUG( 2, "removed semaphore \"" << NamedSemaphore::LegalizeName( name ) << "\"" ); }
		int shmError = SharedMemorySegment::Remove( name );
		if( !shmError ) { hits++; DEBUG( 2, "removed shared memory segment \"" << SharedMemorySegment::LegalizeName( name, false ) << "\"" ); }
		if( hits == 0 ) DEBUG( 2, execname << ( execname.length() ? ": " : "" ) << "could not remove \"" << name << "\" as a readers/writer lock, named semaphore or shared memory segment" );
		DEBUG( 2, "" );
#endif
	return hits;
}
int
KernelResources::RemovalUtility( int argc, const char * argv[] )
{
	/*
	
	Windows and POSIXoid systems behave slightly differently when it comes to
	semaphores and shared memory segments (file-backed or not):
	
		- On Windows, when all processes have finished with a resource it will be removed
		  automatically from the kernel, so it will need to be initialized again (with an
		  explicitly specified size, in the case of shared memory segments) once it has been
		  released.  This cleanup utility is therefore not required (if you try to use it,
		  it will falsely claim that it is successfully removing whatever you ask for).
		  
		- On some POSIX-like systems, the resources will be listed in `lsof -n` (marked
		  PSXSEM for semaphores and PSXSHM for shared memory segments)--they will only
		  appear in this list while they are being used by an active process. On other
		  systems, they will appear in `/dev/shm` (with a `sem.` prefix, for semaphores).
		  In either case, a resource will still be in the kernel and can still be revived,
		  even after it has been released by all processes (and even with nominal size 0,
		  in the case of shared memory).  To remove it entirely, you need to call the
		  corresponding `::Remove()` function, which can be performed by calling this
		  program with one or more resource names.
	*/
	std::string execname = FileUtils::BaseName( *argv++, false ); argc--;
	DebuggingUtils::RaiseDebugLevel( 2 );
	if( argc < 1 ) {  DEBUG( 2, execname << ": must supply one or more names" ); return -1; }
	int returnCode = 1;
	while( argc )
	{
		const char * name = *argv++; argc--;
		if( KernelResources::Remove( name, execname ) > 0 ) returnCode = 0;
	}
	return returnCode;
}


#endif // INCLUDE_ReadWriteMutex_CPP
