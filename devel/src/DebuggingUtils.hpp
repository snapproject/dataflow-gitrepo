/*
# $BEGIN_DATAFLOW_LICENSE$
# 
# This file is part of the DataFlow project, a software library for
# efficient asynchronous multi-stream inter-process communication.
# 
# Copyright (c) 2014-2021 Jeremy Hill
# 
# DataFlow is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program. If not, see http://www.gnu.org/licenses/ .
# 
# Some third-party source code is compiled and linked into the
# DataFlow binary, as follows: devel/src/sem_timedwait_apple.cpp
# is adapted from sem_timedwait.cpp by Keith Shortridge, copyright
# (c) 2013 Australian Astronomical Observatory (AAO), released under
# the MIT License; devel/src/dirent_win.h is copyright (c) 2006
# Toni Ronkko, and released under the MIT License. The DataFlow build
# system also uses some third-party code:
# devel/build/cmake-recipes/DefineFunction_TARGET_ARCHITECTURE.cmake
# which is copyright (c) 2012 Petroules Corporation, released under
# the 2-Clause BSD License. For more details, see the respective
# copyright, licensing and disclaimer information in each of these
# files.
# 
# $END_DATAFLOW_LICENSE$
*/
#ifndef INCLUDE_DebuggingUtils_HPP
#define INCLUDE_DebuggingUtils_HPP

#include "BasicTypes.hpp"
#include "StringUtils.hpp"
#include "ExceptionUtils.hpp" // for RAISE()
#include "SharedDataArchiveStructures.hpp" // for PACKING_TEST_STRUCT and TIGHT

#include <ctype.h>
#include <iostream>
#include <iomanip>
#include <sstream>

#define DB std::cerr << __FILE__ << ":" << __LINE__ << std::endl
// simple "I was here" marker

#ifndef DEBUG_LEVEL
#	define DEBUG_LEVEL 0
#endif
#ifndef INCLUDE_DebuggingUtils_CPP
#	undef DEBUG_LEVEL
#	define DEBUG_LEVEL DebuggingUtils::gDebugLevel
#endif

#define DEBUG( level, x ) { if( ( level ) <= DEBUG_LEVEL ) std::cerr << x << std::endl << std::flush; }
#define DBREPORT( level, x ) DEBUG( level, #x << " = " << x )
#define DBREPORTQ( level, x ) DEBUG( level, #x << " = " << StringUtils::StringLiteral( x ) )

namespace DebuggingUtils
{
	int  Console( const char * msg, int debugLevel );
	void PrintArgv( int argc, const char * argv[] );
	void SanityCheck( char assertEndianness='\0' );
	void SetDebugLevel( int level );
	void RaiseDebugLevel( int level );
	void LowerDebugLevel( int level );
	extern int gDebugLevel;
}

#define DBASSERT( x, msg )  if( ! ( x ) ) _errors << ( _errors.str().length() ? "\n" : "" ) << "failed assertion " #x msg;
#define SANITY_CHECK( PACKING_TEST_STRUCT, TIGHT, ASSERT_ENDIANNESS ) \
{ \
	char _endianness = toupper( ASSERT_ENDIANNESS ); \
	std::stringstream _errors; \
	DBASSERT( sizeof( int8_t    ) == 1, "" ); \
	DBASSERT( sizeof( uint8_t   ) == 1, "" ); \
	DBASSERT( sizeof( int16_t   ) == 2, "" ); \
	DBASSERT( sizeof( uint16_t  ) == 2, "" ); \
	DBASSERT( sizeof( int32_t   ) == 4, "" ); \
	DBASSERT( sizeof( uint32_t  ) == 4, "" ); \
	DBASSERT( sizeof( int64_t   ) == 8, "" ); \
	DBASSERT( sizeof( uint64_t  ) == 8, "" ); \
	DBASSERT( sizeof( float32_t ) == 4, "" ); \
	DBASSERT( sizeof( float64_t ) == 8, "" ); \
	DBASSERT( sizeof( PACKING_TEST_STRUCT ) == TIGHT, "  (not compiled with packed struct alignment)" ); \
	int x = 1; uint8_t * one = ( uint8_t * )&x; \
	if(     _endianness  == 'L' ) { DBASSERT( one[0] == 1, "  (big-endian CPUs are not supported - sorry)" ); } \
	else if( _endianness == 'B' ) { DBASSERT( one[0] == 0, "  (little-endian CPUs are not supported - sorry)" ); } \
	else if( _endianness != '0' && _endianness != '\0' ) RAISE( "unrecognized endianness code '" << _endianness  << "'" ); \
	if( _errors.str().length() ) RAISE( "one or more runtime assertions failed:\n" + _errors.str() ); \
} // NB: This is a runtime sanity check.  See also the compile-time sanity-check enabled by compiling DebuggingUtils.cpp with the macro STATIC_ASSERT defined

////////////////////////////////////////////////////////////////////////////////////////////////////////////



#endif // ifndef INCLUDE_DebuggingUtils_HPP
