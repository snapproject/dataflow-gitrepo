/*
# $BEGIN_DATAFLOW_LICENSE$
# 
# This file is part of the DataFlow project, a software library for
# efficient asynchronous multi-stream inter-process communication.
# 
# Copyright (c) 2014-2021 Jeremy Hill
# 
# DataFlow is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program. If not, see http://www.gnu.org/licenses/ .
# 
# Some third-party source code is compiled and linked into the
# DataFlow binary, as follows: devel/src/sem_timedwait_apple.cpp
# is adapted from sem_timedwait.cpp by Keith Shortridge, copyright
# (c) 2013 Australian Astronomical Observatory (AAO), released under
# the MIT License; devel/src/dirent_win.h is copyright (c) 2006
# Toni Ronkko, and released under the MIT License. The DataFlow build
# system also uses some third-party code:
# devel/build/cmake-recipes/DefineFunction_TARGET_ARCHITECTURE.cmake
# which is copyright (c) 2012 Petroules Corporation, released under
# the 2-Clause BSD License. For more details, see the respective
# copyright, licensing and disclaimer information in each of these
# files.
# 
# $END_DATAFLOW_LICENSE$
*/
#ifndef   INCLUDED_DataNode_CPP
#define   INCLUDED_DataNode_CPP

#include "DataNode.hpp"
#include "ExceptionUtils.hpp"
#include "TimeUtils.hpp"
#include "DebuggingUtils.hpp" // TODO: remove
#include "Confluence.hpp"

#include <stdio.h>
#include <string.h>

const std::string DataNode::Separator = "/";
const std::string DataNode::StringDelim = Dimension::StringDelim; // used to separate elements of string arrays

#define BEGIN_CHILD_ITERATION( ITERATOR, NODE, CHILD ) \
	for( DataNodeMap::iterator ITERATOR = NODE->mChildren.begin(); ITERATOR != NODE->mChildren.end(); ) \
	{ \
		DataNodePtr CHILD = ITERATOR->second; \
		if( !CHILD ) { NODE->mChildren.erase( ITERATOR++ ); continue; }
		
#define END_CHILD_ITERATION( ITERATOR ) \
		ITERATOR++; \
	}


DataNode::DataNode( std::string name, DataNodePtr parent ) :
	mName( name ),
	mParent( parent ),
	mNumberOfEvents( 0 ),
	mDataType( DataType::NONE ),
	mOwnsDataPointer( false ),
	mStringContentLength( 0 ),
	mData( NULL )
{
	
}

DataNode::~DataNode()
{
	ClearData();
	RemoveDescendants();
	if( mParent )
	{
		DataNodeMap::iterator findSelf = mParent->mChildren.find( mName );
		if( findSelf != mParent->mChildren.end() ) findSelf->second = NULL;
	}
}

void
DataNode::ClearData( void )
{
	RemoveDataPointer();
	mDataType = DataType::None;
	mNumberOfEvents = 0;
	mDimensions.clear();
}

void
DataNode::RemoveDescendants( void )
{
	for( DataNodeMap::iterator it = mChildren.begin(); it != mChildren.end(); )
	{
		delete it->second;
		mChildren.erase( it++ );
	}
}

void
DataNode::RemoveDataPointer( void )
{
	if( mOwnsDataPointer ) delete [] mData;
	mStringContentLength = 0;
	mData = NULL;
	mOwnsDataPointer = false;
}

bool
DataNode::HasData( void )
{
	return mNumberOfEvents > 0 && mDataType != DataType::None && GetNumberOfElementsPerEvent() > 0 && mData != NULL;
}

uint64_t
DataNode::GetNumberOfElementsPerEvent( void )
{
	uint64_t product = 1;
	for( DimensionVector::iterator it = mDimensions.begin(); it != mDimensions.end(); it++ ) product *= it->GetExtent();
	return product;
}

uint64_t
DataNode::GetNumberOfNodes( bool includeEmptyNodes )
{
	uint64_t count = ( includeEmptyNodes || HasData() ) ? 1 : 0;
	BEGIN_CHILD_ITERATION( it, this, child )
		count += child->GetNumberOfNodes( includeEmptyNodes );
	END_CHILD_ITERATION( it )
	return count;
}


DataNodePtr
DataNode::Get( uint64_t index, bool includeEmptyNodes )
{
	DataNodePtr result = Get( &index, includeEmptyNodes );
	if( !result ) { RAISE( "data tree index out of range" ); }
	return result;
}

DataNodePtr
DataNode::Get( uint64_t * index, bool includeEmptyNodes )
{
	if( includeEmptyNodes || HasData() )
	{
		if( *index ) --*index;
		else return this;
	}
	BEGIN_CHILD_ITERATION( it, this, child )
		DataNodePtr result = child->Get( index, includeEmptyNodes );
		if( result ) return result;
	END_CHILD_ITERATION( it )
	return NULL;
}

DataNodePtr
DataNode::Next( bool include_children )
{
	if( include_children && mChildren.size() ) return mChildren.begin()->second;
	DataNodePtr child = this;
	DataNodePtr ancestor = mParent;
	while( ancestor )
	{
		DataNodeMap::iterator it = ancestor->mChildren.find( child->mName );
		if( it != ancestor->mChildren.end() && ++it != ancestor->mChildren.end() ) return it->second;
		child = ancestor;
		ancestor = ancestor->mParent;
	}
	return NULL;
}

void *
DataNode::Allocate( uint64_t nBytes )
{
	if( nBytes <= sizeof( mFixedStorage ) )
	{
		if( mOwnsDataPointer ) delete [] mData;
		mData = ( char * )&mFixedStorage;
		mOwnsDataPointer = false;
	}
	else
	{
		if( mOwnsDataPointer ) delete [] mData;
		mData = new char[ as_size_t( nBytes ) ];
		mOwnsDataPointer = true;
	}
	mStringContentLength = 0;
	return ( void * )mData;
}

uint64_t
DataNode::GetContentSizeInBytes( void )
{
	if( mDataType == DataType::String ) return mStringContentLength;
	return mNumberOfEvents * GetBytesPerEvent();
}

std::string
DataNode::GetStringValue( void )
{
	if( !HasData() ) { RAISE( "cannot get a string value from an empty data node" ); }
	if( mDataType != DataType::String ) { RAISE( "cannot get a string value from a data node of type \"" << DataType::ToString( mDataType ) << "\"" ); }
	if( mNumberOfEvents > 1 ) { RAISE( "cannot get a single string value from a multi-event data node" ); }
	return std::string( mData, as_size_t( mStringContentLength ) );
	// No check for GetNumberOfElementsPerEvent() == 1: in the specific case of strings we don't care (yet) whether 
	// its mDimensions describe it as "scalar".  Even in a multi-element string the content can still be read as one
	// big string, with substrings delimited by some particular character.
}

void
DataNode::SetStringValue( const std::string & value )
{
	ClearData();
	mNumberOfEvents = 1;
	mDataType = DataType::String;
	size_t nBytes = value.length() + 1;
	::memcpy( Allocate( nBytes ), value.c_str(), nBytes ); // Allocate() will set mStringContentLength to 0
	mStringContentLength = nBytes;
}

double
DataNode::GetScalarValue( void )
{
	if( !HasData() ) { RAISE( "cannot get a scalar numeric value from an empty data node" ); }
	if( mNumberOfEvents > 1 ) { RAISE( "cannot get a scalar numeric value from a multi-event data node" ); }
	if( GetNumberOfElementsPerEvent() != 1 ) { RAISE( "cannot get a scalar numeric value from a non-scalar data node" ); }
	if(      mDataType == DataType::BOOL    ) return ( double )*(      bool * )mData;
	else if( mDataType == DataType::UINT8   ) return ( double )*(   uint8_t * )mData;
	else if( mDataType == DataType::INT8    ) return ( double )*(    int8_t * )mData;
	else if( mDataType == DataType::UINT16  ) return ( double )*(  uint16_t * )mData;
	else if( mDataType == DataType::INT16   ) return ( double )*(   int16_t * )mData;
	else if( mDataType == DataType::UINT32  ) return ( double )*(  uint32_t * )mData;
	else if( mDataType == DataType::INT32   ) return ( double )*(   int32_t * )mData;
	else if( mDataType == DataType::UINT64  ) return ( double )*(  uint64_t * )mData;
	else if( mDataType == DataType::INT64   ) return ( double )*(   int64_t * )mData;
	else if( mDataType == DataType::FLOAT32 ) return ( double )*( float32_t * )mData;
	else if( mDataType == DataType::FLOAT64 ) return ( double )*( float64_t * )mData;
	else RAISE( "cannot get a scalar numeric value from a data node of type \"" << DataType::ToString( mDataType ) << "\" since this is not a numeric type" );
}

void
DataNode::SetScalarValue( double value, std::string dtype )
{
	SetScalarValue( value, DataType::FromString( dtype ) );
}

void
DataNode::SetScalarValue( double value, DataType::Code dtype )
{
	ClearData();
	mNumberOfEvents = 1;
	mDataType = dtype;
	int nBytes = DataType::SizeInBytes( mDataType );
	Allocate( nBytes ); // sets mStringContentLength to 0
	if(      mDataType == DataType::BOOL    ) *(      bool * )mData = ( value != 0.0 );
	else if( mDataType == DataType::UINT8   ) *(   uint8_t * )mData = (   uint8_t )value;
	else if( mDataType == DataType::INT8    ) *(    int8_t * )mData = (    int8_t )value;
	else if( mDataType == DataType::UINT16  ) *(  uint16_t * )mData = (  uint16_t )value;
	else if( mDataType == DataType::INT16   ) *(   int16_t * )mData = (   int16_t )value;
	else if( mDataType == DataType::UINT32  ) *(  uint32_t * )mData = (  uint32_t )value;
	else if( mDataType == DataType::INT32   ) *(   int32_t * )mData = (   int32_t )value;
	else if( mDataType == DataType::UINT64  ) *(  uint64_t * )mData = (  uint64_t )value;
	else if( mDataType == DataType::INT64   ) *(   int64_t * )mData = (   int64_t )value;
	else if( mDataType == DataType::FLOAT32 ) *( float32_t * )mData = ( float32_t )value;
	else if( mDataType == DataType::FLOAT64 ) *( float64_t * )mData = ( float64_t )value;
	else RAISE( "cannot attach a numeric scalar value to a data node with type \"" << DataType::ToString( mDataType ) << "\" since this is not a valid numeric type" );
}

DataNodePtr
DataNode::Create( std::string path, bool exceptionIfAlreadyExists )
{
	if( exceptionIfAlreadyExists && Get( path, false ) ) RAISE( "cannot create node \"" << path << "\" because it already exists" );
	return Get( path, true );
}

DataNodePtr
DataNode::Get( std::string path, bool createPathIfAbsent )
{
	DataNodePtr node = this;
	StringUtils::StringVector names;
	StringUtils::Split( names, path, DataNode::Separator );
	for( StringUtils::StringVector::iterator it = names.begin(); it != names.end(); it++ )
	{
		std::string name = *it; // StringUtils::Strip( *it );
		if( !name.length() ) continue;
		DataNodeMap::iterator match = node->mChildren.find( name );
		if( match == node->mChildren.end() )
		{
			if( createPathIfAbsent ) node = node->mChildren[ name ] = new DataNode( name, node );
			else { RAISE( "node \"" << name << "\" not found in data tree" ); }
		}
		else node = match->second;
	}
	return node;
}

std::string
DataNode::GetPath( DataNodePtr earliestAncestor )
{
	std::string path;
	for( DataNodePtr node = this; node != NULL && node != earliestAncestor; node = node->mParent )
	{
		if( node->mName == "." ) continue;
		if( path.length() ) path = DataNode::Separator + path;
		path = node->mName + path;
	}
	return path;
}

DataNodePtr
DataNode::SetDimension( uint64_t dimensionIndex, const Dimension & dimension )
{
	uint64_t oldSize = GetBytesPerEvent();
	while( mDimensions.size() <= dimensionIndex ) mDimensions.emplace_back( 1 ); // CPLUSPLUS11
	mDimensions[ as_size_t( dimensionIndex ) ] = dimension;
	uint64_t newSize = GetBytesPerEvent();
	if( mData && newSize != oldSize ) Allocate( newSize );
	return this;
}

DataNodePtr
DataNode::AddDimension( const Dimension & dimension )
{
	return SetDimension( GetNumberOfDimensions(), dimension );
}

void
DataNode::DecodeFormat( const void * serialized )
{
	if( serialized == NULL ) RAISE( "cannot decode format: NULL pointer" );
	const char * p = ( const char * )serialized;
#	define DECODE( X ) { ::memcpy( &X, p, sizeof( X ) ); p += sizeof( X ); }
	DECODE( mDataType );
	uint64_t nDims; DECODE( nDims );
	mDimensions.clear();
	for( uint64_t iDim = 0; iDim < nDims; iDim++ )
	{
		uint64_t extent; DECODE( extent );
		uint64_t stride; DECODE( stride );
		if( *p )
		{
			mDimensions.emplace_back( p, stride ); // CPLUSPLUS11
			if( extent != mDimensions.back().GetExtent() ) RAISE( "internal consistency error: number of labels for dimension does not match encoded extent" );
			while( *p ) p++; // go as far as null-terminator of JoinedLabels string 
		}
		else mDimensions.emplace_back( extent, stride ); // CPLUSPLUS11
		p++; // skip null-terminator
	}
}

std::string
DataNode::SerializeFormat( void )
{
	std::string s;
#	define ENCODE( X )  s.append( ( const char * )&X, sizeof( X ) )
	ENCODE( mDataType );
	uint64_t nDims = GetNumberOfDimensions();
	ENCODE( nDims );
	const char nullterm = '\0';
	uint64_t cumulative = 1;
	for( DimensionVector::iterator it = mDimensions.begin(); it != mDimensions.end(); it++ )
	{
		uint64_t extent = it->GetExtent();
		uint64_t stride = it->GetStride();
		if( stride == 0 ) stride = cumulative;
		cumulative *= extent;
		ENCODE( extent );
		ENCODE( stride );
		s += it->JoinedLabels();
		ENCODE( nullterm );
	}
	return s;
}

#include <iostream>

void
DataNode::ReportTree( void )
{
	for( DataNodePtr node = this, stop = Next( false ); node != stop; node = node->Next() )
		{
		std::cerr << "   " << node->GetPath();
		if( node->HasData() && DataType::IsString(  node->GetDataType() ) ) std::cerr << " = " << StringUtils::StringLiteral( node->GetStringValue() );
		else if( node->HasData() && DataType::IsNumeric( node->GetDataType() ) && node->GetNumberOfElementsPerEvent() == 1 ) std::cerr << " = " << node->GetScalarValue();
		else if( node->HasData() ) std::cerr << " = ..."; // TODO
		std::cerr << "\n";
	}
}

int
DataNode::Demo( int argc, const char * argv[] )
{
	DataNodePtr tree = new DataNode( "" );
	
	tree->Create("Eyetracker/Manufacturer")->SetStringValue( "Tobii" );
	tree->Create("Eyetracker/Model")->SetStringValue( "iTrack+" );
	tree->Create("Eyetracker/ReleaseDate")->SetStringValue("January"); // < returns correct value when pushing

	tree->Create("Eyetracker/SamplingRateInHz")->SetScalarValue(60.0);  // < does not return the correct value when pushing
	tree->Create("Eyetracker/Stream/LeftEye/GazePositionInMm/X")->SetScalarValue(10.0);
	tree->Create("Eyetracker/Stream/LeftEye/GazePositionInMm/Y")->SetScalarValue(20.0);
	//tree->Create("Eyetracker/Stream/LeftEye/EyePositionInMm/X")->SetScalarValue(30.0);
	//tree->Create("Eyetracker/Stream/LeftEye/EyePositionInMm/Y")->SetScalarValue(40.0);
	//tree->Create("Eyetracker/Stream/LeftEye/EyePositionInMm/Z")->SetScalarValue(50.0);
	//tree->Create("Eyetracker/Stream/RightEye/GazePositionInMm/X")->SetScalarValue(60.0);
	//tree->Create("Eyetracker/Stream/RightEye/GazePositionInMm/Y")->SetScalarValue(70.0);
	//tree->Create("Eyetracker/Stream/RightEye/EyePositionInMm/X")->SetScalarValue(80.0);
	//tree->Create("Eyetracker/Stream/RightEye/EyePositionInMm/Y")->SetScalarValue(90.0);
	//tree->Create("Eyetracker/Stream/RightEye/EyePositionInMm/Z")->SetScalarValue(0.0);

	DataNodePtr n = tree->Get( "Eyetracker/SamplingRateInHz" );
	if( !n ) RAISE( "???" );
	DBREPORTQ( 0, n->SerializeFormat() );
	n->AddDimension( Dimension( "A\vB" ) );
	n->AddDimension( Dimension( "One\vTwo\vThree" ) );
	DBREPORTQ( 0, n->SerializeFormat() );
	
	DBREPORT( 0, tree->GetNumberOfNodes() );
	DBREPORT( 0, tree->GetNumberOfNodes( true ) );
	DBREPORTQ( 0, tree->Get( "Eyetracker/Manufacturer" )->GetStringValue() );
	//DBREPORT( 0, tree->Get( "Eyetracker/SamplingRateInHz" )->GetScalarValue() );
	// DBREPORTQ( 0, tree->Get( "foo/bar/woo/bar" )->GetStringValue() ); // should throw an exception
	
	uint64_t nNodes = tree->GetNumberOfNodes( true );
	for( uint64_t iNode = 0; iNode < nNodes; iNode++ )
	{
		DataNodePtr node = tree->Get( iNode, true );
		std::cerr << iNode << ": " << node->GetPath();
		if( node->HasData() && DataType::IsString(  node->GetDataType() ) ) std::cerr << " = " << StringUtils::StringLiteral( node->GetStringValue() );
		else if( node->HasData() && DataType::IsNumeric( node->GetDataType() ) && node->GetNumberOfElementsPerEvent() == 1 ) std::cerr << " = " << node->GetScalarValue();
		else if( node->HasData() ) std::cerr << " = ...";
		std::cerr << "\n";
	}
	tree->ReportTree();

	//Confluence confluence;
	//tree->Push(confluence);

	
	while( true )
	{
		// 1
	//	DataNodePtr eyetrackerStream = tree->Get("/Eyetracker/Stream");
	//	DBREPORTQ( 0, eyetrackerStream->GetName() );
	//	eyetrackerStream->ReportTree();
	//	for( DataNode::iterator it = eyetrackerStreamX->begin(); it != eyetrackerStreamX->end(); it++ )
	//	{
	//		//std::cerr << "   " << node->GetPath() << std::endl;
	//		if (node->HasData() && DataType::IsNumeric(node->GetDataType()) && node->GetNumberOfElementsPerEvent() == 1) node->SetScalarValue(node->GetScalarValue() + 1.0);
	//	}

		// 2
		DataNodePtr eyetrackerStreamX = tree->Get( "/Eyetracker/Stream/LeftEye/GazePositionInMm" );
		eyetrackerStreamX->ReportTree();
		for( DataNode::iterator it = eyetrackerStreamX->begin(); it != eyetrackerStreamX->end(); it++ )
		{
			//std::cerr << "   " << node->GetPath() << std::endl;
			if (it->HasData() && DataType::IsNumeric(it->GetDataType()) && it->GetNumberOfElementsPerEvent() == 1) it->SetScalarValue(it->GetScalarValue() + 1.0);
		}
		//eyetrackerStreamX->Push(confluence);

	//	// 3
	//	DataNodePtr eyetrackerStream2 = tree->Get("/Eyetracker/Stream2");
	//	for (DataNodePtr node = eyetrackerStream2; node; node = node->Next())
	//	{
	//		std::cerr << "   " << node->GetPath() << std::endl;
	//		if (node->HasData() && DataType::IsNumeric(node->GetDataType()) && node->GetNumberOfElementsPerEvent() == 1) node->SetScalarValue(node->GetScalarValue() + 1.0);
	//	}
	//	eyetrackerStream2->Push(confluence);

	//	// 4
	//	DataNodePtr eyetrackerStream3 = tree->Get("/EEG/Stream");
	//	for (DataNodePtr node = eyetrackerStream3; node; node = node->Next())
	//	{
	//		std::cerr << "   " << node->GetPath() << std::endl;
	//		if (node->HasData() && DataType::IsNumeric(node->GetDataType()) && node->GetNumberOfElementsPerEvent() == 1) node->SetScalarValue(node->GetScalarValue() + 1.0);
	//	}
	//	eyetrackerStream3->Push(confluence);

		TimeUtils::SleepSeconds( 1.016 );
		std::cerr << std::endl;
	}

	return 0;
}

void
DataNode::Push( Confluence & confluence )
{
	double timeTaken = 0.0;
	confluence.Push( this, timeTaken );
}

void
DataNode::Push( Confluence & confluence, double & timeTaken )
{
	confluence.Push( this, timeTaken );
}


#endif // INCLUDED_DataNode_CPP
