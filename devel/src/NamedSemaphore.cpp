/*
# $BEGIN_DATAFLOW_LICENSE$
# 
# This file is part of the DataFlow project, a software library for
# efficient asynchronous multi-stream inter-process communication.
# 
# Copyright (c) 2014-2021 Jeremy Hill
# 
# DataFlow is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program. If not, see http://www.gnu.org/licenses/ .
# 
# Some third-party source code is compiled and linked into the
# DataFlow binary, as follows: devel/src/sem_timedwait_apple.cpp
# is adapted from sem_timedwait.cpp by Keith Shortridge, copyright
# (c) 2013 Australian Astronomical Observatory (AAO), released under
# the MIT License; devel/src/dirent_win.h is copyright (c) 2006
# Toni Ronkko, and released under the MIT License. The DataFlow build
# system also uses some third-party code:
# devel/build/cmake-recipes/DefineFunction_TARGET_ARCHITECTURE.cmake
# which is copyright (c) 2012 Petroules Corporation, released under
# the 2-Clause BSD License. For more details, see the respective
# copyright, licensing and disclaimer information in each of these
# files.
# 
# $END_DATAFLOW_LICENSE$
*/
#ifndef INCLUDE_NamedSemaphore_CPP
#define INCLUDE_NamedSemaphore_CPP

#include "NamedSemaphore.hpp"
#include "ConsoleUtils.hpp"

#include <string.h>
#ifdef _WIN32
#else
#	include <fcntl.h>     /* For O_* constants */
#	include <sys/stat.h>  /* For S_* mode constants */
#	include <errno.h>
#endif
#ifdef __APPLE__
	int sem_timedwait( sem_t * sem, const struct timespec * abs_timeout );
#endif

std::string
NamedSemaphore::LegalizeName( std::string name )
{
	if( !name.length() ) { RAISE( "A semaphore cannot have an empty name" ); }
#ifdef    _WIN32
	// Windows will not allow backslashes in `name`
	bool localPrefix = !strncmp( name.c_str(), "Local\\", 6 );
	if( !localPrefix ) name = "Local\\" + name;
	for( std::string::iterator it = name.begin() + 6; it != name.end(); it++ ) if( *it == '\\' ) *it = '/';
#else  // _WIN32
	// Linux systems using tmpfs (/dev/shm) will not allow forward slashes in `name` (for the same reason that you can't create `/foo/bar` when `/foo` doesn't exist)
	for( std::string::iterator it = name.begin() + 1; it != name.end(); it++ ) if( *it == '/' ) *it = '\\';
	if( name[ 0 ] != '/' ) name = "/" + name;
#endif // _WIN32
	return name;
}

NamedSemaphore::NamedSemaphore( std::string name )
:	mAcquired( false ), mOpened( false ), mIsMutex( true ), mHandle( NULL )
{
	mName = LegalizeName( name );
	Open();
}

NamedSemaphore::~NamedSemaphore()
{
	Close();
}

std::string
NamedSemaphore::GetName( void ) const
{
	return mName;
}

void
NamedSemaphore::Open( void )
{
	if( mOpened ) return;
#ifdef    _WIN32
	mHandle = ::CreateSemaphore( NULL, 1, 2, TEXT( mName.c_str() ) );
	if( !mHandle ) { RAISE( "Failed to create/open semaphore \"" << mName << "\" - CreateSemaphore failed with error " << ::GetLastError() ); }
#else  // _WIN32
	mHandle = sem_open( mName.c_str(), O_CREAT, ( S_IRUSR | S_IWUSR ), 1 );
	if( mHandle == SEM_FAILED ) { RAISE( "Failed to create/open semaphore \"" << mName << "\" - sem_open failed with error " << errno ); }
#endif // _WIN32
	mOpened = true;
}

void
NamedSemaphore::SetCooperative( bool setting )
{
	mIsMutex = !setting;
}

void
NamedSemaphore::Close()
{
	if( mIsMutex ) Release();
#ifdef    _WIN32
	if( mHandle ) ::CloseHandle( mHandle ); // NB: a semaphore object is automatically unlinked when its last handle is closed 
#else  // _WIN32
	// sem_unlink( mName.c_str() ); // the interwebs make it look like you should do this before sem_close() and its effect will be deferred until nobody has the semaphore open any more, but on OSX doing this seems to destroy it immediately
	if( mHandle ) sem_close( mHandle );
#endif // _WIN32
	mOpened = false;
}

void
NamedSemaphore::Acquire( double timeoutSeconds ) // Acquire = sem_wait = wait until semaphore value is > 0, then decrement it (our sem_open call creates it with an initial value of 1)
{
	if( mAcquired ) return;
#ifdef    _WIN32
	DWORD timeout_msec = ( timeoutSeconds < 0.0 ? INFINITE : ( DWORD )( timeoutSeconds * 1000.0 ) ); // (may be 0 in which case it checks availability without waiting, and may be INFINITE)
	HANDLE events[ 2 ];
	events[ 0 ] = mHandle;
	events[ 1 ] = ConsoleUtils::GetAbortEvent();
	DWORD result = ::WaitForMultipleObjects( 2, events, FALSE, timeout_msec );
	//DWORD result = ::WaitForSingleObject( mHandle, timeout_msec );
	if(      result == WAIT_TIMEOUT      ) { RAISE_SUBCLASS( TimeoutError, "Failed to acquire semaphore \"" << mName << "\" - timed out after " << timeoutSeconds << " seconds" ); }
	else if( result == WAIT_FAILED       ) { RAISE( "Failed to acquire semaphore \"" << mName << "\" - WaitForMultipleObjects failed with error " << ::GetLastError() ); }
	else if( result == WAIT_OBJECT_0 + 1 ) { RAISE( "Failed to acquire semaphore \"" << mName << "\" - wait aborted" ); }
	else mAcquired = true;
#else  // _WIN32
	int result;
	if(       timeoutSeconds < 0.0 ) result = sem_wait( mHandle ); // never times out
	else if( timeoutSeconds == 0.0 ) result = sem_trywait( mHandle ); // times out immediately
	else
	{
		struct timespec timeout;
		clock_gettime( CLOCK_REALTIME, &timeout );
		time_t wholeSeconds = ( time_t )timeoutSeconds;
		long nanoseconds = ( long )( 1e9 * ( timeoutSeconds - ( double )wholeSeconds ) );
		timeout.tv_sec += wholeSeconds;
		if( ( timeout.tv_nsec += nanoseconds ) > 1000000000 ) { timeout.tv_nsec -= 1000000000; timeout.tv_sec++; }
		result = sem_timedwait( mHandle, &timeout );
	}
	if( result != 0 )
	{
		if( errno == ETIMEDOUT ) { RAISE_SUBCLASS( TimeoutError, "Failed to acquire semaphore \"" << mName << "\" - timed out after " << timeoutSeconds << " seconds" ); }
		else { RAISE( "Failed to acquire semaphore \"" << mName << "\" - sem_*wait failed with error " << errno ); }
	}
	mAcquired = true;
#endif // _WIN32
}

bool
NamedSemaphore::Acquired( void )
{
	return mAcquired;
}

void
NamedSemaphore::Release( void ) // Release = sem_post = increment semaphore value  (but if the mIsMutex flag is set, only release it if this instance previously acquired it)
{
	if( mIsMutex && !mAcquired ) return;
#ifdef    _WIN32
	BOOL result = ::ReleaseSemaphore( mHandle, 1, NULL );
	if( result == FALSE ) { RAISE( "Failed to release semaphore \"" << mName << "\" - ReleaseSemaphore failed with error " << ::GetLastError() ); }
	else mAcquired = false;
	// error code 298 means ERROR_TOO_MANY_POSTS, which means the max value specified in CreateSemaphore() has been exceeded
#else  // _WIN32
	int result = sem_post( mHandle );
	if( result != 0 ) { RAISE( "Failed to release semaphore \"" << mName << "\" - sem_post failed with error " << errno ); }
	mAcquired = false;
#endif // _WIN32
}

void
NamedSemaphore::WaitForSignal( double timeoutSeconds ) // WaitForSignal = sem_wait, sem_wait, sem_post = decrement signal twice, increment once
{
	Acquire( timeoutSeconds );
	mAcquired = false;
	Acquire( timeoutSeconds );
	Release();
}

void
NamedSemaphore::SendSignal( void ) // SendSignal = sem_post = increment semaphore value (after forcing the semaphore into cooperative mode, hence incrementing regardless of whether this instance previously decremented it)
{
	SetCooperative( true );
	Release();
}

int
NamedSemaphore::Remove( void )
{
	Close();
#ifdef    _WIN32
	return 0; // not necessary - a semaphore object is automatically unlinked when its last handle is closed 
#else  // _WIN32
	return ( ::sem_unlink( mName.c_str() ) ? errno : 0 );
#endif // _WIN32
}

int
NamedSemaphore::Remove( std::string name )
{
#ifdef    _WIN32
	return 0; // not necessary---a semaphore object is automatically unlinked when its last handle is closed 
#else  // _WIN32
	name = LegalizeName( name );
	return ( ::sem_unlink( name.c_str() ) ? errno : 0 );
#endif // _WIN32
}

NamedSemaphore::ScopedLock::ScopedLock( NamedSemaphore & base, double timeoutSeconds ) : mBase( &base ) { if( mBase ) mBase->Acquire( timeoutSeconds ); }
NamedSemaphore::ScopedLock::ScopedLock( NamedSemaphore * base, double timeoutSeconds ) : mBase(  base ) { if( mBase ) mBase->Acquire( timeoutSeconds ); }
NamedSemaphore::ScopedLock::~ScopedLock() { if( mBase ) mBase->Release(); }



#include "CommandLine.hpp"
#include "ConsoleUtils.hpp"
#include "TimeUtils.hpp"
#include <stdio.h>
#include <string.h>
#include <iostream>
int NamedSemaphore::Demo( int argc, const char * argv[] )
{
/*
	arguments: [ NAME [TIMEOUT_SECONDS]]
	
	Create or open, then acquire, a NamedSemaphore::ScopedLock
	with name NAME (default "lock"). Once acquired, hold the
	lock until the user presses return to release it. Print a
	timestamp at process launch and at acquisition and release.
	
	Launch multiple instances of this process from multiple
	consoles, to observe the interaction of processes competing
	for the same lock.
	
	TIMEOUT_SECONDS defaults to -1, which means never time out
	while waiting for acquisition.
*/
	START_ARGS;
	if( argc > 3 ) { fprintf( stderr, "usage: %s [ NAME [ TIMEOUT_SECONDS ]]\n", argv[ 0 ] ); return -1; }
	NO_MORE_OPTS( "--" );
	std::string name = "lock"; GET_ARG( name, "string NAME" );
	double timeoutSeconds = -1.0;  GET_ARG( timeoutSeconds, "floating-point number TIMEOUT_SECONDS" );
	NO_MORE_ARGS;

	NamedSemaphore s( name );
	ConsoleUtils::CatchFirstInterrupt();
	printf( "%16.6f   Process launched\n", TimeUtils::PrecisionTime( false ) );
	
	{
		NamedSemaphore::ScopedLock lk( s, timeoutSeconds );
		printf( "%16.6f   NamedSemaphore \"%s\" acquired\n", TimeUtils::PrecisionTime( false ), s.GetName().c_str() );
		ConsoleUtils::GetUserInput( "Press return to release: " );
	}
	
	printf( "%16.6f   NamedSemaphore \"%s\" released\n", TimeUtils::PrecisionTime( false ), s.GetName().c_str() );
	return 0;
}

int NamedSemaphore::SignalingDemo( int argc, const char * argv[] )
{
/*
	arguments: ( wait | send ) [ NAME ]
	
	TODO
*/
	if( argc > 3 || argc < 2 ) { fprintf( stderr, "usage: %s ( wait | send ) [ NAME ]\n", argv[ 0 ] ); return -1; }
	bool wait;
	if(      strcmp( argv[ 1 ], "wait" ) == 0 ) wait = true;
	else if( strcmp( argv[ 1 ], "send" ) == 0 ) wait = false;
	else { fprintf( stderr, "usage: %s ( wait | send ) [ NAME ]\n", argv[ 0 ] ); return -1; }
	
	const char defaultName[] = "lock";
	NamedSemaphore s( argc > 2 ? argv[ 2 ] : defaultName );
	ConsoleUtils::CatchFirstInterrupt();
	if( wait )
	{
		printf( "%16.6f   Waiting for signal\n", TimeUtils::PrecisionTime( false ) );
		s.WaitForSignal();
		printf( "%16.6f   Received signal\n", TimeUtils::PrecisionTime( false ) );
	}
	else
	{
		printf( "%16.6f   Sending signal\n", TimeUtils::PrecisionTime( false ) );
		s.SendSignal();
	}
	return 0;
}

#endif // ifndef INCLUDE_NamedSemaphore_CPP
