/*
# $BEGIN_DATAFLOW_LICENSE$
# 
# This file is part of the DataFlow project, a software library for
# efficient asynchronous multi-stream inter-process communication.
# 
# Copyright (c) 2014-2021 Jeremy Hill
# 
# DataFlow is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program. If not, see http://www.gnu.org/licenses/ .
# 
# Some third-party source code is compiled and linked into the
# DataFlow binary, as follows: devel/src/sem_timedwait_apple.cpp
# is adapted from sem_timedwait.cpp by Keith Shortridge, copyright
# (c) 2013 Australian Astronomical Observatory (AAO), released under
# the MIT License; devel/src/dirent_win.h is copyright (c) 2006
# Toni Ronkko, and released under the MIT License. The DataFlow build
# system also uses some third-party code:
# devel/build/cmake-recipes/DefineFunction_TARGET_ARCHITECTURE.cmake
# which is copyright (c) 2012 Petroules Corporation, released under
# the 2-Clause BSD License. For more details, see the respective
# copyright, licensing and disclaimer information in each of these
# files.
# 
# $END_DATAFLOW_LICENSE$
*/
#ifndef INCLUDE_NetworkUtils_HPP
#define INCLUDE_NetworkUtils_HPP

#include <string>
#include <map>

#if ( defined( WIN32 ) || defined( _WIN32 ) || defined( __WIN32__ ) )
#	include <winsock2.h>
#	include <ws2ipdef.h>
#else
#	include <arpa/inet.h>  // for sockaddr_in
	typedef int SOCKET;
#endif
///////////////////////////////////////////////////////////////
namespace NetworkUtils
{
	class Common
	{
		public:
			Common();
		
			void SetVerbosity( int verbosity );
			bool SetProtocol ( std::string s, bool tolerant=false );
			void SetIPVersion( int version );
			void SetIPVersion( const std::string & version );
			void SetPort( long first, long last=0 );
			void SetPort( const std::string & range );
			void Configure( const std::string & config );
			void AllowDualStack( bool allow=true );
			
			void GetParameters( SOCKET * socket, int * socketType, unsigned short * addressFamily, int * verbosity ) const;
			
			int GetIPVersion( void ) const;
			std::string GetProtocolName( void ) const;
			unsigned short GetPort( void ) const;
			std::string GetDesiredAddress( void ) const;
		
			int            mVerbosity;
		
		protected:
			SOCKET         mSocket;
			int            mSocketType;
			unsigned short mAddressFamily;
			unsigned short mFirstPort;
			unsigned short mLastPort;
			unsigned short mChosenPort;
			bool           mIsServer;
			std::string    mDesiredAddress;
		
			void CheckConfigurationAllowed( const char * attribute );
	};
	
	class Server;
	class Connection;
	
	class Connection : public Common
	{
		public:
			static const size_t   DEFAULT_CHUNK_SIZE = 512;
			static const int      WAIT_FOREVER = -1;
		
			char *       mBuffer;
			long         mBytesReceived;
			long         mTotalBytesReceived;
			long         mBytesSent;
		
			Connection( size_t chunkSize=DEFAULT_CHUNK_SIZE );
			Connection( const std::string & remoteServerIP, unsigned short remoteServerPort=0, const char * protocol=NULL, int verbosity=0, size_t chunkSize=DEFAULT_CHUNK_SIZE );
			Connection( const Server & localServer, double timeoutSeconds=WAIT_FOREVER, size_t chunkSize=DEFAULT_CHUNK_SIZE );
			~Connection();
		
			Connection & SetChunkSize( size_t chunkSize );
			Connection & SetVerbosity( int verbosity );
			Connection & SetProtocol ( std::string s );
			Connection & SetIPVersion( int version );
			Connection & SetIPVersion( const std::string & version );
			Connection & SetPort( long first, long last=0 );
			Connection & SetPort( const std::string & range );
			Connection & Configure( const std::string & config );
			
			unsigned int   ConnectToRemoteServer( std::string target, unsigned short portNumber=0, const char * protocol=NULL, unsigned int multicastSendingIndex=0 );
			long           Accept( const Server & localServer, double timeoutSeconds=WAIT_FOREVER );
			
			bool           Opened( void );

			const char *   RemoteAddress( void );
			long           Receive( void );
			bool           HasData( void ) const;
			std::string    Data( bool trimTrailingNewline=false );
			long           Send( const char * message );
			long           Send( const std::string & message );
			long           Send( const void *buffer, size_t numberOfBytes );
			void           Close( void );

			void           EchoTest( const std::string & message ); // expect the same number of bytes as you sent
			std::string    ExchangeOneliners( std::string message ); // more practical use - behaves like netcat: ensure the message terminates with \n before sending; then Receive() until the last character of the reply is \n
			
		private:
			struct ::sockaddr_in  mRemoteAddress4;
			struct ::sockaddr_in6 mRemoteAddress6;
			std::string           mRemoteAddressString;
			std::string           mDirection;
			bool                  mBorrowedSocket;
			size_t                mChunkSize;
		
	};
	class Server : public Common
	{
		public:
			Server();
			~Server();
		
			Server & SetMaxBacklog( int maxPendingConnections );
			Server & SetVerbosity( int verbosity );
			Server & SetProtocol ( std::string s );
			Server & SetIPVersion( int version );
			Server & SetIPVersion( const std::string & version );
			Server & SetPort( long first, long last=0 );
			Server & SetPort( const std::string & range );
			Server & Configure( const std::string & config );
		
			void EnableMulticast( const std::string & multicastAddress );
			
			unsigned short Listen( const std::string & config="" );
			Connection     NextConnection( double timeoutSeconds=Connection::WAIT_FOREVER, size_t chunkSize=Connection::DEFAULT_CHUNK_SIZE );
			void           StopServing( void );

			void           Echo( void );
		
		protected:
			struct ::sockaddr_in  mAddress4;
			struct ::sockaddr_in6 mAddress6;
			unsigned short        mMaxPendingConnections;
			bool                  mJoinedAnyMulticastGroup;
		
			void TryPort( unsigned short portNumber );
		
	};
	class DiscoveryServer : public Server
	{
		public:
			DiscoveryServer(
				const std::string & multicastTarget, // [multicastAddress]:portNumber
				const std::string & configToAdvertise, // any valid all-in-one server config string
				const std::string & serviceName="",
				int verbosity=1
			);
			DiscoveryServer(
				const std::string & multicastTarget, // [multicastAddress]:portNumber
				unsigned short portToAdvertise,
				const std::string & protocolToAdvertise="TCP",
				const std::string & serviceName="",
				int verbosity=1
			);
			void Advertise( const std::string & serverConfig, const std::string & serviceName="" );
			void Advertise( unsigned short port, const std::string & protocol="TCP", const std::string & serviceName="" );
			void Advertise( const Server & server, const std::string & serviceName="" );
			void HandleRequest( double timeoutSeconds=Connection::WAIT_FOREVER );
			void Run( void );
		private:
			std::map< std::string, std::string > mPayload;
			void Initialize( const std::string & multicastTarget, int verbosity );
	};
	std::string Discover( const std::string & multicastTarget, unsigned short firstPortForReply, unsigned short lastPortForReply=0, const std::string & serviceName="", double timeoutSeconds=2.0, int verbosity=0  );
	std::string Discover( const std::string & multicastTarget, const std::string & portRangeForReply, const std::string & serviceName="", double timeoutSeconds=2.0, int verbosity=0  );
	
	double TimeSyncClient( Connection & client, unsigned long nRounds=500 );
	void   TimeSyncServer( Server & server, bool oneShot=false );
	double ExchangeTimestamps( Connection & remoteClient );
	int    Demo( int argc, const char * argv[] );
} // end namespace NetworkUtils

#endif // ifndef INCLUDE_NetworkUtils_HPP
