/*
# $BEGIN_DATAFLOW_LICENSE$
# 
# This file is part of the DataFlow project, a software library for
# efficient asynchronous multi-stream inter-process communication.
# 
# Copyright (c) 2014-2021 Jeremy Hill
# 
# DataFlow is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program. If not, see http://www.gnu.org/licenses/ .
# 
# Some third-party source code is compiled and linked into the
# DataFlow binary, as follows: devel/src/sem_timedwait_apple.cpp
# is adapted from sem_timedwait.cpp by Keith Shortridge, copyright
# (c) 2013 Australian Astronomical Observatory (AAO), released under
# the MIT License; devel/src/dirent_win.h is copyright (c) 2006
# Toni Ronkko, and released under the MIT License. The DataFlow build
# system also uses some third-party code:
# devel/build/cmake-recipes/DefineFunction_TARGET_ARCHITECTURE.cmake
# which is copyright (c) 2012 Petroules Corporation, released under
# the 2-Clause BSD License. For more details, see the respective
# copyright, licensing and disclaimer information in each of these
# files.
# 
# $END_DATAFLOW_LICENSE$
*/
#ifndef   INCLUDE_FileUtils_CPP
#define   INCLUDE_FileUtils_CPP

#include "FileUtils.hpp"
#include "ExceptionUtils.hpp"
#include "DebuggingUtils.hpp" // used in Demo()

#include <string.h>
#include <fstream>
#include <errno.h>

#ifdef _WIN32
#	include "windows.h"
#	ifdef    _MSC_VER
#		include <direct.h>
#		include "dirent_win.h"
#	else  // _MSC_VER
#		include <dir.h>
#		include <dirent.h>
#	endif // _MSC_VER
#	define getcwd _getcwd // avoid stupid "posix name deprecated" warning https://docs.microsoft.com/en-us/cpp/error-messages/compiler-warnings/compiler-warning-level-3-c4996?view=vs-2019#posix-function-names
#	define mkdir _mkdir   // avoid stupid "posix name deprecated" warning https://docs.microsoft.com/en-us/cpp/error-messages/compiler-warnings/compiler-warning-level-3-c4996?view=vs-2019#posix-function-names
#	define chdir _chdir   // avoid stupid "posix name deprecated" warning https://docs.microsoft.com/en-us/cpp/error-messages/compiler-warnings/compiler-warning-level-3-c4996?view=vs-2019#posix-function-names
#else  // _WIN32
#	include <sys/stat.h>
#	include <dirent.h>
#	include <dlfcn.h>
#	include <unistd.h>
#endif // _WIN32
#ifdef __APPLE__
#	include <libproc.h>
#endif // __APPLE__
bool
FileUtils::DirectoryExists( std::string x )
{
  DIR * p = ::opendir( x.c_str() );
  bool exists = ( p != NULL );
  if( p ) ::closedir( p );
  return exists;
}

bool
FileUtils::FileExists( std::string x )
{
	std::ifstream ifile( x.c_str() );
	return ifile.good();
}

std::string
FileUtils::GetWorkingDirectory( void )
{
	const int bufSize = 1024;
	char buf[bufSize];
	const char *cwd = ::getcwd( buf, bufSize );
	std::string result;
	if( cwd ) result = cwd;
	return StandardizePath( result );
}

std::string
FileUtils::StandardizePath( std::string input ) // standardize slash direction, remove trailing slash (except in Windows drive roots), remove redundant slashes and single-dots,  resolve double-dots
{
	std::string output;
	bool dealWithDoubleDots = false, ignore = false;
	unsigned int start = 0;
	char lastCharAdded = '\0';
#define IS_SLASH( C ) ( C == '\\' || C == '/' )
#ifdef    _WIN32
#	define WINDOWS true
#   define AT_MINIMUM_LENGTH( S, LEN )  ( LEN==0 || ( LEN==1 && S[0]==FILE_SEPARATOR ) || ( LEN==2 && S[0]==FILE_SEPARATOR && S[1]==FILE_SEPARATOR ) || ( LEN == 3 && S[1]==':' && S[2]==FILE_SEPARATOR ) ) // will operate on output (slashes already standardized)
#else // _WIN32
#	define WINDOWS false
#   define AT_MINIMUM_LENGTH( S, LEN )  ( LEN==0 || ( LEN==1 && S[0]==FILE_SEPARATOR ) ) // will operate on output (slashes already standardized)
#endif // _WIN32
	if( WINDOWS && input.length() > 4 && ::strncmp( input.c_str(), "\\\\?\\", 4 ) == 0 ) start = 4; // skip the "long path" prefix if it's there
	for( unsigned int positionInInput = start; positionInInput < input.size(); positionInInput++)
	{
		char c = input[ positionInInput ];
		
		if( c == '.' && lastCharAdded == FILE_SEPARATOR ) // deal with single-dots that follow a slash
		{
			char lookAhead1 = input[ positionInInput + 1 ];
			if( IS_SLASH( lookAhead1 ) || !lookAhead1 ) { positionInInput++; continue; }
			if( lookAhead1 == '.' )
			{
				char lookAhead2 = input[ positionInInput + 2 ];
				if( IS_SLASH( lookAhead2 ) || !lookAhead2 ) dealWithDoubleDots = true;
			}
		}
		if( IS_SLASH( c ) ) // Like Python-on-Windows, accept either backward or forward slashes as file separators (but standardize them to the current platform).
		{                   // Do not double them, unless we're on Windows and it's right at the beginning of the path.
			if( positionInInput && positionInInput + 1 == input.size() ) break;
			if( !ignore ) output += ( lastCharAdded = FILE_SEPARATOR );
			ignore = WINDOWS ? ( output.size() > 1 ) : true;
		}
		else
		{
			output += ( lastCharAdded = c );
			ignore = false;
		}
	}
	while( output[ output.length() - 1 ] == FILE_SEPARATOR && !AT_MINIMUM_LENGTH( output, output.length() ) ) output = output.substr( 0, output.length() - 1 ); // yeah I know pop_back() exists but why introduce a C++ 11 dependency into a file when you don't need to
	if( WINDOWS && output.length() == 2 && output[ 1 ] == ':' ) output += FILE_SEPARATOR; //  at this point it should be C:\ rather than just C: 
	
	if( dealWithDoubleDots ) //  A double-dot right at the start simply disappears if the path is absolute, but remains if relative
	{                        //  A double-dot remains if the thing it would delete is a double-dot that has already been determined to be a remainer.
		input = output;
		std::string  absolutePrefix;
		if( WINDOWS && input.length() >= 2 && input[ 0 ] == FILE_SEPARATOR && input[ 1 ] == FILE_SEPARATOR ) absolutePrefix = input.substr( 0, 2 );
		else if( WINDOWS && input.length() >= 3 && input[ 1 ] == ':' && input[ 2 ] == FILE_SEPARATOR ) absolutePrefix = input.substr( 0, 3 );
		else if( input.length() >= 1 && input[ 0 ] == FILE_SEPARATOR ) absolutePrefix = input.substr( 0, 1 );
		bool isAbsolute = absolutePrefix.length() > 0;
		std::vector< std::string > pieces;
		std::string piece;
		for( size_t positionInInput = absolutePrefix.length(); positionInInput <= input.size(); positionInInput++ )
		{
			char c = input[ positionInInput ];
			if( positionInInput == input.size() || c == FILE_SEPARATOR )
			{
				bool stay = true, kill = false;
				if( piece == ".." )
				{
					if( !pieces.size() ) { stay = !isAbsolute; kill = false; }
					else if( pieces[ pieces.size() - 1 ] == ".." ) { stay = true; kill = false; }
					else { stay = false; kill = true; }
				}
				if( kill ) pieces.pop_back();
				if( stay ) pieces.push_back( piece );
				piece = "";
			}
			else piece += c;
		}
		output = "";
		for( std::vector< std::string >::iterator it = pieces.begin(); it != pieces.end(); it++ )
		{
			if( output.length() ) output += FILE_SEPARATOR;
			output += *it;
		}
		output = absolutePrefix + output;
	}
	
	return output;
}

std::string
FileUtils::JoinPath( std::string parent, std::string child )
{
	if( parent.size() == 0 ) return child;
	return StandardizePath( parent + FILE_SEPARATOR + child );
}

void
FileUtils::SplitPath( std::string fullpath, std::string & parent, std::string & stem, std::string & extension )
{
	fullpath = StandardizePath( fullpath );
	parent = ""; stem = ""; extension = "";
#ifdef    _WIN32
	if( fullpath.length() == 3 && fullpath[ 1 ] == ':' ) return;
#else  // _WIN32
#endif // _WIN32	
	size_t parentLength = fullpath.size();
	for( parentLength = fullpath.size(); parentLength > 0; parentLength-- )
		if( fullpath[parentLength-1] == FILE_SEPARATOR ) break;
	size_t dotPos = fullpath.size();
	for( size_t i = parentLength; i < fullpath.size(); i++ )
		if( fullpath[ i ] == '.' ) dotPos = i;

	if( parentLength ) parent = StandardizePath( fullpath.substr( 0, parentLength ) );
	if( parentLength < fullpath.size() ) stem = fullpath.substr( parentLength, dotPos - parentLength );
	if( dotPos < fullpath.size() ) extension = fullpath.substr( dotPos );
	if( stem == "" && extension == ".." ) { stem = ".."; extension = ""; }
}

std::string
FileUtils::ParentDirectory( std::string x, int levels )
{
	std::string stem, extension;
	x = RealPath( x );
	for( int i = 0; i < levels && x.length(); i++ ) SplitPath( x, x, stem, extension );
	return x;
}

std::string
FileUtils::BaseName( std::string x, bool withExtension )
{
	std::string parent, stem, extension;
	SplitPath( RealPath( x ), parent, stem, extension );
	return withExtension ? ( stem + extension ) : stem;
}

std::string
FileUtils::RealPath( std::string x ) // standardize, absolutify, and follow links --- NB: not thread safe, because it uses chdir() to (temporarily) change a global state
{
	if( x.size() == 0 ) return x;
	x = StandardizePath( x );
	if( !DirectoryExists( x ) )
	{
		std::string parent, stem, xtn;
		SplitPath( x, parent, stem, xtn );
		if( !parent.length() )
		{
			if( x[ 0 ] == FILE_SEPARATOR ) return x;
			parent = ".";
		}
		parent = RealPath( parent );
		return JoinPath( parent, stem + xtn );
	}
	std::string oldd = GetWorkingDirectory();
	int err = ::chdir( x.c_str() );
	if( err == 0 ) x = GetWorkingDirectory();
	err = chdir( oldd.c_str() );
	return x;
}

bool
FileUtils::PathMatch( std::string a, std::string b, bool partial )
{
	a = RealPath( a );
	b = RealPath( b );
	if( partial && a.size() > b.size() ) a = a.substr( 0, b.size() );
	return a == b;
}

int
FileUtils::MakeDirectory( std::string x )
{
	int err;
#ifdef    _WIN32
	err = ::mkdir( x.c_str() );
#else  // _WIN32
	const int rwxr_xr_x = 0755;
	err = ::mkdir( x.c_str(), rwxr_xr_x );
#endif // _WIN32
	if( err ) { RAISE( "failed to create directory " << x ); }
	return err;
}

int
FileUtils::MakePath( std::string x )
{
	if( DirectoryExists( x ) ) return 0;
	std::string parent, name, extn;
	SplitPath( x, parent, name, extn);
	if( parent.size() > 0 && !DirectoryExists( parent ) )
	{
		if( MakePath( parent ) != 0 ) { RAISE( "failed to make directory " << parent ); }
	}
	return MakeDirectory( x );
}

int
FileUtils::RemoveFile( std::string x )
{
#ifdef    _WIN32
	return ( ::DeleteFileA( x.c_str() ) ? 0 : ::GetLastError() );
#else  // _WIN32
	return ( ::unlink( x.c_str() ) ? errno : 0 );
#endif // _WIN32
}

std::string
FileUtils::LibraryPath( const std::string & relativePath, void * funcPtr )
{
// You could *almost* use `LibraryPath(main)` to locate the executable---despite this being off-label, it is the apparent de-facto behaviour of `dladdr().
// But it doesn't quite work on Linux: Windows would work just the same (it's GetModuleFileNameA either way), and macOS does always return an absolutified path,
// but Linux does not---so if the exectuable is called with just its name, `dli_fname` contains just the name, and if `$PATH` (or `$LD_LIBRARY_PATH`) contains a
// relative path, only the same relative path is returned---that's dangerous if your application has changed working directory meanwhile. So, to locate the
// executable, there has to be a different approach---see `ExecutablePath()` below.
	std::string location;
	if( !funcPtr ) funcPtr = ( void * )&LibraryPath;
#ifdef    _WIN32
	char path[ 4097 ] = "";
	HMODULE moduleHandle = NULL;
	// NB if not defined, GET_MODULE_HANDLE_EX_FLAG_FROM_ADDRESS is 4 and GET_MODULE_HANDLE_EX_FLAG_UNCHANGED_REFCOUNT is 2
	if( GetModuleHandleExA( GET_MODULE_HANDLE_EX_FLAG_FROM_ADDRESS | GET_MODULE_HANDLE_EX_FLAG_UNCHANGED_REFCOUNT, ( LPCSTR ) funcPtr, &moduleHandle ) )
	{
		GetModuleFileNameA( moduleHandle, path, sizeof( path ) );
		location = path;
	} // else GetLastError() can get you the error code
#else  // _WIN32
	Dl_info info;
	if( dladdr( funcPtr, &info ) ) location = info.dli_fname;
#endif // _WIN32
	location = JoinPath( location, relativePath ); // even if relativePath is empty, this standardizes the result
	return location;
}

std::string
FileUtils::ExecutablePath( const std::string & relativePath )
// You could *almost* have used `LibraryPath(main)` for this---but not quite (see above)
{
	std::string location;
#	if defined    _WIN32
		char path[ 4097 ] = "";
		GetModuleFileNameA( NULL, path, sizeof( path ) );
#	elif defined __APPLE__
		char path[ PROC_PIDPATHINFO_MAXSIZE ] = "";
		if( proc_pidpath( getpid(), path, sizeof( path ) ) <=  0 ) return "";
#	elif defined __linux__
		char path[ PATH_MAX ] = "";
		int nChars = readlink( "/proc/self/exe", path, sizeof( path ) - 1 );
		if( nChars < 0 ) return ""; else path[ nChars ] = 0;
#	else
		char path[ 1 ] = "";
#	endif
	if( *path ) location = JoinPath( path, relativePath ); // even if relativePath is empty, this standardizes the result
	//else location remains blank
	return location;
}

int
FileUtils::Demo( int argc, const char * argv[] )
{
	std::string parent, stem, extension;
	std::string x = "";
	DEBUG( 0, "" );
	DBREPORTQ( 0, LibraryPath() );
	DBREPORTQ( 0, LibraryPath("..") );
	DEBUG( 0, "" );
	DBREPORTQ( 0, ExecutablePath() );
	DBREPORTQ( 0, ExecutablePath("..") );
	if( argc < 2 ) DEBUG( 0, "supply one or more paths" );
	for( int i = 1; i < argc; i++ )
	{
		DEBUG( 0, "" );
		x = argv[ i ];
		DBREPORTQ( 0, x);
		DBREPORTQ( 0, StandardizePath(x) );
		DBREPORTQ( 0, RealPath(x) );
		DBREPORTQ( 0, parent );
		DBREPORTQ( 0, stem );
		DBREPORTQ( 0, extension );
	}
	return 0;
}


#endif // INCLUDE_FileUtils_CPP
