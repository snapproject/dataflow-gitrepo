/*
# $BEGIN_DATAFLOW_LICENSE$
# 
# This file is part of the DataFlow project, a software library for
# efficient asynchronous multi-stream inter-process communication.
# 
# Copyright (c) 2014-2021 Jeremy Hill
# 
# DataFlow is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program. If not, see http://www.gnu.org/licenses/ .
# 
# Some third-party source code is compiled and linked into the
# DataFlow binary, as follows: devel/src/sem_timedwait_apple.cpp
# is adapted from sem_timedwait.cpp by Keith Shortridge, copyright
# (c) 2013 Australian Astronomical Observatory (AAO), released under
# the MIT License; devel/src/dirent_win.h is copyright (c) 2006
# Toni Ronkko, and released under the MIT License. The DataFlow build
# system also uses some third-party code:
# devel/build/cmake-recipes/DefineFunction_TARGET_ARCHITECTURE.cmake
# which is copyright (c) 2012 Petroules Corporation, released under
# the 2-Clause BSD License. For more details, see the respective
# copyright, licensing and disclaimer information in each of these
# files.
# 
# $END_DATAFLOW_LICENSE$
*/
#ifndef INCLUDE_DataFlow_API_HPP
#define INCLUDE_DataFlow_API_HPP
////////////////////////////////////////////////////////////////////

/*API_VAR   API_NAME          = DataFlow          */
/*API_VAR   API_SHORTNAME     = DF                */
/*API_VAR   API_VERSION       = 0.0.1             */
/*API_VAR   YEARS             = 2014-2021         */
/*API_VAR   COPYRIGHT_HOLDER  = Jeremy Hill       */

/*API_VAR   ASSERT_ENDIANNESS = 0                 */

/*API_VAR   HOMEPAGE    = https://dataflow.readthedocs.io                            */
/*API_VAR   REPOSITORY  = https://bitbucket.org/snapproject/dataflow-gitrepo         */
/*API_VAR   REPO_SHORT  = dataflow-gitrepo                                           */
/*API_VAR   TRACKER     = https://bitbucket.org/snapproject/dataflow-gitrepo/issues  */

/*API_WRAP*/
#include <stdint.h>

////////////////////////////////////////////////////////////////////

/*API_SKIP*/
#include "SharedDataArchive.hpp" // see ::SharedDataArchive below


/*API_COPY*/
#include <string>

namespace DF
{
	class Thing // this class is a minimal test of the way in which the exported interface handles object pointers as input and output arguments
	{
		public: void * mShadow; // NB: must be first member, must be public
		
		/*API_WRAP*/
		public:
			Thing( const char * name );
			~Thing();
			bool        IsCalled( const std::string & name ) const;
			bool        SameName( const DF::Thing * otherThing ) const;
			std::string GetName( void ) const;
			const DF::Thing * FirstNamesake( int numberOfOtherThings, const DF::Thing * otherThings[] ) const;
			const DF::Thing * GetMyAddress( void ) const; // for testing in Python
		
		/*API_SKIP*/
		public: // to preserve "standard layout", all members should have the same access protection (i.e. all public)
			std::string mName;
			
		/*API_COPY*/
	};

	/*API_WRAP*/
	
	const char * Error( void );                              // Definitions
	const char * ClearError( void );                         // for 
	const char * GetVersion( void );                         // these
	const char * GetRevision( void );                        // all
	const char * GetBuildDatestamp( void );                  // get
	void         SanityCheck( void );                        // auto-generated
	
	void         License( void );

	void         SetDebugLevel( int level );
	int          RemoveKernelResource( const std::string & name );

	double       PrecisionTime( bool relativeToFirstCall=true ); // uses steady_clock.  On first call, steady_clock AND system_clock "origin" values will be recorded globally.  If relativeToFirstCall is true, elapsed steady_clock time is returned relative to this origin.
	void         SleepSeconds( double seconds );
	double       SleepUntil( double targetPrecisionTime, bool relativeToFirstCall=true ); // sleep until after a particular specified PrecisionTime() value (return ASAP after the target, but accuracy is not guaranteed)

	void         WaitForSignal( const char * name, double timeoutSeconds=-1.0 );
	void         SendSignal( const char * name );
    
	void         CatchFirstInterrupt( void );
	void         CatchFirstTermination( void );
	void         RaiseCaughtSignal( void );
	int          CaughtSignal( int setValue=-1 );
	
	std::string  LibraryPath(    const std::string & relativePath="", void * funcPtr=NULL );
	std::string  ExecutablePath( const std::string & relativePath="" );

	/*API_COPY*/
		
	
/*API_COPY*/	class SharedDataArchive
/*API_COPY*/	{
/*API_COPY*/		public: void * mShadow; // NB: must be first member, must be public
/*API_COPY*/		
/*API_COPY*/		public:
/*API_WRAP*/			 SharedDataArchive( const std::string & name, uint64_t segmentSize=0, bool forceReformat=false, uint64_t numberOfHashTableSlots=8191 );
/*API_WRAP*/			~SharedDataArchive();
/*API_WRAP*/			void        SynchronizeTimer( void );
/*API_WRAP*/			std::string Report( void );
/*API_WRAP*/			std::string Summary( void );
/*API_WRAP*/			uint64_t    GetClientID( void );
/*API_WRAP*/			std::string GetName( void );
/*API_WRAP*/			void        SetSortingPreference( bool setting );
/*API_WRAP*/			uint64_t    GetSpaceAvailable( bool includeVacatedLots=true );
/*API_COPY*/		
/*API_COPY*/			class File
/*API_COPY*/			{
/*API_COPY*/				public: void * mShadow; // NB: must be first member, must be public
/*API_COPY*/				
/*API_COPY*/				public:
/*API_SKIP*/					// Unfortunately we can't do the same trick that SharedDataArchive.hpp does, of having a protected constructor only accessible via the parent class's Open() method
/*API_SKIP*/					// ---for Python wrapping it has to be a pointer, and since this would get created inside another object's method rather than in the Python-wrapped constructor,
/*API_SKIP*/					// Python would never know that it had the responsibility of deleting the object again.
/*API_WRAP*/					File( DF::SharedDataArchive * sda, const char * name, uint64_t offset, char mode, uint64_t ensureMinimumPayloadSize=0, char type='\0' );
/*API_WRAP*/					~File();
/*API_WRAP*/					std::string                   Report( void );
/*API_WRAP*/					uint64_t                      Tell( void );
/*API_WRAP*/					uint64_t                      TellMin( void );
/*API_WRAP*/					uint64_t                      TellMax( void );
/*API_WRAP*/					uint64_t                      AvailableContentLength( void );
/*API_WRAP*/					uint64_t                      PayloadCapacity( void );
/*API_WRAP*/					uint64_t                      PayloadCapacityUsed( void );
/*API_WRAP*/					uint64_t                      GetFileOffset( void );
/*API_WRAP*/					DF::SharedDataArchive::File * Seek( int64_t offset, int origin=0 );
/*API_WRAP*/					DF::SharedDataArchive::File * Write( const char * data, int64_t nBytes=-1 );
/*API_WRAP*/					DF::SharedDataArchive::File * Truncate( bool shrink=false );
/*API_WRAP*/					DF::SharedDataArchive::File * Shrink( void );
/*API_WRAP*/					DF::SharedDataArchive::File * Wrap( int64_t payloadCapacity, int64_t granularity=1 );
/*API_WRAP*/					DF::SharedDataArchive::File * AutoWrap( int64_t granularity=1 );
/*API_WRAP*/					DF::SharedDataArchive::File * Wipe( void );
/*API_WRAP*/					DF::SharedDataArchive::File * MakeReadOnly( void );
/*API_WRAP*/					void                          Remove( void );
/*API_WRAP*/					uint64_t                      Read( void * buffer, uint64_t nBytesToRead );
/*API_WRAP*/					DF::SharedDataArchive::File * Open( uint64_t offset );
/*API_WRAP*/					DF::SharedDataArchive::File * Walk( uint64_t top=0, int includeDirectories=-1, const char * restrictToTypes=NULL );
/*API_WRAP*/					bool                          Exists( void );
/*API_WRAP*/					uint64_t                      Next( void );
/*API_WRAP*/					const char *                  GetName( void );
/*API_SKIP*/				
/*API_SKIP*/				public: // to preserve "standard layout", all members should have the same access protection (i.e. all public)
/*API_SKIP*/					::SharedDataArchive::File mFile;
/*API_COPY*/			};
/*API_COPY*/		
/*API_SKIP*/		public: // to preserve "standard layout", all members should have the same access protection (i.e. all public)
/*API_SKIP*/			::SharedDataArchive mArchive;
/*API_COPY*/			
/*API_COPY*/	};	
}

////////////////////////////////////////////////////////////////////
#endif // ifndef INCLUDE_DataFlow_API_HPP
