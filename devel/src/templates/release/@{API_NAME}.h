/*
# $BEGIN_DATAFLOW_LICENSE$
# 
# This file is part of the DataFlow project, a software library for
# efficient asynchronous multi-stream inter-process communication.
# 
# Copyright (c) 2014-2021 Jeremy Hill
# 
# DataFlow is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program. If not, see http://www.gnu.org/licenses/ .
# 
# Some third-party source code is compiled and linked into the
# DataFlow binary, as follows: devel/src/sem_timedwait_apple.cpp
# is adapted from sem_timedwait.cpp by Keith Shortridge, copyright
# (c) 2013 Australian Astronomical Observatory (AAO), released under
# the MIT License; devel/src/dirent_win.h is copyright (c) 2006
# Toni Ronkko, and released under the MIT License. The DataFlow build
# system also uses some third-party code:
# devel/build/cmake-recipes/DefineFunction_TARGET_ARCHITECTURE.cmake
# which is copyright (c) 2012 Petroules Corporation, released under
# the 2-Clause BSD License. For more details, see the respective
# copyright, licensing and disclaimer information in each of these
# files.
# 
# $END_DATAFLOW_LICENSE$
*/


/*

	Include this file as a header for your C code. See README.txt for more details.

*/

#ifndef INCLUDED_@{API_NAME}_H
#define INCLUDED_@{API_NAME}_H

#ifndef    DYNAMIC
#	ifdef @{API_SHORTNAME}_STATIC
#		define DYNAMIC
#		define FUNCTION( F ) F
#	else
#		define DYNAMIC  extern
#		define FUNCTION( F ) (*F)
#	endif
#endif

#undef EXTERNC
#if defined __cplusplus && !defined SUPPRESS_EXTERNC
extern "C" {
#define EXTERNC
#endif /* #ifdef __cplusplus */

/* ************************************************************************************ */
/* ************************ Functions for loading the API ***************************** */

const char * Get_@{API_SHORTNAME}_Library_Name( const char *dllname );
const char * Get_@{API_SHORTNAME}_Loader_Error( void );
int          Load_@{API_SHORTNAME}_API( const char * dllname, int autoSearch );
/* `dllname = NULL` means use the default name, incorporating the `@{API_SHORTNAME}_PLATFORM`
   macro, if defined.
   `autoSearch = 1` means use a cross-platform-standardized order of places to look for the
   dynamic library when `dllname `is just a base name (without slashes). Specifically, look
   in the current working directory first, then look right next to the executable (only
   implemented for Windows, Darwin and Linux), and finally search the library path.
   `autoSearch = 0` means look for the dynamic library according to the OS's conventions,
   which are all different (Windows: executable-directory, path, working-directory;
   Darwin: working-directory, path; Linux: path only, so you'd better have set the
  `LD_LIBRARY_PATH` environment variable).
*/

/* ************************************************************************************ */
/* ******************************* The API itself ************************************* */

@{C_PREPROCESSOR_DIRECTIVES}

@{DECLARATIONS}

/* ************************************************************************************ */
/* ************************************************************************************ */

#ifdef EXTERNC
} /* ends extern "C" block */
#endif /* #ifdef EXTERNC */
#endif /* #ifndef INCLUDED_@{API_NAME}_H */
