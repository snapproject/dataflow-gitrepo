/*
# $BEGIN_DATAFLOW_LICENSE$
# 
# This file is part of the DataFlow project, a software library for
# efficient asynchronous multi-stream inter-process communication.
# 
# Copyright (c) 2014-2021 Jeremy Hill
# 
# DataFlow is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program. If not, see http://www.gnu.org/licenses/ .
# 
# Some third-party source code is compiled and linked into the
# DataFlow binary, as follows: devel/src/sem_timedwait_apple.cpp
# is adapted from sem_timedwait.cpp by Keith Shortridge, copyright
# (c) 2013 Australian Astronomical Observatory (AAO), released under
# the MIT License; devel/src/dirent_win.h is copyright (c) 2006
# Toni Ronkko, and released under the MIT License. The DataFlow build
# system also uses some third-party code:
# devel/build/cmake-recipes/DefineFunction_TARGET_ARCHITECTURE.cmake
# which is copyright (c) 2012 Petroules Corporation, released under
# the 2-Clause BSD License. For more details, see the respective
# copyright, licensing and disclaimer information in each of these
# files.
# 
# $END_DATAFLOW_LICENSE$
*/



/*

	Add this file to your C or C++ project.  Call Load_@{API_SHORTNAME}_API() and check that its return
	value is 0 before proceeding to call any @{API_SHORTNAME}_... functions. See README.txt for more details.

*/
#include "@API_NAME.h"

#if defined _WIN32
#  define DYLIB_PREFIX "lib"
#  define DYLIB_EXTENSION ".dll"
#elif defined __APPLE__ 
#  define DYLIB_PREFIX "lib"
#  define DYLIB_EXTENSION ".dylib"
#else
#  define DYLIB_PREFIX "lib"
#  define DYLIB_EXTENSION ".so"
#endif /* #if defined _WIN32 */

#define  STRINGIFY( s ) _STRINGIFY( s )
#define _STRINGIFY( s ) #s
#ifndef   @{API_SHORTNAME}_PLATFORM  /*   Define the @{API_SHORTNAME}_PLATFORM macro when compiling your app to make it easier   */ 
#  define @{API_SHORTNAME}_PLATFORM  /*   for the app to find the correct build of the dynamic library by default   */
#  define @{API_SHORTNAME}_DYLIB_NAME( name ) ( ( name ) ? ( name ) : ( DYLIB_PREFIX "@{API_SHORTNAME}" DYLIB_EXTENSION ) )
#else   /*   Example: if you used the -D@{API_SHORTNAME}_PLATFORM=-Darwin-x86_64 compiler flag then @{API_SHORTNAME}_DYLIB_NAME( NULL ) would expand to "lib@{API_SHORTNAME}-Darwin-x86_64.dylib"   */
#  define @{API_SHORTNAME}_DYLIB_NAME( name ) ( ( name ) ? ( name ) : ( DYLIB_PREFIX "@{API_SHORTNAME}" STRINGIFY( @{API_SHORTNAME}_PLATFORM ) DYLIB_EXTENSION ) )
#endif /* #ifndef @{API_SHORTNAME}_PLATFORM */

#include <string.h>
#include <stdio.h>

#define ERROR_BUFFER_SIZE 1024
char gLoaderError[ ERROR_BUFFER_SIZE ] = "";
//#define snprintf( BUFFER, BUFFER_SIZE, ... ) sprintf( BUFFER, __VA_ARGS__ ) // use this if snprintf is not defined

const char * Get_@{API_SHORTNAME}_Loader_Error( void ) { return gLoaderError; }

#ifdef @{API_SHORTNAME}_STATIC

	const char * Get_@{API_SHORTNAME}_Library_Name( const char *dllname ) { return "##_static_linking_only_##"; }
	int Load_@{API_SHORTNAME}_API( const char *dllname, int autoSearch ) { fprintf( stderr, "Load_@{API_SHORTNAME}_API() is being called redundantly because the @{API_NAME} library has been statically linked\n" ); return 0; }
	
#else /* #ifdef @{API_SHORTNAME}_STATIC */

#	ifdef _WIN32
#		include <windows.h>
#		include <direct.h>
#		define DynamicLibraryPtr                    HINSTANCE
#		define LOAD_LIBRARY( LIBNAME )              LoadLibrary( LIBNAME )
#		define GET_PROC_ADDRESS( DLPTR, FUNCNAME )  GetProcAddress( ( DLPTR ), ( FUNCNAME ) )
#		define getcwd                               _getcwd
#		define SLASH                                '\\'
#	else
#		include <dlfcn.h>
#		define DynamicLibraryPtr                    void *
#		define LOAD_LIBRARY( LIBNAME )              dlopen( LIBNAME, RTLD_NOW | RTLD_GLOBAL )
#		define GET_PROC_ADDRESS( DLPTR, FUNCNAME )  dlsym( ( DLPTR ), ( FUNCNAME ) )
#		define GET_DL_ERROR                         dlerror
#		define SLASH                                '/'
#	endif /* _WIN32 */

#	if defined _WIN32
#		define PATHBUFFERSIZE 4097
#		define GET_EXECUTABLE_PATH( P ) { int _nChars = GetModuleFileNameA( NULL,   P, PATHBUFFERSIZE - 2 ); P[ _nChars > 0 ? _nChars : 0 ] = '\0'; if( strncmp( P, "\\\\?\\", 4 ) == 0 ) memmove( P, P + 4, _nChars - 4 + 1 ); }
#	elif defined __APPLE__ 
#		include <unistd.h>
#		include <libproc.h>
#		define PATHBUFFERSIZE PROC_PIDPATHINFO_MAXSIZE
#		define GET_EXECUTABLE_PATH( P ) { int _nChars = proc_pidpath( getpid(),     P, PATHBUFFERSIZE - 2 ); P[ _nChars > 0 ? _nChars : 0 ] = '\0'; }
#	elif defined __linux__ 
#		include <unistd.h>
#		include <dirent.h>
#		define PATHBUFFERSIZE PATH_MAX
#		define GET_EXECUTABLE_PATH( P ) { int _nChars = readlink( "/proc/self/exe", P, PATHBUFFERSIZE - 2 ); P[ _nChars > 0 ? _nChars : 0 ] = '\0'; }
#	else
#		define PATHBUFFERSIZE 4097
#		define GET_EXECUTABLE_PATH( P ) strcpy( P, "" )
#	endif

#	undef EXTERNC
#	if defined __cplusplus && !defined SUPPRESS_EXTERNC
		extern "C" {
#		define EXTERNC
#	endif /* #if defined __cplusplus */

#	define DEFINE_DYNAMIC( TYPE, NAME, ARGS ) TYPE ( *NAME ) ARGS = ( TYPE ( * ) ARGS )@{API_SHORTNAME}_Stub
#	define   LOAD_DYNAMIC( TYPE, NAME, ARGS ) failures += ( attempts++, ( int )( ( NAME = ( TYPE ( * ) ARGS )GET_PROC_ADDRESS( dll , #NAME ) ) == 0 ) )

	const char * @{API_SHORTNAME}_Stub( void ) { static char msg[] = "@API_NAME API is not loaded"; return msg; }

	@DEFINITIONS

#	ifdef EXTERNC
		} /* ends extern "C" block */
#	endif /* #ifdef EXTERNC */

	const char * Get_@{API_SHORTNAME}_Library_Name( const char *dllname ) { return @{API_SHORTNAME}_DYLIB_NAME( dllname ); }
	
	int Load_@{API_SHORTNAME}_API( const char *dllname, int autoSearch )
	{
		int failures = 0, attempts = 0, searchIteration;
		*gLoaderError = 0;
		DynamicLibraryPtr dll = NULL;
		size_t prefixPathLength, argumentLength;
		char path[ PATHBUFFERSIZE ] = "";
		
		if( dllname && !*dllname ) dllname = NULL;
		argumentLength = strlen( @{API_SHORTNAME}_DYLIB_NAME( dllname ) );
		if( argumentLength >= PATHBUFFERSIZE ) argumentLength = PATHBUFFERSIZE - 1;
		path[ PATHBUFFERSIZE - 1] = '\0';
		
		if( autoSearch )
		{ /* cancel autoSearch if there's already a file-separator in the name */
			unsigned int i;
			strncpy( path, @{API_SHORTNAME}_DYLIB_NAME( dllname ), argumentLength ); path[ argumentLength ] = '\0';
			for( i = 0; path[ i ]; i++ ) if( path[ i ] == '/' || path[ i ] == '\\' ) { autoSearch = 0; break; }
		}
		for( searchIteration = 0; searchIteration < 3; searchIteration++ )
		{
			if( searchIteration == 0 ) { if( !autoSearch ) continue; if( !getcwd( path, PATHBUFFERSIZE - 2 ) ) continue; snprintf( path + strlen( path ), 2, "%c", SLASH ); } /* trailing slash prevents removal (below) of the last part of this result */
			if( searchIteration == 1 ) { if( !autoSearch ) continue; GET_EXECUTABLE_PATH( path ); if( !*path ) continue; }
			if( searchIteration == 2 ) { strcpy( path, "" ); }
			prefixPathLength = strlen( path );
			while( prefixPathLength > 0 && path[ prefixPathLength - 1 ] != SLASH ) prefixPathLength--; /* remove last part of path (executable basename) */
			if( prefixPathLength + argumentLength < PATHBUFFERSIZE )
			{
				strncpy( path + prefixPathLength, @{API_SHORTNAME}_DYLIB_NAME( dllname ), argumentLength );
				path[ prefixPathLength + argumentLength ] = '\0';
			}
			/*printf( "trying \"%s\"\n", path ); */
			dll = LOAD_LIBRARY( path );
			if( dll ) break;
		}
		if( !dll )
		{
			snprintf( gLoaderError, ERROR_BUFFER_SIZE, "Failed to load @{API_NAME} API: failed to find/open dynamic library file %.256s", @{API_SHORTNAME}_DYLIB_NAME( dllname ) );
#			ifdef GET_DL_ERROR
				snprintf( gLoaderError + strlen( gLoaderError ), ERROR_BUFFER_SIZE - strlen( gLoaderError ), "\n%.*s", ( int )( ERROR_BUFFER_SIZE - strlen( gLoaderError ) - 10 ), GET_DL_ERROR() );
#			endif
			return -1;
		}
		@LOADERS
		if( failures > 0 )
		{
			snprintf( gLoaderError, ERROR_BUFFER_SIZE, "Failed to load @{API_NAME} API: failed to load %d of %d functions from %.256s", failures, attempts, @{API_SHORTNAME}_DYLIB_NAME( dllname ) );
		}
		return failures;
	}
	
#	if defined __cplusplus

#	include "@API_NAME.hpp"

	@CPP_MACROS

	@CPP_DEFINITIONS

#	endif /* #if defined __cplusplus */

#endif /* #ifdef @{API_SHORTNAME}_STATIC */

