#!/usr/bin/env python
# $BEGIN_DATAFLOW_LICENSE$
# 
# This file is part of the DataFlow project, a software library for
# efficient asynchronous multi-stream inter-process communication.
# 
# Copyright (c) 2014-2021 Jeremy Hill
# 
# DataFlow is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program. If not, see http://www.gnu.org/licenses/ .
# 
# Some third-party source code is compiled and linked into the
# DataFlow binary, as follows: devel/src/sem_timedwait_apple.cpp
# is adapted from sem_timedwait.cpp by Keith Shortridge, copyright
# (c) 2013 Australian Astronomical Observatory (AAO), released under
# the MIT License; devel/src/dirent_win.h is copyright (c) 2006
# Toni Ronkko, and released under the MIT License. The DataFlow build
# system also uses some third-party code:
# devel/build/cmake-recipes/DefineFunction_TARGET_ARCHITECTURE.cmake
# which is copyright (c) 2012 Petroules Corporation, released under
# the 2-Clause BSD License. For more details, see the respective
# copyright, licensing and disclaimer information in each of these
# files.
# 
# $END_DATAFLOW_LICENSE$

"""
This file is a Python wrapper for the @{API_NAME} API version @{API_VERSION}.
It was generated automatically by @{AUTOGEN}
          at: @{BUILD_DATESTAMP}
    based on: @{API_REVISION}

@{DOCSTRING}

"""


__all__ = [
	
]

__meta__ = {
	'version'   : '@{API_VERSION}',
	'revision'  : '@{API_REVISION}',
	'datestamp' : '@{BUILD_DATESTAMP}',
}
__version__ = __meta__[ 'version' ]

import os as _os
import sys as _sys
import ctypes as _ctypes, ctypes.util as _
import struct as _struct
import inspect as _inspect
import weakref as _weakref
import platform as _platform
import functools as _functools

if _sys.version >= '3':
	_unicode, _bytes = str, bytes;  # bytes is already defined, unicode is not
	from builtins import FileNotFoundError
else:
	_unicode, _bytes = unicode, str # unicode is already defined, bytes is not
	class FileNotFoundError( IOError ): pass
	
def _IfStringThenRawString( x ):
	"""
	The strings we want to represent as bytes may be raw bytes already, or they may be
	unicode that we want to utf8-encode. A simple quoted string literal may or may not
	be raw bytes, depending on Python version---this is a problem.
	
	If `x` is a string then, regardless of Python version and starting format, return the
	"raw bytes" version of it so that we can send it over a serial port, pass it via
	ctypes to a C function, etc.
	
	If `x` is not a string, return it unchanged (so you can use this function to filter a
	whole list of arguments agnostically).
	
	See also `_IfStringThenNormalString()`
	"""
	if not isinstance( x, _unicode ): return x
	try:    return x.encode( 'latin1' ) # characters \x00 to \xff map to bytes \x00 to \xff, but anything beyond that causes an exception 
	except: return x.encode( 'utf-8' )
	
def _IfStringThenNormalString( x ):
	"""
	A string may be either raw bytes or unicode. Depending on Python version, either the
	raw bytes or the unicode might be treated as a "normal" string (i.e. the type you get
	from an ordinary quoted string literal, and the type can be `print()`-ed without
	adornment). This is a problem.
	
	If `x` is a string then, regardless of Python version and starting format, return the 
	"normal string" version of it so that we can print it, use it for formatting, make an
	Exception out of it, get on with our lives, etc.
	
	If `x` is not a string, return it unchanged (so you can feel free to use this function
	to filter a whole list of arguments agnostically).
	
	See also `_IfStringThenRawString()`
	"""
	if str is _bytes or not isinstance( x, _bytes ): return x
	try:    return x.decode( 'utf-8' )
	except: return x.decode( 'latin1' ) # bytes \x00 to \xff map to characters \x00 to \xff (so, cannot fail)

def _LoadSharedLibrary( dllNameStem, usePlatformInflection=True, searchIfNotFoundHere=True, simplifyArchName=True ):
	machine = _platform.machine().lower()
	bits = _struct.calcsize( 'P' ) * 8
	if simplifyArchName and machine in [ '', 'i386', 'x86_64', 'amd64' ]:
		arch = '%dbit' % bits # remember this will depend on the Python executable, not just the machine
	else:
		if machine in [ '', 'x86_64', 'amd64' ] and bits == 32: machine = 'i386' # this is a rough hack, but without it we would *not* be taking into account 32-bit executables running on 64-bit Windows
		arch = machine # NB: this can come out as 'amd64' when CMake says 'x86_64'. Simplification is better.
		if arch.startswith( 'armv' ): arch = arch.rstrip( 'l' )
	uname = _platform.system()
	windows = uname.lower().startswith( 'win' )
	macos   = uname.lower().startswith( 'darwin' )
	if   windows: dllPrefix, dllExtension = '',    '.dll'
	elif macos:   dllPrefix, dllExtension = 'lib', '.dylib'
	else:         dllPrefix, dllExtension = 'lib', '.so'
	platformInflection = '-' + uname + ( '-' if arch else '' ) + arch
	if isinstance( usePlatformInflection, str ) and usePlatformInflection.lower() == 'win64only':
		platformInflection = '64' if windows and bits == 64 else ''
	uninflectedStem = dllNameStem
	if usePlatformInflection: dllNameStem += platformInflection
	dllName = ( '' if dllNameStem.startswith( dllPrefix ) else dllPrefix ) + dllNameStem + dllExtension
	dllNameStem = dllNameStem[ len( dllPrefix ): ]
	try: file = __file__
	except NameError: file = None
	if not file: file = _inspect.getfile( _inspect.currentframe() )
	HERE = _os.path.dirname( _os.path.realpath( file ) )
	dllPath = _os.path.join( HERE, dllName )
	if not _os.path.isfile( dllPath ) and searchIfNotFoundHere:
		found = _ctypes.util.find_library( uninflectedStem )
		if not found and uninflectedStem.startswith( 'lib' ): found = _ctypes.util.find_library( uninflectedStem[ 3: ] )
		if found: dllPath = found
	try: dll = _ctypes.CDLL( dllPath )
	except:
		msg = "file exists, but may be corrupt" if _os.path.isfile( dllPath ) else "file is missing---perhaps it has not been compiled for your platform?"
		raise OSError( "%s\nfailed to load shared library %s\n%s" % ( _sys.exc_info()[ 1 ], _os.path.basename( dllPath ), msg ) )
	return dll


##############################################################################

_DLL = _LoadSharedLibrary( 'lib@{API_SHORTNAME}' )

class @{API_SHORTNAME}Exception( Exception ): pass

def _ErrorCheck( result, func, args ): # NB: assumes the existence of _DLL.@{API_SHORTNAME}_ClearError()
	err = _DLL.@{API_SHORTNAME}_ClearError()
	if err: raise( @{API_SHORTNAME}Exception( _IfStringThenNormalString( err ) ) )
	if func.returnsRawBytes: return result
	else: return _IfStringThenNormalString( result )
	
def _Pointerize( t, nStars ):
	for i in range( nStars ): t = _ctypes.POINTER( t )
	return t
	
def _TranslateType( t ):
	if t.startswith( 'const ' ): t = t[ 6: ]
	t = ( ' ' + t + ' ' ).replace( ' unsigned ', 'u' ).replace( ' ', '' )
	if t == 'void': return None
	nStars = 0
	while t.endswith( '*' ): t = t[ :-1 ]; nStars += 1
	if t == 'void' and nStars: return _Pointerize( _ctypes.c_void_p, nStars - 1 )
	if t == 'char' and nStars: return _Pointerize( _ctypes.c_char_p, nStars - 1 )
	
	result = getattr( _ctypes, 'c_' + t, None )
	if result: return _Pointerize( result, nStars )
	if t.endswith( '_t' ):
		result = getattr( _ctypes, 'c_' + t[ :-2 ], None )
		if result: return _Pointerize( result, nStars )
	if t in [ 'bool', 'bool_t' ]: return _Pointerize( _ctypes.c_int, nStars )  # ancient versions of ctypes that lack c_bool will fall through to here
	
	return _Pointerize( _ctypes.c_void_p, nStars )  # assume unrecognized words are pointers to custom class instances, to be represented as void*

class _TopLevel( object ):
	def __getattr__( self, name, default=None ): return globals().get( name, default )
	def __setattr__( self, name, value ): globals()[ name ] = value

def _GetClass( className, funcName='' ):
	if not className and '.' in funcName: className = '.'.join( funcName.split( '.' )[ :-1 ] )
	if not className: return None
	pieces = className.split( '.' )
	parent = _TopLevel()
	for i, piece in enumerate( pieces ):
		cls = getattr( parent, piece, None )
		if not cls: 
			class cls( object): _ptr = None
			cls.__name__ = piece
			setattr( parent, piece, cls )
		if i == 0 and piece not in __all__: __all__.append( piece )
		cls.__qualname__ = '.'.join( pieces[ : i + 1 ] )
		parent = cls
	return cls
	
class _ObjectPtr( object ):
	known = {}
	initialized = False
	def __new__( cls, ptr, destructor=None ): # when you try to construct two instances using same numeric pointer, you will get the same actual instance
		obj = cls.known.get( ptr, None )
		if obj is not None: obj = obj()
		if obj is None:
			obj = super( _ObjectPtr, cls ).__new__( cls )
			obj.__init__( ptr=ptr, destructor=destructor )
		cls.known[ ptr ] = _weakref.ref( obj )
		return obj
	def __init__( self, ptr, destructor=None ):
		if self.initialized: return
		if destructor == True: destructor = lambda x: _sys.stdout.write( 'destroying _ObjectPtr %d\n' % x )  # for debugging
		self.ptr = ptr
		self.destructor = destructor
		self.initialized = True
	def __del__( self ):
		self.known.pop( self.ptr, None )
		if self.destructor: self.destructor( self.ptr )
		
	def GetPointerValue( cls, x ):
		if hasattr( x, '_ptr' ) and isinstance( x._ptr, cls ): return x._ptr.ptr
		if isinstance( x, cls ): return x.ptr
		return x
	GetPointerValue = classmethod( GetPointerValue ) # cannot use decorator, because of autogen interpolation

def _ResolveArgs( funcptr, *args, **kwargs ):
	args = list( args )
	named = dict( funcptr.argDefaults )
	# if an argument is already given positionally, forget the default this time around:
	for i, name in zip( range( len( args ) ), funcptr.argNames ): named.pop( name, None )
	named.update( kwargs )
	# based on defaults and kwargs, fill in arguments that are not given positionally
	for name in funcptr.argNames[ len( args ): ]:
		if name in named:
			args.append( named.pop( name ) )
		else:
			missing = [ name for name in funcptr.argNames[ len( args ): ] if name not in funcptr.argDefaults and name not in kwargs ]
			raise TypeError( '%s() is missing required positional argument(s): %s' % ( funcptr.__name__, ', '.join( missing ) ) )
	if named: # if there are any unused by now, that's an error
		first = list( named.keys() )[ 0 ]
		raise TypeError( ( '%s() got multiple values for argument %r' if first in funcptr.argNames else '%s() got an unexpected keyword argument %r' ) % ( funcptr.__name__, first ) )
	return args
	
def _WrapFunction( funcptr, name=None, qualname=None, doc=None, outputClass=None ):
	def function( *args, **kwargs ):
		args = _ResolveArgs( funcptr, *args, **kwargs )
		args = [ _ObjectPtr.GetPointerValue( _IfStringThenRawString( arg ) ) for arg in args ]
		result = funcptr( *args )
		return outputClass( _ObjectPtr( result ) ) if outputClass else result
	function.__name__ = name
	function.__qualname__ = qualname
	function.__doc__ = doc
	return function

def _WrapMethod( funcptr, name=None, qualname=None, doc=None, outputClass=None ):
	isConstructor = ( name == '__init__' )
	isDestructor  = ( name == '__del__'  )
	def method( self, *args, **kwargs ):
		if isConstructor and len( args ) == 1 and isinstance( args[ 0 ], _ObjectPtr ): self._ptr = args[ 0 ]; return
		if isDestructor: self._ptr = None; return # funcptr is not called in the __del__ destructor---we've hijacked it, attached it as _cdel, and that will be used in the _ObjectPtr destructor, which is passed...
		args = _ResolveArgs( funcptr, *args, **kwargs )
		args = [ _ObjectPtr.GetPointerValue( _IfStringThenRawString( arg ) ) for arg in args ]
		if isConstructor:
			self._ptr = _ObjectPtr( funcptr( *args ), destructor=self._cdel ) # ...here (and hence *only* used for instances that were constructed here, not for instance pointers owned by the DLL)
			return
		ptr = _ObjectPtr.GetPointerValue( self )
		if not ptr: raise @{API_SHORTNAME}Exception( '%s called with null pointer' % funcptr.__name__ )
		result = funcptr( ptr, *args )
		return outputClass( _ObjectPtr( result ) ) if outputClass else result
	method.__name__ = name
	method.__qualname__ = qualname
	method.__doc__ = doc
	return method

def _Define( className, pyName, cReturnType, cName, cArgString, defaults ):
	# TODO:  deal with functions that are defined as static methods in C++..?
	cls = _GetClass( className, pyName )
	func = getattr( _DLL, cName )
	cArgs = cArgString.split( ',' )
	cArgTypes = [] if cArgString == 'void' else [ ' '.join( arg.strip().split()[ :-1 ] ) for arg in cArgs ]
	cArgNames = [] if cArgString == 'void' else [ arg.split()[ -1 ] for arg in cArgs ]
	cArgNames = [ 'self' if x.lower().strip( '_' ) in [ 'self', 'this' ] else x for x in cArgNames ]
	for i, x in enumerate( cArgNames ):
		if x.replace( ' ', '' ).endswith( '[]' ):
			cArgNames[ i ] = x.rstrip( ' []' )
			cArgTypes[ i ] += '*' # TODO: arrays become pointers here. Under what circumstances will this break? Under what circumstances will it even be useful?
	func.argtypes = tuple( _TranslateType( t ) for t in cArgTypes )
	func.restype = _TranslateType( cReturnType )
	if func is not _DLL.DF_ClearError: func.errcheck = _ErrorCheck
	if className and pyName != '__init__': cArgNames.pop( 0 ); cArgTypes.pop( 0 )
	func.argNames = cArgNames
	if not defaults: defaults = {}
	func.argDefaults = defaults
	func.returnsRawBytes = False # TODO: how can we flag that a function's output should not be decoded?
	rt = cReturnType
	if rt.startswith( 'const ' ): rt = rt[ 6: ].strip()
	outputClass = _GetClass( _CLASSES.get( rt, None ) )
	doc = [ 'Wraps `%s` from %s' % ( cName, _DLL._name ) ]
	if func.argtypes: doc.append( 'Args:\n\n' + '\n\n'.join( '    %s (%s)%s' % ( argName, argType, ( ': default = %s' % defaults[ argName ] if argName in defaults else '' ) ) for argName, argType in zip( cArgNames, cArgTypes ) ) )
	if func.restype:  doc.append( 'Returns: %s' % cReturnType )
	doc = '\n\n'.join( doc ) + '\n'
	if className:
		if pyName == '__del__':
			cls._cdel = func
		else:
			while pyName in cls.__dict__: pyName = '_' + pyName
			method = _WrapMethod( func, name=pyName, qualname=cls.__qualname__ + '.' + pyName, doc=doc, outputClass=outputClass )
			setattr( cls, pyName, method )
	else:
		qualname = pyName
		pyName = pyName.split( '.' )[ -1 ]
		func = _WrapFunction( func, name=pyName, qualname=qualname, doc=doc, outputClass=outputClass )
		if cls:
			if pyName not in cls.__dict__: setattr( cls, pyName, staticmethod( func ) )
		else:
			__all__.append( pyName ); globals()[ pyName ] = func

####################################################################
####################################################################

# Optionally pre-define API-specific classes here if you want to equip them with additional methods/properties, or override DLL methods.
# The DLL methods will be added automatically (if you have already overridden method `X`, the DLL method will be attached as `_X`. Just
# do not override `__init__()` unless you *really* know what you're doing.

_builddir = _os.path.realpath( _os.path.join( __file__, '..', '..', 'devel', 'build' ) ) 
_GetRevisionViaGit = None
if _os.path.isfile( _os.path.join( _builddir, 'autogen.py' ) ):
	_sys.path.insert( 0, _builddir )
	from autogen import GetRevision as _GetRevisionViaGit
	_sys.path.pop( 0 )

def CheckRevision( warnToConsole=True ):
	msg = 'could not query current DataFlow revision via git'
	if _GetRevisionViaGit is None:
		msg += ' (failed to import from autogen.py)'
	else:
		latest = _GetRevisionViaGit()
		if latest == '(unknown revision)':
			msg += ' (perhaps git is not installed, or DataFlow.py is not in a git repository?)'
		else:
			latest = latest.split()[ 1 ]
			binRev = GetRevision().split()[ 1 ]
			if binRev.strip( '+' ) != latest.strip( '+' ):
				msg = 'DataFlow library was compiled from a revision that is different from the current HEAD'
			elif binRev.endswith( '+' ):
				msg = 'DataFlow library was compiled incorporating uncommitted changes'
			else:
				msg = ''
	if msg and warnToConsole: _sys.stderr.write( '\nWARNING: %s\n' % msg )
	return msg

class SharedDataArchive( object ):
	def Open( self, name, mode='r', ensureMinimumPayload=0, type='\0' ):
		if isinstance( name, int ): name, offset = None, name
		else: offset = 0
		f = self.File( self, name, offset, mode, ensureMinimumPayload, type )
		if not f.Exists(): del f; raise FileNotFoundError( 'file %r does not exist' % ( offset if name is None else name ) )
		f.archive = self  # make sure the archive doesn't get garbage-collected before the File
		return f
	def AllFileNames( self, includeDirectories=False, restrictToTypes=None ):
		return [ file.GetName() for file in self.Open( '$d' ).Walk( includeDirectories=includeDirectories, restrictToTypes=restrictToTypes ) ]
	def __str__( self ):
		return self.Report()
	class File( object ):
		def __str__( self ):
			return self.Report()
		def Read( self, nBytesToRead=None ):
			if nBytesToRead is None: nBytesToRead = self.TellMax() - self.Tell()
			buf = ( _ctypes.c_char * nBytesToRead )()
			nBytesRead = self._Read( buf, nBytesToRead )
			return buf.raw[ :nBytesRead ]
		def Write( self, data, nBytes=None ):
			data = _IfStringThenRawString( data )
			if nBytes is None or nBytes < 0 or nBytes > len( data ): nBytes = len( data )
			return self._Write( data, nBytes )
		def Walk( self, includeDirectories=False, restrictToTypes=None ):
			self._Walk( 0, includeDirectories, restrictToTypes )
			while self.Exists(): yield self; self.Next()

####################################################################
####################################################################

_CLASSES = dict( @PYTHON_CLASS_MAPPING )
@PYTHON_DEFINITIONS

if __name__ == '__main__':
	_w = 24
	print( "{\n    'Python': {" )
	print( '        %*r : %r,' % ( _w, '__file__', __file__ ) )
	for _k, _v in __meta__.items():
		_k = '__meta__["%s"]' % _k
		print( '        %*r : %r,' % ( _w, _k, _v ) )
	print( "    },\n    'Binary': {" )
	print( '        %*r : %r,' % ( _w, '_DLL._name', _DLL._name ) )
	for _f in [ 'GetVersion', 'GetRevision', 'GetBuildDatestamp' ]:
		_k = _f + '()'
		if _f in __all__: print( '        %*r : %r,' % ( _w, _k, globals()[ _f ]() ) )
	print( "    },\n}\n" )

