/*
# $BEGIN_DATAFLOW_LICENSE$
# 
# This file is part of the DataFlow project, a software library for
# efficient asynchronous multi-stream inter-process communication.
# 
# Copyright (c) 2014-2021 Jeremy Hill
# 
# DataFlow is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program. If not, see http://www.gnu.org/licenses/ .
# 
# Some third-party source code is compiled and linked into the
# DataFlow binary, as follows: devel/src/sem_timedwait_apple.cpp
# is adapted from sem_timedwait.cpp by Keith Shortridge, copyright
# (c) 2013 Australian Astronomical Observatory (AAO), released under
# the MIT License; devel/src/dirent_win.h is copyright (c) 2006
# Toni Ronkko, and released under the MIT License. The DataFlow build
# system also uses some third-party code:
# devel/build/cmake-recipes/DefineFunction_TARGET_ARCHITECTURE.cmake
# which is copyright (c) 2012 Petroules Corporation, released under
# the 2-Clause BSD License. For more details, see the respective
# copyright, licensing and disclaimer information in each of these
# files.
# 
# $END_DATAFLOW_LICENSE$
*/
#ifndef   INCLUDED_DataNode_HPP
#define   INCLUDED_DataNode_HPP

#include <map>
#include <string>
#include <vector>

#include "BasicTypes.hpp"
#include "StringUtils.hpp"
#include "DataType.hpp"
#include "Dimension.hpp"
class Confluence;

class DataNode;
typedef DataNode * DataNodePtr;
typedef std::map< std::string, DataNodePtr > DataNodeMap;



class DataNode                   // client-side, client-facing
{
	public:
		DataNode( std::string name=".", DataNodePtr parent=NULL );
	
		class iterator
		{
			public:
				iterator( DataNode & n ) { init( &n ); }
				iterator( DataNode * p ) { init(  p ); }
				void init( DataNode * p ) { mCurrent = mBegin = p; mEnd = mBegin ? mBegin->Next( false ) : NULL; }
				~iterator() {}
				iterator & operator++() { mCurrent = mCurrent->Next(); return *this; }
				iterator operator++( int ) { iterator temp = *this; ++*this; return temp; }
				bool finished( void ) const { return mCurrent == NULL || mCurrent == mEnd; }
				bool operator==( const iterator & other ) const { return mCurrent == other.mCurrent || ( finished() && other.finished() ); }
				bool operator!=( const iterator & other ) const { return !( *this == other ); }
				DataNode & operator*() { return *mCurrent; }
				DataNode * operator->() { return mCurrent; }
			private:
				DataNode * mBegin;
				DataNode * mEnd;
				DataNode * mCurrent;
		};
		iterator begin() { return iterator( this ); }
		iterator end() { return iterator( NULL ); } // NB: this is a cheat so that Next(false) does not have to be computed 
													//     on every iteration of a loop.  it is compensated-for in
													//     operator==, but the side-effect is that you might get unexpected
													//     results if you add siblings mid-iteration
		
		~DataNode();
		
		static const std::string Separator;   //   "/"
		static const std::string StringDelim; //   Same as Dimension::StringDelim (should be "\v") and used to separate elements of string arrays
	
		std::string    GetName( void )                      { return mName; }
		DataNodePtr    GetParent( void )                    { return mParent; }
		uint64_t       GetNumberOfEvents( void )            { return mNumberOfEvents; }
		DataType::Code GetDataType( void )                  { return mDataType; }
		uint64_t       GetNumberOfDimensions( void )        { return mDimensions.size(); }
		uint64_t       GetExtent( uint64_t dimensionIndex ) { return GetDimension( dimensionIndex ).GetExtent(); }
		uint64_t       GetStride( uint64_t dimensionIndex ) { return GetDimension( dimensionIndex ).GetStride(); } // TODO: if it's 0, auto-compute it by multiplying predecessors' extents
		uint64_t       GetNumberOfElementsPerEvent( void );
		uint64_t       GetBytesPerEvent( void ) { return GetNumberOfElementsPerEvent() * DataType::SizeInBytes( mDataType ); } // for strings (variable size) this will return 0 because SizeInBytes() returns 0
		bool           HasData( void ); // indicates whether the immediate DataNode has data; if you want to know whether there's data *anywhere* in the composite from there on down, look at GetNumberOfNodes( false );
		
		// For iterating over the composite (including self):
		
		uint64_t       GetNumberOfNodes( bool includeEmptyNodes=false );
		DataNodePtr    Get( uint64_t index, bool includeEmptyNodes=false );
		DataNodePtr    Next( bool include_children=true );
		DataNodePtr    Get( std::string path, bool createPathIfAbsent=false );
		DataNodePtr    Create( std::string path, bool exceptionIfAlreadyExists=false );
		std::string    GetPath( DataNodePtr earliestAncestor=NULL );

		uint64_t       GetContentSizeInBytes( void );
		std::string    GetStringValue( void );
		void           SetStringValue( const std::string & value );
		double         GetScalarValue( void );
		void           SetScalarValue( double value, DataType::Code dtype=DataType::FLOAT64 );
		void           SetScalarValue( double value, std::string dtype );
		
		Dimension &    GetDimension( uint64_t dimensionIndex ) { return mDimensions[ as_size_t( dimensionIndex ) ]; } // TODO: throw exception if index out of range
		DataNodePtr    SetDimension( uint64_t dimensionIndex, const Dimension & dimension );
		DataNodePtr    AddDimension( const Dimension & dimension );
		std::string    SerializeFormat( void );
		void           DecodeFormat( const void * serialized );
		void *         Allocate( uint64_t nBytes );
		
		void           ReportTree( void );
		static int     Demo( int argc, const char * argv[] );
	
		void           Push( Confluence & confluence );
		void           Push( Confluence & confluence, double & timeTaken );
	
	protected:
		void           ClearData( void );
		void           RemoveDescendants( void );
		
	private:
		void           RemoveDataPointer( void );
		DataNodePtr    Get( uint64_t * index, bool includeEmptyNodes );
	
		std::string                          mName;
		DataNodePtr                          mParent;
		uint64_t                             mNumberOfEvents;
		DataType::Code                       mDataType;
		DimensionVector                      mDimensions;
		std::vector< double >                mPushTimeStamps;
		std::vector< double >                mNativeTimeStamps;
		DataNodeMap                          mChildren;
		bool                                 mOwnsDataPointer;
		uint64_t                             mStringContentLength;
		char *                               mData;
		float64_t                            mFixedStorage;
};

#endif // INCLUDED_DataNode_HPP
