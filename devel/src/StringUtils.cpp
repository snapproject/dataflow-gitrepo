/*
# $BEGIN_DATAFLOW_LICENSE$
# 
# This file is part of the DataFlow project, a software library for
# efficient asynchronous multi-stream inter-process communication.
# 
# Copyright (c) 2014-2021 Jeremy Hill
# 
# DataFlow is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program. If not, see http://www.gnu.org/licenses/ .
# 
# Some third-party source code is compiled and linked into the
# DataFlow binary, as follows: devel/src/sem_timedwait_apple.cpp
# is adapted from sem_timedwait.cpp by Keith Shortridge, copyright
# (c) 2013 Australian Astronomical Observatory (AAO), released under
# the MIT License; devel/src/dirent_win.h is copyright (c) 2006
# Toni Ronkko, and released under the MIT License. The DataFlow build
# system also uses some third-party code:
# devel/build/cmake-recipes/DefineFunction_TARGET_ARCHITECTURE.cmake
# which is copyright (c) 2012 Petroules Corporation, released under
# the 2-Clause BSD License. For more details, see the respective
# copyright, licensing and disclaimer information in each of these
# files.
# 
# $END_DATAFLOW_LICENSE$
*/
#ifndef INCLUDED_StringUtils_CPP
#define INCLUDED_StringUtils_CPP

#include "StringUtils.hpp"
#include <ctype.h>
#include <string.h>
#include <iomanip>
#include <sstream>
#include <regex>    // CPLUSPLUS11
#define HEX( width )  std::setfill( '0' ) << std::setw( width ) << std::hex << std::uppercase

std::string
StringUtils::ToLower( const std::string & s )
{
	std::string out = s;
	for( std::string::iterator it = out.begin(); it != out.end(); it++ ) *it = tolower( *it );
	return out;
}

std::string
StringUtils::ToUpper( const std::string & s )
{
	std::string out = s;
	for( std::string::iterator it = out.begin(); it != out.end(); it++ ) *it = toupper( *it );
	return out;
}

std::string
StringUtils::ChompString( std::string & s, const std::string & delims, char groupOpener, char groupCloser, bool countEmpty )
{ // split by any one of the characters in `delims`
#	define ISDELIM( CHAR )  ( delims.find( CHAR ) != std::string::npos )
	std::string out;
	size_t pos = 0, length = s.length();
	if( countEmpty && length && ISDELIM( s[ 0 ] ) ) { s = s.substr( 1 ); return out; }
	while( pos < length && !countEmpty && ISDELIM( s[ pos ] ) ) pos++;
	size_t start = pos, tokenLength = 0, nested=0;
	while( pos < length && ( nested || !ISDELIM( s[ pos ] ) ) )
	{
		if( nested && groupCloser && groupCloser == s[ pos ] ) nested--;
		else if( groupOpener && groupOpener == s[ pos ] ) nested++;
		pos++;
		tokenLength++;
	}
	while( pos < length && ISDELIM( s[ pos ] ) ) 
	{
		pos++;
		if( countEmpty ) break;
	}
	out = s.substr( start, tokenLength );
	s = s.substr( pos );
	return out;
#	undef ISDELIM
}

std::string
StringUtils::Join( const StringVector & v, const std::string & delim, bool removeDelim, size_t startIndex, size_t numberOfElements )
{
	std::string out;
	bool started = false;
	size_t delim_length = delim.length(), nAvailableElements = v.size();
	StringVector::const_iterator start = v.begin() + ( startIndex > nAvailableElements ? nAvailableElements : startIndex );
	StringVector::const_iterator stop  = ( numberOfElements == std::string::npos || startIndex + numberOfElements > nAvailableElements ) ? v.end() : ( v.begin() + startIndex + numberOfElements );
	for( StringVector::const_iterator it = start; it != stop; it++ )
	{
		if( started ) out += delim;
		if( removeDelim )
		{
			for( size_t i = 0; i < it->length(); )
			{
				if( it->substr( i, delim_length ) == delim ) i += delim_length; 
				else out += ( *it )[ i++ ];
			}
		}
		else out += *it;
		started = true;
	}
	return out;
}

void
StringUtils::Split( StringVector & output, std::string input, const std::string & delims, char groupOpener, char groupCloser, bool countEmpty )
{
	output.clear();
	while( input.length() ) output.push_back( ChompString( input, delims, groupOpener, groupCloser, countEmpty ) );
}

bool
StringUtils::Match( const std::string & pattern, const std::string & candidate, const char *options )
{
	// In `options`, you can supply zero or more of the following characters:
	//   i or I  to make the match case-insensitive
	//   *       to allow a '*' character in `pattern` to match zero or more characters of any kind in `candidate`
	//   ?       to allow a '?' character in `pattern` to match exactly one character of any kind in `candidate`
	// 
	// Adapted from wildcmp() by Jack Handy <jakkhandy@hotmail.com>, which did '*' and '?' globbing
	// https://www.codeproject.com/Articles/1088/Wildcard-string-compare-globbing
	bool starWildcards = false;
	bool queryWildcards = false;
	bool caseInsensitive = false;
	for( ; options && *options; options++ )
	{
		if( *options == 'i' || *options == 'I' ) caseInsensitive = true;
		if( *options == '*' ) starWildcards = true;
		if( *options == '?' ) queryWildcards = true;
	}
	const char *wild = pattern.c_str();
	const char *string = candidate.c_str();
	
	char wc, sc;	
	const char *cp = NULL, *mp = NULL;		
	while( *string && !( starWildcards && *wild == '*' ) )
	{
		wc = caseInsensitive ? toupper( *wild   ) : *wild;
		sc = caseInsensitive ? toupper( *string ) : *string;
		if( wc != sc && !( queryWildcards && wc == '?' ) ) return 0;
		wild++;
		string++;
	}
	if( !starWildcards ) return !*wild;
	
	while( *string )
	{
		wc = caseInsensitive ? toupper( *wild   ) : *wild;
		sc = caseInsensitive ? toupper( *string ) : *string;
		if( wc == '*' )
		{
			if( !*++wild ) return true;
			mp = wild;
			cp = string + 1;
		}
		else if( wc == sc || ( queryWildcards && wc == '?' ) )
		{
			wild++;
			string++;
		}
		else
		{
			wild = mp;
			string = cp++;
		}
	}
	while( *wild == '*' ) wild++;
	return !*wild;
}

bool
StringUtils::StartsWith( const std::string & fullString, const std::string & prefix )
{
    return ( 0 == fullString.compare( 0, prefix.length(), prefix ) );
}

bool
StringUtils::EndsWith( const std::string & fullString, const std::string & suffix )
{
    if( fullString.length() < suffix.length() ) return false;
    return ( 0 == fullString.compare( fullString.length() - suffix.length(), suffix.length(), suffix ) );
}

std::string
StringUtils::StringLiteral( const std::string & s, const char * quote )
{
	return StringLiteral( s.c_str(), s.length(), quote );
}

std::string
StringUtils::StringLiteral( const char * s, const char * quote )
{
	return StringLiteral( s, s ? strlen( s ) : 0, quote );
}

std::string
StringUtils::StringLiteral( const char * s, size_t length, const char * quote )
{
	if( s == NULL ) return "NULL";
	std::stringstream ss;
	if( quote ) ss << quote;
	for( size_t i = 0; i < length; i++ )
	{
		char c = s[ i ];
		if(      c == '\0' ) ss << "\\0";
		else if( c == '\n' ) ss << "\\n";
		else if( c == '\r' ) ss << "\\r";
		else if( c == '\t' ) ss << "\\t";
		else if( c == '\\' ) ss << "\\\\";
		else if( quote && c == quote[ 0 ] ) ss << "\\" << c;
		else if( c < 32 || c > 127 )
		{
			// so tired of trying to make setw/setfill/etc work reliably, so:
			char c1 = ( char )( ( unsigned char )c / 16 );
			c1 += ( c1 <= 9 ) ? '0' : ( 'A' - 10 );
			char c2 = ( char )( ( unsigned char )c % 16 );
			c2 += ( c2 <= 9 ) ? '0' : ( 'A' - 10 );
			ss << "\\x" << c1 << c2;
		}
		else ss << c;
	}
	if( quote ) ss << quote;
	return ss.str();
}

std::ostream &
operator<<( std::ostream & os, const StringUtils::StringVector & x )
{
	os << "[";
	for( StringUtils::StringVector::const_iterator it = x.begin(); it != x.end(); it++ )
	{
		if( it != x.begin() ) os << ", ";
		os << StringUtils::StringLiteral( *it );
	}
	return os << "]";
}

void
StringUtils::BuildStringVector( StringVector & sv, int argc, const char * argv[], bool append )
{
	if( !append ) sv.clear();
	for( int i = 0; i < argc; i++ ) sv.push_back( argv[ i ] );
}

StringUtils::StringVector
StringUtils::BuildStringVector( int argc, const char * argv[] )
{
	StringUtils::StringVector sv;
	BuildStringVector( sv, argc, argv );
	return sv;
}

std::string
StringUtils::RegexReplace( const std::string & in, const std::string & pattern, const std::string & replacement )
{
	return std::regex_replace( in, std::regex( pattern ), replacement );
}

std::string
StringUtils::Strip( const std::string & in, const char * end, const std::string & pattern )
{
	char mode = end ? tolower( *end ) : '\0';
	if( mode == 'l' ) return std::regex_replace( in, std::regex( "^" + pattern ), "" );
	if( mode == 'r' ) return std::regex_replace( in, std::regex( pattern + "$" ), "" );
	return std::regex_replace( in, std::regex( "(^" + pattern + ")|(" + pattern + "$)" ), "" );
}

#include <iostream>
int
StringUtils::Demo( int argc, const char * argv[] )
{
	std::cout << StringLiteral("\xFD\xFE\xFF\x01\x02\x03\x04\x05\x06\x07\x08\x09\x0A\x0B\x0C\x0D\x0E\x0F\x10\x11\x12\x13\x14\x15\x16\x17\x18\x19\x1A\x1B\x1C\x1D\x1E\x1F\x20\x21") << "\n";
	return 0;
}
#endif // ifndef INCLUDED_StringUtils_CPP
