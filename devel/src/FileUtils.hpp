/*
# $BEGIN_DATAFLOW_LICENSE$
# 
# This file is part of the DataFlow project, a software library for
# efficient asynchronous multi-stream inter-process communication.
# 
# Copyright (c) 2014-2021 Jeremy Hill
# 
# DataFlow is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program. If not, see http://www.gnu.org/licenses/ .
# 
# Some third-party source code is compiled and linked into the
# DataFlow binary, as follows: devel/src/sem_timedwait_apple.cpp
# is adapted from sem_timedwait.cpp by Keith Shortridge, copyright
# (c) 2013 Australian Astronomical Observatory (AAO), released under
# the MIT License; devel/src/dirent_win.h is copyright (c) 2006
# Toni Ronkko, and released under the MIT License. The DataFlow build
# system also uses some third-party code:
# devel/build/cmake-recipes/DefineFunction_TARGET_ARCHITECTURE.cmake
# which is copyright (c) 2012 Petroules Corporation, released under
# the 2-Clause BSD License. For more details, see the respective
# copyright, licensing and disclaimer information in each of these
# files.
# 
# $END_DATAFLOW_LICENSE$
*/
#ifndef   INCLUDE_FileUtils_HPP
#define   INCLUDE_FileUtils_HPP

#include <string>

namespace FileUtils
{
	bool        DirectoryExists( std::string x );
	bool        FileExists( std::string x );
	std::string GetWorkingDirectory( void ); // get the name of the current directory as a string (empty on failure)
	std::string StandardizePath( std::string x ); // change all occurrences of either '\\' or '/' into the variant appropriate for the current platform, and eliminate multiple slashes (exception to allow for Samba paths: allow double backslashes at the beginning of a path on Windows)
	std::string JoinPath( std::string parent, std::string child );
	void        SplitPath( std::string fullpath, std::string & parent, std::string & stem, std::string & extension ); // break a string into three parts: directory (before the last slash), stem (after the last slash, before the last dot) and extension (after the last slash, from the last dot onwards)
	std::string RealPath( std::string x ); // return the absolute path corresponding to path x (if no such directory exists, just return a standardized copy of x)
	bool        PathMatch( std::string a, std::string b, bool partial=false ); // return true if a is the same directory as b according to RealPath (partial match means that path a is equal to, or is a subdirectory of, b)
	int         MakeDirectory( std::string x ); // mkdir (for mkdir -p see MakePath, below)
	int         MakePath( std::string x ); // mkdir -p
	int         RemoveFile( std::string x ); // returns 0 if successful
	std::string ParentDirectory( std::string x, int levels=1 );
	std::string BaseName( std::string x, bool withExtension=true );
	std::string LibraryPath(    const std::string & relativePath="", void * funcPtr=NULL  );
	std::string ExecutablePath( const std::string & relativePath="" );
	int         Demo( int argc, const char * argv[] );
#ifdef    _WIN32
	const char FILE_SEPARATOR = '\\';
#else  // _WIN32
	const char FILE_SEPARATOR = '/';
#endif // _WIN32
}

#endif // INCLUDE_FileUtils_HPP
