/*
# $BEGIN_DATAFLOW_LICENSE$
# 
# This file is part of the DataFlow project, a software library for
# efficient asynchronous multi-stream inter-process communication.
# 
# Copyright (c) 2014-2021 Jeremy Hill
# 
# DataFlow is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program. If not, see http://www.gnu.org/licenses/ .
# 
# Some third-party source code is compiled and linked into the
# DataFlow binary, as follows: devel/src/sem_timedwait_apple.cpp
# is adapted from sem_timedwait.cpp by Keith Shortridge, copyright
# (c) 2013 Australian Astronomical Observatory (AAO), released under
# the MIT License; devel/src/dirent_win.h is copyright (c) 2006
# Toni Ronkko, and released under the MIT License. The DataFlow build
# system also uses some third-party code:
# devel/build/cmake-recipes/DefineFunction_TARGET_ARCHITECTURE.cmake
# which is copyright (c) 2012 Petroules Corporation, released under
# the 2-Clause BSD License. For more details, see the respective
# copyright, licensing and disclaimer information in each of these
# files.
# 
# $END_DATAFLOW_LICENSE$
*/
#ifndef INCLUDE_DebuggingUtils_CPP
#define INCLUDE_DebuggingUtils_CPP

#include "DebuggingUtils.hpp"

#include "StringUtils.hpp"
#include "ExceptionUtils.hpp"
#include "BasicTypes.hpp"
#include <string.h>
#include <sstream>
#include <iomanip>

int
DebuggingUtils::Console( const char * msg, int debugLevel )
{
	std::cerr << msg << std::endl;
	return debugLevel;
}

void
DebuggingUtils::PrintArgv( int argc, const char * argv[] )
{
	std::cerr << "argc = " << argc << "\n";
	for( int i = 0; i < argc; i++ ) std::cerr << "argv[" << i << "] = " << StringUtils::StringLiteral( argv[ i ] ) << "\n";
}

int DebuggingUtils::gDebugLevel = DEBUG_LEVEL;
void
DebuggingUtils::SetDebugLevel( int level )
{
	gDebugLevel = level;
}
void
DebuggingUtils::RaiseDebugLevel( int level )
{
	if( gDebugLevel < level ) gDebugLevel = level;
}
void
DebuggingUtils::LowerDebugLevel( int level )
{
	if( gDebugLevel > level ) gDebugLevel = level;
}

void
DebuggingUtils::SanityCheck( char assertEndianness )
{
	SANITY_CHECK( PACKING_TEST_STRUCT, TIGHT, assertEndianness );
}
///////////////////////////////////////////////////////////////////
#ifdef STATIC_ASSERT   // compile with /DSTATIC_ASSERT or -DSTATIC_ASSERT to perform the following compile-time
                       // size/alignment checks (this is distinct from the runtime SANITY_CHECK macro or the
                       // SanityCheck() function that wraps it).


#undef STATIC_ASSERT
// Macro for "static-assert" (only useful on compile-time constant expressions)
#define STATIC_ASSERT(exp)           STATIC_ASSERT_II(exp, __LINE__)
// Macro used by STATIC_ASSERT macro (don't use directly)
#define STATIC_ASSERT_II(exp, line)  STATIC_ASSERT_III(exp, line)
// Macro used by STATIC_ASSERT macro (don't use directly)
#define STATIC_ASSERT_III(exp, line) enum static_assertion##line{static_assert_line_##line = 1/(int)(exp)}

STATIC_ASSERT( sizeof( int8_t    ) == 1 ); // if you get a compiler error on any of these
STATIC_ASSERT( sizeof( uint8_t   ) == 1 ); // lines (misleadingly, on MSVC, the error might be
STATIC_ASSERT( sizeof( int16_t   ) == 2 ); // just "expected constant expression") then the
STATIC_ASSERT( sizeof( uint16_t  ) == 2 ); // compiler settings need to be changed
STATIC_ASSERT( sizeof( int32_t   ) == 4 );
STATIC_ASSERT( sizeof( uint32_t  ) == 4 );
STATIC_ASSERT( sizeof( float32_t ) == 4 );
STATIC_ASSERT( sizeof( float64_t ) == 8 );
#ifdef PACKING_TEST_STRUCT
STATIC_ASSERT( sizeof( PACKING_TEST_STRUCT ) == TIGHT ); // If you get an error here (misleadingly, on MSVC, the
                                                         // error may be just "expected constant expression"),
                                                         // then you need to assure that the compiler aligns
                                                         // structs on 1-byte boundaries. The MSVC command-line
                                                         // flag is /Zp1 and for g++ it's -fpack-struct=1 but
                                                         // the #pragma pack( push, 1 ) at the top of this file
                                                         // should also have done it.
#endif // PACKING_TEST_STRUCT
// NB: can't think of a static assertion for little-endianity.  Make sure to use the runtime SanityCheck().
#endif // STATIC_ASSERT
///////////////////////////////////////////////////////////////////


#endif // ifndef INCLUDE_DebuggingUtils_CPP
