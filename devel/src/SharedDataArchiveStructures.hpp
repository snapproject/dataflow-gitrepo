/*
# $BEGIN_DATAFLOW_LICENSE$
# 
# This file is part of the DataFlow project, a software library for
# efficient asynchronous multi-stream inter-process communication.
# 
# Copyright (c) 2014-2021 Jeremy Hill
# 
# DataFlow is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program. If not, see http://www.gnu.org/licenses/ .
# 
# Some third-party source code is compiled and linked into the
# DataFlow binary, as follows: devel/src/sem_timedwait_apple.cpp
# is adapted from sem_timedwait.cpp by Keith Shortridge, copyright
# (c) 2013 Australian Astronomical Observatory (AAO), released under
# the MIT License; devel/src/dirent_win.h is copyright (c) 2006
# Toni Ronkko, and released under the MIT License. The DataFlow build
# system also uses some third-party code:
# devel/build/cmake-recipes/DefineFunction_TARGET_ARCHITECTURE.cmake
# which is copyright (c) 2012 Petroules Corporation, released under
# the 2-Clause BSD License. For more details, see the respective
# copyright, licensing and disclaimer information in each of these
# files.
# 
# $END_DATAFLOW_LICENSE$
*/
#ifndef    INCLUDED_SharedDataArchiveStructures_HPP
#define    INCLUDED_SharedDataArchiveStructures_HPP
//////////////////////////////////////////////////////////////////////
#pragma pack( push, 1 )   // for packed struct alignment --- necessary for this header, or not?

#include "BasicTypes.hpp"
////////////////////////////////////////////////////////////////////////////////////////////////////////////

typedef struct
{
	uint32_t  fSharedMemoryLayoutSignature;
	uint32_t  fSharedMemoryLayoutVersion;
	uint64_t  fSharedMemorySegmentSize;
	uint32_t  fReadWriteMutexCount;
	uint32_t  fNumberOfClients;
	uint64_t  fNumberOfClientsEver;
	uint64_t  fOffsetOfVacatedLotRecords;
	uint64_t  fNumberOfLotSizes;
	uint64_t  fOffsetOfHashTable;
	uint64_t  fNumberOfHashTableSlots;  // should be a prime number
	uint64_t  fSortingPreference;
	uint64_t  fOffsetOfSegmentName;
	uint64_t  fOffsetOfUnallocatedSpace;
	uint64_t  fUnallocatedSize;
	uint8_t   fTimerOrigin[ 8 ];
	float64_t fSecondsSinceEpochAtTimerOrigin;
} SegmentHeader;
// Then fNumberOfLotSizes * VacatedLotRecord: for a fixed number of different lot sizes, record the head and tail of a linked list of allocated-but-vacated lots 
// Then fNumberOfHashTableSlots * uint64_t: the hash-table itself
// Then segment name, with null-terminator
// Then the first Lot (which will be the unallocated one, to begin with).

typedef struct
{
	uint64_t fLotSize;
	uint64_t fOffsetOfFirstVacatedLot;
	uint64_t fOffsetOfLastVacatedLot;
	uint64_t fTotalCapacity;
} VacatedLotRecord;

typedef struct
{
	uint64_t fLotSize; // current lot
	uint64_t fOffsetOfPreviousLotInTerritory;
	uint64_t fOffsetOfNextLotInTerritory;
} LotHeader;

typedef struct
{
	LotHeader fLotHeader;
	// From here on, it's only used if fLotHeader.fPreviousLotOffset==0, i.e. in the first lot of a territory
	uint64_t fTotalCapacity; // sum of all linked lot capacities (not including LotHeader overheads)
	uint64_t fCapacityUsed;  // amount used from here on down, across all lots (not including LotHeader overheads)
	uint64_t fOffsetOfLastLotInTerritory;
	uint64_t fOffsetOfPreviousFileInHashTable;
	uint64_t fOffsetOfNextFileInHashTable;
	uint64_t fOffsetOfParentDirectory;
	uint64_t fOffsetOfPreviousFileInDirectory;
	uint64_t fOffsetOfNextFileInDirectory;
	uint64_t fRelativeOffsetOfPayload;
	uint64_t fPayloadSize; // includes potential "dead" space in the middle of a wrapped file
	uint64_t fContentStart;
	int64_t  fContentWrap;
	uint64_t fHeadMinimum;
	uint64_t fHeadMaximum;
} TerritoryHeader;   // NB: if you're extending or reordering this, make the corresponding change in in SharedDataArchive::Summary() too
// Then key, then null terminator, then payload

typedef struct
{
	uint64_t fOffsetOfFirstChild;
	uint64_t fOffsetOfLastChild;
	uint8_t  fSorted;
} DirectoryRecord;

typedef struct
{
	uint8_t  fOne;
	uint16_t fTwo;
	uint32_t fFour;
	uint64_t fEight;
} PackingTestStruct;
#define PACKING_TEST_STRUCT PackingTestStruct
#define TIGHT 15
	

#pragma pack( pop )
//////////////////////////////////////////////////////////////////////
#endif // INCLUDED_SharedDataArchiveStructures_HPP
