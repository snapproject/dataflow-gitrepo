/*
# $BEGIN_DATAFLOW_LICENSE$
# 
# This file is part of the DataFlow project, a software library for
# efficient asynchronous multi-stream inter-process communication.
# 
# Copyright (c) 2014-2021 Jeremy Hill
# 
# DataFlow is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program. If not, see http://www.gnu.org/licenses/ .
# 
# Some third-party source code is compiled and linked into the
# DataFlow binary, as follows: devel/src/sem_timedwait_apple.cpp
# is adapted from sem_timedwait.cpp by Keith Shortridge, copyright
# (c) 2013 Australian Astronomical Observatory (AAO), released under
# the MIT License; devel/src/dirent_win.h is copyright (c) 2006
# Toni Ronkko, and released under the MIT License. The DataFlow build
# system also uses some third-party code:
# devel/build/cmake-recipes/DefineFunction_TARGET_ARCHITECTURE.cmake
# which is copyright (c) 2012 Petroules Corporation, released under
# the 2-Clause BSD License. For more details, see the respective
# copyright, licensing and disclaimer information in each of these
# files.
# 
# $END_DATAFLOW_LICENSE$
*/
#ifndef    INCLUDED_Request_CPP
#define    INCLUDED_Request_CPP
//////////////////////////////////////////////////////////////////////

#include "Request.hpp"
#include "Confluence.hpp"

const std::string Rule::Wildcard = "*";

Rule::Rule( const std::string & pattern )
{
	StringUtils::Split( mPattern, pattern, DataNode::Separator, '\0', '\0', false );
}

bool
Rule::PathMatches( const char * path )
{
	if( path == NULL ) return false;

	StringUtils::StringVector pathPieces;
	StringUtils::Split( pathPieces, path, DataNode::Separator, '\0', '\0', false );
	
	for( StringUtils::StringVector::iterator iPath = pathPieces.begin(), iPattern = mPattern.begin();; iPath++, iPattern++ )
	{
		if( iPattern == mPattern.end() ) return true;
		if( iPath == pathPieces.end() ) return false;
		if( *iPattern != *iPath && *iPattern != Rule::Wildcard ) return false;
	}
}

//////////////////////////////////////////////////////////////////////

Request::Request( std::string name )
: DataNode( name )
{
	
}

Request::~Request()
{
	mRules.clear();
}

Rule *
Request::NewRule( const std::string & pattern )
{
	mRules.emplace_back( pattern ); // CPLUSPLUS11
	Rule * result = &mRules.back();
	return result;
}

DataNode *
Request::Pull( Confluence & confluence )
{
	double timeTaken = 0.0;
	return confluence.Pull( this, timeTaken );
}

DataNode *
Request::Pull( Confluence & confluence, double & timeTaken )
{
	return confluence.Pull( this, timeTaken );
}

//////////////////////////////////////////////////////////////////////
#endif  // INCLUDED_Request_CPP
