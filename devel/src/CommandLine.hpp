/*
# $BEGIN_DATAFLOW_LICENSE$
# 
# This file is part of the DataFlow project, a software library for
# efficient asynchronous multi-stream inter-process communication.
# 
# Copyright (c) 2014-2021 Jeremy Hill
# 
# DataFlow is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program. If not, see http://www.gnu.org/licenses/ .
# 
# Some third-party source code is compiled and linked into the
# DataFlow binary, as follows: devel/src/sem_timedwait_apple.cpp
# is adapted from sem_timedwait.cpp by Keith Shortridge, copyright
# (c) 2013 Australian Astronomical Observatory (AAO), released under
# the MIT License; devel/src/dirent_win.h is copyright (c) 2006
# Toni Ronkko, and released under the MIT License. The DataFlow build
# system also uses some third-party code:
# devel/build/cmake-recipes/DefineFunction_TARGET_ARCHITECTURE.cmake
# which is copyright (c) 2012 Petroules Corporation, released under
# the 2-Clause BSD License. For more details, see the respective
# copyright, licensing and disclaimer information in each of these
# files.
# 
# $END_DATAFLOW_LICENSE$
*/
#ifndef    INCLUDED_CommandLine_HPP
#define    INCLUDED_CommandLine_HPP
//////////////////////////////////////////////////////////////////////

#include "ExceptionUtils.hpp"
#include "StringUtils.hpp"

namespace CommandLine
{
	std::string  GetExecutableName( int argc, const char * argv[] );
	const char * FilterOption(    int & argc, const char * argv[], const char * targetOption );
	const char * FirstOption(       int argc, const char * argv[], const char * prefix="--" );

	
	// helper class---nothing to see here, just allows _ss >> s without skipping whitespace and without failing when the stream is empty
	class InputStringStream : public std::istringstream { public: InputStringStream( const std::string & s ) : std::istringstream( s ) {} };
}
CommandLine::InputStringStream & operator>>( CommandLine::InputStringStream &ss, std::string &s );

/*
	The following macros implement an option-parsing scheme in which the option values, if present, are
	always attached to the option name with `=`.  So, for example,  `--verbose` is legal, `--verbosity=2`
	is legal, and even `-verbosity=2` or `verbosity=2` can be handled with the right option definitions,
	but `-v2` and `-v 2` would not be handled. (NB: with the current FilterOption() strategy, of modifying
	the contents of `argv` in-place to remove matched options, it would in theory be possible to
	accommodate these with an additional optional `char` argument. However it would be impossible to
	accommodate clustering of options---as in `git -am hello`---whichever one of `-a` or `-m` you matched
	first, you would have to modify the individual argument string itself to remove that option, and that
	cannot be done with a `const char * argv[]`. For that we would have to completely re-strategize with
	a `StringVector` approach or other object-oriented system. The advantage of the existing FilterOption()
	and FirstOption() is that they can be very easily ported to plain C.)

	Note that these macros commit you to having defined `int argc` and `const char * argv[]`  (or
	`char * argv[]`) with those specific names. `START_ARGS` also creates variables with hardwired names,
	in your current space:  `int argi` and `std::string execName`.

	- Use `START_ARGS` before everything else (or otherwise initialize `execName` and `int argi = 1;`
	  by hand).
	  
	- Then use a `GET_OPT()` call to parse each option, e.g.::

	      size_t segmentSize = 4096; // default value
	      GET_OPT( "--segmentSize", segmentSize, "an unsigned integer" );
	
	  Note that you can prefix any number of dashes you like, even zero (then an argument like
	  `segmentSize=4096` would be a legal option).
	  
	- For finer-grained control, instead of `GET_OPT()` you can unpack it into its components: a call to
	  `FilterOption()` followed by `INTERPRET_ARG()`::
	
	      unsigned int verbosityLevel = 0; // default value
	      const char * valueString = CommandLine::FilterOption( argc, argv, "--verbose" );
	      if( valueString && !*valueString ) verbosityLevel = 1;
	      
	      // and then you might want to:
	      if( valueString && *valueString )
		  	INTERPRET_ARG( valueString, verbosityLevel, "--verbose option", "an unsigned integer" );
	      
	      // or you might prefer:
	      if( valueString && *valueString )
		  	RAISE( execName << ": --verbose should be present or absent (no explicit value)" );
		

	- Then use `NO_MORE_OPTS( "--" )` before any `GET_ARG()` calls.
	
	- Then use `GET_ARG()` for each positional argument you want to parse from the arguments that remain
	  after removal of the options::

	      size_t segmentSize = 4096; // default value
	      GET_ARG( segmentSize, "unsigned integer SEGMENT_SIZE" );
	      // ...or you could just say "an unsigned integer"

	- Use `NO_MORE_ARGS` last, if desired.

*/

#define START_ARGS \
	std::string execName = CommandLine::GetExecutableName( argc, argv ); \
	int argi = 1;

#define INTERPRET_ARG( CONST_CHAR_P, VARIABLE, ARGDESC, TYPEDESC ) \
	{ \
		const char * _arg = CONST_CHAR_P; \
		if( _arg ) \
		{ \
			CommandLine::InputStringStream _ss( _arg ); _ss >> VARIABLE; \
			if( _ss.fail() || !_ss.eof() ) RAISE( execName << ": error parsing " << ARGDESC << " (cannot interpret " << StringUtils::StringLiteral( _arg ) << " as " << TYPEDESC << ")" );  \
		} \
	}

#define GET_OPT( OPTION, VARIABLE, TYPEDESC ) \
	{ \
		const char * _param = CommandLine::FilterOption( argc, argv, OPTION ); \
		INTERPRET_ARG( ( _param && !*_param ? "1" : _param ), VARIABLE, OPTION << " option", TYPEDESC ) \
	}

#define GET_ARG( VARIABLE, DESC ) \
	{ if( argi < argc ) INTERPRET_ARG( argv[ argi++ ], VARIABLE, "positional argument #" << argi - 1, DESC ) }

#define NO_MORE_OPTS( PREFIX ) \
	{ \
		const char * _unsupported = CommandLine::FirstOption( argc, argv, PREFIX ); \
		if( _unsupported ) RAISE( execName << ": unrecognized option " << StringUtils::StringLiteral( _unsupported ) ); \
	}
#define NO_MORE_ARGS \
	{ \
		if( argi < argc ) RAISE( execName << ": too many arguments (unexpected argument " << StringUtils::StringLiteral( argv[ argi ] ) << ")" );	\
	}

#define USAGE( OPTION, S ) \
		( CommandLine::FilterOption( argc, argv, OPTION ) && ( std::cout << StringUtils::RegexReplace( S, R"(\$\{execName\})", execName ) ) )

//////////////////////////////////////////////////////////////////////
#endif  // INCLUDED_CommandLine_HPP
