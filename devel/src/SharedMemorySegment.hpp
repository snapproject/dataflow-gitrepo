/*
# $BEGIN_DATAFLOW_LICENSE$
# 
# This file is part of the DataFlow project, a software library for
# efficient asynchronous multi-stream inter-process communication.
# 
# Copyright (c) 2014-2021 Jeremy Hill
# 
# DataFlow is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program. If not, see http://www.gnu.org/licenses/ .
# 
# Some third-party source code is compiled and linked into the
# DataFlow binary, as follows: devel/src/sem_timedwait_apple.cpp
# is adapted from sem_timedwait.cpp by Keith Shortridge, copyright
# (c) 2013 Australian Astronomical Observatory (AAO), released under
# the MIT License; devel/src/dirent_win.h is copyright (c) 2006
# Toni Ronkko, and released under the MIT License. The DataFlow build
# system also uses some third-party code:
# devel/build/cmake-recipes/DefineFunction_TARGET_ARCHITECTURE.cmake
# which is copyright (c) 2012 Petroules Corporation, released under
# the 2-Clause BSD License. For more details, see the respective
# copyright, licensing and disclaimer information in each of these
# files.
# 
# $END_DATAFLOW_LICENSE$
*/
#ifndef   INCLUDE_SharedMemorySegment_HPP
#define   INCLUDE_SharedMemorySegment_HPP

#include "BasicTypes.hpp"
#include <string>

#ifdef     _WIN32
#else  //  _WIN32
#endif //  _WIN32

class SharedMemorySegment
{
	public:
		SharedMemorySegment( std::string name, bool fileBacked=false, size_t size=0 );
		~SharedMemorySegment();
		bool         IsOpen( void );
		char *       Open( bool throwError=true );
		char *       Data( void );
		void         Close( void );
		int          Remove( void );
		size_t       GetSize( void );
		void         SetSize( size_t bytes );
		const char * GetName( void );
	
	private:
		bool         mFileBacked;
		std::string  mPath;
		char *       mData;
		size_t       mSize;
#ifdef     _WIN32
		void *       mFileHandle;
#else  //  _WIN32
		int          mFileHandle;
#endif //  _WIN32
	
	public:
	
		static std::string LegalizeName( std::string name, bool fileBacked );
		static int         Remove( std::string name );
		
		static int         Demo( int argc, const char * argv[] );
};

#endif // INCLUDE_SharedMemorySegment_HPP
