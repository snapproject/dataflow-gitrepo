/*
# $BEGIN_DATAFLOW_LICENSE$
# 
# This file is part of the DataFlow project, a software library for
# efficient asynchronous multi-stream inter-process communication.
# 
# Copyright (c) 2014-2021 Jeremy Hill
# 
# DataFlow is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program. If not, see http://www.gnu.org/licenses/ .
# 
# Some third-party source code is compiled and linked into the
# DataFlow binary, as follows: devel/src/sem_timedwait_apple.cpp
# is adapted from sem_timedwait.cpp by Keith Shortridge, copyright
# (c) 2013 Australian Astronomical Observatory (AAO), released under
# the MIT License; devel/src/dirent_win.h is copyright (c) 2006
# Toni Ronkko, and released under the MIT License. The DataFlow build
# system also uses some third-party code:
# devel/build/cmake-recipes/DefineFunction_TARGET_ARCHITECTURE.cmake
# which is copyright (c) 2012 Petroules Corporation, released under
# the 2-Clause BSD License. For more details, see the respective
# copyright, licensing and disclaimer information in each of these
# files.
# 
# $END_DATAFLOW_LICENSE$
*/
#ifndef    INCLUDED_SharedDataArchive_HPP
#define    INCLUDED_SharedDataArchive_HPP
//////////////////////////////////////////////////////////////////////

#include "BasicTypes.hpp"
#include "SharedMemorySegment.hpp"
#include "ReadWriteMutex.hpp"
#include "StringUtils.hpp"
#include "ExceptionUtils.hpp"

#include "SharedDataArchiveStructures.hpp"

#include <ios> // for std::ios::beg, std::ios::cur, std::ios::end
#include <set> // for std::set

/*
	A SharedDataArchive is a SharedMemorySegment whose contents are laid out in a certain way.
	It includes the counter for its own inter-process ReadWriteMutex and a TimerOrigin for synchronizing timers between
	processes, relative to creation of the SharedDataArchive.
	
	Just as with ReadWriteMutex, concurrent access by different *threads* is possible, but should be implemented as if
	the threads were different processes---in other words, each thread should create its *own* instance of the
	SharedDataArchive class, created with the same name parameter.
	
	Its contents are divided into "files" each of which is indexed by its file name via a hash-table.
	All file names begin with a two-character prefix: a $ character followed by a single-character file-type.
	It's valid to stop there, but if there's any more to the name, it will be separated from the prefix by a forward slash.
	Like a POSIX filesystem, multiple forward-slashes get collapsed into one, trailing forward-slashes are ignored, /./
	goes nowhere, and /../ goes one slash-delimited "level" upward. If a filename is used without an initial $ symbol, the
	default prefix $f/ will get automatically prepended.  File types may be used as you please---the only reserved file
	type is $d, which denotes a "directory".
	
	A directory is simply a file that contains the head and tail of a linked-list of other files---specifically, the files
	whose names (without file-type prefix) are immediately below the name of the directory (without its file-type prefix)
	in the slash-delimited "level" hierarchy.  Unlike most filesystems, it is possible to have a directory (file type $d)
	whose name is identical (except for the file-type prefix) to a file of another type---if so, note that the file will
	*not* be listed in the directory with which it shares its name (similarly, the directory is not listed in itself).
	Unlike most filesystems, there's no need to create or remove directories explicitly: they are automatically created
	when needed, and removed when they become empty. They're just there to enable fast hierarchy walking.
	
	Each "file" has a Territory, which consists of one or more Lots in a double-linked list, where a Lot is a contiguous
	block of shared memory. If the file expands beyond its initial allocation, the file may become fragmented (its Lots
	interleaved with Lots that are part of other Territories) but the aim is to accept this as an occupational hazard and
	make access across multiple Lots as efficient as possible.
	
	Lots can exist in only a finite number of sizes, all powers of 2, starting at some minimum size.
	
	Within the SharedMemorySegment, the memory that starts after the end of the highest-memory-address allocated Lot is
	"unallocated". There is only one unallocated region---there can never be unallocated space between allocated Lots.
	The process of allocating a brand-new Lot out of the unallocated space is called "carving".  If the one Lot that is
	adjacent to unallocated space is no longer required, it will get deallocated and merged back into the unallocated space.
	In general, though, Lots cannot be de-allocated. (Unfortunately, deallocation of the highest-memory-address Lot doesn't
	propagate automatically: chain deallocation would require Lots to devote extra space in their headers to participating
	in a second linked-list listing them in memory-address order, and also to a flag that introspectively records whether
	they are vacant or not; currently they do not do this.)
	Lots can be split into halves (provided they don't go below the minimum size).
	
	A Lot that is no longer used is called "vacated". It retains its size, and is earmarked for potential recycling by
	getting appended to a linked list specifically devoted to vacated lots of the same size (immediately following the
	SegmentHeader, there is a packed array of VacatedLotRecords, one for each possible Lot size).
	
	Once allocated, a file, or any given one of its Lots, will stay put. Nothing will be moved. This may be an advantage
	(files can contain offsets that point into other files).
	
	Each Lot has a LotHeader, documenting its size and allowing it to participate in linked lists.
	"Size" is defined as the external size of something, including all headers.
	"Capacity" is defined as the internal size of a Lot or Territory, minus any LotHeaders.
	Each Territory has a TerritoryHeader in its first Lot, which includes (starts with) the LotHeader for that Lot.
	Some of the capacity for each file will always be used, at the beginning, for the "file header".
	The "file header" is not a single structure---rather it refers to the remainder of the TerritoryHeader (after the
	initial LotHeader) followed by the name of the file, followed by a null-terminator character after the name.
	The file header must be contained within the first Lot (cannot be split across Lots).
	The content after the file header is called the "payload".
	When creating a file, it is possible to stipulate that a certain number of payload must also fit within the first
	Lot---this is used in the case of directory records, where the entire file including its (tiny) payload must fit
	in the first Lot.
*/

class SharedDataArchive;
class SharedDataArchive
{
	public:
		static const uint64_t DEFAULT_HASHTABLE_SIZE = 8191; // numberOfHashTableSlots must be prime---Mersenne primes 2^13-1 = 8191 (64-KiB hash table) and 2^17-1 = 131071 (1-MiB hash-table) are two practicable potential values
		static const std::string DEFAULT_NAME;
		static const uint32_t VERSION = 1;
		static const uint32_t SIGNATURE = 0x53464d53;  // spells SMFS on a little-endian system (SFMS on big-endian)
		static const uint64_t SMALLEST_LOT_SIZE = 256; // If it were 128, then 96 bytes' worth of territory header, key and null-terminator would leave only a handful of bytes for content at best, and that's before you think about format and dimensionality specifiers in the content...
		                                               // That might be fine for directories (works for paths up to 13 characters at least) but it runs the risk of too many trivially-small files getting fragmented
	
		SharedDataArchive( const std::string & name, uint64_t segmentSize=0, bool forceReformat=false, uint64_t numberOfHashTableSlots=DEFAULT_HASHTABLE_SIZE, double timeoutSeconds=-1.0 );
		SharedDataArchive( SharedMemorySegment & segment, bool forceReformat=false, uint64_t numberOfHashTableSlots=DEFAULT_HASHTABLE_SIZE, double timeoutSeconds=-1.0 );
		SharedDataArchive( void * memoryAddress, double timeoutSeconds=-1.0 );
		~SharedDataArchive();
		
		bool Exists( void );
		bool WasInitializedHere( void );
		void SetMutexTimeout( double seconds );
		void SetMutexVerbosity( bool setting );
		void SetSortingPreference( bool setting );
		uint64_t GetSpaceAvailable( bool includeVacatedLots=true );
		
		uint64_t    GetClientID( void );
		std::string GetName( void );
		std::string ReportHeader( uint64_t maxHashTableEntries=20 );
		std::string ReportLotHeader( uint64_t lotOffset );
		std::string ReportPayload( uint64_t startOffset, uint64_t nBytes, bool allhex=false, uint64_t bytesPerRow=16, uint64_t maxRowsToShow=32 );
		std::string ReportFile( const std::string & name, uint64_t maxRowsPerLot=32 );
		std::string ReportFile( uint64_t offsetOfFirstLot, bool toBeContinued=false, uint64_t maxRowsPerLot=32 );
		std::string ReportDirectory( const std::string & name );
		std::string ReportDirectory( uint64_t directoryOffset );
		std::string Summary( const std::string & highlightFile="" );
		std::string FileByFileSummary( const std::string & highlightFile="", uint64_t * returnNumberOfFiles=NULL );
		std::string LotByLotSummary( uint64_t * returnNumberOfFiles=NULL );

		
		uint64_t FindFile( const std::string & name, uint64_t ensureMinimumPayloadSize=0, char type=0 );
		uint64_t FindDirectory( const std::string & name );
		uint64_t ShrinkTerritory( uint64_t fileOffset );
		uint64_t RemoveFile( const std::string & name );
		uint64_t RemoveFile( uint64_t fileOffset );
		
		double Age( void );
		void   SynchronizeTimer( void );
		ReadWriteMutex::ScopedLock Lock( char mode );
		
		class File
		{
			public:
				// The File constructor is protected---only the SharedDataArchive::Open method can construct File instances
				// The File destructor cannot currently be explicitly defined without some rethought.
				//       This is because "move semantics" have been implemented on ReadWriteMutex::ScopedLock, which means
				//       that its copy-constructor is implicitly deleted. Since the File class has a ScopedLock member, that
				//       means *its* copy-constructor is implicitly deleted too, which somehow isn't a problem *until* we try
				//       to define other members of the Big Five. As soon as we do, we have to be careful, because then
				//       `SharedDataArchive::File f = s.Open("blah");` starts trying to use a copy instead of a move---need to
				//       think through the implications of that.
				// TODO: However, as it stands, there's also a problematic edge-case: it's fundamentally possible to create a
				//       SharedDataArchive instance, use its Open() method to create a File, and then delete the archive
				//       *before* the File. That will probably cause trouble that cannot be caught gracefully if you try to
				//       do anything with the File (including deleting it? not sure...). The File instance contains a reference
				//       `mArchive` to the now-destructed SharedDataArchive, and it also contains a ScopedLock instance `mLock`,
				//       which contains a pointer to `mArchive.mReadWriteMutex` which will also have been destructed along with
				//       the archive... 
				
				static const int beg = std::ios::beg;
				static const int cur = std::ios::cur;
				static const int end = std::ios::end;
			
				bool         Exists( void ) const;
				uint64_t     Tell( void ) const;
				uint64_t     TellMin( void ) const;
				uint64_t     TellMax( void ) const;
				uint64_t     AvailableContentLength( void ) const;
				uint64_t     PayloadCapacityUsed( void ) const;
				uint64_t     PayloadCapacity( void ) const;
				File &       Seek( int64_t relativePosition, int originCode=beg );
				File &       Wipe( void );
				File &       Truncate( bool shrink=false );
				File &       Shrink( void );
				File &       Remove( void );
				File &       Wrap( int64_t payloadCapacity, uint64_t granularity=1 );
				File &       AutoWrap( uint64_t granularity=1 );

				uint64_t     Write( const std::string & src );
				uint64_t     Write( const void * src, uint64_t nBytesToWrite );
				uint64_t     Read(  void * dst, uint64_t nBytesToRead );
				std::string  Read(  uint64_t nBytesToRead );
				std::string  Read(  void );
#				define READ_AS( FILE, TYPE, VARIABLE )  TYPE VARIABLE; ( FILE ).Read( &VARIABLE, sizeof( TYPE ) );
				
				uint64_t     GetFileOffset( void ) const;
				const char * GetName( void ) const;
				char         GetType( void ) const;
				std::string  GetDescription( void ) const;
				SharedDataArchive & GetArchive( void ) const;
				std::string  Report( uint64_t maxRowsPerLot=32 ) const;
				
				bool         MakeReadOnly( void );
				void         Close( void );
				uint64_t     Walk( uint64_t top=0, int includeDirectories=-1, const char * restrictToTypes=NULL );
				uint64_t     Next( void );
				uint64_t     Open( uint64_t offsetOfFirstLot );
								
			protected:
				friend class SharedDataArchive;
				File( SharedDataArchive & sda, const std::string * name, uint64_t offsetOfFirstLot, char mode, uint64_t ensureMinimumPayloadSize, char type );
			
			private:
				bool         IncludedInWalk( void );
			
				SharedDataArchive *        mArchive; // pointer rather than reference, helps preserve "standard layout" in this class
				char                       mMode;
				bool                       mIsWriteable;
				ReadWriteMutex::ScopedLock mLock;
				char *                     mSegmentRootAddress;
				TerritoryHeader *          mTerritoryHeader;
				uint64_t                   mOffsetOfFirstLot;
				uint64_t                   mOffsetOfCurrentLot;
				uint64_t                   mCurrentRelativeOffsetWithinLot;
				uint64_t                   mPositionWithinPayload;
				uint64_t                   mHeadPosition;
				uint64_t                   mTop;
				int                        mWalkIncludesDirectories;
				std::set< char >           mRestrictWalkToTypes;

		};
		File Open( const std::string & name, char mode='r', uint64_t ensureMinimumPayloadSize=0, char type='\0' );
		File Open( uint64_t offsetOfFirstLot, char mode='r', char type='\0' );

		
	private:
		void Connect( SharedMemorySegment * segment, const char * name, void * memoryAddress, uint64_t segmentSize=0, bool forceReformat=false, uint64_t numberOfHashTableSlots=DEFAULT_HASHTABLE_SIZE );
		void Disconnect( bool destructor=false );
		void Initialize( uint64_t numberOfHashTableSlots=DEFAULT_HASHTABLE_SIZE, uint64_t smallestLotSize=SMALLEST_LOT_SIZE );
		void SanityCheck( void );
		uint64_t AllocateTerritory( uint64_t fileHeaderOverhead, uint64_t requiredTotalCapacity, uint64_t * offsetOfFirstNewLot, uint64_t * offsetOfLastNewLot, bool throwExceptionIfFailed=true );
		uint64_t CarveNewLot( uint64_t targetCapacity );
		uint64_t ClaimVacatedLot( uint64_t vlri ); // 1-based index into the packed array of VacatedLotRecords
		uint64_t LeaveVacatedLot( uint64_t lotOffset, bool allowDeallocation=true ); // deallocation can only happen if the lot is contiguous with the unallocated space
		uint64_t ExtendTerritory( uint64_t offsetOfFirstLot, uint64_t extraCapacity, bool throwExceptionIfFailed=true );
		uint64_t SplitLot( uint64_t lotOffset, TerritoryHeader * territoryHeader );

		uint64_t & HashTableEntry( uint64_t hashValue );
		uint64_t FindFile( StringUtils::StringVector & path, uint64_t ensureMinimumPayloadSize, uint64_t ensureMinimumPayloadSizeInFirstLot );
		uint64_t FindDirectory( StringUtils::StringVector & path, DirectoryRecord ** directoryRecordOut=NULL );
		uint64_t GetParent( uint64_t fileOffset, DirectoryRecord ** directoryRecordOut=NULL );
		DirectoryRecord * GetDirectoryRecord( uint64_t directoryOffset );
		void SortDirectory( uint64_t directoryOffset );

		ReadWriteMutex *      mReadWriteMutex;
		double                mMutexTimeoutSeconds;
		SharedMemorySegment * mSegment;
		bool                  mOwnsSegmentPointer;
		uint64_t              mClientID;
		char *                mSegmentRootAddress;
															   
		static StringUtils::StringVector & StandardizeName( StringUtils::StringVector & resolvedParts, const std::string & name, char type=0 );
		
	public:
		static int Demo( int argc, const char * argv[] );		
		
		STRINGSUBCLASS( SegmentNotFoundError );
		STRINGSUBCLASS( BadHashTableSizeError );
		STRINGSUBCLASS( SegmentTooSmallError );
		STRINGSUBCLASS( OutOfSpaceError );
		STRINGSUBCLASS( WriteAccessDeniedError );
};

std::ostream & operator<<( std::ostream & os, SharedDataArchive & x );
std::ostream & operator<<( std::ostream & os, SharedDataArchive::File & x );	




//////////////////////////////////////////////////////////////////////
#endif  // INCLUDED_SharedDataArchive_HPP
