/*
# $BEGIN_DATAFLOW_LICENSE$
# 
# This file is part of the DataFlow project, a software library for
# efficient asynchronous multi-stream inter-process communication.
# 
# Copyright (c) 2014-2021 Jeremy Hill
# 
# DataFlow is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program. If not, see http://www.gnu.org/licenses/ .
# 
# Some third-party source code is compiled and linked into the
# DataFlow binary, as follows: devel/src/sem_timedwait_apple.cpp
# is adapted from sem_timedwait.cpp by Keith Shortridge, copyright
# (c) 2013 Australian Astronomical Observatory (AAO), released under
# the MIT License; devel/src/dirent_win.h is copyright (c) 2006
# Toni Ronkko, and released under the MIT License. The DataFlow build
# system also uses some third-party code:
# devel/build/cmake-recipes/DefineFunction_TARGET_ARCHITECTURE.cmake
# which is copyright (c) 2012 Petroules Corporation, released under
# the 2-Clause BSD License. For more details, see the respective
# copyright, licensing and disclaimer information in each of these
# files.
# 
# $END_DATAFLOW_LICENSE$
*/
#ifndef INCLUDE_DataFlow_API_CPP
#define INCLUDE_DataFlow_API_CPP
////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include "DataFlow_API.hpp"
#include "DebuggingUtils.hpp"

DF::Thing::Thing( const char * name )
: mShadow( NULL )
{
	mName = name ? name : "(null)";
	printf( "created Thing \"%s\" at %p\n", mName.c_str(), this );
}

DF::Thing::~Thing()
{
	printf( "destroying Thing \"%s\" at %p\n", mName.c_str(), this );
}

bool
DF::Thing::IsCalled( const std::string & name ) const
{
	return mName == name;
}

bool
DF::Thing::SameName( const DF::Thing * otherThing ) const
{
	return otherThing && mName == otherThing->mName;
}

std::string
DF::Thing::GetName( void ) const
{
	return mName;
}

const DF::Thing *
DF::Thing::FirstNamesake( int numberOfOtherThings, const DF::Thing * otherThings[] ) const
{
	if( !otherThings ) return NULL;
	for( int i = 0; i < numberOfOtherThings; i++ )
	{
		const DF::Thing * otherThing = otherThings[ i ];
		if( SameName( otherThing ) ) return otherThing;
	}
	return NULL;
}

const DF::Thing *
DF::Thing::GetMyAddress( void ) const
{
	return this;
}


DF::SharedDataArchive::SharedDataArchive( const std::string & name, uint64_t segmentSize, bool forceReformat, uint64_t numberOfHashTableSlots )
: mShadow( NULL ), mArchive( name, segmentSize, forceReformat, numberOfHashTableSlots ) { }

DF::SharedDataArchive::~SharedDataArchive() { }
void DF::SharedDataArchive::SynchronizeTimer( void ) { mArchive.SynchronizeTimer(); }
std::string DF::SharedDataArchive::Report( void ) { return mArchive.ReportHeader(); }
std::string DF::SharedDataArchive::Summary( void ) { return mArchive.Summary(); }
std::string DF::SharedDataArchive::GetName( void ) { return mArchive.GetName(); }
uint64_t DF::SharedDataArchive::GetClientID( void ) { return mArchive.GetClientID(); }
void DF::SharedDataArchive::SetSortingPreference( bool setting ) { mArchive.SetSortingPreference( setting ); }
uint64_t DF::SharedDataArchive::GetSpaceAvailable( bool includeVacatedLots ) { return mArchive.GetSpaceAvailable( includeVacatedLots ); }


DF::SharedDataArchive::File::File( DF::SharedDataArchive * sda, const char * name, uint64_t offset, char mode, uint64_t ensureMinimumPayloadSize, char type )
: mShadow( NULL ), mFile(
	name ? sda->mArchive.Open( name,   ( char )mode, ( uint64_t )ensureMinimumPayloadSize, (char )type )
	     : sda->mArchive.Open( offset, ( char )mode, (char )type ) // ensureMinimumPayloadSize cannot be used in this case and will be ignored
) { if( !name && !mFile.GetFileOffset() && ensureMinimumPayloadSize ) RAISE( "ensureMinimumPayloadSize cannot be used to auto-create a file without a file name" ) }

DF::SharedDataArchive::File::~File(){ }
std::string DF::SharedDataArchive::File::Report( void ) { return mFile.Report(); }
DF::SharedDataArchive::File * DF::SharedDataArchive::File::MakeReadOnly( void ) { mFile.MakeReadOnly(); return this; }
DF::SharedDataArchive::File * DF::SharedDataArchive::File::Seek( int64_t offset, int origin ) { mFile.Seek( offset, origin ); return this; }
DF::SharedDataArchive::File * DF::SharedDataArchive::File::Write( const char * data, int64_t nBytes ) { ( nBytes < 0 ) ? mFile.Write( data ) : mFile.Write( data, nBytes ); return this; }
DF::SharedDataArchive::File * DF::SharedDataArchive::File::Truncate( bool shrink ) { mFile.Truncate( shrink ); return this; }
DF::SharedDataArchive::File * DF::SharedDataArchive::File::Shrink( void ) { mFile.Shrink(); return this; }
DF::SharedDataArchive::File * DF::SharedDataArchive::File::Wrap( int64_t payloadCapacity, int64_t granularity ) { mFile.Wrap( payloadCapacity, granularity ); return this; }
DF::SharedDataArchive::File * DF::SharedDataArchive::File::AutoWrap( int64_t granularity ) { mFile.AutoWrap( granularity ); return this; }
DF::SharedDataArchive::File * DF::SharedDataArchive::File::Wipe( void ) { mFile.Wipe(); return this; }
uint64_t DF::SharedDataArchive::File::Read( void *buffer, uint64_t nBytesToRead ) { return mFile.Read( buffer, ( uint64_t )nBytesToRead ); }
uint64_t DF::SharedDataArchive::File::Tell( void ) { return mFile.Tell(); }
uint64_t DF::SharedDataArchive::File::TellMin( void ) { return mFile.TellMin(); }
uint64_t DF::SharedDataArchive::File::TellMax( void ) { return mFile.TellMax(); }
//uint64_t DF::SharedDataArchive::File::BytesUsed( void ) { return mFile.BytesUsed(); }
//uint64_t DF::SharedDataArchive::File::Capacity( void ) { return mFile.Capacity(); }
uint64_t DF::SharedDataArchive::File::AvailableContentLength( void ) { return mFile.AvailableContentLength(); }
uint64_t DF::SharedDataArchive::File::PayloadCapacityUsed( void ) { return mFile.PayloadCapacityUsed(); }
uint64_t DF::SharedDataArchive::File::PayloadCapacity( void ) { return mFile.PayloadCapacity(); }
uint64_t DF::SharedDataArchive::File::GetFileOffset( void ) { return mFile.GetFileOffset(); }
void DF::SharedDataArchive::File::Remove( void ) { mFile.Remove(); }
DF::SharedDataArchive::File * DF::SharedDataArchive::File::Open( uint64_t offset ) { mFile.Open( offset ); return this; }
DF::SharedDataArchive::File * DF::SharedDataArchive::File::Walk( uint64_t top, int includeDirectories, const char * restrictToTypes ) { mFile.Walk( top, includeDirectories, restrictToTypes ); return this; }
bool DF::SharedDataArchive::File::Exists( void ) { return mFile.Exists(); }
uint64_t DF::SharedDataArchive::File::Next( void ) { return mFile.Next(); }
const char * DF::SharedDataArchive::File::GetName( void ) { return mFile.GetName(); } 

void DF::SetDebugLevel( int level ) { DebuggingUtils::SetDebugLevel( level ); }
int  DF::RemoveKernelResource( const std::string & name ) { return KernelResources::Remove( name ); }

void DF::WaitForSignal( const char * name, double timeoutSeconds ) { NamedSemaphore( name ).WaitForSignal( timeoutSeconds ); }
void DF::SendSignal( const char * name ) { NamedSemaphore( name ).SendSignal(); }

#include "ConsoleUtils.hpp"
void DF::CatchFirstInterrupt( void ) { ConsoleUtils::CatchFirstInterrupt(); }
void DF::CatchFirstTermination( void ) { ConsoleUtils::CatchFirstTermination(); }
void DF::RaiseCaughtSignal( void ) { ConsoleUtils::RaiseCaughtSignal(); }
int DF::CaughtSignal( int setValue ) { return ConsoleUtils::CaughtSignal( setValue ); }

#include "TimeUtils.hpp"
double DF::PrecisionTime( bool relativeToFirstCall ) { return TimeUtils::PrecisionTime( relativeToFirstCall ); } // uses steady_clock.  On first call, steady_clock AND system_clock "origin" values will be recorded globally.  If relativeToFirstCall is true, elapsed steady_clock time is returned relative to this origin.
void DF::SleepSeconds( double seconds ) { TimeUtils::SleepSeconds( seconds ); }
double DF::SleepUntil( double targetPrecisionTime, bool relativeToFirstCall ) { return TimeUtils::SleepUntil( targetPrecisionTime, relativeToFirstCall ); } // sleep until after a particular specified PrecisionTime() value (return ASAP after the target, but accuracy is not guaranteed)


#include "FileUtils.hpp"
std::string DF::LibraryPath(    const std::string & relativePath, void * funcPtr ) { return FileUtils::LibraryPath( relativePath, funcPtr ); }
std::string DF::ExecutablePath( const std::string & relativePath ) { return FileUtils::ExecutablePath( relativePath ); }

void DF::License( void )
{
	std::cerr << R"ZARP(

# $BEGIN_DATAFLOW_LICENSE$
# 
# This file is part of the DataFlow project, a software library for
# efficient asynchronous multi-stream inter-process communication.
# 
# Copyright (c) 2014-2021 Jeremy Hill
# 
# DataFlow is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program. If not, see http://www.gnu.org/licenses/ .
# 
# Some third-party source code is compiled and linked into the
# DataFlow binary, as follows: devel/src/sem_timedwait_apple.cpp
# is adapted from sem_timedwait.cpp by Keith Shortridge, copyright
# (c) 2013 Australian Astronomical Observatory (AAO), released under
# the MIT License; devel/src/dirent_win.h is copyright (c) 2006
# Toni Ronkko, and released under the MIT License. The DataFlow build
# system also uses some third-party code:
# devel/build/cmake-recipes/DefineFunction_TARGET_ARCHITECTURE.cmake
# which is copyright (c) 2012 Petroules Corporation, released under
# the 2-Clause BSD License. For more details, see the respective
# copyright, licensing and disclaimer information in each of these
# files.
# 
# $END_DATAFLOW_LICENSE$

)ZARP"; // CPLUSPLUS11 R"()" literal


}


////////////////////////////////////////////////////////////////////
#endif // ifndef INCLUDE_DataFlow_API_CPP
