/*
# $BEGIN_DATAFLOW_LICENSE$
# 
# This file is part of the DataFlow project, a software library for
# efficient asynchronous multi-stream inter-process communication.
# 
# Copyright (c) 2014-2021 Jeremy Hill
# 
# DataFlow is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program. If not, see http://www.gnu.org/licenses/ .
# 
# Some third-party source code is compiled and linked into the
# DataFlow binary, as follows: devel/src/sem_timedwait_apple.cpp
# is adapted from sem_timedwait.cpp by Keith Shortridge, copyright
# (c) 2013 Australian Astronomical Observatory (AAO), released under
# the MIT License; devel/src/dirent_win.h is copyright (c) 2006
# Toni Ronkko, and released under the MIT License. The DataFlow build
# system also uses some third-party code:
# devel/build/cmake-recipes/DefineFunction_TARGET_ARCHITECTURE.cmake
# which is copyright (c) 2012 Petroules Corporation, released under
# the 2-Clause BSD License. For more details, see the respective
# copyright, licensing and disclaimer information in each of these
# files.
# 
# $END_DATAFLOW_LICENSE$
*/
#ifndef   INCLUDE_SharedMemorySegment_CPP
#define   INCLUDE_SharedMemorySegment_CPP

#include "SharedMemorySegment.hpp"
#include "ExceptionUtils.hpp"
#include "DebuggingUtils.hpp" // TODO: remove

#include <stdio.h>
#include <errno.h>

#ifdef    _WIN32
#	include <windows.h>
#else  // _WIN32 not defined
#	include <sys/mman.h>
#	include <sys/stat.h>
#	include <fcntl.h>
#	include <unistd.h>
#endif // _WIN32

std::string
SharedMemorySegment::LegalizeName( std::string name, bool fileBacked )
{
	std::string result;
	bool justHadSeparator = false;
#ifdef    _WIN32
	char sep = '\\';
	if( !fileBacked ) { sep = '!'; justHadSeparator = true; } // prevent non-file-backed segment identifiers from starting with a separator
#else  // _WIN32 not defined
	char sep = '/';
	if( !fileBacked ) { result += sep; justHadSeparator = true; } // non-file-backed segment identifiers *must* start with a slash
#endif // _WIN32
	for( unsigned int i = 0; i < name.length(); i++ )
	{
		char c = name[ i ];
		if( c == '\\' || c == '/' ) c = sep;
		if( c == sep && justHadSeparator ) continue;
		if( c == sep && i == name.length() - 1 ) continue;
		justHadSeparator = ( c == sep );
		result += c;
	}
	return result;
}

SharedMemorySegment::SharedMemorySegment( std::string name, bool fileBacked, size_t size )
{
	mPath = LegalizeName( name, fileBacked );
	mFileBacked = fileBacked;
	mSize = size;
	mData = NULL;
#ifdef    _WIN32
	mFileHandle = NULL;
#else  // _WIN32 not defined
	mFileHandle = -1;
#endif // _WIN32
}

SharedMemorySegment::~SharedMemorySegment()
{
	Close();
}

char *
SharedMemorySegment::Data( void )
{
	if( !mData ) { RAISE( "Shared memory segment \"" << mPath << "\" must be opened before it can be accessed" ) }
	return mData;
}

bool
SharedMemorySegment::IsOpen( void )
{
	return ( mData != NULL );
}

size_t
SharedMemorySegment::GetSize( void )
{
	return mSize;
}

void
SharedMemorySegment::SetSize( size_t bytes )
{
	mSize = bytes;
}


const char *
SharedMemorySegment::GetName( void )
{
	return mPath.c_str();
}

char *
SharedMemorySegment::Open( bool throwError )
{
#define RETURN_NULL_OR_RAISE( X )  { if( throwError ) { RAISE( X ); } else return NULL; }
	if( mData ) return mData;
	if( mFileBacked && mSize )
	{
		FILE * f = ::fopen( mPath.c_str(), "ab+" );
		if( !f ) RETURN_NULL_OR_RAISE( "Failed to open file \"" << mPath << "\"" );
		::fseek( f, 0, SEEK_END );
		if( ( size_t )::ftell( f ) < mSize )
		{
			DBREPORT( 0, mSize - ::ftell( f ) );
			for( size_t extra = mSize - ::ftell( f ); extra; extra-- ) fputc( '\0', f );
		}
		::fclose( f );
	}
#ifdef    _WIN32
	if( mFileBacked )
	{
		HANDLE h = ::CreateFileA( mPath.c_str(), GENERIC_READ | GENERIC_WRITE, FILE_SHARE_READ | FILE_SHARE_WRITE, 0, OPEN_EXISTING, 0, 0 );
		if( !h ) RETURN_NULL_OR_RAISE( "Failed to open file \"" << mPath << "\" (CreateFile error " << ::GetLastError() << ")" );
		DWORD lo, hi = 0;
		lo = ::GetFileSize( h, &hi );
		if( sizeof( mSize ) <= 4 ) mSize = lo;
		else
		{
			mSize = hi;
			mSize <<= 32;
			mSize += lo;
		}
		mFileHandle = ::CreateFileMappingA( h, NULL, PAGE_READWRITE, 0, 0, NULL );
		if( !mFileHandle ) RETURN_NULL_OR_RAISE( "Failed to create file mapping for \"" << mPath << "\" (CreateFileMapping error " << ::GetLastError() << ")" );
	}
	else
	{
		mFileHandle = NULL;
		if( mSize )
		{
			LARGE_INTEGER size;
			size.QuadPart = mSize;
			mFileHandle = ::CreateFileMappingA( INVALID_HANDLE_VALUE, NULL, PAGE_READWRITE, size.HighPart, size.LowPart, mPath.c_str() );
			if( mFileHandle == NULL && ::GetLastError() == ERROR_ALREADY_EXISTS ) RETURN_NULL_OR_RAISE( "Failed to create shared memory segment \"" << mPath << "\" (CreateFileMapping error " << ::GetLastError() << ")" );
		}
	    if( !mFileHandle )
		{
			mFileHandle = ::OpenFileMappingA( SECTION_QUERY | FILE_MAP_READ | FILE_MAP_WRITE, FALSE, mPath.c_str() );
		}
		if( !mFileHandle ) RETURN_NULL_OR_RAISE( "Failed to create mapping for shared memory segment \"" << mPath << "\" (OpenFileMapping error " << ::GetLastError() << ")" );
		// Query de-facto size.  NB: GetFileSize() does not work. The following solution from https://stackoverflow.com/a/47951175
		// is potentially non-future-proof as it relies on the undocumented Windows NT API. The NtQuerySection call is the reason for including the SECTION_QUERY permission above.
		typedef enum _SECTION_INFORMATION_CLASS { SectionBasicInformation, SectionImageInformation } SECTION_INFORMATION_CLASS;
		typedef struct _SECTION_BASIC_INFORMATION { PVOID Base; ULONG Attributes; LARGE_INTEGER Size; } SECTION_BASIC_INFORMATION;
		typedef DWORD ( WINAPI* NTQUERYSECTION )( HANDLE, SECTION_INFORMATION_CLASS, PVOID, ULONG, PULONG );
		NTQUERYSECTION NtQuerySection = ( NTQUERYSECTION )GetProcAddress( LoadLibrary( "ntdll.dll" ), "NtQuerySection" );
		if( NtQuerySection )
		{
			SECTION_BASIC_INFORMATION info = { 0 };
			DWORD error = NtQuerySection( mFileHandle, SectionBasicInformation, &info, sizeof( info ), NULL );
			if( error == 0 ) mSize = ( size_t )info.Size.QuadPart;
		}
	}
	mData = ( char * )::MapViewOfFile( mFileHandle, FILE_MAP_ALL_ACCESS, 0, 0, 0 );
#else  // _WIN32 not defined
	if( mFileBacked )
	{
		mFileHandle = ::open( mPath.c_str(), O_RDWR, S_IRUSR | S_IWUSR );
	}
	else
	{
		mFileHandle = -1;
		if( mSize )
		{
			errno = 0;
			mFileHandle = ::shm_open( mPath.c_str(), O_RDWR | O_CREAT | O_EXCL, S_IRUSR | S_IWUSR );
			if( mFileHandle < 0 && errno != EEXIST ) RETURN_NULL_OR_RAISE( "Failed to create shared memory segment \"" << mPath << "\"" );
			if( mFileHandle >= 0 && ::ftruncate( mFileHandle, mSize ) != 0 )
			{
				::close( mFileHandle );
				mFileHandle = -1;
			}
		}
		if( mFileHandle < 0 )
		{
			mFileHandle = ::shm_open( mPath.c_str(), O_RDWR, S_IRUSR | S_IWUSR );
		}
	}
	if( mFileHandle < 0 ) RETURN_NULL_OR_RAISE( "Failed to open " << ( mFileBacked ? "file" : "shared memory segment" ) << " \"" << mPath << "\"" );
	// Query de-facto size
	struct stat fileStat = { 0 };
	if( ::fstat( mFileHandle, &fileStat ) == 0 ) mSize = fileStat.st_size;
	
	mData = ( char * )::mmap( NULL, mSize, PROT_READ | PROT_WRITE, MAP_SHARED, mFileHandle, 0 );
	// NB: the second argument is supposed to be the size - what implications does it have to pass 0 here?
	if( mData == MAP_FAILED ) mData = NULL;
#endif // _WIN32
	if( !mData )
	{
		Close();
		RETURN_NULL_OR_RAISE( "Failed to create memory map for " << ( mFileBacked ? "file" : "shared memory segment" ) << " \"" << mPath << "\"" );
	}
	return mData;
}

void
SharedMemorySegment::Close( void )
{
#ifdef    _WIN32
	if( mData ) ::UnmapViewOfFile( mData );
	if( mFileHandle ) ::CloseHandle( mFileHandle );
	mFileHandle = NULL;
#else  // _WIN32 not defined
	if( mData ) ::munmap( mData, ( size_t )mSize );
	if( mFileHandle >= 0 ) ::close( mFileHandle );
	mFileHandle = -1;
#endif // _WIN32
	mData = NULL;
}

int
SharedMemorySegment::Remove( void )
{
	Close();
#ifdef    _WIN32
	if( mFileBacked ) return ( ::DeleteFileA( mPath.c_str() ) ? 0 : ::GetLastError() );
	else return 0; // will be auto-deleted when its last handle is closed
#else  // _WIN32 not defined
	if( mFileBacked ) return ( ::unlink( mPath.c_str() ) ? errno : 0 );
	else return ( ::shm_unlink( mPath.c_str() ) ? errno : 0 );
#endif // _WIN32
}

int
SharedMemorySegment::Remove( std::string name )
{
	int error = 0;
#ifdef    _WIN32
	
	// seems not to be necessary for non-file-backed segments---a segment will be auto-deleted when its last handle is closed
	// TODO: delete file?
	
#else  // _WIN32 not defined
	std::string legalizedName = LegalizeName( name, false );
	error = ( ::shm_unlink( legalizedName.c_str() ) ? errno : 0 );
	if( error )
	{
		legalizedName = LegalizeName( name, true );
		error = ( ::unlink( legalizedName.c_str() ) ? errno : 0 );
	}
#endif // _WIN32
	return error;
}

#include "NamedSemaphore.hpp"
#include "StringUtils.hpp"
#include "TimeUtils.hpp"
#include <stdio.h>
#include <stdlib.h>
#include <string>
int
SharedMemorySegment::Demo( int argc, const char * argv[] )
{
/*
	arguments: NAME [ SIZE_IN_BYTES  [ MESSAGE [ TIMES ] ] ]
	
	Create or open SharedMemorySegment with name NAME.
	If NAME ends with ".mmap" then the shared memory segment will be file-backed.
	
	SIZE_IN_BYTES > 0 will be required if the segment doesn't already exist.
	If it does exist, SIZE_IN_BYTES can be omitted or (equivalently) set to 0.
	Windows and Mac behave slightly differently on this point when it comes to
	non-file-backed memory segments:
		- On Windows, when all processes finish using a segment it will be destroyed,
		  so SIZE_IN_BYTES will be required again if the segment has been released.
		- On Darwin, the segment will only be listed in `lsof | grep PSXSHM` while
		  it is being used by an active process, but it can still be revived, even
		  with SIZE_IN_BYTES = 0, even after it has been released by all processes.
		  To remove it entirely, you need to call SharedMemorySegment::Remove()
		  which can be performed by calling this program with the string DELETE or
		  REMOVE in place of SIZE_IN_BYTES, or by using the separate `cleanup`
		  utility.

	MESSAGE is an optional string that will be written into the segment with sprintf.
	If TIMES is given, it specifies the number of repetitions of the same MESSAGE to
	be concatenated together. Null-termination is automatically added at the end.
	
	After opening the segment, and optionally writing the MESSAGE to it, the program
	will print the segment contents once per second for 10 seconds. This allows you
	to call another instance of the binary from another command prompt, and to
	observe changes that that other process makes to the shared memory content.
	
	A NamedSemaphore named according to NAME with the additional suffix "-lock" is
	created and used to coordinate between reads and writes to the segment. 
*/
	if( argc < 2 || argc > 5 ) { fprintf( stderr, "usage: %s NAME [ SIZE [ MSG [ TIMES ] ] ]\n", argv[ 0 ] ); return -1; }
	const char * name = argv[ 1 ];
	if( argc > 2 )
	{
		std::string arg2 = StringUtils::ToUpper( argv[ 2 ] );
		if( arg2 == "REMOVE" || arg2 == "RM" || arg2 == "DELETE" || arg2 == "DEL" || arg2 == "X" )
		{
			int err = SharedMemorySegment::Remove( name );
			if( err ) { RAISE( "failed to remove shared memory segment \"" << name << "\"" ); }
			else printf( "removed shared memory segment \"%s\"\n", name );
			return 0;
		}
	}
	char * end = NULL;
	size_t size = ( argc > 2 ?  strtoul( argv[ 2 ], &end, 10 ) : 0 );
	if( end && *end ) RAISE( "could not interpret \"" << argv[ 2 ] << "\" as an unsigned integer" )
	SharedMemorySegment m( name, StringUtils::EndsWith( name, ".mmap" ), size );
	m.Open();
	DBREPORT( 0, (void*)m.Data() );
	DBREPORT( 0, m.GetSize() );
	std::string semName = name;
	NamedSemaphore sem( semName + "-lock" );
	size_t timesToWrite = 1;
	if( argc > 4 )
	{
		timesToWrite = strtoul( argv[ 4 ], &end, 10 );
		if( end && *end ) RAISE( "could not interpret \"" << argv[ 4 ] << "\" as an unsigned integer" )
	}
	if( argc > 3 )
	{
		NamedSemaphore::ScopedLock lock( sem );
		char * dst = m.Data();
		size_t offset = 0;
		size_t available = m.GetSize();
		for( size_t i = 0; i < timesToWrite; i++ )
		{
			for( const char * src = argv[ 3 ]; src && *src && offset < available; src++ ) dst[ offset++ ] = *src;
		}
		dst[ offset < available ? offset++ : ( available - 1 ) ] = 0;
	}
	for( int i = 0; i < 10; i++ )
	{
		TimeUtils::SleepSeconds( 1 );
		NamedSemaphore::ScopedLock lock( sem );
		printf( "\"%s\"\n", m.Data() );
	}
	return 0;
}

// TODO:
//    resize??
//    
#endif // INCLUDE_SharedMemorySegment_CPP
