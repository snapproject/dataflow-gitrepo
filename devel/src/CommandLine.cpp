/*
# $BEGIN_DATAFLOW_LICENSE$
# 
# This file is part of the DataFlow project, a software library for
# efficient asynchronous multi-stream inter-process communication.
# 
# Copyright (c) 2014-2021 Jeremy Hill
# 
# DataFlow is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program. If not, see http://www.gnu.org/licenses/ .
# 
# Some third-party source code is compiled and linked into the
# DataFlow binary, as follows: devel/src/sem_timedwait_apple.cpp
# is adapted from sem_timedwait.cpp by Keith Shortridge, copyright
# (c) 2013 Australian Astronomical Observatory (AAO), released under
# the MIT License; devel/src/dirent_win.h is copyright (c) 2006
# Toni Ronkko, and released under the MIT License. The DataFlow build
# system also uses some third-party code:
# devel/build/cmake-recipes/DefineFunction_TARGET_ARCHITECTURE.cmake
# which is copyright (c) 2012 Petroules Corporation, released under
# the 2-Clause BSD License. For more details, see the respective
# copyright, licensing and disclaimer information in each of these
# files.
# 
# $END_DATAFLOW_LICENSE$
*/
#ifndef    INCLUDED_CommandLine_CPP
#define    INCLUDED_CommandLine_CPP
//////////////////////////////////////////////////////////////////////

#include "CommandLine.hpp"

#include "FileUtils.hpp"
#include <string.h>

CommandLine::InputStringStream &
operator>>( CommandLine::InputStringStream &ss, std::string &s )
{
	if( ss.peek() != EOF ) std::getline( ss, s, '\0' );
	return ss;
}

std::string
CommandLine::GetExecutableName( int argc, const char * argv[] )
{
	if( !argc || !argv[ 0 ] ) return "";
#	ifdef _WIN32
		return FileUtils::BaseName( argv[ 0 ], false );
#	else
		return FileUtils::BaseName( argv[ 0 ], true );
#	endif
}

const char *
CommandLine::FilterOption( int & argc, const char * argv[], const char * targetOption  )
{	/*
		If `targetOption` (e.g. "--verbose") is not found in `argv` from position 1 onwards, return NULL.
		If it is found, change `argc` and the contents of `argv` such that the option will be skipped in
		future, and return a pointer to the option's parameter value. (If the option is present but there
		is no parameter value, the pointer returned will be a non-NULL empty-string pointer, i.e. it will
		simply point to character '\0'.). If the option occurs multiple times, remove them all and return
		the value of the last one.
	*/
	int scanner, mover, shouldHaveEqualsSign;
	size_t targetOptionLength;
	const char * result = NULL;
	
	targetOptionLength = strlen( targetOption );
	shouldHaveEqualsSign = ( targetOption[ targetOptionLength - 1 ] == '=' );
	if( shouldHaveEqualsSign ) targetOptionLength--;
	// shouldHaveEqualsSign is now unused beyond this - enforcement can be left to the caller given that the return value lets them see the parameter value if any
	
	for( scanner = 1; scanner < argc; scanner++ )
	{
		const char * candidate = argv[ scanner ];
		if( strncmp( candidate, targetOption, targetOptionLength ) == 0 )
		{
			if( candidate[ targetOptionLength ] == '=' ) result = candidate + targetOptionLength + 1;
			else if( candidate[ targetOptionLength ] == '\0' ) result = candidate + targetOptionLength;
			for( --argc, mover = scanner; mover < argc; mover++ ) argv[ mover ] = argv[ mover + 1 ]; // argv[mover+1] is kosher because the upper bound on mover, argc, has just been decremented (this is nothing to do with the argv[argc]==NULL standard)
			scanner--;
		}
	}
	return result;
}

const char *
CommandLine::FirstOption( int argc, const char * argv[], const char * prefix )
{	/*
		Return a pointer to the first item in `argv` that starts with the required prefix,
		or NULL if there is no such string.  It is useful to run this after running FilterOption()
		several times to capture options you actually support.  Then FirstOption() will return the
		first unsupported/unrecognized option, if any.
 	*/
	int argi;
	size_t nPrefixCharsMatched, nPrefixChars = prefix ? strlen( prefix ) : 0;
	for( argi = 1; argi < argc; argi++ )
	{
		const char * arg = argv[ argi ];
		for( nPrefixCharsMatched = 0; prefix && arg[ nPrefixCharsMatched ] == *prefix; nPrefixCharsMatched++ ) prefix++;
		if( nPrefixCharsMatched == nPrefixChars ) return arg;
	}
	return NULL;
}


//////////////////////////////////////////////////////////////////////
#endif  // INCLUDED_CommandLine_CPP
