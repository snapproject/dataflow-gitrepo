/*
# $BEGIN_DATAFLOW_LICENSE$
# 
# This file is part of the DataFlow project, a software library for
# efficient asynchronous multi-stream inter-process communication.
# 
# Copyright (c) 2014-2021 Jeremy Hill
# 
# DataFlow is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program. If not, see http://www.gnu.org/licenses/ .
# 
# Some third-party source code is compiled and linked into the
# DataFlow binary, as follows: devel/src/sem_timedwait_apple.cpp
# is adapted from sem_timedwait.cpp by Keith Shortridge, copyright
# (c) 2013 Australian Astronomical Observatory (AAO), released under
# the MIT License; devel/src/dirent_win.h is copyright (c) 2006
# Toni Ronkko, and released under the MIT License. The DataFlow build
# system also uses some third-party code:
# devel/build/cmake-recipes/DefineFunction_TARGET_ARCHITECTURE.cmake
# which is copyright (c) 2012 Petroules Corporation, released under
# the 2-Clause BSD License. For more details, see the respective
# copyright, licensing and disclaimer information in each of these
# files.
# 
# $END_DATAFLOW_LICENSE$
*/
#ifndef   INCLUDED_Dimension_HPP
#define   INCLUDED_Dimension_HPP

#include "BasicTypes.hpp"
#include "StringUtils.hpp"

class Dimension
{
	public:
		Dimension( uint64_t extent=1, uint64_t stride=0 );
		Dimension( const StringUtils::StringVector & labels, uint64_t stride=0 );
		Dimension( const std::string & labels, uint64_t stride=0 );
		~Dimension();
	
		Dimension( const Dimension & other ) { *this = other; }
		Dimension & operator=( const Dimension & other );
		
		uint64_t                  GetExtent( void ) { return mExtent; }
		uint64_t                  GetStride( void ) { return mStride; }
		StringUtils::StringVector GetLabels( void ) { return mLabels; }
		std::string               GetLabel( uint64_t index );
		std::string               JoinedLabels( void );

		static const std::string StringDelim;
		
	private:
		uint64_t                  mExtent;
		uint64_t                  mStride;
		StringUtils::StringVector mLabels;
};
typedef std::vector< Dimension > DimensionVector;

#endif // INCLUDED_Dimension_HPP