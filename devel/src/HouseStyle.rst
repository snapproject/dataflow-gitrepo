.. default-role:: code


C/C++ house style
=================

- Use CamelCase with an initial capital for all classes and typedefs defined in our code,
  to distinguish them from STL/boost definitions.

- Append the container type or operator type to the name when typedeffing, e.g.::

      typedef vector< int >          IntVector;
      typedef allocator< IntVector > IntVectorAllocator;

- Use CamelCase also for method and function names.

- Use headlessCamelCase for variables:

  * initial letter `m` for class member variables;
  * initial letter `f` for struct fields;
  * initial letter `g` for global variables (but avoid them wherever possible!!);
  * initial letter `n` for counts (`nDimensions` is the number of `Dimension` objects);
  * initial letter `i` for loop indices (so `iDimension` is the index to `Dimension` objects
    within their `DimensionVector` or `DimensionArray` or whatever they're in---avoid using
    `i`, `j`, `k` etc unless the loop is very short and simple on the page, and not nested )

- Use ALL_CAPS_WITH_UNDERSCORES for preprocessor macros and global constants.

- In many ways, our house style is *verbose*:

  * Write out English words longhand in variable names (e.g.  `mDimensions` instead of `mDims`)
  
  * Wherever appropriate, include the physical units in the variable name, so you never 
    confuse a variable that represents `bufferLengthInSamples` with one that represents
    `bufferLengthInSeconds` and you can easily do unit-arithmetic at a glance to check whether
    a conversion is correct::
        
        double bufferLengthInSeconds = bufferLengthInSamples / samplesPerSecond;

  * Getter methods should start with `Get` (and, incidentally, should be made `const` wherever
    possible). Setter methods should start with `Set`.
      
  * Variables or functions that denote boolean qualities of other variables should start with
    `is` (or `mIs` or `fIs`, etc).  Examples::
    
        if( !isDummy )  mIsAlive = x->IsConnected() && IsLegal( y );

  * Use singular for single things, plural for containers-of-things or arrays  (so when you
    see a member variable called `mDimension` you know it's probably of class `Dimension`, whereas
    when you see mDimensions you know it's probably of class `DimensionVector`, or a `Dimension*`
    pointer).

- Dialect/cultural conventions emphasize efficient *universality of understanding* in an
  international collaborative environment:

  * Physical units should be metric, except where there's no widely-recognized alternative
    to a widely-recognized standard (does anyone really think in dots per centimetre??). 
  
  * Human-readable dates should *only ever* be written in ISO format: yyyy-mm-dd. It's the
    only widely-accepted standard that reliably signals *which* standard is being used, and
    furthermore is the only ordering that allows automated processing without inviting
    unnecessary bugs.
  
  * For similar reasons, human-readable times should only ever be written in 24-hour format
    with leading zero: 09:00:00 to 17:00:00
  
  * However, when it comes to spelling, *American* English is the lingua franca of code and
    has been for decades, so let's all get over it and write `backgroundColor`, never
    `backgroundColour`, when naming variables/parameters/etc.

- Use whitespace A LOT, even between brackets/braces/parentheses and their contents. Feel free
  to use spaces to line things up on the page wherever this will make it easier to spot bugs.
  Use TABS for indenting, but only to indent to the logical level of the code: once you've
  reached the correct logical indent level, switch to spaces if you want to line things up
  physically from there on. This lets other people customize the tab size in the editor/viewer
  of their choice, to whatever makes it easiest for them to see the flow control in your code,
  while still preserving any physical prettification/alignment of multiline comments or parallel
  constructions in the code itself.  Coding leading whitespace as `--->` for tabs and `.` for
  spaces, here's an example::

      --->if( mRightHanded )
      --->{
      --->--->if(      mHands[ "right" ]->Contents() == mHands[ "left" ]->Contents() )
      --->--->{
      --->--->--->DoNothing();    
      --->--->}
      --->--->else if( mHands[ "left"  ]->Contents() == "Sword"  
      --->--->......|| mHands[ "right" ]->Contents() == "Shield" )   // best of both worlds
      --->--->{
      --->--->--->SwapHands();
      --->--->}
      --->}

- Put opening braces on a line of their own::

      like
      {
          this
      }
      and not like {
          this
      }
      except sometimes if( the line is not too long && it will make things look tidier )
      {
          then { it's OK to do everything on one line; including both braces; }
          or even ( sometimes )
              this, with an indent but no braces at all;
          
          if( you do it all on one line without braces ) that can also be fine;  // But be careful:
          if( someCondition ) MACRO;  // is very dangerous without braces, because MACRO might expand to multiple statements
          
          // (IMO the decision to use these last three variants is really an aesthetic
          // one. They can all make the code easier to read, or harder to read,
          // depending on the shape. So do whatever you think looks clearest. )
      }
    
    
  And one exception to the rules: if an `extern "C"` block is long (like, the whole file)
  then feel free to do it all on one line and *not* indent the whole file::
    
      #ifdef __cplusplus
      extern "C" {
      #endif  /* ifdef __cplusplus */
      
      Yadda( yadda )
      {
         ...
         ...
         yadda->Yadda();
         ...
         ...
         ...
      }
      
      #ifdef __cplusplus
      } /* end extern "C" */
      #endif  /* ifdef __cplusplus */

- Indenting of preprocessor macros is at your discretion but if you do it, you
  must do it *between* the `#` and the keyword::
    
      #ifdef __cplusplus
      #    if USE_MAGIC
      #        include <magic>
      #    endif
      #endif
    
      // No, I don't like that either, but that's the standard, and some 
      // preprocessors will actually fall over and die if the # isn't the
      // absolute first character of the line.
    
- Put comments at the end of long multiline  `{ ... }`  or  `#if ...  #endif`
  blocks, to indicate which logical block is being closed. For example::
  
              ...
              ... /* pages and pages of code */
              ...
              } /* end iDimension loop */
              ...
          } /* end main event while-loop */
      }
      
      } /* end extern "C" */

- Scope variables low---keep them around only as long as needed. In pure C we
  have to do this::

      int iDimension;
      /*
          various
          statements
          ...
      */
      for( iDimension = 0; iDimension < nDimensions; iDimension++ ) { ... }

  but in C++, it's good to take full advantage of the ability to do the
  following, because it can cut down on bugs::

      for( int iDimension = 0; iDimension < nDimensions; iDimension++ ) { ... }
    
  The exception would be when construction and destruction of a complicated
  object would cause inefficiency, and the code could be optimized by defining
  a single instance outside a loop and re-using it.
    

Python
======        
        
Python house style is similar to the above (and unapologetically differs from PEP 8):
    
- Use CamelCase for the names of classes, methods and functions that are defined in
  our code, and headlessCamelCase for local variable names. Apart from the (perhaps
  subjective) readability improvement relative to `screenful_after_screenful_of_this`,
  it helps to distinguish our creations from keywords and standard library objects,
  and often from third-party objects.  It also helps to distinguish (at least 95% of
  the time) between attributes that are supposed to be read `like.this` (properties
  and plain data attributes) and attributes that are supposed to be read `like.This()`
  (methods or nested classes). Without such a convention, it's a memory game/guessing
  game as to whether you're supposed to say `if myThread.is_alive:` or
  `if myThread.is_alive():`. Necessarily, there are grey areas (e.g. attributes that
  are not methods but whose values happen to be callable object instances). 
  
- Use ALL_CAPS_WITH_UNDERSCORES for global (module-level) singletons and constants
  (or rather, things that are *intended* to remain constant---necessarily, there's
  less rigidity in these definitions than there is in C/C++).

- For the names of other instances, use headlessCamelCase including the same `i` and
  `n` prefixes as described above for C++. The one difference would be that you can and
  should omit the `m` or `f` for members/fields: a member (or, strictly, an attribute)
  like `self.bufferLength` is already clearly distinguishable from a local variable like
  `bufferLength` because of the necessary `self.` bit.

- Use tabs for logical indenting, followed by spaces for prettification if necessary
  (see above). And as above, don't be afraid to use whitespace to align almost-but-not-
  quite identical lines of code. PEP-8 discourages this (on entirely subjective grounds
  AFAICS) but in reality being able to traverse code vertically with your eyes helps
  in spotting copy-paste bugs, so that's a good-enough objective reason to do it.

- Be as conservative as possible in the Python version you support. Don't rush to use
  the latest novel language features no matter how elegant they are. The nature of
  the scientific Python ecosystem is that some of our users, and some of our computers,
  will be stuck two or three minor-versions back in time, for whatever annoying reason.

