/*
# $BEGIN_DATAFLOW_LICENSE$
# 
# This file is part of the DataFlow project, a software library for
# efficient asynchronous multi-stream inter-process communication.
# 
# Copyright (c) 2014-2021 Jeremy Hill
# 
# DataFlow is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program. If not, see http://www.gnu.org/licenses/ .
# 
# Some third-party source code is compiled and linked into the
# DataFlow binary, as follows: devel/src/sem_timedwait_apple.cpp
# is adapted from sem_timedwait.cpp by Keith Shortridge, copyright
# (c) 2013 Australian Astronomical Observatory (AAO), released under
# the MIT License; devel/src/dirent_win.h is copyright (c) 2006
# Toni Ronkko, and released under the MIT License. The DataFlow build
# system also uses some third-party code:
# devel/build/cmake-recipes/DefineFunction_TARGET_ARCHITECTURE.cmake
# which is copyright (c) 2012 Petroules Corporation, released under
# the 2-Clause BSD License. For more details, see the respective
# copyright, licensing and disclaimer information in each of these
# files.
# 
# $END_DATAFLOW_LICENSE$
*/
#ifndef INCLUDE_BasicTypes_HPP
#define INCLUDE_BasicTypes_HPP

#include <stddef.h> // for size_t
#include <stdint.h> // Defines 8-, 16-, 32- and 64-bit signed and unsigned integer types (int8_t, uint8_t, etc).
					// Also defines constants indicating limits: INT64_MIN, UINT32_MAX and friends.
                    // NB: if you change this to <cstdint> then you may get namespace problems (must include
					// this file outside of any of our own namespace definitions) but with stdint.h, it works.
typedef float  float32_t;
typedef double float64_t;

#if SIZE_MAX < UINT64_MAX
	size_t as_size_t( uint64_t i );
#else
#	define as_size_t( X )   ( X )
#endif

#define INCREMENT_WRAPPED( X, DATATYPE )   X = ( X + 1 ) % ( 1 << ( 8 * sizeof( DATATYPE ) ) )
#define IS_SANE( X )   ( ( ( double )( X ) - ( double )( X ) ) == ( ( double )( X ) - ( double )( X ) ) )  // false for NaNs and Infs

#endif // ifndef INCLUDE_BasicTypes_HPP
