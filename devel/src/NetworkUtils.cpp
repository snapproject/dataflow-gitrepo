/*
# $BEGIN_DATAFLOW_LICENSE$
# 
# This file is part of the DataFlow project, a software library for
# efficient asynchronous multi-stream inter-process communication.
# 
# Copyright (c) 2014-2021 Jeremy Hill
# 
# DataFlow is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program. If not, see http://www.gnu.org/licenses/ .
# 
# Some third-party source code is compiled and linked into the
# DataFlow binary, as follows: devel/src/sem_timedwait_apple.cpp
# is adapted from sem_timedwait.cpp by Keith Shortridge, copyright
# (c) 2013 Australian Astronomical Observatory (AAO), released under
# the MIT License; devel/src/dirent_win.h is copyright (c) 2006
# Toni Ronkko, and released under the MIT License. The DataFlow build
# system also uses some third-party code:
# devel/build/cmake-recipes/DefineFunction_TARGET_ARCHITECTURE.cmake
# which is copyright (c) 2012 Petroules Corporation, released under
# the 2-Clause BSD License. For more details, see the respective
# copyright, licensing and disclaimer information in each of these
# files.
# 
# $END_DATAFLOW_LICENSE$
*/

#ifndef INCLUDE_NetworkUtils_CPP
#define INCLUDE_NetworkUtils_CPP

/*
Here is the low-down on IPv6:

- The IPv6 loopback address is `::1` (the equivalent of `127.0.0.1` under IPv4).
  
- An address can be distinguished from a port suffix by enclosing the address portion in square brackets,
  e.g. `[fe80::821f:12ff:fee0:b028%eno1]:8081`

- Servers that `Listen()` over IPv6 are configured to allow "dual stack" connections. This means
  the server will accept connections originating over IPv6 *or* IPv4, with the latter being reported as
  coming from an IPv4 address with an added `::ffff:` prefix (e.g. `::ffff:169.254.227.231`).  Servers
  initialized with `SetIPVersion(4)` before `Listen()` will only be able to accept connections from IPv4
  clients.

- Clients who `ConnectToRemoteServer()` will use IPv6 if there is any `':'` character in the requested
  address string, and IPv4 otherwise. You can use addresses like `::ffff:169.254.176.40` to force IPv6
  even with an IPv4 address.
  
- IPv6 address beginning with `fe80:` are "link-local" addresses. These may not be unique, but they are
  unique within a given "scope". So, to use one of these, in principle you need to specify the scope ID of 
  the *local* adapter you're going through, as well as the remote address. This can be done by appending the
  name or decimal scope-ID number of the local adapter at end of the target remote IPv6 address, delimited
  by a `%` symbol (e.g. `fe80::821f:12ff:fee0:b028%eno1`). On Linux the numeric scope IDs reported by
  `ifconfig` output may be misleading (if so, the error message should help you find the right one, if you
  ask for a non-existent scope string). On Windows, the suffix after the `%` can *only* be numeric (find it
  in the `ipconfig` output---it seems to be accurate). The code here allows you to use a `@` instead of a
  `%` because of the trouble `%` causes on a Windows command-line.  In tests so far, Windows 10 actually
  seems to manage OK (connecting to an IPv6-enabled server) even if you omit the scope suffix, or get it
  wrong within certain limits (Linux/Darwin clients will not tolerate this omission at all and will say
  "no route to host.") Unfortunately, a given local network is likely to be denoted by different scope
  IDs on different computers that are attached to it.
  
  You may see IPv6 link-local addresses get self-assigned on networks that don't otherwise explicitly
  allocate IPv6 addresses (e.g. when attached to IPv6-capable routers that nonetheless are only configured
  to issue IPv4 addresses, via DHCP). This is due to stateless address autoconfiguration (SLAAC).
  
  The equivalent under IPv4, were the addresses in the `169.254.*.*` range. These are self-assigned via
  APIPIA under Windows, or by some other IPv4LL process (try `sudo avahi-autoipd -wD $ADAPTER_NAME` under
  Linux, or just set the "Link Local" option in the adapter-specific part of the Ubuntu network settings
  GUI). Many implementations seem to re-use the same addresses for the same adapters as much as possible.
    
*/

#include "NetworkUtils.hpp"

#include "ExceptionUtils.hpp"
#include "DebuggingUtils.hpp"
#include "StringUtils.hpp"
#include "ConsoleUtils.hpp"

#include <string.h> // strlen(), strerror(), memset()
#include <ctype.h>  // toupper()

#include <sstream>

#if ( defined( WIN32 ) || defined( _WIN32 ) || defined( __WIN32__ ) )
#	pragma comment(lib, "Ws2_32.lib")   // winsock2
#	pragma comment(lib, "IPHLPAPI.lib") // for GetAdaptersAddresses()
#	include <codecvt>
#	include <winsock2.h>
#	include <ws2tcpip.h>   // for inet_pton() and inet_ntop()
#	include <iphlpapi.h>   // for GetAdaptersAddresses()
#	define SOCKET_IS_VALID( S )  ( S != INVALID_SOCKET )
#	define CLOSE_SOCKET closesocket
#	define REPORT_LAST_SOCKET_ERROR  GetWindowsErrorString( ::WSAGetLastError() )
	typedef int socklen_t;
	std::string GetWindowsErrorString( DWORD errorCode, const std::string & prefix=" (", const std::string & suffix=")" )
	{
		if( errorCode == 0 ) return std::string();
		LPSTR messageBuffer = NULL;
		size_t size = FormatMessageA( FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS, NULL, errorCode, MAKELANGID( LANG_NEUTRAL, SUBLANG_DEFAULT ), ( LPSTR )&messageBuffer, 0, NULL );
		std::string message( messageBuffer, size );
		while( message.length() && ::isspace( message[ message.length() - 1 ] ) ) message = message.substr( 0, message.length() -1  );
		LocalFree( messageBuffer );
		if( message.length() ) message = prefix + message + suffix;
		return message;
	}
	void InitializeSockets( void )
	{
		static bool initialized = false;
		WSADATA wsa;
		if( !initialized && WSAStartup( MAKEWORD( 2, 2 ), &wsa ) != 0 )
			RAISE( "failed to initialize winsock" << REPORT_LAST_SOCKET_ERROR );
		initialized = true;
	}
#else
#	include <sys/socket.h> // for socket(), bind(), etc
#	include <arpa/inet.h>  // for sockaddr_in and inet_ntoa()
#	include <ifaddrs.h>    // for getifaddrs()
#	include <netdb.h>      // for getaddrinfo()
#	include <unistd.h>     // close()
#	include <errno.h>      // errno
#	define SOCKET_IS_VALID( S )     ( S >= 0 )
#	define CLOSE_SOCKET close
#	define REPORT_LAST_SOCKET_ERROR " (" << ::strerror( errno ) << ")"
	const SOCKET INVALID_SOCKET = -1; // SOCKET typedeffed in NetworkUtils.hpp
	void InitializeSockets( void ) {}
#endif
#define ALTERNATIVE_TO_PERCENT_SIGN '@'  // because % followed by a number causes havoc on the Windows command-line, allow this as an alternative

///////////////////////////////////////////////////////////////

#define IS_MULTICAST_IPV4( SIN_ADDR  )  ( 0xE0 == ( 0xF0 & ( ( unsigned char * )&( SIN_ADDR.s_addr   ) )[ 0 ] ) )
#define IS_MULTICAST_IPV6( SIN6_ADDR )  ( 0xFF ==          ( ( unsigned char * )&( SIN6_ADDR.s6_addr ) )[ 0 ]   )
#define IS_V6_LINKLOCAL_LOOPBACK( SIN6_ADDR ) ( !::memcmp( "\xFE\x80\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x01", &( ( SIN6_ADDR ).s6_addr), 16 ) )
#ifndef IN6_IS_ADDR_V4MAPPED
#define IN6_IS_ADDR_V4MAPPED( a ) \
		( ( ( ( a )->s6_words[ 0 ] ) == 0 ) && \
		  ( ( ( a )->s6_words[ 1 ] ) == 0 ) && \
		  ( ( ( a )->s6_words[ 2 ] ) == 0 ) && \
		  ( ( ( a )->s6_words[ 3 ] ) == 0 ) && \
		  ( ( ( a )->s6_words[ 4 ] ) == 0 ) && \
		  ( ( ( a )->s6_words[ 5 ] ) == 0xFFFF ) )
#endif

std::string
AddressString( struct ::in_addr & sin_addr )
{
	char buffer[ 64 ];  // must be at least INET_ADDRSTRLEN=16 for IPv4, INET6_ADDRSTRLEN=46 for IPv6
	return std::string( ::inet_ntop( AF_INET,  &sin_addr,  buffer, ( ::socklen_t )sizeof( buffer ) ) ? buffer : "???" );
}

std::string
AddressString( struct ::in6_addr & sin6_addr )
{
	char buffer[ 64 ];  // must be at least INET_ADDRSTRLEN=16 for IPv4, INET6_ADDRSTRLEN=46 for IPv6
	return std::string( ::inet_ntop( AF_INET6, &sin6_addr, buffer, ( ::socklen_t )sizeof( buffer ) ) ? buffer : "???" );
}
	
unsigned short Match( 
	unsigned int matchIndex, std::string addressString, unsigned short addressFamily,
	struct ::sockaddr_in  * a4, struct ::ip_mreq   * g4,
	struct ::sockaddr_in6 * a6, struct ::ipv6_mreq * g6,
	bool initializeOutputStructs=true
)
{ // what a nightmare
	
	// Tolerate the whole address being inside square brackets:
	if( addressString.length() && addressString[ 0 ] == '[' ) addressString = addressString.substr( 1 );
	if( addressString.length() && addressString[ addressString.length() - 1 ] == ']' ) addressString = addressString.substr( 0, addressString.length() - 1 );

	// See if there's a scope appended with a '%' delimiter (or a more windows-command-line-friendly alternative):
	std::string scopeString;
	const long scopeUnspecified   = -1;
	const long scopeNotNumeric    = -2;
	const long scopeIsIPv4Address = -3;
	const long scopeIsIPv6Address = -4;
	long scopeNumber = scopeUnspecified;
	struct ::in_addr  scopeLocalAddress4;
	struct ::in6_addr scopeLocalAddress6;
	size_t scopeDelimiterPosition = addressString.rfind( '%' );
	if( scopeDelimiterPosition == std::string::npos ) scopeDelimiterPosition = addressString.rfind( ALTERNATIVE_TO_PERCENT_SIGN );
	// Split the scope suffix off from the address:
	if( scopeDelimiterPosition != std::string::npos )
	{
		scopeString   = addressString.substr( scopeDelimiterPosition + 1 );
		addressString = addressString.substr( 0, scopeDelimiterPosition );
	}
	// If we don't already know which address family to use, and the address portion of the string is not blank, infer its family:
	if( addressString.length() && addressFamily == 0 )
		addressFamily = ( addressString.find( ':' ) == std::string::npos ) ? AF_INET : AF_INET6;
	if( scopeString.length() )
	{
		char * remainder = NULL;
		scopeNumber = ( scopeString.substr( 0, 2 ) == "0x" ) ? ::strtol( scopeString.c_str() + 2, &remainder, 16 ) :  ::strtol( scopeString.c_str(), &remainder, 10 );
		if( remainder && *remainder ) scopeNumber = scopeNotNumeric;
		else if( scopeNumber < 0 ) scopeNumber = scopeNotNumeric;
	}
	// The only way to specify scope in IPv4 seems to be as a local IPv4 address. So let's allow users to use an address as a scope suffix.
	// If the scope suffix is of this type, (a) try to convert it and (b) if conversion is successful, infer that we're using IPv4 if we don't already know:
	if( scopeString.length() && scopeNumber == scopeNotNumeric && ( addressFamily == 0 || addressFamily == AF_INET  ) )
		if( ::inet_pton( AF_INET,  scopeString.c_str(), &scopeLocalAddress4 ) ) { scopeNumber = scopeIsIPv4Address; addressFamily = AF_INET;  }
	// For completeness, let's also allow local IPv6 addresses to be used as scope strings, using the same logic:
	if( scopeString.length() && scopeNumber == scopeNotNumeric && ( addressFamily == 0 || addressFamily == AF_INET6 ) )
		if( ::inet_pton( AF_INET6, scopeString.c_str(), &scopeLocalAddress6 ) ) { scopeNumber = scopeIsIPv6Address; addressFamily = AF_INET6; }
	// Now if scopeNumber == scopeNotNumeric the only possible interpretation is that it is an interface name.
			
	// if all else fails (addressString was completely blank on entry, and addressFamily was 0) infer addressFamily based on which pointers were supplied
	if( !addressFamily ) addressFamily = ( a6 || g6 ) ? AF_INET6 : ( a4 || g4 ) ? AF_INET : AF_INET6;
	// assert addressFamily sanity
	if(      addressFamily == AF_INET  ) { a6 = NULL; g6 = NULL; if( !a4 && !g4 ) RAISE( "internal error - no IPv4 structures passed to Match()" ); }
	else if( addressFamily == AF_INET6 ) { a4 = NULL; g4 = NULL; if( !a6 && !g6 ) RAISE( "internal error - no IPv6 structures passed to Match()" ); }
	else RAISE( "internal error - invalid address family passed to Match()" );
	
	unsigned short a4port = ( a4 ? a4->sin_port  : 0 ); // save these and restore them at the end (either the caller has
	unsigned short a6port = ( a6 ? a6->sin6_port : 0 ); // already set them, or knows they still have to overwrite them).
	// zero the output structure(s)
	if( initializeOutputStructs )
	{
		if( a4 ) memset( a4, 0, sizeof( *a4 ) );
		if( a6 ) memset( a6, 0, sizeof( *a6 ) );
		if( g4 ) memset( g4, 0, sizeof( *g4 ) );
		if( g6 ) memset( g6, 0, sizeof( *g6 ) );
		if( a4 ) a4->sin_family  = AF_INET;
		if( a6 ) a6->sin6_family = AF_INET6;
	}	
	unsigned int nMatches = 0;
	std::stringstream possibilities;
	
#	if ( defined( WIN32 ) || defined( _WIN32 ) || defined( __WIN32__ ) )
		if( scopeNumber == scopeUnspecified ) nMatches++; // leaving everything zeroed is actually a match: Windows seems to figure it out.
		
		// on Windows, expect the scope to be numeric (unless we're using AF_INET and it's an actual IPv4 address)
		//if( scopeNumber == scopeNotNumeric ) RAISE( "could not interpret suffix " << StringUtils::StringLiteral( scopeString ) << " as a scope number" );
	
		if( nMatches <= matchIndex )
		{
			std::wstring_convert< std::codecvt_utf8_utf16< wchar_t > > converter;
			std::wstring wideScopeString = converter.from_bytes( scopeString );
			PIP_ADAPTER_ADDRESSES adapters = NULL;
			DWORD bytesRequired = 0;
			int errorCode;
			errorCode = GetAdaptersAddresses( AF_UNSPEC, GAA_FLAG_INCLUDE_PREFIX, 0, adapters, &bytesRequired ); // errorCode should be ERROR_BUFFER_OVERFLOW but bytesRequired should now contain the required buffer size
			adapters = ( PIP_ADAPTER_ADDRESSES )malloc( bytesRequired );
			errorCode = GetAdaptersAddresses( AF_UNSPEC, GAA_FLAG_INCLUDE_PREFIX, 0, adapters, &bytesRequired );
			if( errorCode ) { free( adapters ); RAISE( "failed to get adapter information" << GetWindowsErrorString( errorCode ) ); }
			for( PIP_ADAPTER_ADDRESSES adapter = adapters; adapter && nMatches <= matchIndex; adapter = adapter->Next )
			{
				for( PIP_ADAPTER_UNICAST_ADDRESS address = adapter->FirstUnicastAddress; address && nMatches <= matchIndex; address = address->Next )
				{
					if( !address->Address.lpSockaddr || address->Address.lpSockaddr->sa_family != addressFamily ) continue;
					if( addressFamily == AF_INET )
					{
						struct ::sockaddr_in  * candidate = reinterpret_cast< struct ::sockaddr_in  * >( address->Address.lpSockaddr );
						possibilities << " \"" << AddressString( candidate->sin_addr ) << "\", ";
						if(     scopeNumber == scopeUnspecified
						   || ( scopeNumber == scopeNotNumeric && adapter->FriendlyName && wideScopeString == adapter->FriendlyName )
						   || ( scopeNumber == scopeNotNumeric && adapter->AdapterName && scopeString == adapter->AdapterName )
						   || ( scopeNumber == scopeIsIPv4Address && !::memcmp( &scopeLocalAddress4.s_addr,  &candidate->sin_addr.s_addr,   sizeof( scopeLocalAddress4.s_addr  ) ) ) )
						{
							if( matchIndex == nMatches++ )
							{
								if( a4 ) *a4 = *candidate;
								if( g4 ) g4->imr_multiaddr = g4->imr_interface = candidate->sin_addr;   // for now (until overwritten), g4->imr_multiaddr is a local address, invalid for multicasting
								break;
							}
						}
					}
					if( addressFamily == AF_INET6 )
					{
						struct ::sockaddr_in6 * candidate = reinterpret_cast< struct ::sockaddr_in6 * >( address->Address.lpSockaddr );
						possibilities << candidate->sin6_scope_id << ", ";
						if(     scopeNumber == scopeUnspecified
						   || ( scopeNumber == scopeNotNumeric && adapter->FriendlyName && wideScopeString == adapter->FriendlyName )
						   || ( scopeNumber == scopeNotNumeric && adapter->AdapterName && scopeString == adapter->AdapterName )
						   || ( scopeNumber == scopeIsIPv6Address && !::memcmp( &scopeLocalAddress6.s6_addr, &candidate->sin6_addr.s6_addr, sizeof( scopeLocalAddress6.s6_addr ) ) )
						   ||   scopeNumber == candidate->sin6_scope_id )
						{
							if( matchIndex == nMatches++ )
							{
								if( a6 ) *a6 = *candidate;
								if( g6 ) { g6->ipv6mr_multiaddr = candidate->sin6_addr; g6->ipv6mr_interface = candidate->sin6_scope_id; } // for now (until overwritten), g6->ipv6mr_multiaddr is a local address, invalid for multicasting
								break;
							}
						}
					}
				}
			}
			free( adapters );
		}
#	else
		// elsewhere, the scope can be numeric, or it can be an adapter name
		struct ::ifaddrs * pList = NULL;
		::getifaddrs( &pList );
		for( ::ifaddrs * pAdapter = pList; pAdapter; pAdapter = pAdapter->ifa_next )
		{
			if( !pAdapter->ifa_addr || pAdapter->ifa_addr->sa_family != addressFamily ) continue;
			if( addressFamily == AF_INET )
			{
				struct ::sockaddr_in  * candidate = reinterpret_cast< struct ::sockaddr_in  * >( pAdapter->ifa_addr );
				possibilities
					<< " \"" << AddressString( candidate->sin_addr ) << "\""
					<< ": \"" << ( pAdapter->ifa_name ? pAdapter->ifa_name : "???" ) << "\",\n";
				if(     scopeNumber == scopeUnspecified
				   || ( scopeNumber == scopeIsIPv4Address && !::memcmp( &scopeLocalAddress4.s_addr, &candidate->sin_addr.s_addr, sizeof( scopeLocalAddress4.s_addr ) ) )
				   || ( scopeNumber == scopeNotNumeric && pAdapter->ifa_name && scopeString == pAdapter->ifa_name ) )
				{
					if( matchIndex == nMatches++ )
					{
						if( a4 ) *a4 = *candidate;
						if( g4 ) g4->imr_multiaddr = g4->imr_interface = candidate->sin_addr;  // for now (until overwritten), g4->imr_multiaddr is a local address, invalid for multicasting
						break;
					}
				}
			}
			if( addressFamily == AF_INET6 )
			{
				struct ::sockaddr_in6 * candidate = reinterpret_cast< struct ::sockaddr_in6 * >( pAdapter->ifa_addr );

				// stupid mac-specific corner case:
				if( IS_V6_LINKLOCAL_LOOPBACK( candidate->sin6_addr ) && scopeNumber == scopeUnspecified && addressString.length() )
				{
					struct ::in6_addr tmp_sin6_addr;
					if( !::inet_pton( AF_INET6, addressString.c_str(), &tmp_sin6_addr ) ) continue;
					if( !IS_V6_LINKLOCAL_LOOPBACK( tmp_sin6_addr ) ) continue;
				}

				possibilities << " " << candidate->sin6_scope_id << ": \"" << ( pAdapter->ifa_name ? pAdapter->ifa_name : "???" ) << "\",\n";
				if(     scopeNumber == scopeUnspecified
				   ||   scopeNumber == candidate->sin6_scope_id
				   || ( scopeNumber == scopeIsIPv6Address && !::memcmp( &scopeLocalAddress6.s6_addr, &candidate->sin6_addr.s6_addr, sizeof( scopeLocalAddress6.s6_addr ) ) )
				   || ( scopeNumber == scopeNotNumeric && pAdapter->ifa_name && scopeString == pAdapter->ifa_name ) )
				{
					if( matchIndex == nMatches++ )
					{
						if( a6 ) *a6 = *candidate;
						if( g6 ) { g6->ipv6mr_multiaddr = candidate->sin6_addr; g6->ipv6mr_interface = candidate->sin6_scope_id; }  // for now (until overwritten), g6->ipv6mr_multiaddr is a local address, invalid for multicasting
						break;
					}
				}
			}
		}
		::freeifaddrs( pList );
#	endif
	if( scopeString.length() && nMatches == 0 )
	{
		std::string p = possibilities.str();
		if( p.length() )
		{
			if( p[ 0 ] == ' ' ) p[ 0 ] = '{';
			else p = "{" + p;
			p = "; possibilities are:\n" + p.substr( 0, p.length() - 2 ) + "}";
		}
		RAISE( "could not find a match for scope " << StringUtils::StringLiteral( scopeString ) << p );
	}
	if( addressString.length() )
	{
		if( a4 && !::inet_pton( AF_INET,  addressString.c_str(), &a4->sin_addr         ) ) RAISE( "invalid IPv4 address " << StringUtils::StringLiteral( addressString ) );
		if( g4 && !::inet_pton( AF_INET,  addressString.c_str(), &g4->imr_multiaddr    ) ) RAISE( "invalid IPv4 address " << StringUtils::StringLiteral( addressString ) );
		
		// let's not prepend ::ffff: yet, because that won't work for joining multicast groups anyway
		if( g6 && !::inet_pton( AF_INET6, addressString.c_str(), &g6->ipv6mr_multiaddr ) ) RAISE( "invalid IPv6 address " << StringUtils::StringLiteral( addressString ) );
		// now we can add it, if needed:
		if( addressFamily == AF_INET6 && addressString.find( ':' ) == std::string::npos ) addressString = "::ffff:" + addressString; // an IPv6 encoding of an IPv4 address, e.g. ::ffff:169.254.176.40
		if( a6 && !::inet_pton( AF_INET6, addressString.c_str(), &a6->sin6_addr        ) ) RAISE( "invalid IPv6 address " << StringUtils::StringLiteral( addressString ) );
	}
	// NB: if there was no address prefix in addressString, any addresses returned will be local adapter addresses (so g4/g6 *_multiaddr will be invalid for multicasting)
	// Even if there were no matches, the sin*_family and sin*_addr fields will still be filled out to the extent possible (and the other fields zeroed)
	if( a4 ) a4->sin_port  = a4port;
	if( a6 ) a6->sin6_port = a6port;
	return ( nMatches <= matchIndex ) ? 0 : addressFamily;
}

bool
AvailableToRead( SOCKET socket, double timeoutSeconds ) // https://stackoverflow.com/a/44259633
{
	::fd_set descriptorSet;
	FD_ZERO( &descriptorSet );
	FD_SET( socket, &descriptorSet );
	struct ::timeval timeout;
	timeout.tv_sec = ( long )timeoutSeconds;
	timeout.tv_usec = ( long )( 1e6 * ( timeoutSeconds - timeout.tv_sec ) );
	int result = ::select( ( int )socket + 1, &descriptorSet, NULL, NULL, &timeout );
	//if( result < 0 ) RAISE( "select() failed" << REPORT_LAST_SOCKET_ERROR );
	return result > 0;
}

void
GetAddressPtr( struct ::sockaddr ** pointer, ::socklen_t * size, unsigned short addressFamily, struct ::sockaddr_in & a4, struct ::sockaddr_in6 & a6, bool initialize )
{
	if(      addressFamily == AF_INET  ) { *pointer = ( struct ::sockaddr * )&a4; *size = ( ::socklen_t )sizeof( a4 ); }
	else if( addressFamily == AF_INET6 ) { *pointer = ( struct ::sockaddr * )&a6; *size = ( ::socklen_t )sizeof( a6 ); }
	else RAISE( "bug: address family not configured" );
	if( initialize ) ::memset( *pointer, 0, ( size_t )*size );
}

void MakeSocketReusable( SOCKET mSocket )
{ // see https://stackoverflow.com/a/14388707 for tutorial
	const unsigned int yes = 1;
	if( ::setsockopt( mSocket, SOL_SOCKET, SO_REUSEADDR, ( const char * )&yes, sizeof( yes ) ) != 0 )
		RAISE( "setsockopt() failed while trying to enable SO_REUSEADDR" << REPORT_LAST_SOCKET_ERROR );
#	ifdef SO_REUSEPORT
		if( ::setsockopt( mSocket, SOL_SOCKET, SO_REUSEPORT, ( const char * )&yes, sizeof( yes ) ) != 0 )
			RAISE( "setsockopt() failed while trying to enable SO_REUSEPORT" << REPORT_LAST_SOCKET_ERROR );
#	endif
}

///////////////////////////////////////////////////////////////

NetworkUtils::Common::Common()
: mVerbosity( 0 ), mSocket( INVALID_SOCKET ), mSocketType( SOCK_STREAM ), mAddressFamily( AF_INET ),
  mFirstPort( 0 ), mLastPort( 0 ), mChosenPort( 0 ), mIsServer( false )
{ }

void
NetworkUtils::Common::SetVerbosity( int verbosity )
{
	mVerbosity = verbosity;
}

void
NetworkUtils::Common::CheckConfigurationAllowed( const char * attribute )
{
	if( SOCKET_IS_VALID( mSocket ) ) RAISE( "cannot change " << attribute << " while " << ( mIsServer ? "server is running" : "connection is open" ) );
}

bool
NetworkUtils::Common::SetProtocol ( std::string s, bool tolerant )
{
	CheckConfigurationAllowed( "socket type" );
	s = StringUtils::ToUpper( s );
	if(      s == "UDP" ) mSocketType = SOCK_DGRAM;
	else if( s == "TCP" ) mSocketType = SOCK_STREAM;
	else if( tolerant ) return false;
	else RAISE( "unrecognized socket type " << StringUtils::StringLiteral( s ) );
	return true;
}

void
NetworkUtils::Common::SetIPVersion( int version )
{
	CheckConfigurationAllowed( "address family" );
	if(      version == 4 ) mAddressFamily = AF_INET;
	else if( version == 6 ) mAddressFamily = AF_INET6;
	else RAISE( "IP version must be 4 or 6 - it cannot be " << version );
}

void
NetworkUtils::Common::SetIPVersion( const std::string & version )
{
	CheckConfigurationAllowed( "address family" );
	if(      version == "4" ) mAddressFamily = AF_INET;
	else if( version == "6" ) mAddressFamily = AF_INET6;
	else if( version.length() ) RAISE( "IP version must be 4 or 6 - it cannot be " << StringUtils::StringLiteral( version ) );
}

void
NetworkUtils::Common::SetPort( long first, long last )
{
	CheckConfigurationAllowed( "port number" );
	if( first < 1 || first > 65535 || last > 65535 ) RAISE( "port numbers must be in the range 1 to 65535" );
	mFirstPort = ( unsigned short )first;
	mLastPort  = ( unsigned short )( ( last <= 0 ) ? first : last );
	mChosenPort = mFirstPort;
}

void
NetworkUtils::Common::SetPort( const std::string & range )
{
	CheckConfigurationAllowed( "port number" );
	unsigned long first = 0, last = 0;
	char * remainder = NULL;
	first = ::strtoul( range.c_str(), &remainder, 10 );
	if( remainder && *remainder == '-' ) last = ::strtoul( remainder + 1, &remainder, 10 );
	if( remainder && *remainder ) RAISE( "could not interpret " << StringUtils::StringLiteral( range ) << " as a valid port range" );
	return SetPort( ( long )first, ( long )last );
}

void
NetworkUtils::Common::Configure( const std::string & config )
{
	StringUtils::StringVector parts;
	StringUtils::Split( parts, config, ":", '[', ']', true );
	bool specifiedProtocol = parts.size() && Common::SetProtocol( parts[ 0 ], true );
	if( specifiedProtocol ) parts.erase( parts.begin() );
	int portIndex = -1;
	bool isAddress;
	switch( parts.size() )
	{
		case 0:
			mDesiredAddress = "";
			portIndex = -1;
			break;
		case 1:
			isAddress = ( parts[ 0 ].find( '.' ) != std::string::npos || parts[ 0 ].find( ':' ) != std::string::npos );
			mDesiredAddress = isAddress ? parts[ 0 ] : "";
			portIndex       = isAddress ? -1 : 0;
			break;
		case 2:
			mDesiredAddress = parts[ 0 ];
			portIndex = 1;
			break;
		default:
			if( specifiedProtocol ) { RAISE( "configuration " << StringUtils::StringLiteral( config ) << " has too many colon-delimited parts. Put IPv6 addresses inside square brackets if you need to separate them from protocol and port specifiers." ); }
			else { mDesiredAddress = StringUtils::Join( parts, ":" ); portIndex = -1; }
			break;
	}
	while( mDesiredAddress.length() && mDesiredAddress[ 0 ] == '/' ) mDesiredAddress = mDesiredAddress.substr( 1 );
	if( mDesiredAddress.length() && mDesiredAddress[ 0 ] == '[' ) mDesiredAddress = mDesiredAddress.substr( 1 );
	if( mDesiredAddress.length() && mDesiredAddress[ mDesiredAddress.length() - 1 ] == ']' ) mDesiredAddress = mDesiredAddress.substr( 0, mDesiredAddress.length() - 1 );
	//if( mIsServer && mDesiredAddress.length() ) RAISE( "for servers, configuration is  [PROTOCOL:]PORT[-PORT] with no address" ); // *MMM* - note that if we really want to run a server only on a certain adapter, that will have to be implemented
	if( mDesiredAddress.length() )
	{
		// could determine whether already numeric, but doesn't seem necessary
		// TODO: we have no control over whether an IPv4 or IPv6 is interpretation of the name is preferred
		//       (we could use getaddrinfo's third argument `hint` to specify this, but the Configure() method
		//       itself currently has no way of knowing the caller's preference). Interestingly/oddly, when
		//       the client is winsock2, and the IP versions are mismatched in either direction, the first
		//       connection attempt hangs, but subsequent attempts succeed... (tested with a Mac as the server).
		//       When I reversed the roles (Windows server, Mac client doing the lookup), the Mac would
		//       consistently take the IPv6 interpretation first, and if the server was IPv6 this would hang
		//       forever.  The kludge solution below is to prefer IPv4 and fall back on IPv6 if that is not
		//       found, on the grounds that it is the client that will be doing the lookup, and an IPv4 client
		//       can connect to an IPv6 server but not vice versa.
		struct ::addrinfo *firstResult, *eachResult;
		const char * serviceString = ( portIndex >= 0 ) ? parts[ portIndex ].c_str() : NULL;
		InitializeSockets();
		int error = ::getaddrinfo( mDesiredAddress.c_str(), serviceString, NULL, &firstResult );
		if( !error )
		{
			std::string resolved;
			// Look for IPv4 results first, then fall back on IPv6
			for( eachResult = firstResult; eachResult != NULL && !resolved.length(); eachResult = eachResult->ai_next )
			{
				struct ::sockaddr_in  * sa4 = reinterpret_cast< struct ::sockaddr_in  * >( eachResult->ai_addr );
				if( eachResult->ai_addr->sa_family == AF_INET  ) resolved = AddressString( sa4->sin_addr );
				if( resolved.length() ) mDesiredAddress = resolved;
			}
			for( eachResult = firstResult; eachResult != NULL && !resolved.length(); eachResult = eachResult->ai_next )
			{
				struct ::sockaddr_in6 * sa6 = reinterpret_cast< struct ::sockaddr_in6 * >( eachResult->ai_addr );
				if( eachResult->ai_addr->sa_family == AF_INET6 ) resolved = AddressString( sa6->sin6_addr );
				if( resolved.length() ) mDesiredAddress = resolved;
			}
		}		
	}
	if( mDesiredAddress.length() ) mAddressFamily = ( mDesiredAddress.find( ':' ) == std::string::npos ) ? AF_INET : AF_INET6;
	if( portIndex >= 0 ) SetPort( parts[ portIndex ] );
}

void
NetworkUtils::Common::GetParameters( SOCKET * socket, int * socketType, unsigned short * addressFamily, int * verbosity ) const
{
	if( socket ) *socket = mSocket;
	if( socketType ) *socketType = mSocketType;
	if( addressFamily ) *addressFamily = mAddressFamily;
	if( verbosity ) *verbosity = mVerbosity;
}

std::string
NetworkUtils::Common::GetDesiredAddress( void ) const
{
	return mDesiredAddress;
}

std::string
NetworkUtils::Common::GetProtocolName( void ) const
{
	return ( mSocketType == SOCK_DGRAM ? "UDP" : mSocketType == SOCK_STREAM ? "TCP" : "???" );
}

int
NetworkUtils::Common::GetIPVersion( void ) const
{
	return ( mAddressFamily == AF_INET ? 4 : mAddressFamily == AF_INET6 ? 6 : 0 );
}

unsigned short
NetworkUtils::Common::GetPort( void ) const
{
	return mChosenPort;
}

void
NetworkUtils::Common::AllowDualStack( bool allow )
{ // Windows, unlike some others, defaults to disallow dual-stack (IPv4 and IPv6) connections. Let's fix that the way it's done at https://stackoverflow.com/a/67576369 :
	int v6OnlyEnabled = allow ? 0 : 1;
	if( mAddressFamily == AF_INET6 && ::setsockopt( mSocket, IPPROTO_IPV6, IPV6_V6ONLY, ( const char * )&v6OnlyEnabled, sizeof( v6OnlyEnabled ) ) != 0 ) RAISE( "setsockopt() failed to manipulate IPV6_V6ONLY" << REPORT_LAST_SOCKET_ERROR );
}

NetworkUtils::Server     & NetworkUtils::Server::SetMaxBacklog( int maxPendingConnections )      { CheckConfigurationAllowed( "backlog size" ); mMaxPendingConnections = maxPendingConnections; return *this; }
NetworkUtils::Server     & NetworkUtils::Server::SetProtocol ( std::string abbrev )              { Common::SetProtocol( abbrev );     return *this; }
NetworkUtils::Server     & NetworkUtils::Server::SetVerbosity( int verbosity )                   { Common::SetVerbosity( verbosity ); return *this; }
NetworkUtils::Server     & NetworkUtils::Server::SetIPVersion( int version )                     { Common::SetIPVersion( version );   return *this; }
NetworkUtils::Server     & NetworkUtils::Server::SetIPVersion( const std::string & version )     { Common::SetIPVersion( version );   return *this; }
NetworkUtils::Server     & NetworkUtils::Server::SetPort( long first, long last )                { Common::SetPort( first, last );    return *this; }
NetworkUtils::Server     & NetworkUtils::Server::SetPort( const std::string & range )            { Common::SetPort( range );          return *this; }
NetworkUtils::Server     & NetworkUtils::Server::Configure( const std::string & config )         { Common::Configure( config );       return *this; }

NetworkUtils::Connection & NetworkUtils::Connection::SetChunkSize( size_t chunkSize )            { mChunkSize = chunkSize ? chunkSize : DEFAULT_CHUNK_SIZE; delete [] mBuffer; mBuffer = new char[ mChunkSize ]; return *this; }
NetworkUtils::Connection & NetworkUtils::Connection::SetProtocol ( std::string abbrev )          { Common::SetProtocol( abbrev );     return *this; }
NetworkUtils::Connection & NetworkUtils::Connection::SetVerbosity( int verbosity )               { Common::SetVerbosity( verbosity ); return *this; }
NetworkUtils::Connection & NetworkUtils::Connection::SetIPVersion( int version )                 { Common::SetIPVersion( version );   return *this; }
NetworkUtils::Connection & NetworkUtils::Connection::SetIPVersion( const std::string & version ) { Common::SetIPVersion( version );   return *this; }
NetworkUtils::Connection & NetworkUtils::Connection::SetPort( long first, long last )            { Common::SetPort( first, last );    return *this; }
NetworkUtils::Connection & NetworkUtils::Connection::SetPort( const std::string & range )        { Common::SetPort( range );          return *this; }
NetworkUtils::Connection & NetworkUtils::Connection::Configure( const std::string & config )     { Common::Configure( config );       return *this; }

NetworkUtils::Server::Server()
{
	mAddressFamily = AF_INET6; // for maximum compatibility, we'll let connections default to IPv4 and servers default to IPv6 dual-stack
	mIsServer = true;
	mJoinedAnyMulticastGroup = false;
}

NetworkUtils::Server::~Server()
{
	StopServing();
}

void
NetworkUtils::Server::EnableMulticast( const std::string & multicastAddress )
{
	if( !multicastAddress.length() ) return;
	if( mSocketType != SOCK_DGRAM ) RAISE( "can only use multicast with UDP, not TCP" );
	MakeSocketReusable( mSocket );
	int numberOfAttempts = 0, numberOfSuccesses = 0;
	if( mAddressFamily == AF_INET )
	{
		struct ::ip_mreq group;
		for( unsigned int i = 0; Match( i, multicastAddress, mAddressFamily, NULL, &group, NULL, NULL ); i++ )
		{
			if( !IS_MULTICAST_IPV4( group.imr_multiaddr ) ) RAISE( multicastAddress << " is not a multicast address (first byte should be in the range 224-239, and for abitrary use 225-231 is best)" );
			numberOfAttempts++;
			if( ::setsockopt( mSocket, IPPROTO_IP, IP_ADD_MEMBERSHIP, ( const char * )&group, sizeof( group ) ) == 0 ) { numberOfSuccesses++; if( mVerbosity >= 2 ) std::cerr << "  interface %" << AddressString( group.imr_interface ) << " joined multicast group " << multicastAddress << std::endl;}
			else if( mVerbosity >= 2 ) std::cerr <<  "  interface %" << AddressString( group.imr_interface ) << " failed to join multicast group " << multicastAddress << REPORT_LAST_SOCKET_ERROR << std::endl;
		}
	}		
	if( mAddressFamily == AF_INET6 )
	{
		struct ::ipv6_mreq group;
		for( unsigned int i = 0; Match( i, multicastAddress, mAddressFamily, NULL, NULL, NULL, &group ); i++ )
		{
			if( !IS_MULTICAST_IPV6( group.ipv6mr_multiaddr ) ) RAISE( multicastAddress << " is not a multicast address (should start with FF, and for arbitrary uses FFxA through FFxD is best)" );
			// NB: the check above does not consider ::ffff:<valid IPv4 multicast address> to be multicast addresses.  setsockopt() will fail with such addresses anyway.
			numberOfAttempts++;
			if( ::setsockopt( mSocket, IPPROTO_IPV6, IPV6_JOIN_GROUP, ( const char * )&group, sizeof( group ) ) == 0 ) { numberOfSuccesses++; if( mVerbosity >= 2 ) std::cerr << "  interface %" << group.ipv6mr_interface << " joined multicast group " << multicastAddress << std::endl;}
			else if( mVerbosity >= 2 ) std::cerr <<  "  interface %" << group.ipv6mr_interface << " failed to join multicast group " << multicastAddress << REPORT_LAST_SOCKET_ERROR << std::endl;
			// NB:  IPV6_ADD_MEMBERSHIP is not defined on darwin, so we use IPV6_JOIN_GROUP which is identically defined
		}
	}	
	if( mVerbosity == 1 ) std::cerr << "joined multicast group " << multicastAddress << " on " << numberOfSuccesses << " of " << numberOfAttempts << " interfaces" << std::endl;
	if( numberOfSuccesses == 0 ) RAISE( "could not join multicast group " << multicastAddress << " on any network interface" );
	mJoinedAnyMulticastGroup = true;
}

void
NetworkUtils::Server::TryPort( unsigned short portNumber )
{
	if( !portNumber ) RAISE( "cannot serve on port 0" );
	if(      mAddressFamily == AF_INET  ) mAddress4.sin_port  = htons( portNumber );
	else if( mAddressFamily == AF_INET6 ) mAddress6.sin6_port = htons( portNumber );
	mChosenPort = portNumber;
}

unsigned short
NetworkUtils::Server::Listen( const std::string & config )
{
	InitializeSockets();
	if( config.length() ) Configure( config );

	int domain;
	struct ::sockaddr * addressPtr;
	::socklen_t addressSize;
	GetAddressPtr( &addressPtr, &addressSize, mAddressFamily, mAddress4, mAddress6, true );
	
	if( mAddressFamily == AF_INET )
	{
		domain = PF_INET;
		mAddress4.sin_family = AF_INET;
		mAddress4.sin_addr.s_addr = htonl( INADDR_ANY );
	}
	else if( mAddressFamily == AF_INET6 )
	{
		domain = PF_INET6;
		mAddress6.sin6_family = AF_INET6;
		mAddress6.sin6_addr = in6addr_any;
		mAddress6.sin6_scope_id = 0;
	}
	else RAISE( "unknown address family " << mAddressFamily );
	
	mJoinedAnyMulticastGroup = false;
	mSocket = ::socket( domain, mSocketType, 0 );
	if( !SOCKET_IS_VALID( mSocket ) )  RAISE( "failed to create network socket" << REPORT_LAST_SOCKET_ERROR );
	AllowDualStack();
	bool bound = false, joinedThisMulticastGroup = false;	
	for( unsigned int i = 0; !bound; i++ )
	{
		if( mDesiredAddress.length() && !Match( i, mDesiredAddress, mAddressFamily, &mAddress4, NULL, &mAddress6, NULL ) ) break;
		for( mChosenPort = mFirstPort; mChosenPort <= mLastPort; mChosenPort++ )
		{
			if( ( mAddressFamily == AF_INET  && IS_MULTICAST_IPV4( mAddress4.sin_addr  ) ) 
			 || ( mAddressFamily == AF_INET6 && IS_MULTICAST_IPV6( mAddress6.sin6_addr ) ) )
			{
				if( !joinedThisMulticastGroup )
				{
					EnableMulticast( mDesiredAddress ); // EnableMulticast() can be called after Listen(), but when multicast is explicitly requested already, calling it here ensures the address- and port-reusability flags are set on the socket *before* binding, allowing this to be the second (or later) process to bind to the same address on the same computer
					joinedThisMulticastGroup = true;
#					if ( defined( WIN32 ) || defined( _WIN32 ) || defined( __WIN32__ ) )
						mAddress4.sin_addr.s_addr  = htonl( INADDR_ANY );
						mAddress6.sin6_addr = in6addr_any;
#					endif
				}
			}
			TryPort( mChosenPort );
			if( ::bind( mSocket, addressPtr, addressSize ) == 0 ) { bound = true; break; }
		}
		if( !mDesiredAddress.length() ) break;
	}
	if( !bound )
	{
		if( !mLastPort || mLastPort == mFirstPort ) { RAISE( "failed to attach network socket to port " << mFirstPort << REPORT_LAST_SOCKET_ERROR ); }
		else { RAISE( "failed to attach network socket to any port in the range " << mFirstPort << "-" << mLastPort << REPORT_LAST_SOCKET_ERROR ); }
	}
	if( mSocketType == SOCK_STREAM && ::listen( mSocket, mMaxPendingConnections ) != 0 ) RAISE( "listen() failed" << REPORT_LAST_SOCKET_ERROR );
	if( mVerbosity >= 1 ) std::cerr << "listening on port " << mChosenPort << " using " << GetProtocolName() <<  " over IPv" << ( mAddressFamily == AF_INET6 ? 6 : 4 ) << std::endl;
		
	return mChosenPort;
}

void
NetworkUtils::Server::StopServing( void )
{
	if( SOCKET_IS_VALID( mSocket ) )
	{
		CLOSE_SOCKET( mSocket );
		if( mVerbosity >= 1 ) std::cerr << "server stopped\n";
	}
	mSocket = INVALID_SOCKET;
}

NetworkUtils::Connection
NetworkUtils::Server::NextConnection( double timeoutSeconds, size_t chunkSize )
{
	return NetworkUtils::Connection( *this, timeoutSeconds, chunkSize );
}

void
NetworkUtils::Server::Echo( void ) // illustrates how a server can loop through incoming connections
{
	for( Connection c = NextConnection(); c.HasData(); c.Receive() )
	{
		if( mVerbosity >= 1 ) std::cerr << "  handling " << c.mBytesReceived << "-byte chunk " << StringUtils::StringLiteral( c.mBuffer, c.mBytesReceived ) << " from " << c.RemoteAddress() << std::endl;
		for( long i = 0; i < c.mBytesReceived; i++ ) c.mBuffer[ i ] = ::toupper( c.mBuffer[ i ] );
		c.Send( c.mBuffer, c.mBytesReceived ); // echo
	}
}

///////////////////////////////////////////////////////////////

#define CONNECTION_INIT \
	mBytesReceived( 0 ), mTotalBytesReceived( 0 ), mBytesSent( 0 ), mBorrowedSocket( false ), \
	mChunkSize( chunkSize ? chunkSize : DEFAULT_CHUNK_SIZE ), \
	mBuffer( new char[ chunkSize ? chunkSize : DEFAULT_CHUNK_SIZE ] )

NetworkUtils::Connection::Connection( size_t chunkSize ) : CONNECTION_INIT {}
NetworkUtils::Connection::Connection( const std::string & remoteServerIP, unsigned short remoteServerPort, const char * protocol, int verbosity, size_t chunkSize ) : CONNECTION_INIT { mVerbosity = verbosity; ConnectToRemoteServer( remoteServerIP, remoteServerPort, protocol ); }
NetworkUtils::Connection::Connection( const Server & localServer, double timeoutSeconds, size_t chunkSize ) : CONNECTION_INIT { Accept( localServer, timeoutSeconds ); }

NetworkUtils::Connection::~Connection()
{
	Close();
	delete [] mBuffer;
}

STRINGSUBCLASS( NoMoreMulticastSendingOptions );

unsigned int
NetworkUtils::Connection::ConnectToRemoteServer( std::string target, unsigned short portNumber, const char * protocol, unsigned int multicastSendingIndex )
{		
	unsigned int returnValue = 0;
	Configure( target );
	target = mDesiredAddress; // Configure() will have isolated the address portion by stripping the optional UDP: or TCP: prefix, and the :port  or :portMin-portMax suffix
	if( !target.length() ) target = ( mAddressFamily == AF_INET ) ? "127.0.0.1" : "::1";
	
	if( portNumber != 0 ) mFirstPort = mLastPort = mChosenPort = portNumber; // optional second arg can override whatever port suffix was in the string
	if( protocol ) SetProtocol( protocol ); // optional third arg ("UDP" or "TCP") can override whatever protocol prefix was in the string (or the default TCP)
	
	if( mLastPort && mLastPort != mFirstPort ) RAISE( "must supply a single port number, not a range, when connecting to a remote server" ); // TODO - could be exploratory, with timeouts
	if( !mChosenPort ) RAISE( "no remote port number specified" );
	
	InitializeSockets();
	Close();
	mTotalBytesReceived = 0;

	int domain;
	struct ::sockaddr * addressPtr;
	::socklen_t addressSize;
	GetAddressPtr( &addressPtr, &addressSize, mAddressFamily, mRemoteAddress4, mRemoteAddress6, true );
	Match( 0, target, mAddressFamily, &mRemoteAddress4, NULL, &mRemoteAddress6, NULL );
	
	if( mAddressFamily == AF_INET )
	{
		domain = PF_INET;
		mRemoteAddress4.sin_port = htons( mChosenPort );
	}
	else if( mAddressFamily == AF_INET6 )
	{
		domain = PF_INET6;
		mRemoteAddress6.sin6_port = htons( mChosenPort );
	}
	else RAISE( "unknown address family " << mAddressFamily );
	
	//if( mVerbosity >= 1 ) std::cerr << "attempting " << GetProtocolName() << " connection to server at " << target << " on port " << mChosenPort << std::endl;
	mSocket = ::socket( domain, mSocketType, 0 );
	if( !SOCKET_IS_VALID( mSocket ) ) RAISE( "failed to create network socket" << REPORT_LAST_SOCKET_ERROR );
	AllowDualStack();
	if( mAddressFamily == AF_INET && IS_MULTICAST_IPV4( mRemoteAddress4.sin_addr ) )
	{
		MakeSocketReusable( mSocket );
		
		struct ::ip_mreq group;
		struct ::in_addr localScopeThatWorks;
		unsigned int nAttempts = 0, nSuccesses = 0;
		for( unsigned int i = 0; Match( i, target, mAddressFamily, NULL, &group, NULL, NULL ); i++ )
		{
			nAttempts++;
			if( ::setsockopt( mSocket, IPPROTO_IP, IP_ADD_MEMBERSHIP, ( const char * )&group, sizeof( group ) ) == 0 )
			{
				if( nSuccesses++ == multicastSendingIndex )
					localScopeThatWorks = group.imr_interface;
			}
		}
		if( mVerbosity >= 1 ) std::cerr << "joined multicast group on " << nSuccesses << " of " << nAttempts << " interfaces\n";
		if( nSuccesses == 0 ) RAISE( "failed to join multicast group" )
		if( nSuccesses <= multicastSendingIndex ) { RAISE_SUBCLASS( NoMoreMulticastSendingOptions, "ran out of interface options for multicast sending" ); }
		returnValue = nSuccesses - multicastSendingIndex - 1;
		// Specify which interface will be used for SENDING datagrams (if we've got this far, localScopeThatWorks must have been successfully assigned at least once):
		if( ::setsockopt( mSocket, IPPROTO_IP, IP_MULTICAST_IF, ( const char * )&localScopeThatWorks, sizeof( group.imr_interface ) ) != 0 )
			RAISE( "setsockopt() failed while trying to select the interface for sending to the multicast address" << REPORT_LAST_SOCKET_ERROR );
	}
	else if( mAddressFamily == AF_INET6 && IS_MULTICAST_IPV6( mRemoteAddress6.sin6_addr ) )
	{
		MakeSocketReusable( mSocket );
		
		struct ::ipv6_mreq group;
		unsigned int nAttempts = 0, nSuccesses = 0;
		unsigned int localScopeThatWorks;
		for( unsigned int i = 0; Match( i, target, mAddressFamily, NULL, NULL, NULL, &group ); i++ )
		{
			nAttempts++;
			if( ::setsockopt( mSocket, IPPROTO_IPV6, IPV6_JOIN_GROUP, ( const char * )&group, sizeof( group ) ) == 0 )
			{
				if( nSuccesses++ == multicastSendingIndex )
					localScopeThatWorks = group.ipv6mr_interface;
			}
			// NB: IPV6_ADD_MEMBERSHIP is not defined on darwin, so we use IPV6_JOIN_GROUP which is identically defined
		}
		if( mVerbosity >= 1 ) std::cerr << "joined multicast group on " << nSuccesses << " of " << nAttempts << " interfaces\n";
		if( nSuccesses == 0 ) RAISE( "failed to join multicast group" )
		if( nSuccesses <= multicastSendingIndex ) RAISE_SUBCLASS( NoMoreMulticastSendingOptions, "ran out of interface options for multicast sending" );
		returnValue = nSuccesses - multicastSendingIndex - 1;
			
		// Specify which interface will be used for SENDING datagrams (if we've got this far, localScopeThatWorks must have been successfully assigned at least once):
		if( ::setsockopt( mSocket, IPPROTO_IPV6, IPV6_MULTICAST_IF, ( const char * )&localScopeThatWorks, sizeof( localScopeThatWorks ) ) != 0 )
			RAISE( "setsockopt() failed while trying to select the interface for sending to the multicast address" << REPORT_LAST_SOCKET_ERROR );
	}
	else if( multicastSendingIndex ) RAISE( "cannot configure the multicast sending interface with a non-multicast address" );
	int error = 0;
	for( unsigned int i = 0; Match( i, target, mAddressFamily, &mRemoteAddress4, NULL, &mRemoteAddress6, NULL, i==0 ); i++ )
	{
		error = ::connect( mSocket, addressPtr, addressSize ); // TODO: can hang indefinitely - need timeout by putting socket into non-blocking mode and using select()
		if( !error ) break;
		if( mVerbosity >= 1 && mAddressFamily == AF_INET6 ) std::cerr << "failed to connect to " << target << " using scope " << mRemoteAddress6.sin6_scope_id << REPORT_LAST_SOCKET_ERROR << std::endl;
		if( ConsoleUtils::CaughtSignal() ) break;
	}
	if( error ) RAISE( "failed to make a " << GetProtocolName() << " connection to " << RemoteAddress() << " on port " << mChosenPort << REPORT_LAST_SOCKET_ERROR );
	if( mVerbosity >= 1 && SOCKET_IS_VALID( mSocket ) ) std::cerr << "opened " << GetProtocolName() << " connection to server at " << RemoteAddress() << " on port " << mChosenPort << std::endl;
	mDirection = " to ";
	return returnValue;
}

long
NetworkUtils::Connection::Accept( const Server & localServer, double timeoutSeconds )
{
	Close();
	mTotalBytesReceived = 0;
	
	SOCKET localServerSocket;
	localServer.GetParameters( &localServerSocket, &mSocketType, &mAddressFamily, &mVerbosity );
	mChosenPort = localServer.GetPort();
	
	struct ::sockaddr * addressPtr;
	::socklen_t addressSize;
	GetAddressPtr( &addressPtr, &addressSize, mAddressFamily, mRemoteAddress4, mRemoteAddress6, true );
	
	double totalSecondsWaited = 0.0, eachWaitSeconds = 0.1;
	if( timeoutSeconds >= 0.0 && timeoutSeconds < eachWaitSeconds ) eachWaitSeconds = timeoutSeconds;
	while( !ConsoleUtils::CaughtSignal() )                               // * 
	{                                                                    // * All of this is needed to make the server
		if( AvailableToRead( localServerSocket, eachWaitSeconds ) )      // * ctrl-C-interruptible on Windows
		{                                                                // * On POSIXoids, accept() itself is interruptible.
			if( ConsoleUtils::CaughtSignal() ) mSocket = INVALID_SOCKET; // * 
			else if( mSocketType == SOCK_STREAM ) mSocket = ::accept( localServerSocket, addressPtr, &addressSize ); // Wait for a client to connect
			else if( mSocketType == SOCK_DGRAM  ) mSocket = localServerSocket; // accept (obtaining address info) and receipt of data will happen together in ::recvfrom
			break;
		}
		if( timeoutSeconds >= 0.0 && ( totalSecondsWaited += eachWaitSeconds ) >= timeoutSeconds ) break;
	}
	if( SOCKET_IS_VALID( mSocket ) )
	{
		mDirection = " from ";
		if( mSocketType == SOCK_DGRAM ) { Receive(); mBorrowedSocket = true; } // don't close the socket (it belongs to the server) and don't try to Receive() more than once (the second data packet could come from a different client)
		if( mVerbosity >= 1 ) std::cerr << "accepted connection from " << RemoteAddress() << std::endl;
		if( mSocketType != SOCK_DGRAM ) Receive();
	}
	return mBytesReceived;
}

bool
NetworkUtils::Connection::Opened( void )
{
	return SOCKET_IS_VALID( mSocket );
}

const char *
NetworkUtils::Connection::RemoteAddress( void )
{
	if( !SOCKET_IS_VALID( mSocket ) ) return "nowhere";
	if( !mRemoteAddressString.length() )
	{
		if( mAddressFamily == AF_INET )
		{
			mRemoteAddressString = AddressString( mRemoteAddress4.sin_addr );
		}
		else if( mAddressFamily == AF_INET6 )
		{
			mRemoteAddressString = AddressString( mRemoteAddress6.sin6_addr );
			//if( IN6_IS_ADDR_LINKLOCAL( &mRemoteAddress6.sin6_addr ) )
			//if( mRemoteAddress6.sin6_scope_id )
			if( !IN6_IS_ADDR_V4MAPPED( &mRemoteAddress6.sin6_addr ) )
				{ std::stringstream ss; ss << "%" << mRemoteAddress6.sin6_scope_id; mRemoteAddressString += ss.str(); }
		}
		else RAISE( "unknown address family " << mAddressFamily );
	}
	return mRemoteAddressString.c_str();
}

long
NetworkUtils::Connection::Receive( void )
{
	if( !SOCKET_IS_VALID( mSocket ) || !mBuffer || mBorrowedSocket )
		mBytesReceived = 0;
	else if( mSocketType == SOCK_STREAM )
		mBytesReceived = ::recv( mSocket, mBuffer, ( int )mChunkSize, 0 );
	else if( mSocketType == SOCK_DGRAM )
	{
		struct ::sockaddr * addressPtr;
		::socklen_t addressSize;
		GetAddressPtr( &addressPtr, &addressSize, mAddressFamily, mRemoteAddress4, mRemoteAddress6, false );
		mBytesReceived = ::recvfrom( mSocket, mBuffer, ( int )mChunkSize, 0, addressPtr, &addressSize ); // Wait for a client to send something
		// do not set mBorrowedSocket here (in *clients*, UDP sockets are not borrowed)
	}	
	if( mBytesReceived < 0 ) mBytesReceived = 0;
	mTotalBytesReceived += mBytesReceived;
	return mBytesReceived;
}

bool
NetworkUtils::Connection::HasData( void ) const
{
	return ( SOCKET_IS_VALID( mSocket ) && mBuffer && mBytesReceived > 0 );
}

long
NetworkUtils::Connection::Send( const char * message )
{
	return Send( message, ::strlen( message ) );
}

long
NetworkUtils::Connection::Send( const std::string & message )
{
	return Send( message.c_str(), message.length() );
}

long
NetworkUtils::Connection::Send( const void * message, size_t messageLength )
{
	if( mBorrowedSocket )
	{
		struct ::sockaddr * addressPtr;
		::socklen_t addressSize;
		GetAddressPtr( &addressPtr, &addressSize, mAddressFamily, mRemoteAddress4, mRemoteAddress6, false );
		mBytesSent = ::sendto( mSocket, ( const char * )message, ( int )messageLength, 0, addressPtr, addressSize );
	}
	else mBytesSent = ::send( mSocket, ( const char * )message, ( int )messageLength, 0 );
	if( mBytesSent != messageLength ) RAISE( "send() failed" << REPORT_LAST_SOCKET_ERROR );
	return mBytesSent;
}

void
NetworkUtils::Connection::Close( void )
{
	if( mVerbosity >= 1 && mDirection.length() ) std::cerr << "  closing connection" << mDirection << RemoteAddress() << std::endl;
	if( SOCKET_IS_VALID( mSocket ) && !mBorrowedSocket ) CLOSE_SOCKET( mSocket );
	mSocket = INVALID_SOCKET;
	mBorrowedSocket = false;
	mDirection = "";
}

std::string
NetworkUtils::Connection::Data( bool trimTrailingNewline )
{
	return std::string( mBuffer, ( trimTrailingNewline && mBytesReceived && mBuffer[ mBytesReceived - 1 ] == '\n' ) ? mBytesReceived - 1 : mBytesReceived );
}

void
NetworkUtils::Connection::EchoTest( const std::string & message )
{
	Send( message );
	std::cerr << "  received: \"";
	for( mTotalBytesReceived = 0; mTotalBytesReceived < mBytesSent; )
	{
		if( Receive() <= 0 ) RAISE( "recv() failed or connection closed prematurely" );
		std::string s = StringUtils::StringLiteral( mBuffer, mBytesReceived ); // let StringLiteral() believe we're using quotes, so that it will escape \"
		std::cerr << s.substr( 1, s.length() - 2 ); // ...but strip the quotes off, because we may be concatenating multiple chunks here
	}
	std::cerr << "\"" << std::endl;
}

std::string
NetworkUtils::Connection::ExchangeOneliners( std::string message )
{
	if( !message.length() || message[ message.length() - 1 ] != '\n' ) message += '\n';
	Send( message.c_str(), message.length() );
	std::string reply;
	for( char lastCharacter = '\0'; lastCharacter != '\n';  )
	{
		if( Receive() <= 0 ) RAISE( "recv() failed or connection closed prematurely" );
		lastCharacter = mBuffer[ mBytesReceived - 1 ];
		reply.append( mBuffer, ( lastCharacter == '\n' ) ? mBytesReceived - 1 : mBytesReceived );
	}
	return reply;
}


#include "TimeUtils.hpp"

void
NetworkUtils::TimeSyncServer( NetworkUtils::Server & server, bool oneShot )
{
	while( !ConsoleUtils::CaughtSignal() )
	{
		for( NetworkUtils::Connection c = server.NextConnection(); c.HasData(); c.Receive() )
		{
			if( ConsoleUtils::CaughtSignal() ) break;
			ExchangeTimestamps( c );
		}
		if( oneShot ) break;
	}
}

double
NetworkUtils::ExchangeTimestamps( NetworkUtils::Connection & remoteClient )
{
//	It is assumed that the remote client has sent its first timestamp.
//	While the exchange lasts, the server does not care what the client sends
//	beyond the first byte, but by convention both parties send the same
//	thing, with sizeof(local timestamp type, i.e. double) in the first
//	first byte. Assuming that is 8, it will be followed by 7 zero bytes
//	(to fill the first timestamp-sized slot) and then the second timestamp-
//	sized slot will contain the timestamp itself. It will be a fatal error
//	if they have different sizeof(double). The server does not look at the
//	payload, merely checks to see if the first byte is 0---if so, it takes
//	that as the client's signal to end the exchange.
	double adjustment = std::nan( "" );
	double buffer[ 2 ];
	const size_t localTimeStampSize = sizeof( *buffer );
	::memset( buffer, 0, sizeof( buffer ) );
	*( char * )buffer = ( char )localTimeStampSize;
//	TODO: communicate about endianity of timestamp, not just size
	double & outgoing = buffer[ 1 ];
	TimeUtils::PrecisionTime(); // let the first-time origin-setting logic run
	
	while( !ConsoleUtils::CaughtSignal() && remoteClient.HasData() )
	{
		// if the first byte is zero, that's the signal from the client to end the exchange
		if( !*remoteClient.mBuffer )
		{
			if( remoteClient.mBytesReceived == sizeof( double ) * 2 ) adjustment = -( ( double * )( remoteClient.mBuffer ) )[ 1 ]; // TODO: deal with endianity difference if necessary
			remoteClient.Send( "ok\n" );
			break;
		}
		outgoing = TimeUtils::PrecisionTime( false );
		remoteClient.Send( buffer, sizeof( buffer ) );
		remoteClient.Receive();
	}
	return adjustment;
}

double
NetworkUtils::TimeSyncClient( NetworkUtils::Connection & client, unsigned long nRounds )
{
	if( client.GetProtocolName() != "TCP" ) RAISE( "a TCP connection is required for time synchronization - cannot use UDP" );
	double buffer[ 2 ];
	const size_t localTimeStampSize = sizeof( *buffer );
	::memset( buffer, 0, sizeof( buffer ) );
	*( char * )buffer = ( char )localTimeStampSize;
//	TODO: communicate about endianity of timestamp, not just size
	double & outgoing = buffer[ 1 ];

	TimeUtils::PrecisionTime(); // let the origin-setting logic run
	outgoing = TimeUtils::PrecisionTime( false );
	client.Send( buffer, sizeof( buffer ) );
	if( !client.Receive() ) RAISE( "no reply" ); // discard the first response
	
	double * remoteTimeStamps = new double[ nRounds ];
	double * localTimeStamps  = new double[ nRounds + 1 ];
	TimeUtils::Welford local( nRounds + 1 ), remote( nRounds );
	local.SetScaling(  1.0, " seconds" ).Differences().SetScaling( 1e6, " microseconds" );
	remote.SetScaling( 1.0, " seconds" ).Differences().SetScaling( 1e6, " microseconds" );

	for( unsigned long i = 0; i < nRounds; i++ )
	{
		outgoing = local.Process( TimeUtils::PrecisionTime( false ) );
		localTimeStamps[ i ] = outgoing;
		client.Send( buffer, sizeof( buffer ) );
		if( !client.Receive() ) RAISE( "no reply from " << client.RemoteAddress() );
		size_t remoteTimeStampSize = ( size_t )client.mBuffer[ 0 ];
		if( remoteTimeStampSize != localTimeStampSize ) RAISE( "server claims that timestamps have size " << remoteTimeStampSize << " on the remote system - can only work with " << localTimeStampSize );
		if( client.mBytesReceived != sizeof( buffer ) ) RAISE( "expected a " << sizeof( buffer ) << "-byte reply but got " << client.mBytesReceived );
		remoteTimeStamps[ i ] = remote.Process( ( ( double * )client.mBuffer )[ 1 ] ); // TODO: deal with endianity difference if necessary
	}
	localTimeStamps[ nRounds ] = local.Process( TimeUtils::PrecisionTime( false ) );
	double adjustment = remote.Mean() - local.Mean();
	double elapsed = localTimeStamps[ nRounds ] - localTimeStamps[ 0 ];
	
	// an initial zero byte is a signal to the server to stop 
	buffer[ 0 ] = '\0';
	outgoing = adjustment;
	client.Send( buffer, sizeof( buffer ) );
	client.Receive();
	
	if( client.mVerbosity >= 2 )
	{
		std::cout << "local = [\n"; // let's use cout for once, to enable automated processing
		for( unsigned long i = 0; i <= nRounds; i++ ) std::cout << std::fixed << std::setprecision( 9 ) << std::setw( 22 ) <<  localTimeStamps[ i ] << ",\n";
		std::cout << "]\n\nremote = [\n";
		for( unsigned long i = 0; i <  nRounds; i++ ) std::cout << std::fixed << std::setprecision( 9 ) << std::setw( 22 ) << remoteTimeStamps[ i ] << ",\n";
		std::cout << "]\n";
	}
	if( client.mVerbosity >= 1 )
	{
		std::cerr << "  round-trip times measured locally:  " <<  local.Differences().Report() << std::endl;
		std::cerr << "  round-trip times measured remotely: " << remote.Differences().Report() << std::endl;
		std::cerr << "  difference between remote and local timestamps: " << std::fixed << std::setprecision( 6 ) << adjustment << " seconds = " << std::setprecision( 3 ) << adjustment * 1e6 << " microseconds\n";
		std::cerr << "  time taken for estimation: " << std::fixed << std::setprecision( 6 ) << elapsed << " seconds = " << std::setprecision( 1 ) << elapsed * 1e3 << " milliseconds\n";
	}
	delete []  localTimeStamps;
	delete [] remoteTimeStamps;
	return adjustment;
}

///////////////////////////////////////////////////////////////
NetworkUtils::DiscoveryServer::DiscoveryServer(
	const std::string & multicastTarget,
	const std::string & configToAdvertise,
	const std::string & serviceName,
	int verbosity )
{
	Advertise( configToAdvertise, serviceName );
	Initialize( multicastTarget, verbosity );
}

NetworkUtils::DiscoveryServer::DiscoveryServer(
	const std::string & multicastTarget,
	unsigned short portToAdvertise,
	const std::string & protocolToAdvertise,
	const std::string & serviceName,
	int verbosity )
{
	Advertise( portToAdvertise, protocolToAdvertise, serviceName );
	Initialize( multicastTarget, verbosity );
}

void
NetworkUtils::DiscoveryServer::Advertise( const std::string & serverConfig, const std::string & serviceName )
{
	Server fakeServer;
	fakeServer.Configure( serverConfig );
	Advertise( fakeServer, serviceName );
}
void
NetworkUtils::DiscoveryServer::Advertise( unsigned short port, const std::string & protocol, const std::string & serviceName )
{
	Server fakeServer;
	fakeServer.SetProtocol( protocol );
	fakeServer.SetPort( port );
	Advertise( fakeServer, serviceName );
}
void
NetworkUtils::DiscoveryServer::Advertise( const Server & server, const std::string & serviceName )
{ // it doesn't matter whether `server` is a real running Server instance, or a "fake" instance 
  // you only created so that you could use its Configure() method to parse a config string 
	std::stringstream ss;
	ss << " " << server.GetProtocolName() << ":[x]:" << server.GetPort();
	mPayload[ serviceName ] = ss.str();
}

void
NetworkUtils::DiscoveryServer::Initialize( const std::string & multicastTarget, int verbosity )
{
	SetVerbosity( verbosity );
	SetProtocol( "UDP" );
	Configure( multicastTarget );
	if( !GetPort() ) RAISE( "DiscoveryServer multicast targets must include a port suffix" );
	if( GetProtocolName() != "UDP" ) RAISE( "DiscoveryServer must use UDP, not TCP" );
		
	
	// now we have to do a bit of a shuffle to zero out mDesiredAddress, so that
	// we don't go exclusively-multicast (because on Windows that fails)
	std::string multicastAddress = GetDesiredAddress(); // address is now isolated from its port suffix
	Configure( "" ); // blanks out mDesiredAddress without affecting protocol or port setting
	Listen( multicastAddress );
	// Listen(); EnableMulticast( multicastAddress ); // to test serverless discovery clients, uncomment this line and comment out the line above, so that now the discovery server can listen on multicast *and* unicast addresses; also follow all other instructions marked *DDD*
	if( !mJoinedAnyMulticastGroup ) RAISE( "DiscoveryServer target addresses must be multicast addresses" );
}
void
NetworkUtils::DiscoveryServer::Run( void )
{
	ConsoleUtils::CatchFirstInterrupt();
	ConsoleUtils::CatchFirstTermination();
	while( !ConsoleUtils::CaughtSignal() ) HandleRequest();
}

#define MARCO "marco"
#define POLO  "polo"

void
NetworkUtils::DiscoveryServer::HandleRequest( double timeoutSeconds )
{ // TODO: on MacBook Air running system 10.14.6, an IPv4 discovery server works fine, but an IPv6 discovery server (e.g. on [FF12::1]) can reply to discovery requests from the same machine only once, and then hangs while trying to reply to one of the many interface options
	std::string message;
	std::string replyAddress; // NB we won't be using the multicast address here: the fact that you can receive multicast doesn't mean the network admin settings are configured correctly to allow you to send to a multicast address (can still get "Network is unreachable")...
	unsigned short portForReply = 0;
	{
		NetworkUtils::Connection incoming = NextConnection( timeoutSeconds );
		if( !incoming.HasData() ) return;
		message = incoming.Data( true );
		StringUtils::StringVector parts;
		StringUtils::Split( parts, message );
		std::string clientsRef, serviceName;
		if( parts.size() == 4 )
		{
			serviceName = parts[ 3 ];
			parts.pop_back();
		}
		if( parts.size() == 3 && parts[ 0 ] == MARCO )
		{
			clientsRef = parts[ 1 ];
			char * remainder = NULL;
			unsigned long clientsPayload = ::strtoul( parts[ 2 ].c_str(), &remainder, 10 );
			if( !remainder || !*remainder ) portForReply = ( clientsPayload <= 65535 ) ? ( unsigned short )clientsPayload : 0;
		}
		if( !portForReply )
		{
			if( mVerbosity >= 1 ) std::cerr << "  unexpected message " << StringUtils::StringLiteral( message ) << " from " << incoming.RemoteAddress() << std::endl;
			return;
		}
		if( mVerbosity >= 1 ) std::cerr << "  received " << StringUtils::StringLiteral( message ) << " from " << incoming.RemoteAddress() << std::endl;
		if( mPayload.find( serviceName ) == mPayload.end() )
		{
			if( serviceName == "" ) serviceName = mPayload.begin()->first;
			else
			{
				if( mVerbosity >= 1 ) std::cerr << "  ignored request for unknown service " << StringUtils::StringLiteral( serviceName ) << " from " << incoming.RemoteAddress() << std::endl;
				return;
			}
		}
		message = POLO " " + clientsRef + mPayload[ serviceName ];
		replyAddress = incoming.RemoteAddress(); // ...so we need this instead
		// incoming.Send( message ); return; // and this doesn't work either (message just disappears into a black hole)
		// TODO: To test serverless discovery clients, uncomment the line above; also follow all other instructions marked *DDD*
		//       Is there a way of making it work? Then the client wouldn't need to open any ports, configure its firewall, etc.
		//       It works if you send the request to one of the discovery server's *unicast* addresses but fails if you send the
		//       request to the multicast address, even though the discovery server might report the same remote address for the
		//       incoming connection
	}
	try
	{
		NetworkUtils::Connection response( replyAddress, portForReply, "TCP", mVerbosity );
		if( mVerbosity >= 1 ) std::cerr << "  sending " << StringUtils::StringLiteral( message ) << " to " << response.RemoteAddress() << std::endl;		
		response.Send( message );
	}
	catch( const std::string & error )
	{
		if( mVerbosity >= 1 ) std::cerr << "  could not send response to UDP:[" << replyAddress << "]:" << portForReply << " - " << error << std::endl;
	}
}
///////////////////////////////////////////////////////////////
std::string
NetworkUtils::Discover( const std::string & multicastTarget, unsigned short firstPortForReply, unsigned short lastPortForReply, const std::string & serviceName, double timeoutSeconds, int verbosity )
{
	std::stringstream ss; ss << firstPortForReply;
	if( lastPortForReply && lastPortForReply != firstPortForReply ) ss << "-" << lastPortForReply;
	return Discover( multicastTarget, ss.str(), serviceName, timeoutSeconds, verbosity );
}
std::string
NetworkUtils::Discover( const std::string & multicastTarget, const std::string & portRangeForReply, const std::string & serviceName, double timeoutSeconds, int verbosity )
{
	Server listener;
	std::stringstream ssPayload;
	ssPayload << ( uint64_t )( 0.5 + 1e6 * TimeUtils::SecondsSinceEpoch() );
	std::string myRef = ssPayload.str();
	std::string message;
	for( unsigned int multicastSendingIndex = 0, optionsRemaining = 1; optionsRemaining; multicastSendingIndex++ )
	{
		Connection icebreaker;
		icebreaker.SetVerbosity( verbosity );
		try { optionsRemaining = icebreaker.ConnectToRemoteServer( multicastTarget, 0, "UDP", multicastSendingIndex ); }
		catch( const NoMoreMulticastSendingOptions & ) { break; }
		catch( ... ) { continue; }
		if( message.length() == 0 )
		{
			listener.SetVerbosity( verbosity ).SetIPVersion( icebreaker.GetIPVersion() ).SetProtocol( "TCP" ).SetPort( portRangeForReply ).Listen();
			//listener.EnableMulticast( multicastTarget ); // would have to switch back to UDP for this, but there's no point - multicasting here is just an extra point of failure (e.g. we cannot seem to get a route from PNI device to computer via multicast---"Network is unreachable" below---even though it is possible in the other direction) 
			ssPayload << " " << listener.GetPort();
			if( serviceName.length() ) ssPayload << " " << serviceName;
			message = MARCO " " + ssPayload.str();
		}
		if( verbosity >= 1 ) std::cerr << "  sending " << StringUtils::StringLiteral( message ) << " to " << icebreaker.RemoteAddress() << " (interface option " << multicastSendingIndex << ")" << std::endl;
		icebreaker.Send( message );
	}
	if( !message.length() ) RAISE( "failed to send multicast query through any of the available interfaces" );
	Connection reply = listener.NextConnection( timeoutSeconds );
	// Connection & reply = icebreaker; reply.Receive(); // to test serverless discovery clients, uncomment this line, comment out the line above, and comment out the close-brace above that and its corresponding open-brace; also follow all other instructions marked *DDD*
	if( !reply.HasData() )
	{
		std::string serviceNameWrapped;
		if( serviceName.length() ) serviceNameWrapped = " for service " + StringUtils::StringLiteral( serviceName );
		RAISE( "no reply to discovery request after " << timeoutSeconds << " seconds (listening on UDP::" << listener.GetPort() << " after sending request" << serviceNameWrapped << " to UDP:" << multicastTarget << ")\nIf you have a firewall on this computer, make sure it is configured to allow connections on port " << listener.GetPort() << "." );
	}
	message = reply.Data( true );
	if( verbosity >= 1 ) std::cerr << "  received " << StringUtils::StringLiteral( message ) << " from " << reply.RemoteAddress() << std::endl;
	
	StringUtils::StringVector parts;
	StringUtils::Split( parts, message );
	if( parts.size() == 3 && parts[ 0 ] == POLO && parts[ 1 ] == myRef )
		return StringUtils::RegexReplace( parts[ 2 ], "x", reply.RemoteAddress() );
	RAISE( "unexpected reply " << StringUtils::StringLiteral( message ) << " from " << reply.RemoteAddress() );
}


int
NetworkUtils::Demo( int argc, const char * argv[] )
{
	int verbosity = 0;
	std::string command = ( argc > 1 ) ? argv[ 1 ] : "";
	if( command == "--verbose" || command == "-v" )
	{
		verbosity = 1;
		argc--;
		argv++;
	}
	command = ( argc > 1 ) ? argv[ 1 ] : "";
	
	while( command == "serve" )
	{
		std::string config    = ( argc > 2 ) ? argv[ 2 ] : "";
		if( !config.length() ) break;
		std::string ipVersion;
		int argi = 3;
		if( argc > argi  && ( !::strcmp( argv[ argi ], "4" ) || !::strcmp( argv[ argi ], "6" ) ) ) ipVersion = argv[ argi++ ];
		
		ConsoleUtils::CatchFirstInterrupt();
		ConsoleUtils::CatchFirstTermination();
		NetworkUtils::Server server;
		server.SetVerbosity( verbosity + 1 ).Configure( config ).SetIPVersion( ipVersion ).Listen();
		while( argc > argi ) server.EnableMulticast( argv[ argi++ ] );
		while( !ConsoleUtils::CaughtSignal() ) server.Echo();
		return 0;
	}
	while( command == "send" )
	{
		std::string config    = ( argc > 2 ) ? argv[ 2 ] : "";
		if( !config.length() ) break;
		
		ConsoleUtils::CatchFirstInterrupt();
		ConsoleUtils::CatchFirstTermination();
		NetworkUtils::Connection client( config, 0, NULL, verbosity );
		for( int i = 3; !ConsoleUtils::CaughtSignal() && i < argc; i++ )
		{
			//client.EchoTest( argv[ i ] );
			std::cout << "received: " << StringUtils::StringLiteral( client.ExchangeOneliners( argv[ i ] ) ) << std::endl;
		}
		return 0;
	}

	while( command == "discovery-server" )
	{
		std::string multicastTarget   = ( argc > 2 ) ? argv[ 2 ] : "";
		if( !multicastTarget.length() ) break;
		std::string configToAdvertise = ( argc > 3 ) ? argv[ 3 ] : "";
		if( !configToAdvertise.length() ) break;
		std::string serviceName       = ( argc > 4 ) ? argv[ 4 ] : "";
		DiscoveryServer server( multicastTarget, configToAdvertise, serviceName, verbosity + 1 );
		server.Run();
		return 0;
	}
	while( command == "discovery-client" )
	{
		std::string multicastTarget   = ( argc > 2 ) ? argv[ 2 ] : "";
		if( !multicastTarget.length() ) break;
		std::string portRangeForReply = ( argc > 3 ) ? argv[ 3 ] : "";
		if( !portRangeForReply.length() ) break;
		std::string serviceName       = ( argc > 4 ) ? argv[ 4 ] : "";
		std::string result = Discover( multicastTarget, portRangeForReply, serviceName, 2.0, verbosity );
		std::cout << result << std::endl;
		
		return 0;
	}

	while( command == "sync-server" )
	{
		std::string config    = ( argc > 2 ) ? argv[ 2 ] : "";
		std::string ipVersion = ( argc > 3 ) ? argv[ 3 ] : "";
		if( !config.length() ) break;
		
		ConsoleUtils::CatchFirstInterrupt();
		ConsoleUtils::CatchFirstTermination();
		NetworkUtils::Server server;
		server.SetVerbosity( 1 ).Configure( config ).SetProtocol( "TCP" ).Listen();
		
		TimeSyncServer( server );
		return 0;
	}	
	
	while( command == "sync-client" )
	{
		std::string config    = ( argc > 2 ) ? argv[ 2 ] : "";
		if( !config.length() ) break;
		
		char * remainder = NULL;
		unsigned long nRounds = ( argc > 3 ) ? ::strtoul( argv[ 3 ], &remainder, 10 ) : 5;
		if( remainder && *remainder ) RAISE( "could not interpret " << StringUtils::StringLiteral( argv[ 3 ] ) << " as an integer number of round-trips" );
		if( nRounds == 0 ) RAISE( "number of rounds must be greater than zero" );
		if( argc > 4 ) break;
		
		NetworkUtils::Connection client( config, 0, NULL, 1 );
		//NetworkUtils::Connection client( config );
		TimeSyncClient( client, nRounds );

		return 0;
	}	
	std::cerr << R"ZARP(
usage:
    
    ... serve  [PROTOCOL:]PORT  [4|6] 	[MULTICAST_ADDRESS1 [MULTICAST_ADDRESS2 ...]]
    ... send   [PROTOCOL:]ADDRESS[:PORT]  MESSAGE1  MESSAGE2  ... 	
    
    ... sync-server  [PROTOCOL:]PORT  [4|6] 	
    ... sync-client  [PROTOCOL:]ADDRESS[:PORT]  [NUMBER_OF_ROUNDS]
	
	... discovery-server MULTICAST_TARGET CONFIG_TO_ADVERTISE  [SERVICE_NAME]
	... discovery-client MULTICAST_TARGET PORT_RANGE_FOR_REPLY [SERVICE_NAME]
	
	Use `--verbose` or `-v` *before* the subcommand to increase verbosity;
	
)ZARP"; // CPLUSPLUS11 R"()" literal
	return 1;
}

#endif // ifndef INCLUDE_NetworkUtils_CPP

/*
	Known Issues / TODOs:

	- Windows-specific issue: cannot (cleanly) ctrl-C interrupt `::connect()` (connecting
	  to remote server) or `::send()` or `::recv()`. Compare behavior on Windows vs
	  others, when ctrl-c interrupting the following (without having to start a server)::
	  
	      dft --NetworkUtils send [fe80::dead:beef@1]:8081 hello # hangs on macOS
	      dft --NetworkUtils send [fe80::dead:beef@0]:8081 hello # hangs on Windows
	  
	- Allow ConnectToRemoteServer() to explore a range of port numbers until one succeeds
	  (needs some non-trivial programming to make `::connect()` asynchronous and
	  timeout-capable).
			
	- ConnectToRemoteServer() with IPv6 multicast target address is subject to some
	  non-trivial opaque network admin constraints and may not work between computers
	  (even if the scopes are chosen correctly) even though it may work over one scope
	  or another when the client and server are on the same machine.
	
*/
