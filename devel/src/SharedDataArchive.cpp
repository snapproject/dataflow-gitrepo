/*
# $BEGIN_DATAFLOW_LICENSE$
# 
# This file is part of the DataFlow project, a software library for
# efficient asynchronous multi-stream inter-process communication.
# 
# Copyright (c) 2014-2021 Jeremy Hill
# 
# DataFlow is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program. If not, see http://www.gnu.org/licenses/ .
# 
# Some third-party source code is compiled and linked into the
# DataFlow binary, as follows: devel/src/sem_timedwait_apple.cpp
# is adapted from sem_timedwait.cpp by Keith Shortridge, copyright
# (c) 2013 Australian Astronomical Observatory (AAO), released under
# the MIT License; devel/src/dirent_win.h is copyright (c) 2006
# Toni Ronkko, and released under the MIT License. The DataFlow build
# system also uses some third-party code:
# devel/build/cmake-recipes/DefineFunction_TARGET_ARCHITECTURE.cmake
# which is copyright (c) 2012 Petroules Corporation, released under
# the 2-Clause BSD License. For more details, see the respective
# copyright, licensing and disclaimer information in each of these
# files.
# 
# $END_DATAFLOW_LICENSE$
*/
#ifndef    INCLUDED_SharedDataArchive_CPP
#define    INCLUDED_SharedDataArchive_CPP
//////////////////////////////////////////////////////////////////////

#include "SharedDataArchive.hpp"
#include "StringUtils.hpp"
#include "DebuggingUtils.hpp"
#include "Introspection.hpp"
#include "TimeUtils.hpp"
#include "FileUtils.hpp"
#include "ConsoleUtils.hpp"
#include "CommandLine.hpp"

#include <stdio.h>  // for SEEK_SET/SEEK_CUR/SEEK_END, stdout, printf(), getchar(), fileno() (for isatty/_isatty, use unistd.h/windows.h)
#include <stdlib.h> // for NULL
#include <string.h>
#include <signal.h> // for raise()

#include <algorithm>
#include <iomanip>
#include <string>
#include <sstream>

#define DBOC 2    // debug level for opening and closing of archives
#define DBMM 5    // debug level for verbose memory-management operations

#define INITIAL_HASH_VALUE 5381
#define REHASH( HASH, BYTE )  HASH = ( ( ( HASH ) << 5 ) + ( HASH ) ) ^ ( BYTE )
/*
	This is called djb2 by Dan Bernstein. It is adapted from http://www.cse.yorku.ca/~oz/hash.html which I found via
	https://stackoverflow.com/a/7666577 .  Two possible variants are mentioned:
		hash = hash * 33 + eachByte
		hash = hash * 33 ^ eachByte
	The XOR operator ^ has even lower precedence than +, so they're directly analogous. The webpage mentions that the
	original author now prefers the XOR variant. The number 33 is magic. The webpage talks about multiplication by 33
	but the implementation it gives actually does it as a bit-shift and addition---I'm not sure why:
		hash = ( ( hash << 5 ) + hash ) + eachByte
		hash = ( ( hash << 5 ) + hash ) ^ eachByte
	Maybe it's faster? Maybe it matters that it handles overflows differently? In either case part of the existing
	hash stays in the least-significant bit position, so all the input bytes, no matter how old, will continue to
	affect the result.
*/

uint64_t
Hash( const void * s, size_t nBytes )
{
	const char * bytes = ( const char * )s;
	uint64_t hash = INITIAL_HASH_VALUE;
	while( nBytes-- ) REHASH( hash, *bytes++ );
	return hash;
}

uint64_t
Hash( const char * s ) // assumed null-terminated
{
	uint64_t hash = INITIAL_HASH_VALUE;
	if( !s ) return hash;
	while( *s ) REHASH( hash, *s++ );
	return hash;
}

uint64_t
Hash( const std::string & s )
{
	return Hash( s.c_str(), s.length() );
}

bool
OrderedByFileName( const void * p, const void * q ) // This can be used as a LinkedList::OrderingTest for files in a SharedDataArchive, based on file names
{
	return strcmp( ( const char * )p + sizeof( TerritoryHeader ), ( const char * )q + sizeof( TerritoryHeader ) ) <= 0;
}

class LinkedList
{	
	private:
		char * mBaseAddress;
		char * mPreviousBase;
		char * mNextBase;
		uint64_t * mHeadStorage;
		uint64_t * mTailStorage;
		
	public:
		typedef bool ( *OrderingTest )( const void *, const void * ); // returns true if p should be sorted before, or equal to, q; returns false if q should come before p
		
		LinkedList( char * baseAddress, uint64_t previousFieldOffset, uint64_t nextFieldOffset, uint64_t * headStorage=NULL, uint64_t * tailStorage=NULL ) : mBaseAddress( baseAddress ), mPreviousBase( baseAddress + previousFieldOffset ), mNextBase( baseAddress + nextFieldOffset ) { Ends( headStorage, tailStorage ); }
		LinkedList & Ends( uint64_t * headStorage, uint64_t * tailStorage=NULL ) { mHeadStorage = headStorage; mTailStorage = tailStorage; return *this; }
		uint64_t Head() const { return mHeadStorage ? *mHeadStorage : 0; }
		uint64_t Tail() const { return mTailStorage ? *mTailStorage : 0; }
#		define PREVIOUS( NODE )  *( uint64_t *)( mPreviousBase + ( NODE ) )
#		define NEXT(     NODE )  *( uint64_t *)( mNextBase     + ( NODE ) )
		uint64_t Next(     uint64_t node ) const { return NEXT(     node ); }
		uint64_t Previous( uint64_t node ) const { return PREVIOUS( node ); }
		uint64_t Count( void )
		{
			if( !mHeadStorage ) return 0;
			uint64_t n = 0;
			for( uint64_t node = *mHeadStorage; node; node = NEXT( node ) ) n++;
			return n;
		}
		uint64_t FindHead( uint64_t node, uint64_t * headStorage=NULL )
		{
			if( mHeadStorage ) node = *mHeadStorage;
			else
			{
				uint64_t predecessor;
				while( ( predecessor = PREVIOUS( node ) ) != 0 ) node = predecessor;
			}
			if( headStorage ) mHeadStorage = headStorage;
			if( mHeadStorage ) *mHeadStorage = node;
			return node;
		}
		uint64_t FindTail( uint64_t node, uint64_t * tailStorage=NULL )
		{
			if( mTailStorage ) node = *mTailStorage;
			else
			{
				uint64_t successor;
				while( ( successor = NEXT( node ) ) != 0 ) node = successor;
			}
			if( tailStorage ) mTailStorage = tailStorage;
			if( mTailStorage ) *mTailStorage = node;
			return node;
		}
		
		uint64_t Remove( uint64_t node, uint64_t lastInSubchain=0 ) // lastInSubchain can be zero unless you're removing an extended subchain (starting at `node`, ending at `lastInSubChain`) from the main chain
		{
			if( node == 0 ) return node;
			if( lastInSubchain == 0 ) lastInSubchain = node;
			uint64_t & predecessor = PREVIOUS( node );
			uint64_t & successor = NEXT( lastInSubchain );
			if( predecessor ) NEXT( predecessor ) = successor; 
			if( successor ) PREVIOUS( successor ) = predecessor;
			if( mHeadStorage && *mHeadStorage == node ) *mHeadStorage = successor;
			if( mTailStorage && *mTailStorage == lastInSubchain ) *mTailStorage = predecessor;
			predecessor = successor = 0;
			PREVIOUS( node ) = 0;
			NEXT( lastInSubchain ) = 0;
			return node;
		}
		uint64_t InsertAfter( uint64_t predecessor, uint64_t node, uint64_t lastInSubchain=0 ) // lastInSubchain can be zero unless you're inserting an extended subchain (starting at `node`, ending at `lastInSubChain`) into another chain
		{
			if( node == 0 ) return node;
			if( lastInSubchain == 0 ) lastInSubchain = node;
			if( predecessor == 0 )
			{
				if( !mHeadStorage ) RAISE( "cannot insert a new node at the beginning because the storage location for the list head pointer is unset" );
				if( *mHeadStorage ) return InsertBefore( *mHeadStorage, node, lastInSubchain );
				Remove( node, lastInSubchain );
				*mHeadStorage = node;
				if( mTailStorage ) *mTailStorage = lastInSubchain;
				return node;
			}
			Remove( node, lastInSubchain );
			PREVIOUS( node ) = predecessor;
			uint64_t successor = NEXT( predecessor );
			NEXT( predecessor ) = node;
			NEXT( lastInSubchain ) = successor;
			if( successor ) PREVIOUS( successor ) = lastInSubchain;
			if( mTailStorage && *mTailStorage == predecessor ) *mTailStorage = lastInSubchain;
			return node;
		}
#		define PREPEND( NODE ) InsertAfter( 0, ( NODE ) )
#		define PREPEND_CHAIN( FIRST, LAST ) InsertAfter( 0, ( FIRST ), ( LAST ) )
		uint64_t InsertBefore( uint64_t successor, uint64_t node, uint64_t lastInSubchain=0 ) // lastInSubchain can be zero unless you're inserting an extended subchain (starting at `node`, ending at `lastInSubChain`) into another chain
		{
			if( node == 0 ) return node;
			if( lastInSubchain == 0 ) lastInSubchain = node;
			if( successor == 0 )
			{
				if( !mTailStorage ) RAISE( "cannot insert a new node at the end because the storage location for the list tail pointer is unset" );
				if( *mTailStorage ) return InsertAfter( *mTailStorage, node, lastInSubchain );
				Remove( node, lastInSubchain );
				*mTailStorage = lastInSubchain;
				if( mHeadStorage ) *mHeadStorage = node;
				return node;
			}
			Remove( node, lastInSubchain );
			NEXT( lastInSubchain ) = successor;
			uint64_t predecessor = PREVIOUS( successor );
			PREVIOUS( successor ) = lastInSubchain;
			PREVIOUS( node ) = predecessor;
			if( predecessor ) NEXT( predecessor ) = node;
			if( mHeadStorage && *mHeadStorage == successor ) *mHeadStorage = node;
			return node;
		}
#		define APPEND( NODE ) InsertBefore( 0, ( NODE ) )
#		define APPEND_CHAIN( FIRST, LAST ) InsertBefore( 0, ( FIRST ), ( LAST ) )
		void Swap( uint64_t nodeA, uint64_t nodeB )
		{
			if( mHeadStorage )
			{
				if(      *mHeadStorage == nodeA ) *mHeadStorage = nodeB;
				else if( *mHeadStorage == nodeB ) *mHeadStorage = nodeA;
			}
			if( mTailStorage )
			{
				if(      *mTailStorage == nodeA ) *mTailStorage = nodeB;
				else if( *mTailStorage == nodeB ) *mTailStorage = nodeA;
			}
			uint64_t & predecessorA = PREVIOUS( nodeA );
			uint64_t & successorA   = NEXT( nodeA );
			uint64_t & predecessorB = PREVIOUS( nodeB );
			uint64_t & successorB   = NEXT( nodeB );
			if( successorA ) PREVIOUS( successorA ) = nodeB;
			if( successorB ) PREVIOUS( successorB ) = nodeA;
			if( predecessorA ) NEXT( predecessorA ) = nodeB;
			if( predecessorB ) NEXT( predecessorB ) = nodeA;
			uint64_t tmp;
#			define SWAP( A, B ) ( tmp = ( A ), ( A ) = ( B ), ( B ) = tmp )
			SWAP( predecessorA, predecessorB );
			SWAP( successorA, successorB );
		}
		std::string Report( bool reverse=false )
		{
			std::stringstream ss;
			ss << "[";
			if( reverse ) { for( uint64_t node = Tail(); node; node = Previous( node ) ) ss << ( node == *mTailStorage ? "" : "," ) << node; }
			else          { for( uint64_t node = Head(); node; node = Next( node ) )     ss << ( node == *mHeadStorage ? "" : "," ) << node; }
			ss << "]";
			return ss.str();
		}
		uint64_t Sort( const char * rootAddress, OrderingTest orderingTest )
		{
			if( !mHeadStorage ) RAISE( "cannot sort LinkedList if mHeadStorage pointer is NULL" );
			return Sort( *mHeadStorage, rootAddress, orderingTest );
		}
		uint64_t Sort( uint64_t head, const char * rootAddress, OrderingTest orderingTest )
		{
			// Adapted from Simon Tatham's Mergesort for Linked Lists (non-circular double-linked) https://www.chiark.greenend.org.uk/~sgtatham/algorithms/listsort.html
			uint64_t p, q, e, tail;
			int inSize, nMerges, pSize, qSize, i;
			if( !head ) return head;
			inSize = 1;
			while( true )
			{
				p = head;
				head = tail = 0;
				nMerges = 0;  /* count number of merges we do in this pass */
				while( p )
				{
					nMerges++;  /* there exists a merge to be done */
					q = p; 
					pSize = 0;
					for( i = 0; i < inSize; i++ ) /* step `inSize' places along from p */
					{
						pSize++;
						q = NEXT( q );
						if( !q ) break;
					}
					/* if q hasn't fallen off end, we have two lists to merge */
					qSize = inSize;
					/* now we have two lists; merge them */
					while( pSize > 0 || ( qSize > 0 && q ) )
					{
						/* decide whether next element of merge comes from p or q */
						if( pSize == 0 )
						{
							/* p is empty; e must come from q. */
							e = q; q = NEXT( q ); qSize--;
						}
						else if( qSize == 0 || !q )
						{
							/* q is empty; e must come from p. */
							e = p; p = NEXT( p ); pSize--;
						}
						else if( orderingTest( rootAddress + p, rootAddress + q ) )
						{
							/* First element of p is lower (or same); * e must come from p. */
							e = p; p = NEXT( p ); pSize--;
						}
						else
						{
							/* First element of q is lower; e must come from q. */
							e = q; q = NEXT( q ); qSize--;
						}
						/* add the next element to the merged list */
						if( tail ) NEXT( tail ) = e;
						else head = e;
						PREVIOUS( e ) = tail;
						tail = e;
					}
					/* now p has stepped `inSize' places along, and q has too */
					p = q;
				}
				NEXT( tail ) = 0;
				if( mHeadStorage ) *mHeadStorage = head;
				if( mTailStorage ) *mTailStorage = tail;
				/* If we have done only one merge, we're finished. */
				if( nMerges <= 1 ) return head;  /* allow for nMerges==0, the empty list case */
				/* Otherwise repeat, merging lists twice the size */
				inSize *= 2;
			}
		}
};

std::ostream &
operator<<( std::ostream & os, const LinkedList & x )
{
	os << "[";
	for( uint64_t node = x.Head(); node; node = x.Next( node ) )
	{
		if( node != x.Head() ) os << ",";
		os << node;
	}
	os << "]";
	return os;
}

#define LINKED_LIST( NAME, STRUCT, PREV_FIELD, NEXT_FIELD, HEAD_STORAGE, TAIL_STORAGE )  LinkedList NAME( mSegmentRootAddress, offsetof( STRUCT, PREV_FIELD ), offsetof( STRUCT, NEXT_FIELD ), ( HEAD_STORAGE ), ( TAIL_STORAGE ) )
#define ARCHIVE_NAME    ( mSegmentRootAddress ? ( mSegmentRootAddress + SH->fOffsetOfSegmentName ) : NULL )
#define SH             ( ( SegmentHeader * )mSegmentRootAddress )
#define TH( OFFSET ) ( ( TerritoryHeader * )( mSegmentRootAddress + ( OFFSET ) ) )
#define LH( OFFSET )       ( ( LotHeader * )( mSegmentRootAddress + ( OFFSET ) ) )
#define FILENAME( OFFSET )      ( ( char * )( mSegmentRootAddress + ( OFFSET ) + sizeof( TerritoryHeader ) ) )
#define FILETYPE( OFFSET )      ( FILENAME( OFFSET )[ 1 ] )
#define LOCK( MODE ) ReadWriteMutex::ScopedLock _lock( mReadWriteMutex, ( MODE ), mMutexTimeoutSeconds )
#define RECORD( TYPE, NAME, OFFSET )  TYPE * NAME = ( TYPE * )( mSegmentRootAddress + ( OFFSET ) )

const std::string SharedDataArchive::DEFAULT_NAME = "sda";

#define SDA_INIT    mSegment( NULL ), mOwnsSegmentPointer( false ), mClientID( 0 ), \
                    mReadWriteMutex( NULL ), mMutexTimeoutSeconds( timeoutSeconds )

SharedDataArchive::SharedDataArchive( const std::string & name, uint64_t segmentSize, bool forceReformat, uint64_t numberOfHashTableSlots, double timeoutSeconds )
: SDA_INIT
{
	Connect( NULL, name.c_str(), NULL, segmentSize, forceReformat, numberOfHashTableSlots );
}

SharedDataArchive::SharedDataArchive( SharedMemorySegment & segment, bool forceReformat, uint64_t numberOfHashTableSlots, double timeoutSeconds )
: SDA_INIT
{
	Connect( &segment, NULL, NULL, 0, forceReformat, numberOfHashTableSlots );
}

SharedDataArchive::SharedDataArchive( void * memoryAddress, double timeoutSeconds )
: SDA_INIT
{
	Connect( NULL, NULL, memoryAddress, 0, 0 );
	mSegmentRootAddress = ( char * )memoryAddress;
}

SharedDataArchive::~SharedDataArchive()
{
	Disconnect( true );
	// TODO: only this one of the Big Three/Four is implemented: need copy constructor and assignment operator...?
}

void
SharedDataArchive::Connect( SharedMemorySegment * segment, const char * name, void * memoryAddress, uint64_t segmentSize, bool forceReformat, uint64_t numberOfHashTableSlots )
{
	bool segmentCreatedHere = false;
	
	Disconnect();
	if( segment )
	{
		mOwnsSegmentPointer = false;
		mSegment = segment;
		mSegmentRootAddress = mSegment->Open();  // throws an error if the segment cannot be opened (e.g. if it doesn't already exist and it doesn't have a size baked into it)
		// If the segment didn't previously exist, it will now have been created using the size baked into the segment object---the `segmentSize` argument to this method will not be used
	}
	else if( name && *name )
	{
		mSegment = new SharedMemorySegment( name, false, 0 ); // try first to open the segment on the assumption that it already exists
		mOwnsSegmentPointer = true;
		mSegmentRootAddress = mSegment->Open( false );
		if( !mSegmentRootAddress && segmentSize > 0 )
		{
			segmentCreatedHere = true;
			mSegment->SetSize( as_size_t( segmentSize ) );
			mSegmentRootAddress = mSegment->Open( false );
		}
		if( !mSegmentRootAddress )
		{
			std::string segmentName = mSegment->GetName();
			delete mSegment;
			mSegment = NULL;
			mOwnsSegmentPointer = false;
			if( !segmentSize ) RAISE_SUBCLASS( SegmentNotFoundError, "SharedDataArchive failed to open shared memory segment \"" << segmentName << "\" because it does not already exist (must supply non-zero segmentSize to initialize from scratch)" );
			RAISE( "SharedDataArchive failed to create shared memory segment \"" << segmentName << "\"" );
		}
		
	}
	else if( memoryAddress )
	{
		mSegmentRootAddress = ( char * )memoryAddress;
		// Currently this route doesn't support the `forceReformat` option (if attempted, Initialize() will throw an exception because the mSegment pointer is NULL).
		// Arguably that is how it should be. If the memory content *isn't* already initialized then there's no authoritative source for its name or size.
		// The `segmentSize` argument to this method will also be ignored---there's no way to resize the segment.
	}
	else return;
		
	if( segmentCreatedHere || forceReformat )
	{
		try
		{
			Initialize( numberOfHashTableSlots ); // creates a ReadWriteMutex, locks it in write mode, populates the SegmentHeader
		}
		catch ( ... )
		{
			if( segmentCreatedHere )
			{
				if( mSegment ) mSegment->Remove(); // automatically calls Close()
				mSegmentRootAddress = NULL;
				if( mOwnsSegmentPointer ) delete mSegment;
				mSegment = NULL;
				mOwnsSegmentPointer = false;
			}
			throw;
		}
	}
	else 
	{
		SanityCheck();
		mReadWriteMutex = new ReadWriteMutex( mSegment->GetName(), &SH->fReadWriteMutexCount );
		DEBUG( DBOC, "Registering as new client for existing SharedDataArchive " << StringUtils::StringLiteral( mSegment->GetName() ) );
		LOCK( 'w' );
		++SH->fNumberOfClients;
		mClientID = ++SH->fNumberOfClientsEver;
	}
	
	// TODO: now that we've succeeded in opening a segment, what do we do if the requested `segmentSize` is greater 
	//       than what's already there? We can't expand it.  Throw an error? Or just interpret the argument as (or rename it to) `sizeIfNotAlreadyInitialized`?
}


void
SharedDataArchive::SynchronizeTimer( void )
{
	LOCK( 'w' );
	TimeUtils::SetTimerOrigin( *( TimeUtils::TimerOrigin * )&SH->fTimerOrigin, SH->fSecondsSinceEpochAtTimerOrigin );
}

void
SharedDataArchive::Disconnect( bool destructor )
{
	// Release ReadWriteMutex; decrement fNumberOfClients: remove all resources from the kernel if that means it has gone to zero (last one out turns off the lights)
	if( mSegment )
	{
		try
		{
			if( mReadWriteMutex )
			{
				if( mReadWriteMutex->Acquired() == 'r' ) mReadWriteMutex->Release( true );
				mReadWriteMutex->Acquire( 'w', mMutexTimeoutSeconds ); // TODO: this is a problem if the lock is acquired in read mode anywhere else
			}
		}
		EXCEPT( NamedSemaphore::TimeoutError, error )
		{
			DEBUG( DBOC, "Failed to sign out of SharedDataArchive " << StringUtils::StringLiteral( mSegment->GetName() ) << " gracefully (timed out waiting for writer lock)" );
			if( !destructor ) throw( error );
		}
		if( mSegmentRootAddress )
		{
			if( --SH->fNumberOfClients == 0 )
			{
				DEBUG( DBOC, "Last client out the door is dismantling SharedDataArchive " << StringUtils::StringLiteral( mSegment->GetName() ) );
				if( mReadWriteMutex ) mReadWriteMutex->SetCountPointer( NULL ); // because the mReadWriteMutex->Remove() statement below will Release() it, *after* the shared memory that contains its count is destroyed
				mSegment->Remove();
				if( mReadWriteMutex ) mReadWriteMutex->Remove(); // releases
			}
			else
			{
				DEBUG( DBOC, "Disengaging from SharedDataArchive " << StringUtils::StringLiteral( mSegment->GetName() ) );
			}
		}
	}
	delete mReadWriteMutex; // releases if not already released
	mReadWriteMutex = NULL;
	if( mOwnsSegmentPointer ) delete mSegment;
	mSegment = NULL;
	mSegmentRootAddress = NULL;
	mOwnsSegmentPointer = false;
	mClientID = 0;
}


void
SharedDataArchive::Initialize( uint64_t numberOfHashTableSlots, uint64_t smallestLotSize )
{
	if( !mSegment ) RAISE( "cannot initialize SharedDataArchive without a fully-fledged SharedMemorySegment instance" );
	DEBUG( DBOC, "Initializing SharedDataArchive " << StringUtils::StringLiteral( mSegment->GetName() ) );
	
	for( uint64_t factor = 2; factor * factor < numberOfHashTableSlots; factor++ )
		if( numberOfHashTableSlots == factor * ( numberOfHashTableSlots / factor ) )
			RAISE_SUBCLASS( BadHashTableSizeError, "cannot initialize SharedDataArchive with " << numberOfHashTableSlots << " hash-table slots (this number should be prime, but it is divisible by " << factor << ")" );
	
	uint64_t keyLength = strlen( mSegment->GetName() ); // NB: excludes null-terminator	
	do{ keyLength++; } while( keyLength % 8 ); // now includes null-terminator and padding for alignment to 8-byte boundary
	uint64_t headerSpaceNeededAfterLotRecords = numberOfHashTableSlots * sizeof( uint64_t ) + keyLength;
	uint64_t minimalSize = sizeof( SegmentHeader ) + sizeof( VacatedLotRecord ) + headerSpaceNeededAfterLotRecords + smallestLotSize;
	if( mSegment->GetSize() < minimalSize ) RAISE_SUBCLASS( SegmentTooSmallError, "SharedMemorySegment is too small (" << mSegment->GetSize() << " bytes) to provide " << numberOfHashTableSlots << " hash table slots and even one lot of size " << smallestLotSize << " (this would require at least " << minimalSize << " bytes)" );
	if( sizeof( SH->fTimerOrigin ) < sizeof( TimeUtils::TimerOrigin ) ) RAISE( "not enough space to store TimerOrigin" );
	
	if( mReadWriteMutex ) delete mReadWriteMutex;
	uint32_t fakeCount = 0; // because we don't want to base our mutex on a count that's potentially junk (could be uninitialized memory)
	mReadWriteMutex = new ReadWriteMutex( mSegment->GetName(), &fakeCount );
	LOCK( 'w' ); // but we do want to acquire the lock, just in case there are other readers/writers out there using a non-fake count---this waits until that non-fake count goes to zero
	SH->fReadWriteMutexCount = 0; // the count is for readers: writers don't count
	mReadWriteMutex->SetCountPointer( &SH->fReadWriteMutexCount );
	SH->fSharedMemoryLayoutSignature = SharedDataArchive::SIGNATURE;
	SH->fSharedMemoryLayoutVersion   = SharedDataArchive::VERSION;
	SH->fSharedMemorySegmentSize = mSegment->GetSize();
	SH->fNumberOfClients = 1;
	SH->fNumberOfClientsEver = mClientID = 1;
	SH->fNumberOfLotSizes = 0;
	SH->fSortingPreference = 0;
	TimeUtils::TimerOrigin t0 = TimeUtils::GetTimerOrigin();
	SH->fSecondsSinceEpochAtTimerOrigin = TimeUtils::SecondsSinceEpoch( true );
	*( TimeUtils::TimerOrigin * )&SH->fTimerOrigin = t0;
	
	uint64_t offset = sizeof( SegmentHeader );
#	define USE( TYPE ) ( TYPE * )( mSegmentRootAddress + ( offset += sizeof( TYPE ) ) - sizeof( TYPE ) ) 

	// Then fNumberOfLotSizes * VacatedLotRecord: for a fixed number of different lot sizes, record the head and tail of a linked list of allocated-but-vacated lots
	SH->fOffsetOfVacatedLotRecords = offset;
	if( smallestLotSize < SharedDataArchive::SMALLEST_LOT_SIZE ) smallestLotSize = SharedDataArchive::SMALLEST_LOT_SIZE;
	VacatedLotRecord * vlr;
	for( uint64_t lotSize = smallestLotSize; ; lotSize *= 2 )
	{
		uint64_t committed = offset + headerSpaceNeededAfterLotRecords;
		if( lotSize + sizeof( VacatedLotRecord ) + committed >= SH->fSharedMemorySegmentSize )
		{
			SH->fUnallocatedSize = ( SH->fSharedMemorySegmentSize > committed ? SH->fSharedMemorySegmentSize - committed : 0 );
			break;
		}
		
		vlr = USE( VacatedLotRecord );
		vlr->fLotSize = lotSize;
		vlr->fOffsetOfFirstVacatedLot = 0;
		vlr->fOffsetOfLastVacatedLot = 0;
		vlr->fTotalCapacity = 0;
		SH->fNumberOfLotSizes++;
	}
	
	// Then fNumberOfHashTableSlots * uint64_t: the hash-table itself
	SH->fNumberOfHashTableSlots = numberOfHashTableSlots;  // NB: should be a prime number
	SH->fOffsetOfHashTable = offset;
	for( uint64_t i = 0; i < SH->fNumberOfHashTableSlots; i++ ) *USE( uint64_t ) = 0;
	
	// Then segment name, with null terminator
	SH->fOffsetOfSegmentName = offset;
	strcpy( mSegmentRootAddress + offset, mSegment->GetName() ); // will copy null terminator
	offset += keyLength; // includes null terminator and alignment padding
	
	// Then the unallocated space begins
	SH->fOffsetOfUnallocatedSpace = offset;
	
	// Automatically create a "root" directory "$d" (this is the only directory that does not get auto-deleted when empty).
	StringUtils::StringVector path;
	StandardizeName( path, "", 'd' );
	FindDirectory( path );
}

bool
SharedDataArchive::WasInitializedHere( void )
{
	return mClientID == 1;
}

void
SharedDataArchive::SanityCheck( void )
{
	// don't use the ReadWriteMutex for this
	if( !mSegmentRootAddress ) RAISE( "SharedDataArchive failure: underlying SharedMemorySegment is not open" );
	if( SH->fSharedMemoryLayoutSignature != SharedDataArchive::SIGNATURE ) RAISE( "SharedDataArchive failure: SharedMemorySegment does not start with the expected signature" );
	if( SH->fSharedMemoryLayoutVersion != SharedDataArchive::VERSION ) RAISE( "SharedDataArchive failure: SharedMemorySegment layout has version " << SH->fSharedMemoryLayoutVersion << " whereas the API was compiled with version " << SharedDataArchive::VERSION );
}

void
SharedDataArchive::SetMutexTimeout( double seconds )
{
	mMutexTimeoutSeconds = seconds;
}

void
SharedDataArchive::SetMutexVerbosity( bool setting )
{
	mReadWriteMutex->mVerbose = setting;
}


uint64_t &
SharedDataArchive::HashTableEntry( uint64_t hashValue )
{
	return ( ( uint64_t * )( mSegmentRootAddress + SH->fOffsetOfHashTable ) )[ hashValue % SH->fNumberOfHashTableSlots ];
}

StringUtils::StringVector &
SharedDataArchive::StandardizeName( StringUtils::StringVector & resolvedParts, const std::string & name, char type )
{
	StringUtils::StringVector parts;
	StringUtils::Split( parts, name, "/" );
	resolvedParts.clear();
	
	if( !type ) type = StringUtils::StartsWith( name, "$" ) ? name[ 1 ] : 'f';
	resolvedParts.push_back( "$" );
	resolvedParts.back() += type;
	
	for( StringUtils::StringVector::iterator it = parts.begin(); it != parts.end(); it++ )
	{
		if( it == parts.begin() && StringUtils::StartsWith( *it, "$" ) ) continue;
		if( *it == ".." ) { if( resolvedParts.size() > 1 ) resolvedParts.pop_back(); continue; }
		if( *it != "" && *it != "." ) resolvedParts.push_back( *it );
	}
	return resolvedParts;
}

ReadWriteMutex::ScopedLock
SharedDataArchive::Lock( char mode )
{
	LOCK( mode );
	return _lock;
}

uint64_t
SharedDataArchive::FindDirectory( const std::string & name )
{
	return FindFile( name, 0, 'd' );
}

uint64_t
SharedDataArchive::FindDirectory( StringUtils::StringVector & path, DirectoryRecord ** directoryRecordOut )
{
	if( path.size() == 0 || path.front() != "$d" ) RAISE( "first element of `path` must be \"$d\" when initializing a directory" );
	uint64_t offset = FindFile( path, sizeof( DirectoryRecord ), sizeof( DirectoryRecord ) );
	if( offset )
	{
		TerritoryHeader * territoryHeader = ( TerritoryHeader * )( mSegmentRootAddress + offset );
		DirectoryRecord * directoryRecord = ( DirectoryRecord * )( mSegmentRootAddress + offset + territoryHeader->fRelativeOffsetOfPayload );
		if( !territoryHeader->fPayloadSize )
		{
			territoryHeader->fPayloadSize  += sizeof( DirectoryRecord );
			territoryHeader->fCapacityUsed += sizeof( DirectoryRecord );
			directoryRecord->fOffsetOfFirstChild = 0;
			directoryRecord->fOffsetOfLastChild = 0;
			directoryRecord->fSorted = 1;
		}
		if( directoryRecordOut ) *directoryRecordOut = directoryRecord;
	}
	else if( directoryRecordOut ) *directoryRecordOut = NULL;
	return offset;
}

uint64_t
SharedDataArchive::FindFile( const std::string & name, uint64_t ensureMinimumPayloadSize, char type )
{
	StringUtils::StringVector path;
	StandardizeName( path, name, type );
	return FindFile( path, ensureMinimumPayloadSize, 0 );
}

uint64_t
SharedDataArchive::FindFile( StringUtils::StringVector & path, uint64_t ensureMinimumPayloadSize, uint64_t ensureMinimumPayloadSizeInFirstLot )
{
	uint64_t offsetOfFirstLot = 0;
	if( ensureMinimumPayloadSize < ensureMinimumPayloadSizeInFirstLot ) ensureMinimumPayloadSize = ensureMinimumPayloadSizeInFirstLot;
	LOCK( ensureMinimumPayloadSize ? 'w' : 'r' );
	std::string standardizedName = StringUtils::Join( path, "/" );
	uint64_t & hashTableEntry = HashTableEntry( Hash( standardizedName ) ); // offset of first file in LinkedList of hashTableSiblings
	// TODO: maybe base hash on StringUtils::ToLower( standardizedName ) for a case-insensitive match?
	DEBUG( DBMM, "\n----------------FindFile()---------------" ); DBREPORT( DBMM, path ); DBREPORTQ( DBMM, standardizedName ); DBREPORT( DBMM, Hash(standardizedName)%SH->fNumberOfHashTableSlots ); DBREPORT( DBMM, hashTableEntry );
	LINKED_LIST( hashTableSiblings, TerritoryHeader, fOffsetOfPreviousFileInHashTable, fOffsetOfNextFileInHashTable, &hashTableEntry, NULL );
	uint64_t previousHashTableSibling = 0, nextHashTableSibling = 0;
	if( hashTableEntry )
	{
		const char * cname = standardizedName.c_str();
		for( uint64_t scan = hashTableSiblings.Head(); scan; scan = hashTableSiblings.Next( scan ) )
		{
			int cmp = strcmp( FILENAME( scan ), cname );
			if( cmp < 0 ) previousHashTableSibling = scan;
			else
			{
				if( cmp == 0 ) offsetOfFirstLot = scan;
				else nextHashTableSibling = scan;
				break;
			}
		}
	}
	
	do { standardizedName += '\0'; } while( standardizedName.length() % 8 );
	uint64_t totalFirstLotOverhead = sizeof( TerritoryHeader ) + standardizedName.length();
	uint64_t fileHeaderOverhead = totalFirstLotOverhead - sizeof( LotHeader );
	uint64_t capacity = fileHeaderOverhead + ensureMinimumPayloadSize;
	
	// if file already exists but doesn't have as much as ensureMinimumPayloadSize allocated, expand its allocation...	
	if( offsetOfFirstLot && ensureMinimumPayloadSize )
	{
		DEBUG( DBMM, "ensuring minimum payload size of " << ensureMinimumPayloadSize << " for file " << offsetOfFirstLot << ": " << StringUtils::StringLiteral( FILENAME( offsetOfFirstLot ) ) );
		TerritoryHeader * territoryHeader = TH( offsetOfFirstLot );
		if( ensureMinimumPayloadSizeInFirstLot )
		{
			uint64_t payloadInFirstLot = territoryHeader->fLotHeader.fLotSize - territoryHeader->fRelativeOffsetOfPayload;
			if( ensureMinimumPayloadSizeInFirstLot > payloadInFirstLot )
				RAISE( "file " << StringUtils::StringLiteral( FILENAME( offsetOfFirstLot ) ) << "at offset " << offsetOfFirstLot << " cannot fit " << ensureMinimumPayloadSizeInFirstLot << " bytes of payload into its already-allocated first Lot" )
		}
		uint64_t currentMaxPayloadSize = territoryHeader->fTotalCapacity - fileHeaderOverhead;
		if( ensureMinimumPayloadSize > currentMaxPayloadSize )
		{
			uint64_t extraCapacity = ensureMinimumPayloadSize - currentMaxPayloadSize;
			DEBUG( DBMM, "capacity must be enlarged by at least " << extraCapacity << " bytes" );
			ExtendTerritory( offsetOfFirstLot, extraCapacity );			
		}
	}
	
	if( offsetOfFirstLot || !ensureMinimumPayloadSize ) return offsetOfFirstLot;
	
	// ensure directory hierarchy exists
	uint64_t offsetOfParent = 0;
	DirectoryRecord * parentDirectoryRecord = NULL;
	if( path.size() == 1 && path.front() != "$d" ) RAISE( "cannot create \"" << path.front() << "\" (i.e. a nameless file of type '" << path.front()[ 1 ] << "') - only the root directory \"$d\" can be nameless" );
	path.pop_back();
	if( path.size() )
	{
		path.front() = "$d";
		offsetOfParent = FindDirectory( path, &parentDirectoryRecord );
	}

	uint64_t tail = 0;
	try
	{
		capacity = AllocateTerritory( fileHeaderOverhead + ensureMinimumPayloadSizeInFirstLot, capacity, &offsetOfFirstLot, &tail );
	}
	catch( ... )
	{
		if( parentDirectoryRecord && !parentDirectoryRecord->fOffsetOfFirstChild ) RemoveFile( offsetOfParent );
		throw;
	}
	DBREPORT( DBMM, offsetOfFirstLot ); DBREPORT( DBMM, tail );
	if( !offsetOfFirstLot ) return offsetOfFirstLot;
	TerritoryHeader * territoryHeader = TH( offsetOfFirstLot );
	territoryHeader->fCapacityUsed = fileHeaderOverhead;
	territoryHeader->fTotalCapacity = capacity; // on output from AllocateTerritory this will be the de-facto total reserved amount
	territoryHeader->fOffsetOfLastLotInTerritory = tail;
	territoryHeader->fPayloadSize  = 0;
	territoryHeader->fContentStart = 0;
	territoryHeader->fContentWrap  = 0;
	territoryHeader->fHeadMinimum  = 0;
	territoryHeader->fHeadMaximum  = 0;
	territoryHeader->fRelativeOffsetOfPayload = totalFirstLotOverhead;
	territoryHeader->fOffsetOfPreviousFileInHashTable = 0;
	territoryHeader->fOffsetOfNextFileInHashTable = 0;
	territoryHeader->fOffsetOfParentDirectory = offsetOfParent;
	territoryHeader->fOffsetOfPreviousFileInDirectory = 0;
	territoryHeader->fOffsetOfNextFileInDirectory = 0;
	::memcpy( FILENAME( offsetOfFirstLot ),  standardizedName.c_str(), standardizedName.length() );
	
	if( parentDirectoryRecord )
	{
		if( !parentDirectoryRecord->fOffsetOfFirstChild ) parentDirectoryRecord->fOffsetOfFirstChild = parentDirectoryRecord->fOffsetOfLastChild = offsetOfFirstLot;
		else
		{
			LINKED_LIST( directorySiblings, TerritoryHeader, fOffsetOfPreviousFileInDirectory, fOffsetOfNextFileInDirectory, &parentDirectoryRecord->fOffsetOfFirstChild, &parentDirectoryRecord->fOffsetOfLastChild );
			if( SH->fSortingPreference )
			{
				uint64_t sibling = parentDirectoryRecord->fOffsetOfFirstChild;
				while( sibling && strcmp( FILENAME( sibling ), FILENAME( offsetOfFirstLot ) ) < 0 ) sibling = directorySiblings.Next( sibling );
				directorySiblings.InsertBefore( sibling, offsetOfFirstLot ); 
			}
			else
			{
				directorySiblings.APPEND( offsetOfFirstLot );
				parentDirectoryRecord->fSorted = 0;
				// For a while, I toyed with the idea of always doing this, and then just sorting when we want to access the data in sorted order.
				// This is cheaper at file creation time, but the practical limitation of doing sort on access is that you either need to have established
				// write permission ahead of time,  or you have to sort a local-memory copy.  Sort-as-you-create seemed to be a better idea because
				// presumably file creation will be rarer than access.
			}
		}
	}	
	if( hashTableEntry == 0 ) hashTableEntry = offsetOfFirstLot; // hashTableEntry is a reference to the actual shared memory location of the hash table entry, so it will have changed if the parent directory snuck in there
	else
	{
		// otherwise, check that the addition of parent directories hasn't changed the hashTableSiblings - update previousHashTableSibling if so
		uint64_t newNextHashTableSibling = previousHashTableSibling ? hashTableSiblings.Next( previousHashTableSibling ) : hashTableSiblings.Head();
		if( newNextHashTableSibling != nextHashTableSibling )
		{
			const char * cname = standardizedName.c_str();
			for( uint64_t scan = newNextHashTableSibling; scan; scan = hashTableSiblings.Next( scan ) )
			{
				int cmp = strcmp( FILENAME( scan ), cname );
				if( cmp < 0 ) previousHashTableSibling = scan;
				else break;
			}
		}
		hashTableSiblings.InsertAfter( previousHashTableSibling, offsetOfFirstLot );
	}
	return offsetOfFirstLot;  
}

uint64_t
SharedDataArchive::GetParent( uint64_t fileOffset, DirectoryRecord ** directoryRecordOut )
{
	if( directoryRecordOut ) *directoryRecordOut = NULL;
	if( !fileOffset ) return 0;
	RECORD( TerritoryHeader, territoryHeader, fileOffset );
	if( directoryRecordOut ) *directoryRecordOut = GetDirectoryRecord( territoryHeader->fOffsetOfParentDirectory );
	return territoryHeader->fOffsetOfParentDirectory;
}

DirectoryRecord *
SharedDataArchive::GetDirectoryRecord( uint64_t directoryOffset )
{
	if( !directoryOffset ) return NULL;
	RECORD( TerritoryHeader, directoryHeader, directoryOffset );
	RECORD( DirectoryRecord, directoryRecord, directoryOffset + directoryHeader->fRelativeOffsetOfPayload );
	// we can do this confidently because, when directories are created, they assert that the whole payload
	// should fit in the first lot
	return directoryRecord;
}

uint64_t
SharedDataArchive::RemoveFile( const std::string & name )
{
	LOCK( 'w' );
	uint64_t fileOffset = FindFile( name );
	return RemoveFile( fileOffset );
}

uint64_t
SharedDataArchive::RemoveFile( uint64_t fileOffset )
{
	if( !fileOffset ) return 0;
	LOCK( 'w' );
	// remove from hash table
	uint64_t & hashTableEntry = HashTableEntry( Hash( FILENAME( fileOffset ) ) );
	LINKED_LIST( hashTableSiblings, TerritoryHeader, fOffsetOfPreviousFileInHashTable, fOffsetOfNextFileInHashTable, &hashTableEntry, NULL );
	hashTableSiblings.Remove( fileOffset );
	
	// unhook from directory record
	DirectoryRecord * directoryRecord;
	uint64_t offsetOfParent = GetParent( fileOffset, &directoryRecord );
	LINKED_LIST( directorySiblings, TerritoryHeader, fOffsetOfPreviousFileInDirectory, fOffsetOfNextFileInDirectory, directoryRecord ? &directoryRecord->fOffsetOfFirstChild : NULL, directoryRecord ? &directoryRecord->fOffsetOfLastChild : NULL );
	directorySiblings.Remove( fileOffset );
	
	// vacate and/or deallocate Lots
	uint64_t recoveredSize = 0;
	TerritoryHeader * territoryHeader = TH( fileOffset );
	territoryHeader->fCapacityUsed = 0;
	recoveredSize += ShrinkTerritory( fileOffset );
	
	// remove the parent directory record if it has now become empty (unless it is the root directory---always keep that, even if empty)
	if( !directorySiblings.Head() )
	{
		if( directoryRecord ) directoryRecord->fSorted = 1;
		if( GetParent( offsetOfParent ) ) recoveredSize += RemoveFile( offsetOfParent );
	}
	return recoveredSize;
}

uint64_t
SharedDataArchive::ShrinkTerritory( uint64_t fileOffset )
{
	if( !fileOffset ) return 0;
	LOCK( 'w' );
	TerritoryHeader * territoryHeader = TH( fileOffset );
	RECORD( VacatedLotRecord, smallestVlrPtr, SH->fOffsetOfVacatedLotRecords );
	LINKED_LIST( lots, LotHeader, fOffsetOfPreviousLotInTerritory, fOffsetOfNextLotInTerritory, &fileOffset, &territoryHeader->fOffsetOfLastLotInTerritory );
	uint64_t recoveredSize = 0;
	while( true )
	{
		uint64_t spareCapacity = territoryHeader->fTotalCapacity - territoryHeader->fCapacityUsed;
		LotHeader * finalLotHeader = LH( territoryHeader->fOffsetOfLastLotInTerritory );
		if( spareCapacity < finalLotHeader->fLotSize / 2 ) break;
		uint64_t finalLotCapacity = finalLotHeader->fLotSize - sizeof( LotHeader );
		if( spareCapacity >= finalLotCapacity )
		{
			LeaveVacatedLot( lots.Remove( territoryHeader->fOffsetOfLastLotInTerritory ) );
			territoryHeader->fTotalCapacity -= finalLotCapacity;
			recoveredSize += finalLotHeader->fLotSize;
		}
		else
		{
			recoveredSize += SplitLot( territoryHeader->fOffsetOfLastLotInTerritory, territoryHeader );
			break;
		}
	}
	return recoveredSize;
}

uint64_t 
SharedDataArchive::SplitLot( uint64_t lotOffset, TerritoryHeader * territoryHeader )
{
	RECORD( VacatedLotRecord, smallestVlrPtr, SH->fOffsetOfVacatedLotRecords );
	RECORD( LotHeader, lotHeader, lotOffset );
	if( lotHeader->fLotSize / 2 < smallestVlrPtr->fLotSize ) return 0;
	uint64_t recoveredSize = lotHeader->fLotSize / 2;
	if( territoryHeader ) territoryHeader->fTotalCapacity -= lotHeader->fLotSize / 2;
	lotHeader->fLotSize /= 2;
	uint64_t newLotOffset = lotOffset + lotHeader->fLotSize;
	RECORD( LotHeader, newLotHeader, newLotOffset );
	newLotHeader->fLotSize = lotHeader->fLotSize;
	newLotHeader->fOffsetOfNextLotInTerritory = 0;
	newLotHeader->fOffsetOfPreviousLotInTerritory = 0;
	LeaveVacatedLot( newLotOffset );
	return recoveredSize;
}

uint64_t
SharedDataArchive::AllocateTerritory( uint64_t fileHeaderOverhead, uint64_t requiredTotalCapacity, uint64_t * offsetOfFirstNewLot, uint64_t * offsetOfLastNewLot, bool throwExceptionIfFailed )
{
	// "Capacity" is in all cases defined as amount of allocated memory MINUS any LotHeaders.
	// It does not exclude anything else (the rest of the TerritoryHeader).
	// fileHeaderOverhead means how much capacity will be used, AFTER the initial LotHeader, to store
	// the REST of the TerritoryHeader, plus the key and its null-terminator (this should not be broken across lots).
	// On input, capacity is the minimum capacity required (includes file header but not its lot header or the lot headers of any other linked lots).
	// On output, capacity is the total reserved capacity (summed across all lots, excluding lot headers).
	DEBUG( DBMM, "----------------AllocateTerritory()---------------" );
	if( requiredTotalCapacity < fileHeaderOverhead ) RAISE( "nonsensical AllocateTerritory call: requiredTotalCapacity < fileHeaderOverhead" );
	if( !offsetOfFirstNewLot || !offsetOfLastNewLot ) RAISE( "NULL pointer passed to AllocateTerritory" );
	uint64_t minimumSizeOfNextLot = fileHeaderOverhead + sizeof( LotHeader );
	uint64_t sizeOfSingleLotSolution = requiredTotalCapacity + sizeof( LotHeader );

	uint64_t totalAllocatedCapacity = 0;
	uint64_t & offsetOfFirstLot = *offsetOfFirstNewLot;
	uint64_t & offsetOfLastLot  = *offsetOfLastNewLot;
	offsetOfFirstLot = offsetOfLastLot = 0;
	LINKED_LIST( allocatedLots, LotHeader, fOffsetOfPreviousLotInTerritory, fOffsetOfNextLotInTerritory, &offsetOfFirstLot, &offsetOfLastLot );
	// appending the first item to allocatedLots will automatically set offsetOfFirstLot (and offsetOfLastLot, which will also change on subsequent appends)
	
	RECORD( VacatedLotRecord, vlrPtr, SH->fOffsetOfVacatedLotRecords );
	vlrPtr--; // because vlri... will be one-based indices, 0 indicating "not available"
	uint64_t vlriNextSizeUp = 0, vlriNextSizeDown = 0;	
	for( uint64_t i = 1; i <= SH->fNumberOfLotSizes; i++ )
	{
		if( vlrPtr[ i ].fLotSize < sizeOfSingleLotSolution ) vlriNextSizeDown = i;
		if( vlrPtr[ i ].fLotSize >= sizeOfSingleLotSolution ) { vlriNextSizeUp = i; break; }
	}
#	define USE_LOT( OFFSET )   { \
		uint64_t _offset = ( OFFSET ); \
		if( _offset ) \
		{ \
			RECORD( LotHeader, _lh, allocatedLots.APPEND( _offset ) ); \
			totalAllocatedCapacity += _lh->fLotSize - sizeof( LotHeader ); \
			minimumSizeOfNextLot = sizeof( LotHeader ); \
		} \
	}
#   define LOT_SIZE( VLRI ) ( ( VLRI ) ? vlrPtr[ VLRI ].fLotSize : 0 )

	
	uint64_t sizeNextSizeDown = LOT_SIZE( vlriNextSizeDown );
	uint64_t sizeNextSizeUp   = LOT_SIZE( vlriNextSizeUp   );
	uint64_t capacityNextSizeDown = sizeNextSizeDown ? ( sizeNextSizeDown - sizeof( LotHeader ) ) : 0;
	uint64_t capacityNextSizeUp   = sizeNextSizeUp   ? ( sizeNextSizeUp   - sizeof( LotHeader ) ) : 0;
	DBREPORT( DBMM, sizeNextSizeDown ); DBREPORT( DBMM, sizeNextSizeUp );

#	define CLAIM_OR_CARVE( VLRI, LOT_OFFSET_VARIABLE ) \
	{ \
		LOT_OFFSET_VARIABLE = 0; \
		uint64_t _vlri = VLRI; \
		if( _vlri ) \
		{ \
			uint64_t _size = vlrPtr[ _vlri ].fLotSize; /* uses vlPtr */ \
			if( _size < minimumSizeOfNextLot ) \
			{ \
				DEBUG( DBMM, "cannot consider a lot of size " << _size << " because at least " << minimumSizeOfNextLot << " bytes are required for headers and/or first-lot payload"); \
			} \
			else \
			{ \
				DEBUG( DBMM, "attempting to claim a vacated lot of size " << _size ); \
				LOT_OFFSET_VARIABLE = ClaimVacatedLot( _vlri ); \
				if( !LOT_OFFSET_VARIABLE ) DEBUG( DBMM, "failed to claim a vacated lot - attempting to carve a new lot of size " << _size << " instead" ); \
				if( !LOT_OFFSET_VARIABLE ) LOT_OFFSET_VARIABLE = CarveNewLot( _size ); \
				if( !LOT_OFFSET_VARIABLE ) DEBUG( DBMM, "failed to carve" ); \
				USE_LOT( LOT_OFFSET_VARIABLE ); /* modifies allocatedLots, totalAllocatedCapacity, minimumSizeOfNextLot */ \
				if( totalAllocatedCapacity >= requiredTotalCapacity ) break; \
			} \
		} \
	}
	
	do { // only once, but with the opportunity to break out
		
		
		uint64_t lotOffset;
		CLAIM_OR_CARVE( vlriNextSizeUp, lotOffset );

		if( vlriNextSizeDown )
		{
			// * Will next-size-down lots work (for a start, are they big enough for the header)? If so, will they provide enough 
			//   capacity, with the help of an additionally-carved next-size-down lot if necessary and possible? If so, use them.
			uint64_t totalVacatedCapacityInNextSizeDown = vlriNextSizeDown ? vlrPtr[ vlriNextSizeDown ].fTotalCapacity : 0;
			if( totalVacatedCapacityInNextSizeDown + capacityNextSizeDown >= requiredTotalCapacity )
			{
				// if we need to carve a new one, and a new one would even be big enough for the header, then attempt to carve it
				if( totalVacatedCapacityInNextSizeDown < requiredTotalCapacity && sizeNextSizeDown >= minimumSizeOfNextLot )
				{
					DEBUG( DBMM, "strategy 3 part A: attempting to carve a new lot of size " << sizeNextSizeDown );
					uint64_t carved = CarveNewLot( sizeNextSizeDown );
					USE_LOT( carved );
					if( !carved ) DEBUG( DBMM, "failed to carve" );
				}
				// if we're sure we're going to have enough, and we've either got past the header issue or the first lot is going to be big enough, proceed to add vacated lots
				if( totalVacatedCapacityInNextSizeDown + totalAllocatedCapacity >= requiredTotalCapacity && sizeNextSizeDown >= minimumSizeOfNextLot )
				{
					while( totalAllocatedCapacity < requiredTotalCapacity )
					{
						DEBUG( DBMM, "strategy 3 part B: claiming vacated lot of size " << sizeNextSizeDown );
						USE_LOT( ClaimVacatedLot( vlriNextSizeDown ) );
					}
				}
				if( totalAllocatedCapacity >= requiredTotalCapacity ) break;
				DEBUG( DBMM, "strategy 3 failed" );
			}
		}	
		
		// Do any existing (vacated) larger-sized lots exist? Halve them in as few strokes as possible, until you get a vlriNextSizeUp-sized lot, and use that
		uint64_t vlriLargerVacatedLot = 0;
		for( uint64_t vlri = vlriNextSizeUp; vlri && vlri <= SH->fNumberOfLotSizes; vlri++ )
		{
			if( vlrPtr[ vlri ].fTotalCapacity ) { vlriLargerVacatedLot = vlri; break; }
		}
		if( vlriLargerVacatedLot )
		{
			lotOffset = ClaimVacatedLot( vlriLargerVacatedLot );
			RECORD( LotHeader, lh, lotOffset );
			while( lh->fLotSize > sizeNextSizeUp ) SplitLot( lotOffset, NULL );
			USE_LOT( lotOffset );
			if( totalAllocatedCapacity >= requiredTotalCapacity ) break;
		}
		// * Will existing (vacated) lots of all smaller sizes provide enough capacity, with the help of an additionally-carved
		//   next-size-down lot if necessary and possible? If so, use them.
		
		uint64_t totalCapacityOfAllSmallerVacatedLots = 0, vlriLargestSmallerVacated = 0, vlriLargestCarvableLot = 0;
		for( uint64_t vlri = 1; vlri <= vlriNextSizeDown; vlri++ )
		{
			totalCapacityOfAllSmallerVacatedLots += vlrPtr[ vlri ].fTotalCapacity;
			if( vlrPtr[ vlri ].fTotalCapacity > 0 ) vlriLargestSmallerVacated = vlri;
			if( vlrPtr[ vlri ].fLotSize <= SH->fUnallocatedSize ) vlriLargestCarvableLot = vlri;
		}
		uint64_t capacityObtainableByCarving = 0, hypotheticalUnallocatedSpace = SH->fUnallocatedSize;
		for( uint64_t vlri = vlriLargestCarvableLot; vlri > 0; vlri-- )
		{
			uint64_t size = vlrPtr[ vlri ].fLotSize;
			uint64_t capacity = size - sizeof( LotHeader );
			while( size < hypotheticalUnallocatedSpace )
			{
				capacityObtainableByCarving += capacity;
				hypotheticalUnallocatedSpace -= size;
			}
		}
		// if it's impossible to meet the capacity from all reclaimed lots and the *all* successively-carvable ones, give up
		if( totalCapacityOfAllSmallerVacatedLots + capacityObtainableByCarving < requiredTotalCapacity ) break;

		uint64_t sizeLargestSmallerVacated = LOT_SIZE( vlriLargestSmallerVacated );
		uint64_t sizeLargestCarvableLot = LOT_SIZE( vlriLargestCarvableLot );
		// if it's impossible to reclaim or carve a single lot that will meet the minimum size constraint for the first lot, give up
		if( sizeLargestSmallerVacated < minimumSizeOfNextLot && sizeLargestCarvableLot < minimumSizeOfNextLot ) break;
		
		uint64_t vlri = ( sizeLargestCarvableLot > sizeLargestSmallerVacated ) ? vlriLargestCarvableLot : vlriLargestSmallerVacated;
		while( totalAllocatedCapacity < requiredTotalCapacity && vlri > 0 )
		{
			CLAIM_OR_CARVE( vlri, lotOffset );
			if( !lotOffset ) vlri--;
		}
		if( totalAllocatedCapacity >= requiredTotalCapacity ) break;
	} while( 0 );
	DBREPORT( DBMM, allocatedLots );
	DBREPORT( DBMM, totalAllocatedCapacity );
	DBREPORT( DBMM, SH->fUnallocatedSize );
	
	if( totalAllocatedCapacity < requiredTotalCapacity )
	{
		if( throwExceptionIfFailed ) RAISE_SUBCLASS( OutOfSpaceError, "failed to allocate lots with total capacity " << requiredTotalCapacity << " bytes or more" );
		while( offsetOfLastLot ) LeaveVacatedLot( allocatedLots.Remove( offsetOfLastLot ) );
		totalAllocatedCapacity = 0;
	}
	return totalAllocatedCapacity;
}

uint64_t
SharedDataArchive::CarveNewLot( uint64_t targetSize )
{
	if( !targetSize || SH->fUnallocatedSize < targetSize ) return 0;
	SH->fUnallocatedSize -= targetSize;
	uint64_t newLotOffset = SH->fOffsetOfUnallocatedSpace;
	SH->fOffsetOfUnallocatedSpace += targetSize;
	RECORD( LotHeader, newLotHeader, newLotOffset );
	newLotHeader->fLotSize = targetSize;
	newLotHeader->fOffsetOfNextLotInTerritory = 0;
	newLotHeader->fOffsetOfPreviousLotInTerritory = 0;
	DEBUG( DBMM, "carved new Lot of size " << targetSize << " at offset " << newLotOffset );
	return newLotOffset;
}

uint64_t
SharedDataArchive::ClaimVacatedLot( uint64_t vlri ) // 1-based index into the packed array of VacatedLotRecords
{
	if( !vlri ) return 0;
	RECORD( VacatedLotRecord, vlrPtr, SH->fOffsetOfVacatedLotRecords );
	vlrPtr += vlri - 1;
	if( !vlrPtr->fOffsetOfFirstVacatedLot ) return 0;
	LINKED_LIST( vacatedLots, LotHeader, fOffsetOfPreviousLotInTerritory, fOffsetOfNextLotInTerritory, &vlrPtr->fOffsetOfFirstVacatedLot, &vlrPtr->fOffsetOfLastVacatedLot );
	uint64_t lotOffset = vacatedLots.Remove( vacatedLots.Head() );
	if( lotOffset ) vlrPtr->fTotalCapacity -= vlrPtr->fLotSize - sizeof( LotHeader );
	return lotOffset;
}

uint64_t
SharedDataArchive::LeaveVacatedLot( uint64_t lotOffset, bool allowDeallocation )
{
	if( !lotOffset ) return 0;
	RECORD( LotHeader, lh, lotOffset );
	if( allowDeallocation && lotOffset + lh->fLotSize == SH->fOffsetOfUnallocatedSpace )
	{
		SH->fOffsetOfUnallocatedSpace = lotOffset;
		SH->fUnallocatedSize += lh->fLotSize;
		DEBUG( DBMM, "deallocated Lot " << lotOffset );
		return 0;
	}
	RECORD( VacatedLotRecord, vlrPtr, SH->fOffsetOfVacatedLotRecords );
	for( uint64_t i = 0; i < SH->fNumberOfLotSizes; i++, vlrPtr++ )
	{
		if( vlrPtr->fLotSize == lh->fLotSize )
		{
			LINKED_LIST( vacatedLots, LotHeader, fOffsetOfPreviousLotInTerritory, fOffsetOfNextLotInTerritory, &vlrPtr->fOffsetOfFirstVacatedLot, &vlrPtr->fOffsetOfLastVacatedLot );
			vacatedLots.APPEND( lotOffset );
			vlrPtr->fTotalCapacity += lh->fLotSize - sizeof( LotHeader );
			return lotOffset;
		}
	}
	RAISE( "there's no place to register a " << lh->fLotSize << "-byte vacated lot" );
}

uint64_t
SharedDataArchive::ExtendTerritory( uint64_t offsetOfFirstLot, uint64_t extraCapacity, bool throwExceptionIfFailed )
{
	if( !offsetOfFirstLot || !extraCapacity ) return 0;
	RECORD( TerritoryHeader, territoryHeader, offsetOfFirstLot );
	LINKED_LIST( lots, LotHeader, fOffsetOfPreviousLotInTerritory, fOffsetOfNextLotInTerritory, &offsetOfFirstLot, &territoryHeader->fOffsetOfLastLotInTerritory );
	LotHeader * finalLotHeader = LH( territoryHeader->fOffsetOfLastLotInTerritory );

	// if this can be done by doubling, quadrupling, etc, the existing final lot directly into unallocated space, then do that:
	if( territoryHeader->fOffsetOfLastLotInTerritory + finalLotHeader->fLotSize == SH->fOffsetOfUnallocatedSpace )
	{
		uint64_t factor;
		for( factor = 2; finalLotHeader->fLotSize * ( factor - 1 ) < extraCapacity; ) factor *= 2; // sizeof( LotHeader ) does not need to be taken into account here
		uint64_t extraSpace = finalLotHeader->fLotSize * ( factor - 1 );
		if( SH->fUnallocatedSize >= extraSpace )
		{
			DEBUG( DBMM, "expanding by " << extraSpace << " bytes into unallocated space" );
			finalLotHeader->fLotSize += extraSpace;
			territoryHeader->fTotalCapacity += extraSpace;
			SH->fOffsetOfUnallocatedSpace += extraSpace;
			SH->fUnallocatedSize -= extraSpace;
			return extraSpace;
		}
	}
	// otherwise, allocate new lots in the normal way, and chain them onto the final lot of this file
	uint64_t annexFirst, annexLast;
	DEBUG( DBMM, "cannot expand into unallocated space: calling AllocateTerritory(0, " << extraCapacity << ", ...)" );
	extraCapacity = AllocateTerritory( 0, extraCapacity, &annexFirst, &annexLast, throwExceptionIfFailed );
	if( annexFirst ) lots.APPEND_CHAIN( annexFirst, annexLast );
	territoryHeader->fTotalCapacity += extraCapacity;
	return extraCapacity;
}


std::string
Pad( char fillChar, int width, std::stringstream & z )
{
	std::string zs = z.str();
	z.str( "" );
	if( StringUtils::StartsWith( zs, "\x1B " ) ) return zs.substr( 2 );
	width -= ( int )zs.length();
	width -= 1;
	std::string s( width > 0 ? width : 0, fillChar );
	s += " ";
	s += zs;
	return s;
}

#define OFFSET_OF( ADDRESS ) ( ( uint64_t )( ADDRESS ) - uint64_t( mSegmentRootAddress ) )
#define REPORT( NAME, THING, OFFSET ) ( ss \
	<< std::setfill( ' ' ) << std::setw(  8 ) << std::right << ( OFFSET ) << "  " \
	<< std::setfill( '.' ) << std::setw( 35 ) << std::left  << ( NAME )   \
	<< ( ss2 << std::fixed << ( THING ), Pad( '.', 11, ss2 ) )  << std::endl )
//	<< std::setfill( '.' ) << std::setw( 12 ) << std::right << ( THING )  << std::endl )
#define REPORT_FIELD( STRUCTPTR, FIELD ) REPORT( #FIELD, STRUCTPTR->FIELD, OFFSET_OF( &STRUCTPTR->FIELD ) )
#define HR "-------------"
//#define SUBHEADING( X )  HR << " " << X << " " << HR << std::endl
#define SUBHEADING( X )  X << ":" << std::endl

#define SREPORT( KEY, VALUE, ALIGN, EXTRA ) ( ss \
	<< std::setfill( ' ' ) << std::setw( 16 ) << std::right << ( KEY ) << ": " \
	<< std::setfill( ' ' ) << std::setw(  9 ) << std::ALIGN << std::fixed << VALUE \
	<< std::setw( 1 ) << std::left << EXTRA \
	<< std::endl )
#define SREPORTL( KEY, VALUE ) SREPORT( KEY, VALUE, left,  "" )
#define SREPORTR( KEY, VALUE ) SREPORT( KEY, VALUE, right, "" )
#define ASSERTION( EXPR ) ( ( EXPR ) ? "" : " [?! FAILED ASSERTION: " #EXPR "]" )

double
SharedDataArchive::Age( void )
{
	return TimeUtils::PrecisionTime( ( TimeUtils::TimerOrigin * )&SH->fTimerOrigin );
}

uint64_t
SharedDataArchive::GetSpaceAvailable( bool includeVacatedLots )
{
	uint64_t spaceAvailable = SH->fUnallocatedSize;
	if( !includeVacatedLots ) return spaceAvailable;
	VacatedLotRecord * vlrPtr = ( VacatedLotRecord * )( mSegmentRootAddress + SH->fOffsetOfVacatedLotRecords );
	for( uint64_t i = 0; i < SH->fNumberOfLotSizes; i++ )
	{
		uint64_t nLots = vlrPtr[ i ].fTotalCapacity / ( vlrPtr[ i ].fLotSize - sizeof( LotHeader ) );
		spaceAvailable += nLots * vlrPtr[ i ].fLotSize;
	}
	return spaceAvailable;
}

std::string
SharedDataArchive::Summary( const std::string & highlightFile )
{
	std::stringstream ss;
	double creationDate = SH->fSecondsSinceEpochAtTimerOrigin;
	bool writer = mReadWriteMutex->Acquired() == 'w';
	double afterUnlock, afterLock, beforeLock = Age();
	
	{
		LOCK( 'r' );
		afterLock = Age();
		SREPORTL( "Summarized at", "\x01NOW\x01" ); // NB: datestamp to be substituted later, after releasing lock (FormatDateStamp takes a good few hundred microseconds on Windows)
		SREPORTL( "Archive created", "\x01CREATION_DATE\x01" << " (" << std::fixed << std::setprecision( 6 ) << afterLock << " seconds ago)" );
		SREPORTL( "Archive name", StringUtils::StringLiteral( ARCHIVE_NAME ) );
		SREPORT( "Archive layout", StringUtils::StringLiteral( ( const char * )&SH->fSharedMemoryLayoutSignature, sizeof( SH->fSharedMemoryLayoutSignature ), "'" ) << " version " << SH->fSharedMemoryLayoutVersion << ", " << ( SH->fSortingPreference ? "sorted" : "unsorted" ), setw( 1 ), "" );
		SREPORT( "Library version", Introspection::GetVersion() << " - " << Introspection::GetRevision(), setw( 1 ), "" );
		SREPORTL( "Library built", Introspection::GetBuildDatestamp() );
		SREPORTL( "Library location", FileUtils::LibraryPath() );
		SREPORTR( "Active readers", SH->fReadWriteMutexCount - ( writer ? 0 : 1 ) << " of " << SH->fNumberOfClients - 1 << " other clients have reader lock" );
		
		VacatedLotRecord * vlrPtr = ( VacatedLotRecord * )( mSegmentRootAddress + SH->fOffsetOfVacatedLotRecords );
		std::stringstream ssv;
		ssv << vlrPtr[ SH->fNumberOfLotSizes - 1 ].fLotSize;
		int64_t lotSizeWidth = ssv.tellp(), countWidth = 1, lineLength = 1;
		const uint64_t lotSizesPerRow = 4;
		uint64_t spaceAvailable = SH->fUnallocatedSize;
		for( uint64_t i = 0; i < SH->fNumberOfLotSizes; i++ )
		{
			uint64_t nLots = vlrPtr[ i ].fTotalCapacity / ( vlrPtr[ i ].fLotSize - sizeof( LotHeader ) );
			spaceAvailable += nLots * vlrPtr[ i ].fLotSize;
			ssv.str( "" ); ssv << nLots;
			if( countWidth < ssv.tellp() ) countWidth = ssv.tellp();
		}
		uint64_t spaceUsed = SH->fSharedMemorySegmentSize - spaceAvailable;
		
		SREPORTR( "Unallocated",  SH->fUnallocatedSize          << " bytes   = " << std::setprecision( 6 ) << std::setw( 11 ) << ( double )SH->fUnallocatedSize          / 1048576.0 << " MiB   = " << std::setprecision( 2 ) << std::setw( 6 ) << 100.0 * ( double )SH->fUnallocatedSize          / ( double )SH->fSharedMemorySegmentSize  << "%" );
		SREPORTR( "Allocated",    SH->fOffsetOfUnallocatedSpace << " bytes   = " << std::setprecision( 6 ) << std::setw( 11 ) << ( double )SH->fOffsetOfUnallocatedSpace / 1048576.0 << " MiB   = " << std::setprecision( 2 ) << std::setw( 6 ) << 100.0 * ( double )SH->fOffsetOfUnallocatedSpace / ( double )SH->fSharedMemorySegmentSize  << "%" );
		SREPORT(  "Segment size", SH->fSharedMemorySegmentSize  << " bytes   = " << std::setprecision( 6 ) << std::setw( 11 ) << ( double )SH->fSharedMemorySegmentSize  / 1048576.0 << " MiB", right, ASSERTION( SH->fOffsetOfUnallocatedSpace + SH->fUnallocatedSize == SH->fSharedMemorySegmentSize ) );
		SREPORTR( "Available",    spaceAvailable                << " bytes   = " << std::setprecision( 6 ) << std::setw( 11 ) << ( double )spaceAvailable                / 1048576.0 << " MiB   = " << std::setprecision( 2 ) << std::setw( 6 ) << 100.0 * ( double )spaceAvailable                / ( double )SH->fSharedMemorySegmentSize  << "%" );
		SREPORTR( "Used",         spaceUsed                     << " bytes   = " << std::setprecision( 6 ) << std::setw( 11 ) << ( double )spaceUsed                     / 1048576.0 << " MiB   = " << std::setprecision( 2 ) << std::setw( 6 ) << 100.0 * ( double )spaceUsed                     / ( double )SH->fSharedMemorySegmentSize  << "%" );
		
		ssv.str( "" ); ssv <<  "    Vacated Lots: { ";
		for( uint64_t i = 1; i <= SH->fNumberOfLotSizes; i++, vlrPtr++ )
		{
			LINKED_LIST( vacatedLots, LotHeader, fOffsetOfPreviousLotInTerritory, fOffsetOfNextLotInTerritory, &vlrPtr->fOffsetOfFirstVacatedLot, &vlrPtr->fOffsetOfLastVacatedLot );
			uint64_t nLots = vlrPtr->fTotalCapacity / ( vlrPtr->fLotSize - sizeof( LotHeader ) );
			bool mismatch = ( nLots != vacatedLots.Count() );
			ssv << std::setw( i == 1 ? 7 : i % lotSizesPerRow == 1 ? 10 : lotSizeWidth ) << vlrPtr->fLotSize << ": ";
			ssv << std::setw( countWidth ) << nLots << ( mismatch ? "?!*" : "" );
			if( i == SH->fNumberOfLotSizes ) ssv << std::setw( lineLength - ssv.tellp() + 1 ) << " }";
			else { ssv << ", "; if( i % lotSizesPerRow ==  0 ) lineLength = ssv.tellp(); }
			if( i % lotSizesPerRow == 0 || i == SH->fNumberOfLotSizes )
			{
				ss << ssv.str() << std::endl;
				ssv.str( "" ); ssv << "                 ";
			}
		} 
		uint64_t nSlotsFull = 0, nDirectoriesCountedViaHashTable = 0, nFilesCountedViaHashTable = 0, nFilesCountedViaWalk = 0; // temporary semantics, just for the purpose of this summary: a directory is not considered a kind of file
		size_t maxPathLength = 0;
		for( uint64_t i = 0; i < SH->fNumberOfHashTableSlots; i++ )
		{
			uint64_t hashTableEntry = HashTableEntry( i );
			if( !hashTableEntry ) continue;
			nSlotsFull++;
			LINKED_LIST( hashTableSiblings, TerritoryHeader, fOffsetOfPreviousFileInHashTable, fOffsetOfNextFileInHashTable, &hashTableEntry, NULL );
			for( uint64_t scan = hashTableSiblings.Head(); scan; scan = hashTableSiblings.Next( scan ) )
			{
				if( FILETYPE( scan ) == 'd' ) nDirectoriesCountedViaHashTable++;
				else nFilesCountedViaHashTable++;
				// TODO: maybe count and report each file type separately
			}
		}
		SREPORT( "Hash table usage", nSlotsFull << " of " << SH->fNumberOfHashTableSlots << " slots filled", right, "" );
		
		std::string view = ( highlightFile == "$Lots" ) ? LotByLotSummary( &nFilesCountedViaWalk ) : FileByFileSummary( highlightFile, &nFilesCountedViaWalk );
		SREPORT( "Number of items", nDirectoriesCountedViaHashTable + nFilesCountedViaHashTable << " (directories: " << nDirectoriesCountedViaHashTable << ";  other types: " << nFilesCountedViaHashTable << ")", right, ASSERTION( nFilesCountedViaWalk == nFilesCountedViaHashTable )  );
		ss << std::endl << view;
	}
	afterUnlock = Age();
	std::string output = ss.str();
	output = StringUtils::RegexReplace( output, "\x01CREATION_DATE\x01", TimeUtils::FormatDateStamp( creationDate, true ) ); // we do this after-the-fact because FormatDateStamp takes disproportionately long (and so would hold the ReadWriteMutex for disproportionately long) relative to other operations
	output = StringUtils::RegexReplace( output, "\x01NOW\x01",           TimeUtils::FormatDateStamp( creationDate + afterLock, true ) );
	
	std::stringstream sst;
	if( writer )
	{
		sst << "Summarizer inherited writer lock.\n";
		sst << "                   Composed report in " << std::fixed << std::setprecision( 3 ) << std::setw( 7 ) << 1e6 * (      Age() - beforeLock ) << " microseconds\n";
	}
	else
	{
		sst << "Summarizer waited for reader lock for " << std::fixed << std::setprecision( 3 ) << std::setw( 7 ) << 1e6 * ( afterLock  - beforeLock ) << " microseconds\n";
		sst << "    and then retained reader lock for " << std::fixed << std::setprecision( 3 ) << std::setw( 7 ) << 1e6 * ( afterUnlock - afterLock ) << " microseconds\n";
	}
	sst << std::endl;
	return sst.str() + output;
}

std::string
SharedDataArchive::FileByFileSummary( const std::string & highlightFile, uint64_t * returnNumberOfFiles )
{
	std::stringstream ssf;
	size_t maxPathLength = 0;
	File f = Open( "$d", 'r' ); // acts as lock within this scope
	uint64_t root = f.GetFileOffset();
	if( returnNumberOfFiles ) *returnNumberOfFiles = 0;
	for( f.Walk(); f.Exists(); f.Next() )
	{
		if( returnNumberOfFiles ) ( *returnNumberOfFiles )++;
		std::string path = StringUtils::StringLiteral( f.GetName(), "" );
		if( path.length() > maxPathLength ) maxPathLength = path.length();
	}
	f.Open( root );
	for( f.Walk(); f.Exists(); f.Next() )
	{
		std::stringstream ssfi;
		std::string path = StringUtils::StringLiteral( f.GetName(), "" );
		ssfi << ( highlightFile == f.GetName()  ? "> " : "  " );
		ssfi << path << " " << std::string( maxPathLength - path.length() + 1, '.' ) << " ";
		uint64_t payloadBytesUsed = f.PayloadCapacityUsed();
		ssfi << payloadBytesUsed << " / " << f.PayloadCapacity();
		int numberOfLots = 0;
		LINKED_LIST( lots, LotHeader, fOffsetOfPreviousLotInTerritory, fOffsetOfNextLotInTerritory, NULL, NULL );
		std::stringstream ssl;
		uint64_t totalSize = 0, totalPayloadCapacity = 0, firstLot = f.GetFileOffset();
		for( uint64_t lotOffset = firstLot; lotOffset; lotOffset = lots.Next( lotOffset ) )
		{
			uint64_t lotSize = LH( lotOffset )->fLotSize;
			uint64_t lotPayloadCapacity = lotSize - ( lotOffset == firstLot ? TH( lotOffset )->fRelativeOffsetOfPayload : sizeof( LotHeader ) );
			ssl << ( numberOfLots++ ? " + " : "" ) << ( payloadBytesUsed < lotPayloadCapacity ? payloadBytesUsed : lotPayloadCapacity ) << "/" << lotPayloadCapacity << "/" << lotSize;
			if( payloadBytesUsed < lotPayloadCapacity ) payloadBytesUsed = 0; else payloadBytesUsed -= lotPayloadCapacity;
			totalSize += lotSize;
			totalPayloadCapacity += lotPayloadCapacity;
		}
		ssfi << " / " << totalSize << ASSERTION( f.PayloadCapacity() == totalPayloadCapacity );
		ssfi << " in " << numberOfLots << " Lot" << ( numberOfLots == 1 ? "" : "s" );
		if( numberOfLots > 1 ) ssfi << ":\n      " << ssl.str();
		ssf << ssfi.str() << std::endl;
	}
	return ssf.str();
}

uint64_t
SharedDataArchive::GetClientID( void )
{
	return mClientID;
}

std::string
SharedDataArchive::GetName( void )
{
	SanityCheck();
	// LOCK( 'r' ); // Locking *should* be unnecessary...
	const char * name = ARCHIVE_NAME;
	return name ? name : "";
}

std::string
SharedDataArchive::ReportHeader( uint64_t maxHashTableEntries )
{
	std::stringstream ss, ss2;
	LOCK( 'r' );
	ss << "SharedDataArchive" << std::endl;	
	ss << SUBHEADING( "SegmentHeader");
	REPORT_FIELD( SH, fSharedMemoryLayoutSignature );
	REPORT_FIELD( SH, fSharedMemoryLayoutVersion );
	REPORT_FIELD( SH, fSharedMemorySegmentSize );
	REPORT_FIELD( SH, fReadWriteMutexCount );
	REPORT_FIELD( SH, fNumberOfClients );
	REPORT_FIELD( SH, fNumberOfClientsEver );
	REPORT_FIELD( SH, fOffsetOfVacatedLotRecords );
	REPORT_FIELD( SH, fNumberOfLotSizes );
	REPORT_FIELD( SH, fOffsetOfHashTable );
	REPORT_FIELD( SH, fNumberOfHashTableSlots );  // should be a prime number
	REPORT_FIELD( SH, fSortingPreference );
	REPORT_FIELD( SH, fOffsetOfSegmentName );
	REPORT_FIELD( SH, fOffsetOfUnallocatedSpace );
	REPORT_FIELD( SH, fUnallocatedSize );
	REPORT( "sizeof(fTimerOrigin)", sizeof(SH->fTimerOrigin), OFFSET_OF( &SH->fTimerOrigin ) );
	REPORT_FIELD( SH, fSecondsSinceEpochAtTimerOrigin );
	ss << SUBHEADING( "VacatedLotRecord[" << SH->fNumberOfLotSizes << "]" );
	VacatedLotRecord * vlrPtr = ( VacatedLotRecord * )( mSegmentRootAddress + SH->fOffsetOfVacatedLotRecords );
	for( unsigned int i = 0; i < SH->fNumberOfLotSizes; i++, vlrPtr++ )
	{
		REPORT_FIELD( vlrPtr, fLotSize );
		REPORT_FIELD( vlrPtr, fOffsetOfFirstVacatedLot );
		REPORT_FIELD( vlrPtr, fOffsetOfLastVacatedLot );
		REPORT_FIELD( vlrPtr, fTotalCapacity );
		LINKED_LIST( vacatedLots, LotHeader, fOffsetOfPreviousLotInTerritory, fOffsetOfNextLotInTerritory, &vlrPtr->fOffsetOfFirstVacatedLot, &vlrPtr->fOffsetOfLastVacatedLot );
		REPORT( "List of lots", vacatedLots, "" );
		//ss << HR << HR << HR << std::endl;
	}
	ss << SUBHEADING( "hash table" );
	uint64_t * hte = ( uint64_t * )( mSegmentRootAddress + SH->fOffsetOfHashTable );
	for( unsigned int i = 0; i < SH->fNumberOfHashTableSlots; i++, hte++ )
	{
		std::stringstream label;
		label << "hash table entry " << std::setw( 4 ) << std::right << std::setfill( ' ' ) << i;
		if( i < maxHashTableEntries || i == SH->fNumberOfHashTableSlots - 1 )
		{
			LINKED_LIST( hashTableSiblings, TerritoryHeader, fOffsetOfPreviousFileInHashTable, fOffsetOfNextFileInHashTable, hte, NULL );
			REPORT( label.str(), hashTableSiblings, OFFSET_OF( hte ) );
			//REPORT( label.str(), *hte, OFFSET_OF( hte ) );
			//REPORT( "List of files", hashTableSiblings, "" );
		}
		else if( i == maxHashTableEntries ) ss << "[...]\n";
	}
	const char * p = ( const char * )( mSegmentRootAddress + SH->fOffsetOfSegmentName );
	ss << SUBHEADING( "segment name" );
	REPORT( "Segment name", StringUtils::StringLiteral( p ), OFFSET_OF( p ) ); 
	//ss << HR << HR << HR << std::endl;
	ss << std::endl;
	return ss.str();
}

void
SharedDataArchive::SortDirectory( uint64_t directoryOffset )
{
	if( !directoryOffset ) return;
	LOCK( 'w' );
	if( FILETYPE( directoryOffset ) != 'd' ) RAISE( "file " << directoryOffset << " (" << StringUtils::StringLiteral( FILENAME( directoryOffset ) ) << ") is not a directory" );
	DirectoryRecord * directoryRecord = GetDirectoryRecord( directoryOffset );
	if( directoryRecord->fSorted ) return;
	LINKED_LIST( children, TerritoryHeader, fOffsetOfPreviousFileInDirectory, fOffsetOfNextFileInDirectory, &directoryRecord->fOffsetOfFirstChild, &directoryRecord->fOffsetOfLastChild );
	children.Sort( mSegmentRootAddress, OrderedByFileName ); // NB: this changes the list links in-place
	directoryRecord->fSorted = 1;
	//DBREPORT_TIME_TAKEN( 0, 1000000, children.Sort( mSegmentRootAddress, OrderedByFileName ) );
}

std::string
SharedDataArchive::ReportDirectory( const std::string & name )
{
	std::stringstream ss;
	LOCK( 'r' );
	uint64_t directoryOffset = FindDirectory( name );
	if( directoryOffset ) ss << ReportDirectory( directoryOffset );
	else ss << "Directory " <<  StringUtils::StringLiteral( name ) << " not found\n" << std::endl;
	return ss.str();
}

std::string
SharedDataArchive::ReportDirectory( uint64_t directoryOffset )
{
	std::stringstream ss, ss2;
	LOCK( 'r' );
	ss << ReportFile( directoryOffset, true );
	if( !directoryOffset ) return ss.str();
	if( FILETYPE( directoryOffset ) != 'd' ) RAISE( "file " << directoryOffset << " (" << StringUtils::StringLiteral( FILENAME( directoryOffset ) ) << ") is not a directory" );
	
	
	DirectoryRecord * directoryRecord = GetDirectoryRecord( directoryOffset );
	LINKED_LIST( children, TerritoryHeader, fOffsetOfPreviousFileInDirectory, fOffsetOfNextFileInDirectory, &directoryRecord->fOffsetOfFirstChild, &directoryRecord->fOffsetOfLastChild );
	//if( SH->fSortingPreference && !directoryRecord->fSorted ) SortDirectory( directoryOffset ); // this sort-on-access would require 'w' access
	REPORT_FIELD( directoryRecord, fOffsetOfFirstChild );
	REPORT_FIELD( directoryRecord, fOffsetOfLastChild  );
	REPORT( "List of children", children, "" );
	//REPORT( "Children in reverse", children.Report( true ), "" );
	if( children.Head() ) ss << "          Names of children: " << std::endl;
	for( uint64_t f = children.Head(); f; f = children.Next( f ) ) ss << "              " << StringUtils::StringLiteral( FILENAME( f ) ) << std::endl;
	//ss << HR << HR << HR << std::endl;
	ss << std::endl;
	return ss.str();
}

std::string
SharedDataArchive::ReportFile( const std::string & name, uint64_t maxRowsPerLot )
{
	std::stringstream ss;
	LOCK( 'r' );
	uint64_t offsetOfFirstLot = FindFile( name );
	if( offsetOfFirstLot ) ss << ReportFile( offsetOfFirstLot, false, maxRowsPerLot );
	else ss << "(non-existent file " <<  StringUtils::StringLiteral( name ) << ")\n" << std::endl;
	return ss.str();
}

std::string
SharedDataArchive::ReportFile( uint64_t offsetOfFirstLot, bool toBeContinued, uint64_t maxRowsPerLot )
{
	std::stringstream ss, ss2;
	
	ss << "file " << offsetOfFirstLot;
	if( !offsetOfFirstLot ) { ss << " (NULL)"; return ss.str(); }
	
	LOCK( 'r' );
	
	RECORD( TerritoryHeader, th, offsetOfFirstLot );
	const char * name = FILENAME( offsetOfFirstLot ); 
	uint64_t firstDirectorySibling = offsetOfFirstLot, firstHashTableSibling = offsetOfFirstLot;
	LINKED_LIST( linkedLots,        LotHeader,       fOffsetOfPreviousLotInTerritory,  fOffsetOfNextLotInTerritory,  &offsetOfFirstLot, NULL );
	LINKED_LIST( directorySiblings, TerritoryHeader, fOffsetOfPreviousFileInDirectory, fOffsetOfNextFileInDirectory, &firstDirectorySibling, NULL );
	LINKED_LIST( hashTableSiblings, TerritoryHeader, fOffsetOfPreviousFileInHashTable, fOffsetOfNextFileInHashTable, &firstHashTableSibling, NULL );
	for( uint64_t prev = offsetOfFirstLot; prev; prev = directorySiblings.Previous( prev ) ) firstDirectorySibling = prev;
	for( uint64_t prev = offsetOfFirstLot; prev; prev = hashTableSiblings.Previous( prev ) ) firstHashTableSibling = prev;
	
	ss << ": " << StringUtils::StringLiteral( name ) << std::endl;
	ss << ReportLotHeader( offsetOfFirstLot );
	REPORT( "List of lots in territory", linkedLots, "" );
	ss << SUBHEADING( "file header" );
	REPORT_FIELD( th, fTotalCapacity );
	REPORT_FIELD( th, fCapacityUsed );
	REPORT_FIELD( th, fOffsetOfLastLotInTerritory );
	REPORT_FIELD( th, fOffsetOfPreviousFileInHashTable );
	REPORT_FIELD( th, fOffsetOfNextFileInHashTable );
	REPORT( "List of hash-table siblings", hashTableSiblings, "" );
	REPORT_FIELD( th, fOffsetOfParentDirectory );
	REPORT_FIELD( th, fOffsetOfPreviousFileInDirectory );
	REPORT_FIELD( th, fOffsetOfNextFileInDirectory );
	REPORT( "List of directory siblings", directorySiblings, "" );
	REPORT_FIELD( th, fRelativeOffsetOfPayload );
	REPORT_FIELD( th, fPayloadSize );
	REPORT_FIELD( th, fContentStart );
	REPORT_FIELD( th, fContentWrap );
	REPORT_FIELD( th, fHeadMinimum );
	REPORT_FIELD( th, fHeadMaximum );
	REPORT( "File name", StringUtils::StringLiteral( name ), OFFSET_OF( name ) ); 
	const char * payload = mSegmentRootAddress + offsetOfFirstLot + th->fRelativeOffsetOfPayload;
	ss << SUBHEADING( "payload" );
	if( !toBeContinued )
	{
		if( th->fPayloadSize == 0 ){ REPORT( "(empty)", "", OFFSET_OF( payload ) ); }
		else
		{
			LotHeader * lotHeader = &th->fLotHeader;
			uint64_t relativeOffset = th->fRelativeOffsetOfPayload;
			for( uint64_t bytesToReport = th->fPayloadSize; bytesToReport; )
			{
				uint64_t bytesToReportInThisLot = lotHeader->fLotSize - relativeOffset;
				if( bytesToReportInThisLot > bytesToReport ) bytesToReportInThisLot = bytesToReport;
				ss << ReportPayload( OFFSET_OF( payload ), bytesToReportInThisLot, false, 16, maxRowsPerLot );
				bytesToReport -= bytesToReportInThisLot;
				if( bytesToReport )
				{
					uint64_t gap = lotHeader->fOffsetOfNextLotInTerritory - OFFSET_OF( lotHeader ) - lotHeader->fLotSize;
					if( gap ) ss << "jump " << ( int64_t )gap << " bytes, then ";
					ss << ReportLotHeader( lotHeader->fOffsetOfNextLotInTerritory ) << SUBHEADING( "payload (cont'd)" );
				}
				lotHeader = LH( lotHeader->fOffsetOfNextLotInTerritory );
				relativeOffset = sizeof( LotHeader );
				payload = ( char * )lotHeader + relativeOffset;
			}
		}
		//ss << HR << HR << HR << std::endl;
		ss << std::endl;
	}
	return ss.str();
}

std::string
SharedDataArchive::ReportLotHeader( uint64_t lotOffset )
{
	std::stringstream ss, ss2;
	ss << SUBHEADING( "LotHeader" );
	if( lotOffset == 0 ) ss << "(null Lot - ??""?)";
	else
	{
		RECORD( LotHeader, lotHeader, lotOffset );
		REPORT_FIELD( lotHeader, fLotSize );
		REPORT_FIELD( lotHeader, fOffsetOfPreviousLotInTerritory );
		REPORT_FIELD( lotHeader, fOffsetOfNextLotInTerritory );
	}
	return ss.str();
}

std::string
SharedDataArchive::ReportPayload( uint64_t offset, uint64_t nBytes, bool allhex, uint64_t bytesPerRow, uint64_t maxRowsToShow )
{
	std::stringstream ss, ss2;
	
	uint64_t suspend = offset + bytesPerRow * ( ( maxRowsToShow + 1 ) / 2 );
	maxRowsToShow /= 2;
	if( maxRowsToShow && nBytes % bytesPerRow ) maxRowsToShow--;
	uint64_t resume = offset + nBytes - ( nBytes % bytesPerRow ) - ( bytesPerRow * maxRowsToShow );
	
	while( offset && nBytes )
	{
		std::stringstream row;
		uint64_t nBytesReadThisRow = 0;
		for( uint64_t i = 0; i < bytesPerRow && nBytes; i++ )
		{
			char c = mSegmentRootAddress[ offset + nBytesReadThisRow++ ];
			if(      c == '\0' && !allhex ) row << "\\0";
			else if( c == '\t' && !allhex ) row << "\\t";
			else if( c == '\n' && !allhex ) row << "\\n";
			else if( c == '\v' && !allhex ) row << "\\v";
			else if( c == '\r' && !allhex ) row << "\\r";
			else if( c >= 32 && c < 127 && !allhex ) row << c << " ";
			else row << std::setfill( '0' ) << std::setw( 2 ) << std::hex << std::uppercase << ( int )( unsigned char )c << std::setw( 0 );
			row << " ";
			nBytes--;
		}
		uint64_t renderedLength = row.tellp();
		if( renderedLength )
		{
			uint64_t end = 3 * bytesPerRow;
			if( renderedLength < end ) row << std::string( ( unsigned int )( end - renderedLength - 1 ), '.' );
			REPORT( row.str(), "\x1B ", offset );
		}
		offset += nBytesReadThisRow;
		if( offset >= suspend && resume > offset )
		{
			ss << "[...]\n";
			nBytes -= ( resume - offset );
			offset = resume;
		}
	}
	return ss.str();
}

SharedDataArchive::File
SharedDataArchive::Open( const std::string & name, char mode, uint64_t ensureMinimumPayloadSize, char type )
{
	return SharedDataArchive::File( *this, &name, 0, mode, ensureMinimumPayloadSize, type );
}

SharedDataArchive::File
SharedDataArchive::Open( uint64_t offsetOfFirstLot, char mode, char type )
{
	return SharedDataArchive::File( *this, NULL, offsetOfFirstLot, mode, 0, type );
}

SharedDataArchive::File::File( SharedDataArchive & sda, const std::string * name, uint64_t offsetOfFirstLot, char mode, uint64_t ensureMinimumPayloadSize, char type )
: mArchive( &sda ), mMode( tolower( mode ) ), mIsWriteable( mMode != 'r' ), mLock( mArchive->mReadWriteMutex, mIsWriteable ? 'w' : 'r' )
// A new lock is created from the same ReadWriteMutex, rather than creating a new ReadWriteMutex instance with the same resource name.
// This means multiple files can be open for writing at the same time---crucially, it also means the File object isn't competing with the locks of the lower-level SharedDataArchive methods that implement its operations
{
	if( name ) offsetOfFirstLot = mArchive->FindFile( *name, ensureMinimumPayloadSize, type );
	mSegmentRootAddress = mArchive->mSegmentRootAddress;
	mTop = 0;
	mWalkIncludesDirectories = -1;
	Open( offsetOfFirstLot );
}

uint64_t
SharedDataArchive::File::Open( uint64_t offsetOfFirstLot )
{
	mTerritoryHeader = TH( offsetOfFirstLot );
	mOffsetOfFirstLot = offsetOfFirstLot;
	mOffsetOfCurrentLot = offsetOfFirstLot;
	mCurrentRelativeOffsetWithinLot = offsetOfFirstLot ? mTerritoryHeader->fRelativeOffsetOfPayload : 0;
	mPositionWithinPayload = 0;
	mHeadPosition = 0;
	
	if( offsetOfFirstLot && FILETYPE( offsetOfFirstLot ) != 'd' )
	{
		if( mMode == 'w' ) Wipe();
		else if( mMode == 'a' ) Seek( 0, end );
		else if( mTerritoryHeader->fContentWrap > 0 ) Seek( mTerritoryHeader->fHeadMinimum, beg );
	}
	return offsetOfFirstLot;
}

bool
SharedDataArchive::File::Exists( void ) const
{
	return ( mOffsetOfFirstLot != 0 );
}

SharedDataArchive &
SharedDataArchive::File::GetArchive( void ) const
{
	return *mArchive;
}

SharedDataArchive::File &
SharedDataArchive::File::Wipe( void )
{
	if( !mIsWriteable ) RAISE_SUBCLASS( WriteAccessDeniedError, "cannot wipe " << GetDescription() << " because it was opened as read-only" );
	if( !mOffsetOfFirstLot ) return *this;
	mTerritoryHeader->fContentStart = 0; // but leave fContentWrap as it is
	mTerritoryHeader->fHeadMinimum  = 0;
	mTerritoryHeader->fHeadMaximum  = 0;
	mTerritoryHeader->fPayloadSize  = 0;
	mTerritoryHeader->fCapacityUsed = mTerritoryHeader->fRelativeOffsetOfPayload - sizeof( LotHeader ); // LotHeaders don't count as using up "capacity" but the rest of the TerritoryHeader does
	mOffsetOfCurrentLot = mOffsetOfFirstLot;
	mCurrentRelativeOffsetWithinLot = mTerritoryHeader->fRelativeOffsetOfPayload;
	mPositionWithinPayload = 0;
	mHeadPosition = 0;
	return *this;
}

SharedDataArchive::File &
SharedDataArchive::File::Truncate( bool shrink )
{
	if( !mIsWriteable ) RAISE_SUBCLASS( WriteAccessDeniedError, "cannot truncate " << GetDescription() << " because it was opened as read-only" );
	if( !mOffsetOfFirstLot ) return *this;
	mTerritoryHeader->fHeadMaximum = mHeadPosition;
	if( mPositionWithinPayload >= mTerritoryHeader->fContentStart ) mTerritoryHeader->fPayloadSize = mPositionWithinPayload;
	mTerritoryHeader->fCapacityUsed = mTerritoryHeader->fPayloadSize + mTerritoryHeader->fRelativeOffsetOfPayload - sizeof( LotHeader );
	if( shrink ) Shrink();
	return *this;
}

SharedDataArchive::File &
SharedDataArchive::File::Shrink( void )
{
	if( !mIsWriteable ) RAISE_SUBCLASS( WriteAccessDeniedError, "cannot shrink " << GetDescription() << " because it was opened as read-only" );
	mArchive->ShrinkTerritory( mOffsetOfFirstLot );
	return *this;
}

SharedDataArchive::File &
SharedDataArchive::File::Remove( void )
{
	if( !mIsWriteable ) RAISE_SUBCLASS( WriteAccessDeniedError, "cannot remove " << GetDescription() << " because it was opened as read-only" );
	mArchive->RemoveFile( mOffsetOfFirstLot );
	mOffsetOfFirstLot = 0;
	return *this;
}

uint64_t
SharedDataArchive::File::GetFileOffset( void ) const
{
	return mOffsetOfFirstLot;
}

const char *
SharedDataArchive::File::GetName( void ) const
{
	if( !mOffsetOfFirstLot ) return NULL;
	return FILENAME( mOffsetOfFirstLot );
}

char
SharedDataArchive::File::GetType( void ) const
{
	if( !mOffsetOfFirstLot ) return 0;
	return FILETYPE( mOffsetOfFirstLot );
}

std::string
SharedDataArchive::File::GetDescription( void ) const
{
	std::stringstream ss;
	ss << "file " << mOffsetOfFirstLot << " (" << ( mOffsetOfFirstLot ? StringUtils::StringLiteral( FILENAME( mOffsetOfFirstLot ) ) : "NULL" ) << ")";
	return ss.str();
}

uint64_t
SharedDataArchive::File::Tell( void ) const
{
	return mOffsetOfFirstLot ? mHeadPosition : 0;
}

uint64_t
SharedDataArchive::File::TellMin( void ) const
{
	return mOffsetOfFirstLot ? mTerritoryHeader->fHeadMinimum : 0;
}

uint64_t
SharedDataArchive::File::TellMax( void ) const
{
	return mOffsetOfFirstLot ? mTerritoryHeader->fHeadMaximum : 0;
}

//uint64_t
//SharedDataArchive::File::BytesUsed( void ) const
//{
//	return mOffsetOfFirstLot ? mTerritoryHeader->fPayloadSize + mTerritoryHeader->fRelativeOffsetOfPayload : 0; // TODO: would need to add overhead of LotHeaders of non-initial Lots too
//}

//uint64_t
//SharedDataArchive::File::Capacity( void ) const // total capacity (including file header but not LotHeaders)
//{
//	return mOffsetOfFirstLot ? mTerritoryHeader->fTotalCapacity : 0;
//}


uint64_t
SharedDataArchive::File::AvailableContentLength( void ) const
{
	return mOffsetOfFirstLot ? mTerritoryHeader->fHeadMaximum - mTerritoryHeader->fHeadMinimum : 0;
}

uint64_t
SharedDataArchive::File::PayloadCapacityUsed( void ) const
{
	return mOffsetOfFirstLot ? mTerritoryHeader->fPayloadSize : 0;
}

uint64_t
SharedDataArchive::File::PayloadCapacity( void ) const // total capacity that can be used for payload (i.e. excluding file header)
{
	return mOffsetOfFirstLot ? ( mTerritoryHeader->fTotalCapacity + sizeof( LotHeader ) - mTerritoryHeader->fRelativeOffsetOfPayload ) : 0;
}

#define CURRENT_LOT_HEADER   ( ( LotHeader * )currentLotBaseAddress )
uint64_t
SharedDataArchive::File::Read( void * dst, uint64_t nBytesToRead ) // not const
{
	if( !mOffsetOfFirstLot || !dst || !nBytesToRead ) return 0;
	if( mHeadPosition + nBytesToRead > mTerritoryHeader->fHeadMaximum )
		nBytesToRead = mTerritoryHeader->fHeadMaximum - mHeadPosition;
	
	char * out = ( char * )dst;
	const char * currentLotBaseAddress = mSegmentRootAddress + mOffsetOfCurrentLot;
	while( nBytesToRead )
	{
		if( mTerritoryHeader->fContentWrap > 0 && mPositionWithinPayload >= ( uint64_t )mTerritoryHeader->fContentWrap )
		{
			mOffsetOfCurrentLot = mOffsetOfFirstLot;
			mCurrentRelativeOffsetWithinLot = mTerritoryHeader->fRelativeOffsetOfPayload;
			mPositionWithinPayload = 0;
			currentLotBaseAddress = mSegmentRootAddress + mOffsetOfCurrentLot;
		}
		else if( mCurrentRelativeOffsetWithinLot >= CURRENT_LOT_HEADER->fLotSize )
		{
			if( !CURRENT_LOT_HEADER->fOffsetOfNextLotInTerritory ) break; // differs between Read and Write
			mOffsetOfCurrentLot = CURRENT_LOT_HEADER->fOffsetOfNextLotInTerritory;
			mCurrentRelativeOffsetWithinLot = sizeof( LotHeader );
			currentLotBaseAddress = mSegmentRootAddress + mOffsetOfCurrentLot;
		}
		else
		{
			uint64_t batchSize = CURRENT_LOT_HEADER->fLotSize - mCurrentRelativeOffsetWithinLot;
			if( batchSize > nBytesToRead ) batchSize = nBytesToRead;
			if( mTerritoryHeader->fContentWrap > 0 && mPositionWithinPayload + batchSize > ( uint64_t )mTerritoryHeader->fContentWrap )
				batchSize = mTerritoryHeader->fContentWrap - mPositionWithinPayload;
			memcpy( out, currentLotBaseAddress + mCurrentRelativeOffsetWithinLot, as_size_t( batchSize ) );
			out += batchSize;
			nBytesToRead -= batchSize;
			mCurrentRelativeOffsetWithinLot += batchSize;
			mPositionWithinPayload += batchSize;
			mHeadPosition += batchSize;
		}
	}
	return ( uint64_t )( out - ( char * )dst );
}

uint64_t
SharedDataArchive::File::Write( const void * src, uint64_t nBytesToWrite )
{
	if( !mIsWriteable ) RAISE_SUBCLASS( WriteAccessDeniedError, "cannot write to " << GetDescription() << " because it was opened as read-only" );
	if( !mOffsetOfFirstLot || !src || !nBytesToWrite) return 0;
	
	if( mTerritoryHeader->fContentWrap <= 0 )
	{
		if( mTerritoryHeader->fContentWrap < 0 )
		{
			uint64_t granularity = ( uint64_t )( -mTerritoryHeader->fContentWrap );
			if( ( mPositionWithinPayload + nBytesToWrite ) % granularity )
				RAISE( GetDescription() << " is set to AutoWrap() with a granularity of " << granularity << " bytes. For this to work, it can only accept Writes() that will leave its internal content pointer (currently " << mPositionWithinPayload << ") at a multiple of " << granularity );
		}
		uint64_t nBytesAvailable = mTerritoryHeader->fTotalCapacity - mTerritoryHeader->fCapacityUsed;
		if( nBytesToWrite > nBytesAvailable )
		{
			// Try to extend territory
			uint64_t requiredExtraCapacity = nBytesToWrite - nBytesAvailable;
			uint64_t desiredExtraCapacity = mTerritoryHeader->fTotalCapacity; // aim to double capacity
			while( desiredExtraCapacity < requiredExtraCapacity ) desiredExtraCapacity *= 2;
			uint64_t addedCapacity = 0;
			while( true )
			{
				addedCapacity = mArchive->ExtendTerritory( mOffsetOfFirstLot, desiredExtraCapacity, false );
				if( addedCapacity ) break; // success
				if( desiredExtraCapacity <= requiredExtraCapacity ) break; // failure
				if( desiredExtraCapacity + sizeof( LotHeader ) <= SMALLEST_LOT_SIZE ) break; // failure
				desiredExtraCapacity = requiredExtraCapacity + ( desiredExtraCapacity - requiredExtraCapacity ) / 2;
			}
			if( addedCapacity < requiredExtraCapacity )
			{
				if( mTerritoryHeader->fContentWrap < 0 && PayloadCapacity() <= INT64_MAX )
				{
					uint64_t granularity = ( uint64_t )( -mTerritoryHeader->fContentWrap ); // negative value decoded as the negative of the desired granularity
					Wrap( ( int64_t )PayloadCapacity(), granularity );
				}
				else { RAISE_SUBCLASS( OutOfSpaceError, "failed to extend territory for file " << StringUtils::StringLiteral( FILENAME( mOffsetOfFirstLot ) ) ); }
			}
		}
	}
	
	// NB: from here on the Read() and Write() code is very similar, with the exceptions marked "differs" below
	//     so they could potentially be merged on the DRY principle
	const char * in = ( const char * ) src;
	char * currentLotBaseAddress = mSegmentRootAddress + mOffsetOfCurrentLot;
	while( nBytesToWrite )
	{
		if( mTerritoryHeader->fContentWrap > 0 && mPositionWithinPayload >= ( uint64_t )mTerritoryHeader->fContentWrap )
		{
			mOffsetOfCurrentLot = mOffsetOfFirstLot;
			mCurrentRelativeOffsetWithinLot = mTerritoryHeader->fRelativeOffsetOfPayload;
			mPositionWithinPayload = 0;
			currentLotBaseAddress = mSegmentRootAddress + mOffsetOfCurrentLot;
		}
		else if( mCurrentRelativeOffsetWithinLot >= CURRENT_LOT_HEADER->fLotSize )
		{
			if( !CURRENT_LOT_HEADER->fOffsetOfNextLotInTerritory )
				RAISE( "bug - SharedDataArchive::File::Write() somehow neglected to extend territory far enough" ); // differs between Read and Write
			mOffsetOfCurrentLot = CURRENT_LOT_HEADER->fOffsetOfNextLotInTerritory;
			mCurrentRelativeOffsetWithinLot = sizeof( LotHeader );
			currentLotBaseAddress = mSegmentRootAddress + mOffsetOfCurrentLot;
		}
		else
		{
			uint64_t batchSize = CURRENT_LOT_HEADER->fLotSize - mCurrentRelativeOffsetWithinLot;
			if( batchSize > nBytesToWrite ) batchSize = nBytesToWrite;
			if( mTerritoryHeader->fContentWrap > 0 && mPositionWithinPayload + batchSize > ( uint64_t )mTerritoryHeader->fContentWrap )
				batchSize = mTerritoryHeader->fContentWrap - mPositionWithinPayload;
			memcpy( currentLotBaseAddress + mCurrentRelativeOffsetWithinLot, in, as_size_t( batchSize ) ); // differs between Read and Write
			in += batchSize;
			nBytesToWrite -= batchSize;
			mCurrentRelativeOffsetWithinLot += batchSize;
			mPositionWithinPayload += batchSize;
			mHeadPosition += batchSize;
			if( mHeadPosition > mTerritoryHeader->fHeadMaximum ) // differs between Read and Write
			{
				if( mTerritoryHeader->fContentWrap > 0 )
				{
					if( mTerritoryHeader->fHeadMaximum > mTerritoryHeader->fHeadMinimum &&
					    mPositionWithinPayload - batchSize <= mTerritoryHeader->fContentStart &&
					    mPositionWithinPayload > mTerritoryHeader->fContentStart )
					{
						uint64_t push = mPositionWithinPayload - mTerritoryHeader->fContentStart;
						mTerritoryHeader->fHeadMinimum += push;
						mTerritoryHeader->fContentStart += push;
						if( mTerritoryHeader->fContentStart >= ( uint64_t )mTerritoryHeader->fContentWrap ) mTerritoryHeader->fContentStart -= mTerritoryHeader->fContentWrap;
					}
				}
				mTerritoryHeader->fHeadMaximum = mHeadPosition;
				/* wrapping-TODO:
				   - randomized testing, especially in fragmented files
				*/
			}
		}
	}
	if( mTerritoryHeader->fContentWrap > 0 && mTerritoryHeader->fContentStart + mTerritoryHeader->fHeadMaximum - mTerritoryHeader->fHeadMinimum >= ( uint64_t )mTerritoryHeader->fContentWrap )
		mTerritoryHeader->fPayloadSize = ( uint64_t )mTerritoryHeader->fContentWrap;
	else if( mTerritoryHeader->fPayloadSize < mPositionWithinPayload )
		mTerritoryHeader->fPayloadSize = mPositionWithinPayload;
	mTerritoryHeader->fCapacityUsed = mTerritoryHeader->fPayloadSize + mTerritoryHeader->fRelativeOffsetOfPayload - sizeof( LotHeader );


	return ( uint64_t )( in - ( char * )src );
}

SharedDataArchive::File & 
SharedDataArchive::File::Wrap( int64_t payloadCapacity, uint64_t granularity )
{
	if( !mIsWriteable ) RAISE_SUBCLASS( WriteAccessDeniedError, "cannot change wrapping of " << GetDescription() << " because it was opened as read-only" );
	if( payloadCapacity > 0 && granularity > INT64_MAX ) RAISE( "granularity cannot be larger than " << INT64_MAX );
	if( payloadCapacity > 0 && granularity > 1 ) payloadCapacity -= payloadCapacity % ( int64_t )granularity;
	if( payloadCapacity == mTerritoryHeader->fContentWrap )	return *this;
	
	if( payloadCapacity < 0 )
	{
		granularity = ( uint64_t )( -payloadCapacity );
		if( mPositionWithinPayload % granularity )
			RAISE( "cannot set auto-wrapping on " << GetDescription() << " with a granularity of " << granularity << " unless its internal content pointer is at a multiple of " << granularity << " (currently it is at " <<mPositionWithinPayload << ")" );

	}
	uint64_t contentStop = mTerritoryHeader->fContentStart + mTerritoryHeader->fHeadMaximum - mTerritoryHeader->fHeadMinimum;
	if( payloadCapacity <= 0 && mTerritoryHeader->fContentStart > 0 ) RAISE( "cannot remove wrapping from " << GetDescription() << " because its existing content is wrapped (does not start at the beginning of the payload)" );
	if( mTerritoryHeader->fContentWrap > 0 && contentStop > ( uint64_t )mTerritoryHeader->fContentWrap ) RAISE( "cannot change wrapping of " << GetDescription() << " because its content has already wrapped past its current boundary" );
	if( payloadCapacity > 0 && contentStop > ( uint64_t )payloadCapacity ) RAISE( "cannot change wrapping of " << GetDescription() << " to " << payloadCapacity << " because its content has already been written past that point" );
	
	mTerritoryHeader->fContentWrap = payloadCapacity;
	return *this;
}

SharedDataArchive::File & 
SharedDataArchive::File::AutoWrap( uint64_t granularity )
{
	if( granularity > INT64_MAX ) RAISE( "granularity cannot be larger than " << INT64_MAX );
	return Wrap( -( int64_t )granularity,  1 );
	// storing a negative payloadCapacity in mTerritoryHeader->fContentWrap means
	// "wrap wherever you have to when you finally run out of space, using the absolute of this value as your granularity"
}

std::string
SharedDataArchive::File::Read( uint64_t nBytesToRead ) // not const
{
	std::string s;
	if( nBytesToRead )
	{
		// NB: This performs one more allocation and one more copy than we would really like.
		// We could instead possibly do   std::string s(nBytesToRead, 0); Read( ( char * )s.data(), s.length() );
		// C++11 and later usually allows this in practice, but strictly it is not guaranteed and is therefore "undefined behaviour":
		// see https://stackoverflow.com/a/35630047/ and https://en.cppreference.com/w/cpp/string/basic_string/data
		char * buffer = new char[ as_size_t( nBytesToRead ) ];
		uint64_t nBytesRead = Read( buffer, nBytesToRead );
		s.assign( buffer, as_size_t( nBytesRead ) );
		delete [] buffer;
	}
	return s;
}

std::string
SharedDataArchive::File::Read( void ) // not const
{
	return Read( mTerritoryHeader->fHeadMaximum - mHeadPosition );
}

uint64_t
SharedDataArchive::File::Write( const std::string & src )
{
	return Write( ( const void * )src.c_str(), src.length() );
}

SharedDataArchive::File &
SharedDataArchive::File::Seek( int64_t relativePosition, int originCode ) // not const
{
	if( !mOffsetOfFirstLot ) return *this;
	
	uint64_t target;
	if (     originCode == std::ios_base::beg ) target = 0;
	else if( originCode == std::ios_base::cur ) target = mHeadPosition;
	else if( originCode == std::ios_base::end ) target = mTerritoryHeader->fHeadMaximum;
	else RAISE( "File::Seek() received unrecognized originCode " << originCode );
	
	if( relativePosition < 0 && ( uint64_t )( -relativePosition ) >= target ) target = 0; // 0 is fine; it does not need to be adjusted for wrapping (this line just prevents wraparound of the unsigned integer representation)
	else target += relativePosition;
	if( target > mTerritoryHeader->fHeadMaximum ) target = mTerritoryHeader->fHeadMaximum;
	if( target < mTerritoryHeader->fHeadMinimum ) RAISE( "cannot seek earlier than " << mTerritoryHeader->fHeadMinimum << " in file " << GetName() ); // cannot seek into forgotten content of a wrapped file
	mHeadPosition = target;
	
	target -= mTerritoryHeader->fHeadMinimum;
	if( mTerritoryHeader->fContentWrap > 0 ) target = ( target + mTerritoryHeader->fContentStart ) % mTerritoryHeader->fContentWrap;
	// now `target` is in unlooped coordinates, just like mPositionWithinPayload
	
	if( mPositionWithinPayload == target ) return *this;
		
	uint64_t distanceToTarget = ( mPositionWithinPayload > target ) ? ( mPositionWithinPayload - target ) : ( target - mPositionWithinPayload );
	if( target < distanceToTarget ) // rewind, because it's quicker to get there from the beginning
	{
		mPositionWithinPayload = 0;
		distanceToTarget = target;
		mOffsetOfCurrentLot = mOffsetOfFirstLot;
		mCurrentRelativeOffsetWithinLot = mTerritoryHeader->fRelativeOffsetOfPayload;
	}
	uint64_t maxPayloadSize = PayloadCapacity();
	if( maxPayloadSize - target < distanceToTarget ) // skip to the end (of the entire territory, not just of the payload, because we won't know for sure in which Lot it currently ends, until we get there)
	{
		mPositionWithinPayload = maxPayloadSize;
		distanceToTarget = mPositionWithinPayload - target;
		mOffsetOfCurrentLot = mTerritoryHeader->fOffsetOfLastLotInTerritory;
		mCurrentRelativeOffsetWithinLot = LH( mOffsetOfCurrentLot )->fLotSize;
	}
	
	LINKED_LIST( lots, LotHeader, fOffsetOfPreviousLotInTerritory, fOffsetOfNextLotInTerritory, &mOffsetOfFirstLot, &mTerritoryHeader->fOffsetOfLastLotInTerritory );
	while( mPositionWithinPayload < target )
	{
		RECORD( LotHeader, lot, mOffsetOfCurrentLot );
		uint64_t maxJumpWithinCurrentLot = lot->fLotSize - mCurrentRelativeOffsetWithinLot;
		if( distanceToTarget > maxJumpWithinCurrentLot || ( distanceToTarget == maxJumpWithinCurrentLot && lot->fOffsetOfNextLotInTerritory ) )
		{
			mPositionWithinPayload += maxJumpWithinCurrentLot;
			mOffsetOfCurrentLot = lot->fOffsetOfNextLotInTerritory;
			if( !mOffsetOfCurrentLot ) RAISE( "bug - SharedDataArchive::File::Seek() jumped forwards into limbo" );
			mCurrentRelativeOffsetWithinLot = sizeof( LotHeader );
			distanceToTarget -= maxJumpWithinCurrentLot;
		}
		else
		{
			mPositionWithinPayload += distanceToTarget;
			mCurrentRelativeOffsetWithinLot += distanceToTarget;
			distanceToTarget = 0;
		}
	}
	while( mPositionWithinPayload > target )
	{
		RECORD( LotHeader, lot, mOffsetOfCurrentLot );
		uint64_t maxJumpWithinCurrentLot = mCurrentRelativeOffsetWithinLot - sizeof( LotHeader );
		if( distanceToTarget > maxJumpWithinCurrentLot )
		{
			mPositionWithinPayload -= maxJumpWithinCurrentLot;
			mOffsetOfCurrentLot = lot->fOffsetOfPreviousLotInTerritory;
			if( !mOffsetOfCurrentLot ) RAISE( "bug - SharedDataArchive::File::Seek() jumped backwards into limbo" );
			mCurrentRelativeOffsetWithinLot = LH( mOffsetOfCurrentLot )->fLotSize;
			distanceToTarget -= maxJumpWithinCurrentLot;
		}
		else
		{
			mPositionWithinPayload -= distanceToTarget;
			mCurrentRelativeOffsetWithinLot -= distanceToTarget;
			distanceToTarget = 0;
		}
	}
	return *this;
}

bool
SharedDataArchive::File::MakeReadOnly( void )
{
	bool result = mLock.Downgrade();
	if( result ) mMode = 'r';
	return result;
}

void
SharedDataArchive::File::Close( void )
{
	mLock.UnlockPrematurely();
	mOffsetOfFirstLot = 0;
}

std::string
SharedDataArchive::File::Report( uint64_t maxRowsPerLot ) const
{
	if( GetType() == 'd' )  return mArchive->ReportDirectory( mOffsetOfFirstLot );
	else return mArchive->ReportFile( mOffsetOfFirstLot, false, maxRowsPerLot );
}

uint64_t
SharedDataArchive::File::Walk( uint64_t top, int includeDirectories, const char * restrictToTypes )
{
	if( !mOffsetOfFirstLot ) return 0;
	char fileType = FILETYPE( mOffsetOfFirstLot );	
	bool isDirectory = ( fileType == 'd' );
	if( top ) mTop = top;
	if( !mTop ) mTop = ( isDirectory ? mOffsetOfFirstLot : mTerritoryHeader->fOffsetOfParentDirectory );
	if( includeDirectories >= 0 ) mWalkIncludesDirectories = includeDirectories;
	if( restrictToTypes )
	{
		mRestrictWalkToTypes.clear();
		while( *restrictToTypes ) mRestrictWalkToTypes.insert( *restrictToTypes++ );
	}
	if( mWalkIncludesDirectories < 0 ) mWalkIncludesDirectories = 0;
	return IncludedInWalk() ? mOffsetOfFirstLot : Next();
}

bool
SharedDataArchive::File::IncludedInWalk( void )
{
	if( !mOffsetOfFirstLot ) return true; // because 0 can and should be returned by Next() when appropriate
	char fileType = FILETYPE( mOffsetOfFirstLot );	
	bool isDirectory = ( fileType == 'd' );
	return ( isDirectory && mWalkIncludesDirectories ) || ( !isDirectory && mRestrictWalkToTypes.empty() ) || mRestrictWalkToTypes.count( fileType );
}

uint64_t
SharedDataArchive::File::Next( void )
{
	while( mOffsetOfFirstLot )
	{
		uint64_t candidate;
		// if we're at a directory and there is a descendant, go there
		if( FILETYPE( mOffsetOfFirstLot ) =='d' && 0 != ( candidate = mArchive->GetDirectoryRecord( mOffsetOfFirstLot )->fOffsetOfFirstChild ) )
		{
			Open( candidate );
		}
		// otherwise, if we have a successor sibling in the same directory, go there
		else if( 0 != ( candidate = mTerritoryHeader->fOffsetOfNextFileInDirectory ) )
		{
			Open( candidate );
		}
		else // otherwise, it's time to climb the tree looking for uncles and great-uncles:
		{
			// go up as many levels as it takes,...
			while( 0 != ( candidate = mTerritoryHeader->fOffsetOfParentDirectory )  )
			{
				if( candidate == mTop ) // ...stopping when we reach the top of the pre-defined scope, ...
				{
					Open( 0 );
					break; // out of the climb
				}
				Open( candidate ); // ...and as we go, we won't yield the ancestor itself (we will have already been there), ...
				// ... but if the ancestor has a successor sibling, go there:
				if( 0 != ( candidate = mTerritoryHeader->fOffsetOfNextFileInDirectory ) )
				{
					Open( candidate );
					break; // out of the climb
				}
			}
		}
		if( mOffsetOfFirstLot == mTop ) Open( 0 ); // guard against the possibility that we're dealing with a completely empty tree at the root location
		if( IncludedInWalk() ) break;
	}
	return mOffsetOfFirstLot;
}

void
SharedDataArchive::SetSortingPreference( bool setting )
{
	LOCK( 'w' );
	SH->fSortingPreference = setting;
	if( !setting ) return;
	File f = Open( "$d" );
	for( f.Walk( 0, true ); f.Exists(); f.Next() )
	{
		if( f.GetType() == 'd' ) SortDirectory( f.GetFileOffset() );
	}
}

class TemporaryLotRecord
{
	public:
		TemporaryLotRecord( uint64_t offset, uint64_t size, uint64_t offsetOfParent, uint64_t indexWithinChain, uint64_t previous, uint64_t next )
		: mOffset( offset ), mSize( size ), mOffsetOfParent( offsetOfParent ), mIndexWithinChain( indexWithinChain ), mPrevious( previous ), mNext( next ) {}
		uint64_t mOffset;
		uint64_t mSize;
		uint64_t mOffsetOfParent;
		uint64_t mIndexWithinChain;
		uint64_t mPrevious;
		uint64_t mNext;
};
bool TemporaryLotRecordSorter( const TemporaryLotRecord & lhs, const TemporaryLotRecord & rhs ) { return lhs.mOffset < rhs.mOffset; }

std::string
SharedDataArchive::LotByLotSummary( uint64_t * returnNumberOfFiles )
{
	std::stringstream ss;
	if( returnNumberOfFiles ) *returnNumberOfFiles = 0;
	std::vector< TemporaryLotRecord > allLots;
	File f = Open( "$d", 'r' ); // acts as lock within this scope
	RECORD( VacatedLotRecord, vlrPtr, SH->fOffsetOfVacatedLotRecords );
	for( uint64_t vlri = 0; vlri < SH->fNumberOfLotSizes; vlri++, vlrPtr++ )
	{
		LINKED_LIST( lots, LotHeader, fOffsetOfPreviousLotInTerritory, fOffsetOfNextLotInTerritory, &vlrPtr->fOffsetOfFirstVacatedLot, &vlrPtr->fOffsetOfLastVacatedLot );
		for( uint64_t i = 0, lot = lots.Head(); lot; i++, lot = lots.Next( lot ) )
			allLots.emplace_back( lot, vlrPtr->fLotSize, 0,  i, LH( lot )->fOffsetOfPreviousLotInTerritory, LH( lot )->fOffsetOfNextLotInTerritory ); // CPLUSPLUS11
	}
	for( f.Walk( 0, true ); f.Exists(); f.Next() )
	{
		if( returnNumberOfFiles && f.GetType() != 'd' ) ( *returnNumberOfFiles )++;
		uint64_t first = f.GetFileOffset();
		LINKED_LIST( lots, LotHeader, fOffsetOfPreviousLotInTerritory, fOffsetOfNextLotInTerritory, &first, &TH( first )->fOffsetOfLastLotInTerritory );
		for( uint64_t i = 0, lot = first; lot; i++, lot = lots.Next( lot ) )
			allLots.emplace_back( lot, LH( lot )->fLotSize, first, i, LH( lot )->fOffsetOfPreviousLotInTerritory, LH( lot )->fOffsetOfNextLotInTerritory );  // CPLUSPLUS11
	}
	allLots.emplace_back( SH->fOffsetOfUnallocatedSpace, 0, 0, 0, 0, 0 );
	std::sort( allLots.begin(), allLots.end(), TemporaryLotRecordSorter );
	uint64_t expectedOffset = SH->fOffsetOfSegmentName;
	while( mSegmentRootAddress[ expectedOffset ] ) expectedOffset++; // segment name
	expectedOffset++; // null terminator
	while( expectedOffset % 8 ) expectedOffset++; // padding null-terminated segment name to nearest multiple of 8 bytes
	uint64_t len, maxWidthOfOffset = 0, maxWidthOfSize = 0, maxWidthOfOffsetOfParent = 0, maxWidthOfIndexWithinChain = 0, maxWidthOfPrevious = 0, maxWidthOfNext = 0, maxWidthOfName = 0;
#	define MAX_LENGTH( THING, VAR ) { ss << THING; len = ss.tellp(); ss.str( "" ); if( len > VAR ) VAR = len; }
#	define MAX_FIELD_LENGTH( FIELD ) MAX_LENGTH( it->m ## FIELD, maxWidthOf ## FIELD )
	for( std::vector< TemporaryLotRecord >::iterator it = allLots.begin(); it != allLots.end(); it++ )
	{
		MAX_FIELD_LENGTH( Offset );
		MAX_FIELD_LENGTH( Size );
		MAX_FIELD_LENGTH( OffsetOfParent );
		MAX_FIELD_LENGTH( IndexWithinChain );
		MAX_FIELD_LENGTH( Previous );
		MAX_FIELD_LENGTH( Next ); 
		if( it->mOffset == it->mOffsetOfParent ) MAX_LENGTH( StringUtils::StringLiteral( FILENAME( it->mOffset ) ), maxWidthOfName );
	}
	for( std::vector< TemporaryLotRecord >::iterator it = allLots.begin(); it != allLots.end(); it++ )
	{
		std::stringstream ssl;
		
		if( it->mOffset != expectedOffset ) ss << "*** ?!  " << it->mOffset - expectedOffset << "-byte gap  ***\n";
		ssl << std::setw( maxWidthOfOffset ) << it->mOffset << "  - ";
		if( it->mSize == 0 )
		{
			ssl << "   " << std::setw( maxWidthOfOffset ) << " " << std::setw( maxWidthOfSize ) << " ";
			ssl << ": unallocated space";
		}
		else
		{
			ssl << std::setw( maxWidthOfOffset ) << it->mOffset + it->mSize - 1;
			ssl << " (" << std::setw( maxWidthOfSize ) << it->mSize << ")";
			ssl << ": Lot " << std::setw( maxWidthOfIndexWithinChain )<< it->mIndexWithinChain;
			if( it->mOffsetOfParent ) ssl << " of file " << std::setw( maxWidthOfOffsetOfParent ) << it->mOffsetOfParent << ", " << std::setw( maxWidthOfName ) << std::left << StringUtils::StringLiteral( FILENAME( it->mOffsetOfParent ) ) << std::right;
			else
			{
				ssl << " of chain of vacated " << it->mSize << "-byte Lots";
				ssl << std::setw( 24 + maxWidthOfOffset + maxWidthOfOffset + maxWidthOfSize + maxWidthOfIndexWithinChain + maxWidthOfOffsetOfParent + maxWidthOfName - ssl.tellp() ) << " ";
			}
		}
		if( it->mPrevious || it->mNext )
		{
			ssl << "  ";
			if( it->mPrevious ) ssl << std::setw( maxWidthOfPrevious ) << it->mPrevious << "<-"; 
			else ssl << "  " << std::setw( maxWidthOfPrevious ) << " ";
			ssl << "*";
			if( it->mNext )     ssl << "->" << std::setw( maxWidthOfNext ) << std::left << it->mNext << std::right;
			
		}
		ss << ssl.str() << std::endl;
		expectedOffset = it->mOffset + it->mSize;
	}
	return ss.str();
}

// TODO: method for mopping up vacated lots that can be deallocated and merged back into the unallocated space?

std::ostream & operator<<( std::ostream & os, SharedDataArchive::File & x ) { return os << x.Report(); }
std::ostream & operator<<( std::ostream & os, SharedDataArchive & x ) { return os << x.ReportHeader(); }

//////////////////////////////////////////////////////////////////////


int
SharedDataArchive::Demo( int argc, const char * argv[] )
{
		
	START_ARGS	
	if( USAGE( "--help", R"ZARP(
Arguments/options for `SharedDataArchive` mode
----------------------------------------------

`ARCHIVE_NAME`
    The first positional argument dictates the name of the
    SharedDataArchive (default: `sda`).
    
`SUBCOMMAND`
    The second positional argument specifies the subcommand.
    The default value is `test`.

`--segmentSize=BBB`
    If the named segment does not already exist, specifying a
    segment size in bytes allows it to be automatically created.
    In `test`, this defaults to 4096.  In other subcommands the
    default is 0 (no auto-creation).
    
`--numberOfHashTableSlots=HHH`
    If the segment does not already exist, use `HHH` as the
    number of hash table slots when creating it. Defaults to
    8191 if the segment is large enough for that, or 11 if
    not (tiny hash tables are useful for collision testing).


`SharedDataArchive` subcommands
-------------------------------

`test`
    Test mode: (currently) if an archive of the specified
    `ARCHIVE_NAME` doesn't already exist, create it and populate it
    with a few arbitrarily-named files with arbitrary content (Or,
    if the archive already exists, make a change to one of the pre-
    existing files.) Then, wait for ctrl-C before exiting.  

`summary`
    Output a diagnostic summary of the named archive, with the
    contents broken down file by file.

`memory`
    Output a diagnostic summary of the named archive, with the
    contents broken down Lot by Lot.

`header`
    Output the archive's header.

`file FILE_NAME`
    Output the header of the named "file" and (some of) its payload.
    

Additional options for some subcommands
---------------------------------------

The `summary`, `memory`, `header` and `file` subcommands can
all recognize the following options:

`--period=SSS`
    Automatically refresh the output every `SSS` seconds (the
    default value of 0 means no refresh---just exit after the
    first output). While waiting for an auto-refresh, keyboard
    interaction is possible (see keyboard shortcuts below), and
    the screen will refresh immediately if a key is pressed.

`--numberOfLines=NNN`
    Restrict output to `NNN` lines. If the period `SSS` is 0,
    then the default number of lines `NNN` is also 0, which
    means "no limit". Otherwise (in automatic refresh mode)
    the default `NNN` value will be determined according to
    the console window size at launch.

`--abbreviate=LLL`
    When reporting a file, display at most `LLL` lines of
    payload in each Lot. When reporting the segment header,
    display at most `LLL` hash table entries. A value of 0
    means "display everything" (at least, up to 1 billion
    lines).  The default value is 16.


Keyboard shortcuts during auto-refresh
--------------------------------------

The following keyboard shortcuts are available when using the
`summary`, `memory`, `header` or `file` subcommands with a
non-zero `--period`:

==============  ==================================================
Key             Action
==============  ==================================================
down            scroll display down by one line
up              scroll display up by one line
space           scroll display down by one page
`b`             scroll display up by one page
`g`             scroll to top
`G`             scroll to bottom

`s`             switch to "summary" screen (file-by-file view)
`m`   / `l`     switch to "memory" screen (Lot-by-Lot view)
`h`             switch to "header" screen
`f`             switch to "file" screen

`]`   / tab     (summary or file screen): select the next file 
`[`             (summary or file screen): select the previous file
right / enter   (summary screen): view the selected file
left  / `u`     (file screen): go back to summary screen
`-`             (file or header screen): show fewer lines
`+`   / `=`     (file or header screen): show more lines

ctrl-`c`        exit
==============  ==================================================
)ZARP" ) ) return 0; // CPLUSPLUS11 R"()" literal
	
	double   period = 0.0;
	GET_OPT( "--period", period, "a number" );
	
	uint64_t numberOfLines = ( period > 0 ) ? ConsoleUtils::GetNumberOfRows() : 0;
	GET_OPT( "--numberOfLines", numberOfLines, "an unsigned integer" );
	
	uint64_t segmentSize = 0;
	GET_OPT( "--segmentSize", segmentSize, "an unsigned integer" );
	
	uint64_t numberOfHashTableSlots = 0;
	GET_OPT( "--numberOfHashTableSlots", numberOfHashTableSlots, "an unsigned integer" );
	
	uint64_t abbreviate = 16;
	GET_OPT( "--abbreviate", abbreviate, "an unsigned integer" );
	
	if( !numberOfHashTableSlots ) numberOfHashTableSlots = ( segmentSize >= 66000 ? DEFAULT_HASHTABLE_SIZE : 11 );
	NO_MORE_OPTS( "--" )
	
	std::string archiveName = DEFAULT_NAME; GET_ARG( archiveName, "string ARCHIVE_NAME" );
	std::string subcommand;                 GET_ARG( subcommand,  "a subcommand" );
	std::string filename;
	int32_t startLine = 0;
	
	DebuggingUtils::SetDebugLevel( 2 );
	if( subcommand == "test" ) subcommand = "";
	if( subcommand.length() )
	{		
		if( subcommand == "summary" || subcommand == "header" )
		{
			if( segmentSize == 0 )
			{
				GET_ARG( segmentSize,            "unsigned integer SEGMENT_SIZE" );
				GET_ARG( numberOfHashTableSlots, "unsigned integer HASHTABLE_SIZE" );
			}
		}
		if( subcommand == "file" )
		{
			GET_ARG( filename, "string FILE_PATH" ); if( !filename.length() ) RAISE( execName << " ... file:  must supply a file path" );
		}
		NO_MORE_ARGS

		int returnValue = 0;
		while( !ConsoleUtils::CaughtSignal() )
		{
			double t, deadline = TimeUtils::PrecisionTime() + period;
			try
			{
				ConsoleUtils::CatchFirstInterrupt();
				SharedDataArchive s( archiveName, segmentSize, false, numberOfHashTableSlots, period ); // the last argument is the timeout for acquiring the writer lock that is required for registering as a new client
				while( !ConsoleUtils::CaughtSignal() )
				{
					std::string output;
					try
					{
						if(      subcommand == "summary" ) output = s.Summary( filename );
						else if( subcommand == "header"  ) output = s.ReportHeader( abbreviate ? abbreviate : 1000000000 );
						else if( subcommand == "memory"  ) output = s.Summary( "$Lots" );
						else if( subcommand == "file"    )
						{
							File f = s.Open( filename, 'r' );
							if( f.Exists() ) { output = f.Report( abbreviate ? abbreviate : 1000000000 ); filename = f.GetName(); }
							else if( !filename.length() ) output = "no file selected";
							else output = "file " + StringUtils::StringLiteral( filename ) + " does not exist\n";
						}
						else RAISE( "unrecognized command \"" << StringUtils::StringLiteral( subcommand ) << "\"" );
						std::string footer = "";
						bool theEndIsInSight = false;
						if( startLine > 0 || numberOfLines > 0 )
						{
							StringUtils::StringVector lines;
							StringUtils::Split( lines, output, "\r\n", 0, 0, true );
							if( !lines.back().length() ) lines.pop_back();
							size_t stopLine = lines.size();
							if( startLine < 0 ) startLine = 0;
							if( numberOfLines > 0 && stopLine < numberOfLines ) startLine = 0;
							if( ( uint64_t )startLine > stopLine - numberOfLines + 1 ) startLine = ( int )( stopLine - numberOfLines + 1 );
							//if( numberOfLines > 0 && startLine + numberOfLines < stopLine ) stopLine = startLine + numberOfLines;
							if( numberOfLines > 0 && startLine + numberOfLines <= stopLine ) { stopLine = as_size_t( startLine + numberOfLines - 1 ); footer = "--- more ---"; }
							else if( numberOfLines > 0 && startLine + numberOfLines > stopLine ) { footer = "--- end ---"; theEndIsInSight = true; }
							output = "";
							for( size_t i = startLine; i < stopLine; i++ ) output += lines[ i ] + "\n";
						}
						output += footer;
						if( period > 0.0 ) ConsoleUtils::ClearConsole();
						std::cout << output << std::flush;
						returnValue = 0;
						if( period <= 0.0 ) break;
						std::string key;
						while( !ConsoleUtils::CaughtSignal() && ( t = TimeUtils::PrecisionTime() ) < deadline )
						{
							TimeUtils::SleepSeconds( 0.010 );
							key = ConsoleUtils::GetKeystroke();
							if( key.length() ) break;
						}
						deadline = t + period;
						if( key.length() )
						{
							if( key == "l" || key == "L" ) key = "m";
							if( key == "=" ) key = "+";
							if( key == "\t" ) key = "]";
							if( key == "right" ) key = "\n";
							if( key == "left" || key == "\x08" ) key = "u";
							if( key == " " ) key = "page down";
							if( key == "b" ) key = "page up";
							if( key == "g" ) key = "home";
							if( key == "G" ) key = "end";
							
							
							if(      ( key == "q" || key == "Q" ) ) raise( SIGINT );
							else if( ( key == "h" || key == "H" ) && subcommand != "header"  ) { startLine = 0; subcommand = "header";  }
							else if( ( key == "s" || key == "S" ) && subcommand != "summary" ) { startLine = 0; subcommand = "summary"; }
							else if( ( key == "f" || key == "F" ) && subcommand != "file"    ) { startLine = 0; subcommand = "file";    }
							else if( ( key == "m" || key == "M" ) && subcommand != "memory"  ) { startLine = 0; subcommand = "memory";    }
							else if( ( key == "u" || key == "U" ) && subcommand == "file" ) { startLine = 0; subcommand = "summary"; }
							else if( ( key == "-" ) && ( subcommand == "file" || subcommand == "header" ) ) { abbreviate = ( abbreviate <= 4 ) ? 2 : abbreviate / 2; }
							else if( ( key == "+" ) && ( subcommand == "file" || subcommand == "header" ) ) { abbreviate *= 2; }
							else if( ( key == "\n" ) && subcommand == "summary" && filename.length() ) { startLine = 0; subcommand = "file"; }
							else if( ( key == "[" || key == "]" ) && ( subcommand == "summary" || subcommand == "file" ) )
							{
								if( subcommand == "summary" && filename.length() )
								{
									bool match = false;
									File f = s.Open( "$d" );
									for( f.Walk(); f.Exists(); f.Next() )
									{
										if( filename == f.GetName() ) { match = true; break; }
									}
									if( !match ) filename = "";
								}
								File f = s.Open( "$d" );
								std::string previousFileName;
								for( f.Walk(); f.Exists(); f.Next() )
								{
									if( !filename.length() && key == "]" ) { filename = f.GetName(); break; }
									if( f.GetName() == filename && key == "[" ) { filename = previousFileName; break; }
									if( f.GetName() == filename && key == "]" ) filename = "";
									previousFileName = f.GetName();
								}
								if( !filename.length() && key == "[" ) filename = previousFileName;
								if( subcommand == "file" ) startLine = 0;
							}
							else if( key == "up"   ) { if( startLine > 0 ) startLine--; }
							else if( key == "down" ) { if( output.length() > 1 && !theEndIsInSight ) startLine++; }
							else if( key == "page down" ) startLine += ( int )numberOfLines;
							else if( key == "page up"   ) startLine -= ( int )numberOfLines;
							else if( key == "home" ) startLine = 0;
							else if( key == "end"  ) startLine = INT32_MAX;
							//else { std::cout << TimeUtils::FormatDateStamp() << " : " << StringUtils::StringLiteral( key ) << std::endl; TimeUtils::SleepSeconds( 1.0 ); }
						}
					}
					EXCEPT( NamedSemaphore::TimeoutError, error )
					{
						returnValue = 2;
						if( error.length() ) std::cout << TimeUtils::FormatDateStamp() << " : still waiting for reader lock\n";
						if( period <= 0.0 ) break;
					}
				}
				if( period <= 0.0 ) break;
			}
			EXCEPT( SegmentNotFoundError, error )
			{
				returnValue = 1;
				if( period > 0.0 ) ConsoleUtils::ClearConsole();
				if( error.length() ) std::cout << TimeUtils::FormatDateStamp() << " : SharedDataArchive " << StringUtils::StringLiteral( archiveName ) << " does not exist\n";
				if( period <= 0.0 ) break;
				for( TimeUtils::Timer clock( period ); clock.Ticking() && !ConsoleUtils::CaughtSignal(); clock.Tick( 0.010 ) )
					if( StringUtils::ToLower( ConsoleUtils::GetKeystroke() ) == "q" ) raise( SIGINT );
			}
			EXCEPT( NamedSemaphore::TimeoutError, error )
			{
				returnValue = 2;
				if( error.length() ) std::cout << TimeUtils::FormatDateStamp() << " : timed out waiting for writer lock\n\n";
				if( period <= 0.0 ) break;
			}
		}
		return returnValue;
	}
	
	DebuggingUtils::PrintArgv( argc, argv );
	if( segmentSize == 0 ) segmentSize = 4096;
	SharedDataArchive s( archiveName, segmentSize, false, numberOfHashTableSlots );
	s.SynchronizeTimer();
	
	if( s.WasInitializedHere() )
	{
		StringUtils::StringVector names; StringUtils::Split( names, "Source/Name Storage/SubjectSession Source/SamplingRate Source/NumberOfChannels Source/SampleBlockSize Storage/SubjectName" );
		for( StringUtils::StringVector::iterator it = names.begin(); it != names.end(); it++ ) s.FindFile( *it, 20 );
	}
	
	DBREPORT( 0, s );

	DebuggingUtils::SetDebugLevel( 3 ); // level 3 makes ReadWriteMutexes verbose if they weren't already
	if( s.WasInitializedHere() )
	{
		SharedDataArchive::File f = s.Open( "Source/Name", 'w', 1 );
		for( int i = 0; i < 50; i++ )
		{
			//f.Write( "hello world!\n" );
			f.Write( "\x00\x01\x02\x03\x04\x05\x06\x07\x08\x09", 10 );
			DBREPORT( DBMM, i );
			DBREPORT( DBMM, f.Tell() );
		}
		DBREPORT( 0, f );
	}
	else
	{
		SharedDataArchive::File f = s.Open( "Source/Name", 'w' );
		f.Write( "HOW ARE YOU?\n" );
		DBREPORT( 0, f );
	}

	//for( StringUtils::StringVector::iterator it = names.begin(); it != names.end(); it++ ) printf( "%s", s.ReportFile( *it ).c_str() );
	ConsoleUtils::WaitForKeyboardInterrupt();

	SharedDataArchive::File f = s.Open( "$d", 'r' );
	for( f.Walk(); f.Exists(); f.Next() )
	{
		DBREPORTQ( 0, f.GetName() );
	}
	
	DEBUG( 0, "exiting" );

	//s.SetMutexVerbosity( true ); { ReadWriteMutex::ScopedLock l = s.Lock( 'r' ); DEBUG( 0, "don't unlock yet" ); } DEBUG( 0, "OK now you should be unlocked" );
	
	return 0;
}

typedef struct
{
	uint64_t fOffsetOfLot;
	uint64_t fRelativeOffsetWithinLot;
	uint64_t fPositionWithinPayload;
} Bookmark;

/*
	TODO

	- need a better error message when the segmentSize is too high and we fail to create the segment
	  NB: max size on macOS 10.13.6 High Sierra appears to be one byte less than 2 GiB, i.e. 1024*1024*1024*2-1 = 2147483647 = 2**31-1
	  (as if the system, somewhere somehow, is using a signed int32_t to code the size)

	- RenameFile()
		- Only allow renaming if rounded-up length of null-terminated new name fits in the existing space.
		  "Existing space" means before fRelativeOffsetOfPayload if any payload bytes have been used,
		  or before the end of the Lot if not (fRelativeOffsetOfPayload can be adjusted in that case).
		- If the target is a directory, either prohibit the operation, or make sure it's possible to rename
          ALL the children, grandchildren, etc...  On the plus side, when you do rename the descendants, you
		  won't have to create new ancestor directories, or detach from/reattach to lists of directory siblings.
		- Create new directory/directories if necessary (easiest way might be to create a tiny doomed sibling)
		- Detach from old directory siblings list (destroy directory if that leaves it empty)
		- Detach from old hash-table siblings list
		- Write new name inside the file, and compute its hash
		- Attach to new hash-table siblings list
		- Attach to new directory siblings list
	- CopyFile() - ask for complete payload size up front, which will therefore created a defragmented copy if possible.
	    - Cannot do this with directories (the linked-list of children has to be unique to the parent each child knows)
		- Must update ringbuffer bookmarks, if any (position-within-payload will not change, but relative-offset-within-Lot may and offset-of-current-lot will)
	- DefragmentFile() - RenameFile(old, temporaryName); new = CopyFile( old ); RemoveFile( old );
	
*/

//////////////////////////////////////////////////////////////////////
#endif  // INCLUDED_SharedDataArchive_CPP
