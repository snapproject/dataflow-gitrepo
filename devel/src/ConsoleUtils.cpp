/*
# $BEGIN_DATAFLOW_LICENSE$
# 
# This file is part of the DataFlow project, a software library for
# efficient asynchronous multi-stream inter-process communication.
# 
# Copyright (c) 2014-2021 Jeremy Hill
# 
# DataFlow is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program. If not, see http://www.gnu.org/licenses/ .
# 
# Some third-party source code is compiled and linked into the
# DataFlow binary, as follows: devel/src/sem_timedwait_apple.cpp
# is adapted from sem_timedwait.cpp by Keith Shortridge, copyright
# (c) 2013 Australian Astronomical Observatory (AAO), released under
# the MIT License; devel/src/dirent_win.h is copyright (c) 2006
# Toni Ronkko, and released under the MIT License. The DataFlow build
# system also uses some third-party code:
# devel/build/cmake-recipes/DefineFunction_TARGET_ARCHITECTURE.cmake
# which is copyright (c) 2012 Petroules Corporation, released under
# the 2-Clause BSD License. For more details, see the respective
# copyright, licensing and disclaimer information in each of these
# files.
# 
# $END_DATAFLOW_LICENSE$
*/
#ifndef    INCLUDED_ConsoleUtils_CPP
#define    INCLUDED_ConsoleUtils_CPP
//////////////////////////////////////////////////////////////////////

#include "ConsoleUtils.hpp"
#include "TimeUtils.hpp"


#include <stdio.h>
#include <stdlib.h> // for system()
#include <signal.h> // for sigaction(), raise()

#include <fstream>
#include <iomanip>
#include <iostream>
#include <sstream>

FILE * gConsoleFilePointer = NULL;
void RestoreConsole( void );
void CleanUpConsole( void )
{
	if( gConsoleFilePointer )
	{
		RestoreConsole();
		if( gConsoleFilePointer != stdin ) fclose( gConsoleFilePointer );
	}
	gConsoleFilePointer = NULL;
}

#if _WIN32 // Begin Windows implementation
#	include <Windows.h>
#	define TTY "CONIN$"
FILE * GetConsole( char desiredMode )
{
	if( !gConsoleFilePointer ) gConsoleFilePointer = fopen( TTY, "r" );
	return desiredMode ? gConsoleFilePointer : NULL;
}
void RestoreConsole( void ) {}
#else      // Begin Posix implementation
#	include <sys/select.h>
#	include <sys/ioctl.h>
#	include <termios.h>
#	include <fcntl.h>
#	include <unistd.h>
#	define TTY "/dev/tty"
FILE * GetConsole( char desiredMode )
{
	static int      fileDescriptor;
	static tcflag_t originalFlags;
	static int      originalBufferingMode;
	static char     mode = 'o';
	static termios term;
	
	if( !gConsoleFilePointer )
	{
		//gConsoleFilePointer = stdin;
		gConsoleFilePointer = fopen( TTY, "r" );
		fileDescriptor = fileno( gConsoleFilePointer );
		tcgetattr( fileDescriptor, &term );
		originalFlags = term.c_lflag;
		originalBufferingMode = ( originalFlags & ICANON ) ? _IOLBF : _IONBF; // unfortunately there's no way of querying the buffering mode so this is an educated guess
		atexit( CleanUpConsole );
	}
	if( mode == desiredMode ) return gConsoleFilePointer;
	else mode = desiredMode;
	
	if( mode == 'n' ) // non-canonical
	{
		tcgetattr( fileDescriptor, &term ); // not sure if needed
		term.c_lflag &= ~ICANON;
		term.c_lflag &= ~ECHO;
		tcsetattr( fileDescriptor, TCSANOW, &term );
		setvbuf( gConsoleFilePointer, NULL, _IONBF, 0 );
	}
	if( mode == 'c' ) // canonical
	{
		tcgetattr( fileDescriptor, &term ); // not sure if needed
		term.c_lflag |= ICANON;
		term.c_lflag |= ECHO;
		tcsetattr( fileDescriptor, TCSANOW, &term );
		setvbuf( gConsoleFilePointer, NULL, _IOLBF, 0 );
	}
	if( mode == 'o' ) // restore to original
	{
		tcgetattr( fileDescriptor, &term ); // not sure if needed
		term.c_lflag = originalFlags;
		tcsetattr( fileDescriptor, TCSANOW, &term );
		setvbuf( gConsoleFilePointer, NULL, originalBufferingMode, 0 );
	}
	return gConsoleFilePointer;
}
void RestoreConsole( void ) { GetConsole( 'o' ); }
#endif     // End OS-specific implementations



int
ConsoleUtils::ClearConsole( void )
{
#if _WIN32 // Begin Windows implementation
	return system( "cls" );
#else      // Begin Posix implementation
	return system( "clear" );
	//std::cout << "\x1b[1J" << std::flush; // sort-of works, at least on Linux
#endif     // End OS-specific implementations
}


int
ConsoleUtils::GetNumberOfRows( void )
{
#	if _WIN32 // Begin Windows implementation
		CONSOLE_SCREEN_BUFFER_INFO csbi;
		GetConsoleScreenBufferInfo( GetStdHandle( STD_OUTPUT_HANDLE ), &csbi );
		return csbi.srWindow.Bottom - csbi.srWindow.Top + 1;
#	else      // Begin Posix implementation
		struct winsize w;
		ioctl( STDOUT_FILENO, TIOCGWINSZ, &w );
		return w.ws_row;
#	endif     // End OS-specific implementations
}

int
ConsoleUtils::GetNumberOfColumns( void )
{
#	if _WIN32 // Begin Windows implementation
		CONSOLE_SCREEN_BUFFER_INFO csbi;
		GetConsoleScreenBufferInfo( GetStdHandle( STD_OUTPUT_HANDLE ), &csbi );
		return csbi.srWindow.Right - csbi.srWindow.Left + 1;
#	else      // Begin Posix implementation
		struct winsize w;
		ioctl( STDOUT_FILENO, TIOCGWINSZ, &w );
		return w.ws_col;
#	endif     // End OS-specific implementations
}

std::string
ConsoleUtils::GetKeystroke( void )
{
	std::string s;
#ifdef _WIN32 // Windows implementation
	// Adapted from https://stackoverflow.com/a/422855/
	static HANDLE handle = NULL;
	//if( !handle ) handle = GetStdHandle( STD_INPUT_HANDLE ); // will fail if stdin has been redirected (e.g. as a hack to avoid "Terminate batch job (Y/N)?" in wrappers)
	if( !handle ) handle = CreateFile( "CONIN$", GENERIC_READ | GENERIC_WRITE, FILE_SHARE_READ, 0, OPEN_EXISTING, 0, 0 );
	DWORD events;
	INPUT_RECORD buffer;
	PeekConsoleInput( handle, &buffer, 1, &events );
	if( events > 0 )
	{
		ReadConsoleInput( handle, &buffer, 1, &events );
		if( !buffer.Event.KeyEvent.bKeyDown ) return s;
		char ascii = buffer.Event.KeyEvent.uChar.AsciiChar;
		int vkey = buffer.Event.KeyEvent.wVirtualKeyCode;
		if( ascii == '\r' && vkey == VK_RETURN ) ascii = '\n';
		if( ascii ) s += ascii;
		else
		{
			switch( vkey )
			{	// see https://docs.microsoft.com/en-us/windows/win32/inputdev/virtual-key-codes
#				define VK_CODE( CODE, STR ) case CODE: s = ( STR ); break
				VK_CODE( 0,          "" );
				VK_CODE( VK_CONTROL, "" );
				VK_CODE( VK_SHIFT,   "" );
				VK_CODE( VK_MENU,    "" );
				VK_CODE( VK_CAPITAL, "" );
				VK_CODE( VK_LEFT,    "left" );
				VK_CODE( VK_UP,      "up" );
				VK_CODE( VK_RIGHT,   "right" );
				VK_CODE( VK_DOWN,    "down" );
				VK_CODE( VK_PRIOR,   "page up" );
				VK_CODE( VK_NEXT,    "page down" );
				VK_CODE( VK_END,     "end" );
				VK_CODE( VK_HOME,    "home" );
				VK_CODE( VK_DELETE,  "delete" );

				default:
					std::stringstream ss;
					ss << "VK 0x" << std::setfill( '0' ) << std::setw( 2 ) << std::hex << std::uppercase << vkey;
					s = ss.str();
					break;
			}
		}
	}
#else // POSIX implementation
	// Adapted from "Linux (POSIX) implementation of _kbhit()" by Morgan McGuire <morgan@cs.brown.edu>  https://www.flipcode.com/archives/_kbhit_for_Linux.shtml
	// also using wisdom gained at https://stackoverflow.com/questions/62962794
	FILE * filePointer = GetConsole( 'n' );
	int fileDescriptor = fileno( filePointer );
	fd_set rdset;
	timeval timeout;
	while( true )
	{
		FD_ZERO( &rdset );
		FD_SET( fileDescriptor, &rdset );
		timeout.tv_sec  = 0;
		timeout.tv_usec = 0;
		if( !select( fileDescriptor + 1, &rdset, NULL, NULL, &timeout ) ) break;
		char c = fgetc( filePointer );
		if( c == 0x7f ) c = 0x08; // backspace
		s += c;
		//std::stringstream ss; ss << (int)c << " "; s += ss.str();
		if( s.length() == 1 && c != 0x1B ) break;
		if( s.length()  > 2 && c >= 0x40 && c <= 0x7E ) break;
	}
#	define ESCAPE_CODE( CODE, STR ) else if( s == "\x1B" CODE ) s = ( STR )
	if( !s.length() ) return s;
	ESCAPE_CODE( "[A",  "up" );
	ESCAPE_CODE( "[B",  "down" );
	ESCAPE_CODE( "[C",  "right" );
	ESCAPE_CODE( "[D",  "left" );
	ESCAPE_CODE( "[5~", "page up" );
	ESCAPE_CODE( "[6~", "page down" );
	ESCAPE_CODE( "[F",  "end" );
	ESCAPE_CODE( "[H",  "home" );
	ESCAPE_CODE( "[3~", "delete" );
	
#endif // end platform-specific implementation
	return s;
}

std::string
ConsoleUtils::GetUserInput( const std::string & prompt, bool useStdin )
{
	fflush( stderr );
	fflush( stdout );
	std::cerr << std::flush;
	std::cout << prompt << std::flush;
	std::string	s;
	if( useStdin ) std::getline( std::cin, s );
	else
	{
#	ifdef _WIN32	// I'd like to use this for all platforms but (a) it has macOS-specific problems (https://stackoverflow.com/questions/63077462 ) and (b) it's nice to have the GetConsole('c') in there given that a previoius call to GetKeystroke() might have messed things up
		std::ifstream f( TTY );
		if( f.good() ) std::getline( f, s );
#	else
		char * line = NULL;
		size_t len = 0;
		ssize_t read = -1;
		FILE * fp = GetConsole( 'c' );
		if( fp ) read = getline( &line, &len, fp );
		if( line )
		{
			if( read > 0 && line[ read - 1 ] == '\n' ) line[ read - 1 ] = '\0';
			s = line;
			free( line );
		}
#	endif
	}
	return s;
}

#ifdef    _WIN32
	volatile HANDLE gAbortEvent = NULL;
	void * ConsoleUtils::GetAbortEvent( void ) { return ( void * )( gAbortEvent ? gAbortEvent : ( gAbortEvent = ::CreateEvent( NULL, FALSE, FALSE, NULL ) ) ); }
	int ConsoleUtils::SetSignalHandler( int signum, void(*handler)(int) ) { return signal( signum, handler ) == SIG_ERR ? -1 : 0; }
#else  // _WIN32
	void * ConsoleUtils::GetAbortEvent( void ) { return NULL; }
	
	// Replace signal(), which is flaky on some systems, by an equally simple call that wraps the more reliable sigaction():
	int ConsoleUtils::SetSignalHandler( int signum, void(*handler)(int) )
	{
		struct sigaction act = { 0 };
		act.sa_handler = handler;
		return sigaction( signum, &act, NULL );
	}
#endif // _WIN32

void
ConsoleUtils::CatchFirstInterrupt( void )
{
	SetSignalHandler( SIGINT, IgnoreOnce );
}

void
ConsoleUtils::CatchFirstTermination( void )
{
	SetSignalHandler( SIGTERM, IgnoreOnce );
}

volatile sig_atomic_t gCaughtSignal = 0; // the only type of variable that can reliably (per standard) be accessed from inside a signal handler

void
ConsoleUtils::IgnoreOnce( int signum )
{
	if( signum <= 0 ) return;
	gCaughtSignal = signum;
#ifdef    _WIN32
	::SetEvent( gAbortEvent );
#endif // _WIN32
	std::cout << std::endl << std::flush; // TODO: it is not necessarily kosher to do this inside a handler
	SetSignalHandler( signum, SIG_DFL );
}

void
ConsoleUtils::IgnoreAlways( int signum )
{
	if( signum <= 0 ) return;
	gCaughtSignal = signum;
#ifdef    _WIN32
	::SetEvent( gAbortEvent );
#endif // _WIN32
	std::cout << std::endl << std::flush; // TODO: it is not necessarily kosher to do this inside a handler
}
// The idea here is: let's say you're running a short SharedDataArchive-related reporting utility via the `watch` command-line utility.
// You stop `watch` using ctrl-C. When you press ctrl-C, the SIGINT will be sent not only to `watch`, but to the underlying binary (here).
// That's fine 99.9% of the time if your summarization utility is only be running for a millisecond or two out of every one- or two-second
// cycle. However, on the rare occasions when your ctrl-C catches this program mid-run, we don't want it to terminate (thereby failing to
// sign off gracefully from the SharedDataArchive, and probably even leaving its ReadWriteMutex locked). Instead, it should ignore the
// signal, continue running, and exit normally. However, it should not allow SIGINT to be ignored like this more than once: if you actually
// need to get out of a deadlock, you still want to be able to press ctrl-C a second time and get out. For this reason, the handler itself
// calls `SetSignalHandler()` to re-establish default handling.

void
ConsoleUtils::WaitForKeyboardInterrupt( const std::string & prompt )
{
	fflush( stdout ); // because stdout and cout are entirely separate channels on some systems
	fflush( stderr ); // likewise stderr and cerr
	std::cerr << std::flush;
	if( prompt.length() ) std::cout << prompt;
	std::cout << std::flush;
	gCaughtSignal = 0;
	CatchFirstInterrupt();
	while( !gCaughtSignal ) TimeUtils::SleepSeconds( 0.010 );
}

void
ConsoleUtils::RaiseCaughtSignal( void )
{
	if( gCaughtSignal > 0 ) raise( gCaughtSignal );
}

int
ConsoleUtils::CaughtSignal( int setValue )
{
	int previousValue = gCaughtSignal;
	if( setValue >= 0 ) gCaughtSignal = setValue;
	return previousValue;
}

//////////////////////////////////////////////

#include "StringUtils.hpp"

int
ConsoleUtils::Demo( int argc, const char * argv[] )
{
	std::string key;
	double t, scheduled = 0.0;	
	ClearConsole();
	std::cout << "Console is " << GetNumberOfColumns() << " x " << GetNumberOfRows() << std::endl;
	std::string name = GetUserInput( "Enter your name: " );
	std::cout << "name = " << StringUtils::StringLiteral( name ) << "\n\n";
	name = StringUtils::Strip( name );
	
	CatchFirstInterrupt();
	while( true )
	{
		std::cout << "Press any key" << std::flush;
		while( !CaughtSignal() && !( key = GetKeystroke() ).length() )
		{
			if( ( t = TimeUtils::PrecisionTime() ) >= scheduled )
			{
				scheduled = t + 1.0;
				std::cout << "." << std::flush;
				fflush( stdout );
			}
			else TimeUtils::SleepSeconds( 0.010 );
		}
		if( CaughtSignal() ) break;
		std::cout << "\nkey = " << StringUtils::StringLiteral( key ) << "\n";
	}
	if( name == "" )
	{
		name = GetUserInput( "\nI didn't catch your name before. Can you type it again: " );
		std::cout << "name = " << StringUtils::StringLiteral( name ) << "\n";
		name = StringUtils::Strip( name );
	}
	std::cout << "\nGoodbye then" << ( name.length() ? ", " + name : "" ) << ".\n";
	//RaiseCaughtSignal();
	return 0;
}

//////////////////////////////////////////////////////////////////////
#endif  // INCLUDED_ConsoleUtils_CPP
