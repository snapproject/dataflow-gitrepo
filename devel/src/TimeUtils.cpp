/*
# $BEGIN_DATAFLOW_LICENSE$
# 
# This file is part of the DataFlow project, a software library for
# efficient asynchronous multi-stream inter-process communication.
# 
# Copyright (c) 2014-2021 Jeremy Hill
# 
# DataFlow is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program. If not, see http://www.gnu.org/licenses/ .
# 
# Some third-party source code is compiled and linked into the
# DataFlow binary, as follows: devel/src/sem_timedwait_apple.cpp
# is adapted from sem_timedwait.cpp by Keith Shortridge, copyright
# (c) 2013 Australian Astronomical Observatory (AAO), released under
# the MIT License; devel/src/dirent_win.h is copyright (c) 2006
# Toni Ronkko, and released under the MIT License. The DataFlow build
# system also uses some third-party code:
# devel/build/cmake-recipes/DefineFunction_TARGET_ARCHITECTURE.cmake
# which is copyright (c) 2012 Petroules Corporation, released under
# the 2-Clause BSD License. For more details, see the respective
# copyright, licensing and disclaimer information in each of these
# files.
# 
# $END_DATAFLOW_LICENSE$
*/
#ifndef INCLUDE_TimeUtils_CPP
#define INCLUDE_TimeUtils_CPP

#include "TimeUtils.hpp"

#include <thread>

#include <time.h>
#include <math.h>
#include <string.h>
#include <ctype.h>

bool gTimerInitialized = false;
TimeUtils::TimerOrigin gTimerOrigin; // CPLUSPLUS11
double gSecondsSinceEpochAtTimerOrigin;

double
TimeUtils::SecondsSinceEpoch( bool atTimerOrigin )
{
	if( atTimerOrigin )
	{
		if( !gTimerInitialized ) PrecisionTime();
		return gSecondsSinceEpochAtTimerOrigin;
	}
	std::chrono::time_point< std::chrono::system_clock > t = std::chrono::system_clock::now(); // CPLUSPLUS11
	std::chrono::duration< double > delta = t.time_since_epoch(); // CPLUSPLUS11
	return delta.count();
}

double
TimeUtils::PrecisionTime( bool relativeToFirstCall )
{
	return TimeUtils::PrecisionTime( relativeToFirstCall ? &gTimerOrigin : NULL );
}

double
TimeUtils::PrecisionTime( TimeUtils::TimerOrigin * origin )
{
	std::chrono::time_point< std::chrono::steady_clock > t = std::chrono::steady_clock::now(); // CPLUSPLUS11
	if( !gTimerInitialized ) { gSecondsSinceEpochAtTimerOrigin = SecondsSinceEpoch(); gTimerOrigin = t; gTimerInitialized = true; }
	std::chrono::duration< double > delta = origin ? t - *origin : t.time_since_epoch(); // CPLUSPLUS11
	return delta.count();
}

TimeUtils::TimerOrigin
TimeUtils::GetTimerOrigin( void )
{
	if( !gTimerInitialized ) PrecisionTime();
	return gTimerOrigin;
}


void
TimeUtils::SetTimerOrigin( TimeUtils::TimerOrigin origin, double secondsSinceEpoch )
{
	gTimerOrigin = origin;
	if( secondsSinceEpoch == NOW ) secondsSinceEpoch = SecondsSinceEpoch();
	gSecondsSinceEpochAtTimerOrigin = secondsSinceEpoch;
	gTimerInitialized = true;
}

#ifdef _WIN32
#	include <Windows.h>
#else
#	include <sys/select.h> // for select() in SleepSeconds()
#endif

double
TimeUtils::SleepUntil( double targetPrecisionTime, bool relativeToFirstCall )
{
	const double margin    = 0.7  / 1000.0;
	const double sleepTime = 0.5  / 1000.0;
	double remaining;
	do
	{
		remaining = targetPrecisionTime - PrecisionTime( relativeToFirstCall );
		if( remaining >= margin ) SleepSeconds( sleepTime );
	} while( remaining > 0.0 );
	return targetPrecisionTime - remaining;
}

void
TimeUtils::SleepSeconds( double seconds ) // note that you're specifying a minimum---the system may wait for longer than you ask for
{
#	if _WIN32 // Begin Windows implementation

#		ifndef WINSLEEP
#			define WINSLEEP 3
#		endif
	
		// On Windows, it's hard (maybe impossible, depending on hardware) to get a granularity below 10--15ms at least in typical low-load cases.
		// The first few lines, adapted from https://stackoverflow.com/a/31411628 , changes the granularity using an undocumented NT API call. 
		// Granularity reduces from 10ms down to about or 0.5ms (solutions 3 & 4) on a ThinkCentre M290t.  Granularity for solutions 1 & 2 may
		// or may not also reduce, but not as much.
		static ULONG actualResolution = 0;
		static NTSTATUS(__stdcall *ZwSetTimerResolution)(IN ULONG RequestedResolution, IN BOOLEAN Set, OUT PULONG ActualResolution) = (NTSTATUS(__stdcall*)(ULONG, BOOLEAN, PULONG)) GetProcAddress(GetModuleHandle("ntdll.dll"), "ZwSetTimerResolution");
		if( !actualResolution )
		{
			ZwSetTimerResolution( 1, true, &actualResolution );
			//DBREPORT(0, actualResolution);
			//DBREPORT(0, WINSLEEP);
		}
		seconds -= actualResolution / 2e7; // This correction is based on my own observations---it may be wrong in general.
		
		if( seconds < 0.0 ) seconds = 0.0;
#		if WINSLEEP == 1    // Solution 1 - inferior to solutions 2 and 3 because of the quantization to milliseconds (which would also mess up the actualResolution correction)
			long msec = ( long )( 0.5 + seconds * 1000.0 );
			::Sleep( msec );
	
#		elif WINSLEEP == 2 // Solution 2 - from C++11 standard library
			std::this_thread::sleep_for( std::chrono::duration< double >( seconds ) );
			
#		elif WINSLEEP == 3  // Solution 3 (from MS official docs)
			HANDLE hTimer = NULL;
			LARGE_INTEGER dueTime;
			dueTime.QuadPart = -( LONGLONG )( seconds * 1e7 );  // measured in 100ns intervals, negative signalling relative and positive signalling absolute FILETIME
			hTimer = CreateWaitableTimer( NULL, TRUE, NULL );
			if( hTimer == NULL) RAISE( "CreateWaitableTimer failed with error code " << GetLastError() );
			if( !SetWaitableTimer( hTimer, &dueTime, 0, NULL, NULL, 0 ) ) RAISE( "SetWaitableTimer failed with error code " << GetLastError() );
			if( WaitForSingleObject( hTimer, INFINITE ) != WAIT_OBJECT_0 ) RAISE( "WaitForSingleObject failed with error code " << GetLastError() ); // Could alternatively set the timer for longer and use its timeout, i.e. WaitForSingleObject( hTimer, msec ); without checking return value. It's no better though.
		
#		elif WINSLEEP == 4  // Solution 4 (from stackoverflow and using an additional undocumented NT API call - performance similar to 3 on ThinkCentre M290t)
			static NTSTATUS(__stdcall *NtDelayExecution)(BOOL Alertable, PLARGE_INTEGER DelayInterval) = (NTSTATUS(__stdcall*)(BOOL, PLARGE_INTEGER)) GetProcAddress(GetModuleHandle("ntdll.dll"), "NtDelayExecution");
			LARGE_INTEGER dueTime;
			dueTime.QuadPart = -( LONGLONG )( seconds * 1e7 );  // measured in 100ns intervals, negative signalling relative and positive signalling absolute FILETIME
			NtDelayExecution( false, &dueTime );
			
#		elif WINSLEEP == 5 // https://stackoverflow.com/a/41862592 - flawed
			static constexpr std::chrono::duration<double> minSleepDuration( 0 );
			double stop = PrecisionTime( false ) + seconds; // NB: should really use uncorrected seconds here...
			while( PrecisionTime( false ) < stop ) std::this_thread::sleep_for( minSleepDuration ); // ...but who care because it's no good anyway - it busy-waits
			
#		endif // end WINSLEEP solution selector

#	else  // Begin Posix implementation
		struct ::timeval t;
		t.tv_sec = ( long )seconds;
		t.tv_usec = ( long )( 0.5 + ( seconds - t.tv_sec ) * 1e6 );
		select( 0, 0, 0, 0, &t );
#	endif // End OS-specific implementations
}

std::string
TimeUtils::FormatRelativeTimeStamp( double t, unsigned int precision )
{
  if( t < 0 ) return "---";
  std::stringstream ss;
  ss << std::fixed << std::setprecision( precision ) << std::setw( precision + 3 ) << t;
  return ss.str();
}

struct tm localtime_threadsafe( time_t * time )
{
	struct tm tm_snapshot;
#if ( defined( WIN32 ) || defined( _WIN32 ) || defined( __WIN32__ ) )
	localtime_s( &tm_snapshot, time ); // Windows
#else
	localtime_r( time, &tm_snapshot ); // POSIX
#endif
	return tm_snapshot;
}

struct tm gmtime_threadsafe( time_t * time )
{
	struct tm tm_snapshot;
#if ( defined( WIN32 ) || defined( _WIN32 ) || defined( __WIN32__ ) )
	gmtime_s( &tm_snapshot, time ); // Windows
#else
	gmtime_r( time, &tm_snapshot ); // POSIX
#endif
	return tm_snapshot;
}

std::string
TimeUtils::FormatDateStamp( double secondsSinceEpoch, bool local, const char *fmt )
{
	if( secondsSinceEpoch == NOW ) secondsSinceEpoch = SecondsSinceEpoch();
	if( std::isnan( secondsSinceEpoch ) ) return "???"; // CPLUSPLUS11
	time_t rawTime = ( time_t )secondsSinceEpoch;
	if( !fmt ) fmt = local ? "%Y-%m-%d %H:%M:%S %z" : "%Y-%m-%dT%H:%M:%SZ";
	size_t bufferSize = strlen( fmt ) * 12 + 1;
	char * buffer = new char[ bufferSize ];
	
	struct tm humanReadableTime = local ? localtime_threadsafe( &rawTime ) : gmtime_threadsafe( &rawTime );
	strftime( buffer, bufferSize, fmt, &humanReadableTime );
	std::string s = buffer;
	delete [] buffer;
	return s;
}
// NB: the inverse of this is very difficult to implement, because
// `struct tm` does not represent time-zone. So if you want to translate
// a string into a time, you have to hack-detect whether the string
// specifies a timezone. If you manage to satisfy yourself that it
// specifies either UTC, or the exact local timezone your system happens
// to have configured, then you can feed the result of strptime into
// timegm() or timelocal() respectively (or equivalently _mktime() and
// _mkgmtime() on Windows). But if the timezone spec is anything else,
// you somehow have to apply whatever offset that timezone implies, by hand.

#ifdef _WIN32 // strptime is not available on Windows, so here's a drop-in replacement adapted from https://stackoverflow.com/a/33542189
	extern "C" char* strptime( const char * s, const char * fmt, struct tm * tm )
	{
		std::istringstream input( s );
		input.imbue( std::locale( ::setlocale( LC_ALL, NULL ) ) );
		input >> std::get_time( tm, fmt );
		if( input.eof() ) s += ::strlen( s );
		else if( input.fail() ) return NULL;
		else s += ( uint64_t )input.tellg();
		return ( char* )s;
	}
#endif

double
TimeUtils::ParseDateStamp( const std::string & s, bool local, const char *fmt )
{
	struct ::tm brokenDownTime;
	long adjustment = 0;
	const char *start = s.c_str(), *stop;
	while( ::isspace( *start ) ) start++;
	for( int i = 0; ; i++ )
	{
		switch( i )
		{
			case 0:                             break;
			case 1:  fmt = "%Y-%m-%d %H:%M:%S"; break;
			case 2:  fmt = "%Y-%m-%dT%H:%M:%S"; break;
			case 3:  fmt = "%Y.%m.%d-%H:%M:%S"; break;
			case 4:  fmt = "%Y%m%d_%H%M%S";     break;
			case 5:  fmt = "%Y%m%d-%H%M%S";     break;
			case 6:  fmt = "%Y%m%d%H%M%S";      break;
			default: fmt = NULL;                break;
		}
		if( !fmt && i == 0 ) continue;
		if( !fmt ) break;
		stop = ::strptime( start, fmt, &brokenDownTime );
		if( !stop ) continue; // give it away, give it away, give it away now
		while( ::isspace( *stop ) ) stop++;
		if( *stop == 'Z' ) { local = false; stop++; }
		else
		{
			if( ::strncmp( stop, "UTC", 3 ) == 0 )  { local = false; stop += 3; }
			else if( ( *stop == '-' || *stop == '+' ) && ::isdigit( stop[ 1 ] ) && ::isdigit( stop[ 2 ] ) && ::isdigit( stop[ 3 ] ) && ::isdigit( stop[ 4 ] ) )
			{
				std::string tz( stop, 5 );
				char * remainder = NULL;
				adjustment = ::strtol( tz.c_str(), &remainder, 10 );
				long sign = ( adjustment < 0 ) ? -1 : 1;
				adjustment *= sign;
				long hours = adjustment / 100;
				adjustment -= hours * 40; // looks like 100 minutes per hour, should be 60
				adjustment *= 60;
				adjustment *= sign;
				local = false;
				stop += 5;
			}
		}
		while( ::isspace( *stop ) ) stop++;
		if( !*stop ) break;
	}
	if( !stop || *stop ) return std::nan( "" ); // CPLUSPLUS11
	double secondsSinceEpoch;
	brokenDownTime.tm_isdst = -1;
	if( local ) secondsSinceEpoch = ( double )mktime( &brokenDownTime );
#if ( defined( WIN32 ) || defined( _WIN32 ) || defined( __WIN32__ ) )
	else secondsSinceEpoch = ( double )( _mkgmtime( &brokenDownTime ) - adjustment ); // Windows
#else
	else secondsSinceEpoch = ( double )(    timegm( &brokenDownTime ) - adjustment ); // POSIX
#endif
	
	return secondsSinceEpoch;
}

TimeUtils::Timer::Timer( double durationSeconds, double startSecondsSinceEpoch )
{
	mDeadline = PrecisionTime() + durationSeconds + ( ( startSecondsSinceEpoch == NOW ) ? 0.0 : startSecondsSinceEpoch - SecondsSinceEpoch() );
}
bool TimeUtils::Timer::Ticking( void ) { return PrecisionTime() < mDeadline; }
void TimeUtils::Timer::Tick( double sleepSeconds ) { if( sleepSeconds ) SleepSeconds( sleepSeconds ); }

TimeUtils::Welford::Welford( uint64_t limit )
: mMean( 0.0 ), mM2( 0.0 ), mCount( 0 ), mLimit( limit ), mScaling( 1.0 ),
  mDifferences( NULL ), mPreviousValue( std::nan( "" ) ) { }
TimeUtils::Welford::~Welford() { if( mDifferences ) delete mDifferences; }
void     TimeUtils::Welford::Reset( void ) { mMean = 0.0; mM2 = 0.0; mCount = 0; mPreviousValue = std::nan( "" ); if( mDifferences ) mDifferences->Reset(); }
uint64_t TimeUtils::Welford::SetLimit( uint64_t limit ) { return mLimit = limit; }
uint64_t TimeUtils::Welford::Limit( void ) const { return mLimit; }
uint64_t TimeUtils::Welford::Count( void ) const { return mCount; }
bool     TimeUtils::Welford::Finished( void ) const { return mCount >= mLimit; }
double   TimeUtils::Welford::Mean( void ) const { return mCount ? mMean : std::nan( "" ); }
double   TimeUtils::Welford::StandardDeviation( uint64_t sampleBiasCorrection ) const
{
	if( mCount <= sampleBiasCorrection ) return std::nan( "" );
	return std::sqrt( mM2 / ( mCount - sampleBiasCorrection ) );
}
double
TimeUtils::Welford::Process( double x )
{
	if( mDifferences )
	{
		if( mCount ) mDifferences->Process( x - mPreviousValue );
		mPreviousValue = x;
	}
	double xScaled = x * mScaling;
	double delta = xScaled - mMean;
	mMean += delta / ++mCount;
	mM2 += delta * ( xScaled - mMean ); /* Welford's algorithm */
	return x;
}
TimeUtils::Welford &
TimeUtils::Welford::SetScaling( double scaling, const std::string & units )
{
	mScaling = scaling;
	mUnits = units;
	return *this;
}
std::string
TimeUtils::Welford::Report( uint64_t sampleBiasCorrection, int precision ) const
{
	std::stringstream ss;
	ss << std::fixed << std::setprecision( precision ) << mMean << mUnits << " "
	   << "(+/- " << StandardDeviation( sampleBiasCorrection ) << " SD, N=" << mCount << ")";
	return ss.str();
}
TimeUtils::Welford &
TimeUtils::Welford::Differences( void )
{
	if( !mDifferences ){ Reset(); mDifferences = new Welford(); }
	return *mDifferences;
}

#include "CommandLine.hpp"
int
TimeUtils::Demo( int argc, const char * argv[] )
{
	START_ARGS;
if( USAGE( "--help", R"ZARP(

Arguments: SLEEP_MSEC TOTAL_SECONDS

Estimates call time and timer resolution for both `PrecisionTime()`
and `SecondsSinceEpoch()`.

Then sleeps for `SLEEP_MSEC` 200 times to estimate the actual time
taken.

Top up the duration of the whole procedure to `TOTAL_SECONDS` to
assess the accuracy of `SleepUtil()`

)ZARP" ) ) return 0; // CPLUSPLUS11 R"()" literal
	NO_MORE_OPTS( "--" );
	double SLEEP_MSEC    = 1.0; GET_ARG( SLEEP_MSEC,    "floating-point sleep duration SLEEP_MSEC" );
	double TOTAL_SECONDS = 3.0; GET_ARG( TOTAL_SECONDS, "floating-point total duration TOTAL_SECONDS" );
	std::string DATESTAMP; GET_ARG( DATESTAMP, "string DATESTAMP" );
	NO_MORE_ARGS;
	
	double start = PrecisionTime();
	DEBUG( 0, "" );
	if( sizeof( time_t ) < 8 ) { DEBUG( 0, "sizeof(time_t) is " << sizeof( time_t ) << ": this binary has a Y2038 problem" ); }
	else DBREPORT( 0, sizeof(time_t) ); // If this is 4, your binary has a Year 2038 problem	
	if( DATESTAMP.length() )
	{
		DEBUG( 0, "" );
		double d = ParseDateStamp( DATESTAMP );
		DEBUG( 0, "\"" << DATESTAMP << "\" -> " << FormatDateStamp( d, false ) );
		DEBUG( 0, "\"" << DATESTAMP << "\" -> " << FormatDateStamp( d, true  ) );
	}
	DEBUG( 0, "" );
	DBREPORT_TIME_TAKEN(       0, 10000, PrecisionTime() );
	DBREPORT_TIMER_RESOLUTION( 0,   100, PrecisionTime() );
	DEBUG( 0, "" );
	DBREPORT_TIME_TAKEN(       0, 10000, SecondsSinceEpoch() );
	DBREPORT_TIMER_RESOLUTION( 0,   100, SecondsSinceEpoch() );
	DEBUG( 0, "" );
	DBREPORT( 0, SLEEP_MSEC );
	DBREPORT_TIME_TAKEN(       0,   100, SleepSeconds( SLEEP_MSEC / 1000.0 ) );
	DBREPORT_TIME_TAKEN(       0,   100, SleepSeconds( SLEEP_MSEC / 1000.0 ) );
	DBREPORT_TIME_TAKEN_WITH_VARIANCE( 0,   100, SleepSeconds( SLEEP_MSEC / 1000.0 ) );
	DBREPORT_TIME_TAKEN_WITH_VARIANCE( 0,   100, SleepSeconds( SLEEP_MSEC / 1000.0 ) );
	double sseAtTimerOrigin = SecondsSinceEpoch( true ), sseNow = SecondsSinceEpoch();
	double ptSinceOrigin = PrecisionTime(), ptRaw = PrecisionTime( false );
	DEBUG( 0, "" );
	DEBUG( 0, "SecondsSinceEpoch(true) = " << std::setw( 20 ) << std::fixed << std::setprecision( 9 ) << sseAtTimerOrigin );
	DEBUG( 0, "SecondsSinceEpoch()     = " << std::setw( 20 ) << std::fixed << std::setprecision( 9 ) << sseNow );
	DEBUG( 0, "PrecisionTime()         = " << std::setw( 20 ) << std::fixed << std::setprecision( 9 ) << ptSinceOrigin );
	DEBUG( 0, "PrecisionTime(false)    = " << std::setw( 20 ) << std::fixed << std::setprecision( 9 ) << ptRaw );
	DBREPORT( 0, sizeof(gTimerOrigin) );
	DEBUG( 0, "" );
	DBREPORT( 0, TOTAL_SECONDS );
	double wakeupTime = start + TOTAL_SECONDS;
	double oversleptMicroseconds = 1e6 * ( SleepUntil( wakeupTime ) - wakeupTime );
	DBREPORT( 0, oversleptMicroseconds  );
	
	return 0;
}

#endif // ifndef INCLUDE_TimeUtils_CPP
