/*
# $BEGIN_DATAFLOW_LICENSE$
# 
# This file is part of the DataFlow project, a software library for
# efficient asynchronous multi-stream inter-process communication.
# 
# Copyright (c) 2014-2021 Jeremy Hill
# 
# DataFlow is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program. If not, see http://www.gnu.org/licenses/ .
# 
# Some third-party source code is compiled and linked into the
# DataFlow binary, as follows: devel/src/sem_timedwait_apple.cpp
# is adapted from sem_timedwait.cpp by Keith Shortridge, copyright
# (c) 2013 Australian Astronomical Observatory (AAO), released under
# the MIT License; devel/src/dirent_win.h is copyright (c) 2006
# Toni Ronkko, and released under the MIT License. The DataFlow build
# system also uses some third-party code:
# devel/build/cmake-recipes/DefineFunction_TARGET_ARCHITECTURE.cmake
# which is copyright (c) 2012 Petroules Corporation, released under
# the 2-Clause BSD License. For more details, see the respective
# copyright, licensing and disclaimer information in each of these
# files.
# 
# $END_DATAFLOW_LICENSE$
*/
#ifndef   INCLUDED_DataType_CPP
#define   INCLUDED_DataType_CPP

#include "DataType.hpp"
#include "StringUtils.hpp"

bool
DataType::IsFullyFormed( DataType::Code t )
{
#	undef DefineDataType
#	define DefineDataType( NAME, CODE ) if( t == ( CODE ) ) return true;
#	include "DataType.hpp"
	return false;
}

bool
DataType::IsString( DataType::Code t )
{
	return ( t & DataType::String ) != 0;
}

bool
DataType::IsFloatingPoint( DataType::Code t )
{
	return ( t & DataType::FloatingPoint ) != 0;
}

bool
DataType::IsInteger( DataType::Code t )
{
	return ( t & DataType::Integer ) != 0;
}

bool
DataType::IsSigned( DataType::Code t )
{
	return ( t & DataType::Signed ) != 0;
}

bool
DataType::IsNumeric( DataType::Code t )
{
	return ( t & ( DataType::FloatingPoint | DataType::Integer ) ) != 0;
}

int
DataType::SizeInBits( DataType::Code t )
{
	return ( 1 | 8 | 16 | 32 | 64 ) & t;
}

int
DataType::SizeInBytes( DataType::Code t )
{
	int bits = ( 1 | 8 | 16 | 32 | 64 ) & t; return bits / 8 + ( ( bits % 8 ) ? 1 : 0 );
}

const char *
DataType::ToString( DataType::Code t )
{
#	undef DefineDataType
#	define DefineDataType( NAME, CODE )  if( t == ( CODE ) ) return #NAME;
#	include "DataType.hpp"
	return "UnknownDataType";
}

DataType::Code
DataType::FromString( std::string s )
{
#	undef DefineDataType
#	define DefineDataType( NAME, CODE )  if( StringUtils::Match( #NAME, s, "i" ) ) return ( CODE );
#	include "DataType.hpp"
	return DataType::Unknown;
}

char
InterpretType( std::string & term, const std::string & termLowerCase )
{ // TODO: as-yet unused, this implementation was rescued from DataFlow.cpp where it was found during demolition. Is it useful? Can it be restructured by using the header? Should it return a DataType::Code? 
#	define MATCH( SHORT, LONG, RETURNVALUE, CANONICAL )   if( term == SHORT || termLowerCase == LONG ) { term = CANONICAL; return RETURNVALUE; }
	MATCH( "s",  "",        's', "string" ); // default for empty strings
	MATCH( "S",  "string",  's', "string" );
	MATCH( "b",  "int8",    'b', "int8" );
	MATCH( "B",  "uint8",   'B', "uint8" );
	MATCH( "h",  "int16",   'h', "int16" );
	MATCH( "H",  "uint16",  'H', "uint16" );
	MATCH( "l",  "int32",   'l', "int32" );
	MATCH( "L",  "uint32",  'L', "uint32" );
	MATCH( "f",  "float32", 'f', "float32" );
	MATCH( "f",  "single",  'f', "float32" );
	MATCH( "d",  "float64", 'd', "float64" );
	MATCH( "d",  "double",  'd', "float64" );
	return '\0';
}


#endif // INCLUDED_DataType_CPP
