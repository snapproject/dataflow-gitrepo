/*
# $BEGIN_DATAFLOW_LICENSE$
# 
# This file is part of the DataFlow project, a software library for
# efficient asynchronous multi-stream inter-process communication.
# 
# Copyright (c) 2014-2021 Jeremy Hill
# 
# DataFlow is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program. If not, see http://www.gnu.org/licenses/ .
# 
# Some third-party source code is compiled and linked into the
# DataFlow binary, as follows: devel/src/sem_timedwait_apple.cpp
# is adapted from sem_timedwait.cpp by Keith Shortridge, copyright
# (c) 2013 Australian Astronomical Observatory (AAO), released under
# the MIT License; devel/src/dirent_win.h is copyright (c) 2006
# Toni Ronkko, and released under the MIT License. The DataFlow build
# system also uses some third-party code:
# devel/build/cmake-recipes/DefineFunction_TARGET_ARCHITECTURE.cmake
# which is copyright (c) 2012 Petroules Corporation, released under
# the 2-Clause BSD License. For more details, see the respective
# copyright, licensing and disclaimer information in each of these
# files.
# 
# $END_DATAFLOW_LICENSE$
*/
#ifndef INCLUDED_ThreadGlobals_CPP
#define INCLUDED_ThreadGlobals_CPP

#include "ThreadGlobals.hpp"

#include <map>
#include <mutex> // CPLUSPLUS11
#include <thread>

#include <stdio.h>
#include <stdexcept>


class ThreadSpace  // A unique ThreadSpace gets automatically created for every thread
{                  // that tries to access it via GetThreadSpaceForCurrentThread()
	public:
		ThreadSpace() : mStringBufferCursor( 0 ) { mLast[ "error" ] = NULL; }
		~ThreadSpace() {}
		
		const char * CacheString( const std::string & s )
		{
			std::string & stored = mStrings[ mStringBufferCursor++ ];
			mStringBufferCursor %= ThreadGlobals::NUMBER_OF_STRINGS;
			stored = s; // creates a copy of the underlying content
			return stored.c_str();
		}
		const char * SetLast( const std::string & key, const std::string & s ) { return mLast[ key ] = CacheString( s ); }
		const char * GetLast( const std::string & key ) { return mLast[ key ]; }
		const char * ClearLast( const std::string & key ) { const char * e = mLast[ key ]; mLast[ key ] = NULL; return e; }
		const char * SetLastError( const std::string & s ) { return SetLast( "error", s ); }
		const char * GetLastError( void ) { return GetLast( "error" ); }
		const char * ClearLastError( void ) { return ClearLast( "error" ); }
		
	private:
		std::string  mStrings[ ThreadGlobals::NUMBER_OF_STRINGS ];
		int          mStringBufferCursor;
		std::map< std::string, const char * > mLast;
};
std::recursive_mutex gThreadSpaceRegistryMutex;  // CPLUSPLUS11
typedef std::map< std::thread::id, ThreadSpace > ThreadSpaceRegistry; // CPLUSPLUS11
ThreadSpaceRegistry gThreadSpaceRegistry;

ThreadSpace &
GetThreadSpaceForCurrentThread()
{
	std::thread::id currentThread = std::this_thread::get_id(); // CPLUSPLUS11: in theory this should be a C++11-or-later feature, but seems to compile on Mac gcc even without -std=c++11
	// TODO: might it be necessary to move the lock_guard up here?
	try { return gThreadSpaceRegistry.at( currentThread ); }
	catch( std::out_of_range )
	{
		std::lock_guard< std::recursive_mutex > lock( gThreadSpaceRegistryMutex ); // CPLUSPLUS11
		gThreadSpaceRegistry[ currentThread ] = ThreadSpace(); // creates a copy and/or relies on moveability of this class, but in this case who cares
		//gThreadSpaceRegistry.emplace( std::piecewise_construct, std::make_tuple( currentThread ), std::make_tuple() ); // version of the same operation as above, but without copying/moving (this definitely does require -std=c++11)
		return gThreadSpaceRegistry.at( currentThread );
	}
}



const char *
ThreadGlobals::CacheString( const std::string & s ) // A sanctuary for orphaned strings where they can persist in peace---for a while.
{	
	return GetThreadSpaceForCurrentThread().CacheString( s );
}

const char * ThreadGlobals::SetLast(   const std::string & key, const std::string & s ) { return GetThreadSpaceForCurrentThread().SetLast(   key, s ); }
const char * ThreadGlobals::GetLast(   const std::string & key )                        { return GetThreadSpaceForCurrentThread().GetLast(   key ); }
const char * ThreadGlobals::ClearLast( const std::string & key )                        { return GetThreadSpaceForCurrentThread().ClearLast( key ); }
const char * ThreadGlobals::SetLastError( const std::string & s )                       { return GetThreadSpaceForCurrentThread().SetLastError( s ); }
const char * ThreadGlobals::GetLastError(   void )                                      { return GetThreadSpaceForCurrentThread().GetLastError(); }
const char * ThreadGlobals::ClearLastError( void )                                      { return GetThreadSpaceForCurrentThread().ClearLastError(); }

int
ThreadGlobals::Demo( int argc, const char * argv[] )
{
#define CHECK if( ThreadGlobals::GetLastError() ) printf( "error: %s\n", ThreadGlobals::ClearLastError() )

	CHECK;
	ThreadGlobals::SetLastError( "first!" );
	CHECK;
	CHECK;
	ThreadGlobals::SetLastError( "second!" );
	CHECK;
	CHECK;
	
	return 0;
}
#endif // #ifndef INCLUDED_ThreadGlobals_CPP
