/*
# $BEGIN_DATAFLOW_LICENSE$
# 
# This file is part of the DataFlow project, a software library for
# efficient asynchronous multi-stream inter-process communication.
# 
# Copyright (c) 2014-2021 Jeremy Hill
# 
# DataFlow is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program. If not, see http://www.gnu.org/licenses/ .
# 
# Some third-party source code is compiled and linked into the
# DataFlow binary, as follows: devel/src/sem_timedwait_apple.cpp
# is adapted from sem_timedwait.cpp by Keith Shortridge, copyright
# (c) 2013 Australian Astronomical Observatory (AAO), released under
# the MIT License; devel/src/dirent_win.h is copyright (c) 2006
# Toni Ronkko, and released under the MIT License. The DataFlow build
# system also uses some third-party code:
# devel/build/cmake-recipes/DefineFunction_TARGET_ARCHITECTURE.cmake
# which is copyright (c) 2012 Petroules Corporation, released under
# the 2-Clause BSD License. For more details, see the respective
# copyright, licensing and disclaimer information in each of these
# files.
# 
# $END_DATAFLOW_LICENSE$
*/
#ifndef    INCLUDED_Request_HPP
#define    INCLUDED_Request_HPP
//////////////////////////////////////////////////////////////////////

#include <string>
#include <list>

#include "StringUtils.hpp"
#include "DataNode.hpp"
class Confluence;

class Rule;
class Request;
typedef std::list< Rule > RuleList;


class Rule
{
	public:
		static const std::string Wildcard;
		
		Rule( const std::string & pattern );
		// TODO: sensible default values...
		~Rule() {}
		
		double   GetTimeStamp( void ) { return mSinceTimeStamp; }
		Rule *   SetTimeStamp( double t ) {    mSinceTimeStamp = t; return this; }
		
		double   GetMinimumSpan( void ) { return    mMinimumSpanInSeconds; }
		Rule *   SetMinimumSpan( double seconds ) { mMinimumSpanInSeconds = seconds; return this; }

		double   GetMaximumSpan( void ) { return    mMaximumSpanInSeconds; }
		Rule *   SetMaximumSpan( double seconds ) { mMaximumSpanInSeconds = seconds; return this; }
		
		Rule *   SetSpan( double min, double max ) { mMinimumSpanInSeconds = min; mMaximumSpanInSeconds = max; return this; }
		
		uint64_t GetMinimumNumberOfEvents( void ) { return mMinimumNumberOfEvents; }
		Rule *   SetMinimumNumberOfEvents( uint64_t n ) {  mMinimumNumberOfEvents = n; return this; }
		
		uint64_t GetMaximumNumberOfEvents( void ) { return mMaximumNumberOfEvents; }
		Rule *   SetMaximumNumberOfEvents( uint64_t n ) {  mMaximumNumberOfEvents = n; return this; }
		
	
		bool     PathMatches( const char * path );
		
		
	private:
		StringUtils::StringVector  mPattern;
		double                     mSinceTimeStamp;
		double                     mMinimumSpanInSeconds;
		double                     mMaximumSpanInSeconds;
		uint64_t                   mMinimumNumberOfEvents;
		uint64_t                   mMaximumNumberOfEvents;
};

class Request : public DataNode
{
	public:
		Request( std::string name="." );
		~Request();
		
		Rule *      NewRule( const std::string & pattern );
		DataNode *  Pull( Confluence & confluence );
		DataNode *  Pull( Confluence & confluence, double & timeTaken );
		
	private:
		RuleList mRules;
};

//////////////////////////////////////////////////////////////////////
#endif  // INCLUDED_Request_HPP
