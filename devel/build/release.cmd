#!/usr/bin/env python
""" "
@echo off
cls
:::::::::::::::::::::::::::::::::::::::::::::::::::::::::


set "CUSTOMPYTHONHOME=%PYTHONHOME_DEFAULT%
if "%CUSTOMPYTHONHOME%"=="" goto :skipconfig
set PYTHONHOME=%CUSTOMPYTHONHOME%
set PATH=%PYTHONHOME%;%PATH%
:skipconfig

python "%0" %*

:::::::::::::::::::::::::::::::::::::::::::::::::::::::::
goto :eof
" """

# $BEGIN_DATAFLOW_LICENSE$
# 
# This file is part of the DataFlow project, a software library for
# efficient asynchronous multi-stream inter-process communication.
# 
# Copyright (c) 2014-2021 Jeremy Hill
# 
# DataFlow is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program. If not, see http://www.gnu.org/licenses/ .
# 
# Some third-party source code is compiled and linked into the
# DataFlow binary, as follows: devel/src/sem_timedwait_apple.cpp
# is adapted from sem_timedwait.cpp by Keith Shortridge, copyright
# (c) 2013 Australian Astronomical Observatory (AAO), released under
# the MIT License; devel/src/dirent_win.h is copyright (c) 2006
# Toni Ronkko, and released under the MIT License. The DataFlow build
# system also uses some third-party code:
# devel/build/cmake-recipes/DefineFunction_TARGET_ARCHITECTURE.cmake
# which is copyright (c) 2012 Petroules Corporation, released under
# the 2-Clause BSD License. For more details, see the respective
# copyright, licensing and disclaimer information in each of these
# files.
# 
# $END_DATAFLOW_LICENSE$

import os, sys, shutil, time, glob

def main():

	if len( sys.argv ) != 2: sys.stderr.write( 'usage: %s DSTDIR\n' % sys.argv[ 0 ] ); sys.exit( 1 )
	srcdir = os.path.realpath( sys.argv[ 0 ] + '/../../../release' ).replace( '\\', '/' ).rstrip( '/' )
	dstdir = os.path.realpath( sys.argv[ 1 ] ).replace( '\\', '/' ).rstrip( '/' )

	print( '     SOURCE = %r' % srcdir )
	print( 'DESTINATION = %r' % dstdir )

	if not os.path.isdir( dstdir ):
		sys.stderr.write( "destination directory does not exist" )
		return 1

	sys.path.append( srcdir )
	import DataFlow as DF
	version = DF.GetAPIVersion()
	print( "DLL version = " + version )

	year = time.localtime()[ 0 ]
	if year == 2014: years = '2014'
	else: years = '2014-%d' % year
	translation = {
		'YEARS'            : years,
		'VERSION'          : version,
		'COPYRIGHT_HOLDER' : 'The DataFlow Team',
	}

	def mkdir( path ):
		if not os.path.isdir( path ): os.mkdir( path )
		return path
		
	def copyfile( filename, binary=False ):
		filename = srcdir + '/' + filename
		for filename in sorted( glob.glob( filename ) ):
			source = filename
			destination = dstdir + '/' + os.path.basename( filename )
			print( "copying %s -> %s" % ( source, destination ) )
			if binary:
				shutil.copyfile( source, destination )
			else:
				txt = open( source, 'rt' ).read()
				for match, replace in translation.items(): txt = txt.replace( '@{' + match + '}', replace )
				open( destination, 'wt' ).write( txt )

	copyfile( 'DataFlow.py' )
	copyfile( 'DataFlow.h' )
	copyfile( 'DataFlow_API_Loader.c' )
	copyfile( 'libDF*', binary=True)
	copyfile( 'demo.c' )
	copyfile( 'changelog.txt' )
	copyfile( 'README.txt' )


if __name__ == '__main__':
	sys.exit( main() )