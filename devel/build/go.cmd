#!/bin/sh
cd . && echo \
@goto cmd \
>/dev/null

# $BEGIN_DATAFLOW_LICENSE$
# 
# This file is part of the DataFlow project, a software library for
# efficient asynchronous multi-stream inter-process communication.
# 
# Copyright (c) 2014-2021 Jeremy Hill
# 
# DataFlow is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program. If not, see http://www.gnu.org/licenses/ .
# 
# Some third-party source code is compiled and linked into the
# DataFlow binary, as follows: devel/src/sem_timedwait_apple.cpp
# is adapted from sem_timedwait.cpp by Keith Shortridge, copyright
# (c) 2013 Australian Astronomical Observatory (AAO), released under
# the MIT License; devel/src/dirent_win.h is copyright (c) 2006
# Toni Ronkko, and released under the MIT License. The DataFlow build
# system also uses some third-party code:
# devel/build/cmake-recipes/DefineFunction_TARGET_ARCHITECTURE.cmake
# which is copyright (c) 2012 Petroules Corporation, released under
# the 2-Clause BSD License. For more details, see the respective
# copyright, licensing and disclaimer information in each of these
# files.
# 
# $END_DATAFLOW_LICENSE$

###################################################################
# posix shell code

STARTDIR=$(pwd)
EXEC=$(readlink "$0")
if [ "$EXEC" = "" ]; then EXEC="$0"; fi
TARGETDIR=$(dirname "$EXEC")
cd "$TARGETDIR"
JUNK=junk
rm -f ../src/autogen/build_info.h
which python3 >/dev/null 2>&1 && PYTHON=python3 || PYTHON=python

# grok possible -DCMAKE_TOOLCHAIN_FILE=... argument for cross-compiling:
XCTC=$( echo "$1" | sed -e '/^\-DCMAKE_TOOLCHAIN_FILE=/{s///;}' -e t -e 's/.*//' )  # made compatible with both GNU and BSD sed
if [ ! -z "$XCTC" ]   # NB, even if the toolchain file is native.cmake, we should use a different junk directory from the default one (output location is cached there)
then
	echo "$XCTC" | grep -iq '.cmake$' || XCTC="$XCTC".cmake
	export STARTDIR XCTC
	XCTC=$($PYTHON -c "from os import environ; from os.path import *;x=environ['XCTC'];print(relpath(x,'.') if isabs(x) else x if isfile(x) else relpath(join(environ['STARTDIR'],x),'.'))")
	shift; set -- "-DCMAKE_TOOLCHAIN_FILE=$XCTC" "$@"
	XCTC=$( echo "$XCTC" | perl -pe 's/\.cmake$//i; s/\s\+/_/g; s/^(.*\/)?//')  # BSD sed was being annoying so this was switched to perl
	JUNK="$JUNK-$XCTC"
fi

#$PYTHON -c "import sys;print(sys.argv[1:])" "$@"
cmake -H. "-B$JUNK" -G "Unix Makefiles" "$@"
cd "$JUNK"
make all
cd "$STARTDIR"


###################################################################
exit
:cmd
@echo off
cls
:: ################################################################
:: Windows code

python --version >NUL 2>NUL || (echo. && echo. && echo python not found && goto :eof ) 

setlocal
setlocal EnableDelayedExpansion

set "TARGET=Win64"
set "IDE="
if not "%~1"=="" set "TARGET=%~1"
if not "%~2"=="" set "IDE=%~2"
:: NB: further args will be passed to msbuild below

call %~dp0VisualStudio %TARGET% %IDE% || exit /b 1
set "GENERATOR=Visual Studio %VSVERSION%"
if "%VS_USE_CMAKE_ARCH_FLAG%"=="" (
	:: e.g.: cmake -G "Visual Studio 12 Win64"
	if not "%TARGET%"=="Win32" set "GENERATOR=%GENERATOR% %TARGET%"
) else (
	:: it will be equal to -A and its correct usage is, for example, cmake -G "Visual Studio 16" -A x64
	if "%TARGET%"=="Win32" set "VS_USE_CMAKE_ARCH_FLAG=%VS_USE_CMAKE_ARCH_FLAG% %TARGET%"
	if "%TARGET%"=="Win64" set "VS_USE_CMAKE_ARCH_FLAG=%VS_USE_CMAKE_ARCH_FLAG% x64"
)
set "JUNK=junk-VS%VSVERSION%-%TARGET%"
set "STARTDIR=%CD%
cd "%~dp0"
::echo."%GENERATOR%" && echo."%VSVERSION%"

erase ..\src\autogen\build_info.h
cmake "-H." "-B%JUNK%" -G "%GENERATOR%" %VS_USE_CMAKE_ARCH_FLAG%
set PLATFORM=
msbuild /p:Configuration=Release "%JUNK%\DataFlowAPI.sln"
cd %STARTDIR%
endlocal

:: ################################################################
pause
