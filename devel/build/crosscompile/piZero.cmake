# Uses the `$TOOLCHAINS/raspberrypi-tools/arm-bcm2708/arm-linux-gnueabihf` toolchain
# Set up as follows (64-bit Linux host)::
# 
#     export TOOLCHAINS="$HOME/crosscompile"   # or wherever you want
#     mkdir -p "$TOOLCHAINS"
#     git clone https://github.com/raspberrypi/tools  "$TOOLCHAINS/raspberrypi-tools"
# 
# And then::
# 
#     ./devel/build/go.cmd -CMAKE_TOOLCHAIN_FILE=crosscompile/piZero.cmake


IF( "$ENV{TOOLCHAINS}" STREQUAL "" )
	MESSAGE( FATAL_ERROR "\nYou must set the TOOLCHAINS environment variable first (absolute path to the parent directory of your gcc toolchain distributions).\n" )
ENDIF()

SET( CROSSCOMPILE_TARGET arm-linux-gnueabihf )
SET( TOOLCHAIN           $ENV{TOOLCHAINS}/raspberrypi-tools/arm-bcm2708/${CROSSCOMPILE_TARGET} )
SET( PREFIX              ${CROSSCOMPILE_TARGET}- )

SET( CMAKE_SYSTEM_NAME      Linux )
SET( CMAKE_SYSTEM_PROCESSOR armv6 )

SET( CMAKE_AR           ${TOOLCHAIN}/bin/${PREFIX}ar  )
SET( CMAKE_ASM_COMPILER ${TOOLCHAIN}/bin/${PREFIX}as  )
SET( CMAKE_C_COMPILER   ${TOOLCHAIN}/bin/${PREFIX}gcc )
SET( CMAKE_CXX_COMPILER ${TOOLCHAIN}/bin/${PREFIX}g++ )
SET( CMAKE_LINKER       ${TOOLCHAIN}/bin/${PREFIX}ld  )
SET( CMAKE_OBJCOPY      ${TOOLCHAIN}/bin/${PREFIX}objcopy )
SET( CMAKE_RANLIB       ${TOOLCHAIN}/bin/${PREFIX}ranlib )
SET( CMAKE_SIZE         ${TOOLCHAIN}/bin/${PREFIX}size )
SET( CMAKE_STRIP        ${TOOLCHAIN}/bin/${PREFIX}strip )


SET( CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER )
SET( CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY )
SET( CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY )
SET( CMAKE_FIND_ROOT_PATH_MODE_PACKAGE ONLY )

SET( CMAKE_TRY_COMPILE_TARGET_TYPE     STATIC_LIBRARY )

IF( NOT EXISTS "${CMAKE_CXX_COMPILER}" )
	MESSAGE( FATAL_ERROR "\nFailed to find toolchain: ${CMAKE_CXX_COMPILER}\n" )
ENDIF()
