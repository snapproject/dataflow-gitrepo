
# Requires the toolchain `$TOOLCHAINS/gcc-arm-none-linux-gnueabihf`
# which can be downloaded as `gcc-arm-VERSIONDATESTAMP-x86_64-arm-none-linux-gnueabihf.tar.xz`
# from https://developer.arm.com/tools-and-software/open-source-software/developer-tools/gnu-toolchain/gnu-a/downloads
# Example setup as follows::
# 
#     VERSIONDATESTAMP=10.2-2020.11     # or whatever
#     TARGET=arm-none-linux-gnueabihf   
# 
#     UNAME=$(uname); UNAME=${UNAME%%[0123456789_-+= ]*}
#     if [[ "$UNAME" == "MINGW" ]]
#     then
#         HOSTPLATFORM=mingw-w64-i686                     # mingw-hosted
#         export TOOLCHAINS="/c/neurotech/crosscompile"   # or wherever you want
#     else
#         HOSTPLATFORM=x86_64                      # linux-hosted
#         export TOOLCHAINS="$HOME/crosscompile"   # or wherever you want
#     fi
#     mkdir -p "$TOOLCHAINS"
#     pushd "$TOOLCHAINS"
#     curl -L https://developer.arm.com/-/media/Files/downloads/gnu-a/$VERSIONDATESTAMP/binrel/gcc-arm-$VERSIONDATESTAMP-$HOSTPLATFORM-$TARGET.tar.xz | tar xvJ
#     if [[ "$UNAME" == "MINGW" ]]; then cmd //c "mklink /J  gcc-$TARGET  gcc-arm-$VERSIONDATESTAMP-$HOSTPLATFORM-$TARGET"
#     else  ln -sfn  gcc-arm-$VERSIONDATESTAMP-$HOSTPLATFORM-$TARGET  gcc-$TARGET
#     fi
#     popd
# 
# And then::
# 
#     ./devel/build/go.cmd -DCMAKE_TOOLCHAIN_FILE=crosscompile/armv7.cmake
# 

IF( "$ENV{TOOLCHAINS}" STREQUAL "" )
	MESSAGE( FATAL_ERROR "\nYou must set the TOOLCHAINS environment variable first (absolute path to the parent directory of your gcc toolchain distributions).\n" )
ENDIF()

SET( CROSSCOMPILE_TARGET arm-none-linux-gnueabihf )
SET( TOOLCHAIN           $ENV{TOOLCHAINS}/gcc-${CROSSCOMPILE_TARGET} )
SET( PREFIX              ${CROSSCOMPILE_TARGET}- )

SET( CMAKE_SYSTEM_NAME      Linux )
SET( CMAKE_SYSTEM_PROCESSOR armv7 )

# SET( CMAKE_SYSROOT /home/devel/rasp-pi-rootfs )
# SET( CMAKE_STAGING_PREFIX /home/devel/stage   )

SET( CMAKE_AR           ${TOOLCHAIN}/bin/${PREFIX}ar  )
SET( CMAKE_ASM_COMPILER ${TOOLCHAIN}/bin/${PREFIX}as  )
SET( CMAKE_C_COMPILER   ${TOOLCHAIN}/bin/${PREFIX}gcc )
SET( CMAKE_CXX_COMPILER ${TOOLCHAIN}/bin/${PREFIX}g++ )
SET( CMAKE_LINKER       ${TOOLCHAIN}/bin/${PREFIX}ld  )
SET( CMAKE_OBJCOPY      ${TOOLCHAIN}/bin/${PREFIX}objcopy )
SET( CMAKE_RANLIB       ${TOOLCHAIN}/bin/${PREFIX}ranlib )
SET( CMAKE_SIZE         ${TOOLCHAIN}/bin/${PREFIX}size )
SET( CMAKE_STRIP        ${TOOLCHAIN}/bin/${PREFIX}strip )

SET( CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER )
SET( CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY )
SET( CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY )
SET( CMAKE_FIND_ROOT_PATH_MODE_PACKAGE ONLY )

SET( CMAKE_TRY_COMPILE_TARGET_TYPE     STATIC_LIBRARY )

IF( NOT EXISTS "${CMAKE_CXX_COMPILER}" )
	MESSAGE( FATAL_ERROR "\nFailed to find toolchain: ${CMAKE_CXX_COMPILER}\n" )
ENDIF()

