# This file is empty. Specifying it as your `-DCMAKE_TOOLCHAIN_FILE` value has no
# effect except to build the test binaries in a platform-inflected subdirectory of
# `devel/bin`, parallel to the binaries cross-compiled for other platforms.
# As a necessary side-effect (due to having to cache a record of this different
# location) a different `junk` directory will also be used.