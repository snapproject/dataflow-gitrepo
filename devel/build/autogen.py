#!/usr/bin/env python
# $BEGIN_DATAFLOW_LICENSE$
# 
# This file is part of the DataFlow project, a software library for
# efficient asynchronous multi-stream inter-process communication.
# 
# Copyright (c) 2014-2021 Jeremy Hill
# 
# DataFlow is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program. If not, see http://www.gnu.org/licenses/ .
# 
# Some third-party source code is compiled and linked into the
# DataFlow binary, as follows: devel/src/sem_timedwait_apple.cpp
# is adapted from sem_timedwait.cpp by Keith Shortridge, copyright
# (c) 2013 Australian Astronomical Observatory (AAO), released under
# the MIT License; devel/src/dirent_win.h is copyright (c) 2006
# Toni Ronkko, and released under the MIT License. The DataFlow build
# system also uses some third-party code:
# devel/build/cmake-recipes/DefineFunction_TARGET_ARCHITECTURE.cmake
# which is copyright (c) 2012 Petroules Corporation, released under
# the 2-Clause BSD License. For more details, see the respective
# copyright, licensing and disclaimer information in each of these
# files.
# 
# $END_DATAFLOW_LICENSE$

# TODO: 
#        - Easier access to `Load_DF_API` errors

"""
What it Generates
=================

In `release/`:

* Header files,  `@{API_NAME}.h` and `@{API_NAME}.hpp`, to be included by the client
  C or C++ applications, respectively.

* "Porch" files, `@{API_NAME}_API_Loader.c` which are the "stub library" sources that
  the client application has to compile. It can be compiled in either C or C++, although
  the latter may generate a warning because of the `.c` extension, unless you explicitly
  assert that the language should be C++, (use the `-xc++` flag with g++, `/TP` in MSVC).

* The equivalent Python porch,  `@{API_NAME}.py`


In `devel/src/autogen/`:

* `door.def`, full of `EXPORTS` statements required by Microsoft for making the DLL.

* `lobby.cpp` (equivalent of the old `@{API_NAME}_C_Interface.cpp`) wraps core C++
  calls in exportable C functions (`Foo_Thing_New()` wraps `new Foo::Thing()`, etc.)
  Required for making the dynamic library or for making a statically-linked C
  application (e.g. `ctest`).

* `dll.rc`, a nice-to-have for making DLLs on Microsoft because it allows meta-info
  like the version number to be embedded in the DLL in the way that Windows Explorer
  understands.
  

What it Requires
================

Primarily, this script consumes C++ header files which are passed as filenames/glob
patterns in this script's `argv`: for example `devel/src/*.hpp`. The ones that
actually get translated are the ones that are marked up with::

	/* API_NAME = DataFlow */
	/* API_SHORTNAME =  DF */
	
	/*API_WRAP*/
	...
	
	/*API_COPY*/
	...
	
	/*API_SKIP*/
	...

(see the `EXAMPLE` below for more details`)

In addition, two "template" files are required, in `devel/src/templates` (whose
sub-directory structure mirrors the overall distro):

* `devel/src/templates/release/@{API_NAME}.h`
* `devel/src/templates/release/@{API_NAME}_API_Loader.c`

Also, the following C++ library source pairs are mandatory:

* `devel/src/ThreadGlobals.cpp`  for its `SetLastError()` and `CacheString()` definitions
* `devel/src/ThreadGlobals.hpp`  included in the auto-generated `lobby.cpp`
* `devel/src/DebuggingUtils.cpp` for its `SanityCheck()` definition
* `devel/src/DebuggingUtils.hpp` included in the auto-generated `lobby.cpp`
* `devel/src/Introspection.cpp`  for its `GetVersion()`, `GetRevision()` and `GetBuildDatestamp()` definitions
* `devel/src/Introspection.hpp`  included in the auto-generated `lobby.cpp`

Your API itself must follow the following rules:

1.  At least one of your headers must specify values for `API_NAME` and `API_SHORTNAME`,
    using comments as above, and there cannot be conflicting definitions for these.

2.  `API_SHORTNAME` should match the name of your API's top-level namespace (`DF` in this
    example).

3.  The top-level namespace must declare an API-wrapped function called `ClearError`.
    For example, in one of the headers you might see::
   
        namespace DF
        {
            /*API_WRAP*/ const char * ClearError(void);
            /*API_COPY*/
        }

    The actual definition of this function, which is a straight wrapper around
    `ThreadGlobals::ClearLastError()`, will be generated automatically.  There
    are other automatically-generated functions, for which you can also provide 
    wrapped declarations if you wish. The complete set is::
    
        /*API_WRAP*/
        const char * ClearError( void );                         // Definitions
        const char * Error( void );                              // for
        const char * GetVersion( void );                         // these
        const char * GetRevision( void );                        // all
        const char * GetBuildDatestamp( void );                  // get
        void         SanityCheck( void );                        // auto-generated

4.  Your input arguments in your wrapped functions and methods should not be arrays or
    references (exception: `std::string` references).  When passing other custom objects
    whose class declarations are wrapped in the API, use pointers.

5.  All declarations of wrapped classes must *start* with (in `/*API_COPY*/` mode) the
    following line::

        public: void * mShadow; // NB: must be first member, must be public
    

Example of usage in `CMakeLists.txt`::

	ADD_CUSTOM_COMMAND(
		OUTPUT
			${DF_API_ROOT}/devel/src/autogen/lobby.cpp
			${DF_API_ROOT}/devel/src/autogen/door.def
			${DF_API_ROOT}/devel/src/autogen/dll.rc
			${DF_API_ROOT}/release/DataFlow.h
			${DF_API_ROOT}/release/DataFlow.hpp
			${DF_API_ROOT}/release/DataFlow.py
			${DF_API_ROOT}/release/DataFlow_API_Loader.c
		COMMAND python "${DF_API_ROOT}/devel/build/autogen.py" "${DF_API_ROOT}/devel/src/*.hpp"
		DEPENDS         ${DF_API_ROOT}/devel/build/autogen.py   ${DF_API_ROOT}/devel/src/*.hpp  ${DF_API_ROOT}/devel/src/templates/release/* ${DF_API_ROOT}/devel/src/templates/devel/src/autogen/*
		COMMENT "Running autogen"
	)


"""

EXAMPLE = r"""

/* API_VAR  API_SHORTNAME =  DF */
/* API_VAR  API_NAME = DataFlow */

#include <string>       // default mode is "copy", so lines like this will appear in the release .hpp header but will not be wrapped (will not appear in the release .h header)

/*API_WRAP*/
#include <stdint.h>     // c- and c++-compatible includes, for any types that may be needed in public (wrapped) declarations
/*API_SKIP*/
#include "Whatever.hpp" // includes that define any types that may be needed for private (non-wrapped) parts of this header
/*API_COPY*/

namespace DF
{
	/*API_WRAP*/
	void Sleep( double seconds );
	/*API_COPY*/
	
	class DataNode
	{
		public: DataNode * mShadow; // must be first member, initialized to NULL, for all publicly exposed classes
		                            // Note that all non-static members should be public, to help ensure this class has "standard layout"
		public: /*API_WRAP*/
			DataNode( const std::string & name
			/* blah */); // blah blah
			~DataNode();
			DataNode * GetNext( bool depthFirst );
			const char * GetName( void );
		private: /*API_SKIP*/
			DoSomething( void );
		/*API_COPY*/
	};
}
/*API_WRAP*/
void SanityCheck( void );
/*API_COPY*/


/*API_COPY*/ namespace DF
/*API_COPY*/ {
/*API_WRAP*/ 	void Sleep( double seconds );
/*API_COPY*/ 	class DataNode
/*API_COPY*/ 	{
/*API_COPY*/ 		public: DataNode * mShadow; // must be first member, initialized to NULL, for all publicly exposed classes
/*API_COPY*/ 		public:
/*API_WRAP*/ 			DataNode( const std::string & name );
/*API_WRAP*/  			~DataNode();
/*API_SKIP*/ 		private:
/*API_SKIP*/ 			DoSomething( void );
/*API_COPY*/ 	};
/*API_COPY*/ }
"""

import re
import os
import sys
import time
import glob
import shlex
import subprocess
import collections

"""
Let's define "type 1 construction" as calling `new Foo()` inside the DLL, passing the
pointer outside the DLL and then casting it as `(Foo *)` there. If that's all we ever
do, then it suffices to have a porch-side destructor that does::
 
 	~Foo(){ API_Foo_Delete(this); }
 
and other methods accordingly::

	Blah * SomeMethod(int x){ return (Blah *)API_Foo_SomeMethod(this, x); }

However, if we *also* want to enable type 2 construction, i.e. to be able to say
`Foo f();` or `Foo * fp = new Foo();` *outside* the DLL, then we need to defer the
*real* construction work to `fp = API_Foo_New();`: then we need to  store `fp` and
somehow ensure that DLL methods always operate on `fp` instead of `this`. For this
reason all classes have an `mShadow` member that is only ever written-to by the
porch-side constructor, and in which the output of `API_Foo_New()` is stored. All
porch methods now operate on `THIS` which is a macro for `(mShadow ? mShadow : this )`.
Flipping the coin back over, we have to ensure compatibility with type-1-constructed
instances.  This means that DLL-internal class definitions must also have a pointer
member in the same memory location as `mShadow` (so it has to be the very first member,
in both class definitions) and that it remains NULL. We might also have to worry about
member *alignment* (potential TODO...)
"""


ROOT = os.path.realpath( os.path.join( __file__, '..', '..', '..' ) )

MARKER = dict(
	copy='/*API_COPY*/',
	wrap='/*API_WRAP*/',
	skip='/*API_SKIP*/',
)

COMMENT_PATTERN = re.compile( r'//.*?$|/\*.*?\*/|\'(?:\\.|[^\\\'])*\'|"(?:\\.|[^\\"])*"', re.DOTALL | re.MULTILINE )
NAMESPACE_PATTERN = re.compile( r"""
	^
	(?P<type>class|namespace)
	\s+
	(?P<name>\S+)
	\s*
	\{?
	$
""", re.VERBOSE )
PROTOTYPE_PATTERN = re.compile( r"""
	^
	(
		(?P<CPP_RETURN_TYPE>.*)
		\s+
	)?
	(?P<CPP_NAME>\S+)
	\s*\(\s*
	(?P<CPP_DEFINE_ARGS_WITH_DEFAULTS>.*)
	\s*\)\s*
	(?P<CPP_QUALIFIERS>.*)
	\s*
	\;
	$
""", re.VERBOSE )

def RemoveCppComments( text ):
    def replacer( match ): s = match.group(0); return " " if s.startswith( '/' ) else s
    return re.sub( COMMENT_PATTERN, replacer, text )
    
SPACE_PLACEHOLDER   = '\x01' # can be any otherwise-useless single non-space character
NEWLINE_PLACEHOLDER = '\x02' # can be any otherwise-useless single non-space character

def Bang( cmd, shell=False, stdin=None, cwd=None, raiseException=False ):
	windows = sys.platform.lower().startswith('win')
	# If shell is False, we have to split cmd into a list---otherwise the entirety of the string
	# will be assumed to be the name of the binary. By contrast, if shell is True, we HAVE to pass it
	# as all one string---in a massive violation of the principle of least surprise, subsequent list
	# items would be passed as flags to the shell executable, not to the targeted executable.
	# Note: Windows seems to need shell=True otherwise it doesn't find even basic things like ``dir``
	# On other platforms it might be best to pass shell=False due to security issues, but note that
	# you lose things like ~ and * expansion
	if isinstance( cmd, str ) and not shell:
		if windows: cmd = cmd.replace( '\\', '\\\\' ) # otherwise shlex.split will decode/eat backslashes that might be important as file separators
		cmd = shlex.split( cmd ) # shlex.split copes with quoted substrings that may contain whitespace
	elif isinstance( cmd, ( tuple, list ) ) and shell:
		quote = '"' if windows else "'"
		cmd = ' '.join( ( quote + item + quote if ' ' in item else item ) for item in cmd )
	try: sp = subprocess.Popen( cmd, shell=shell, cwd=cwd, stdout=subprocess.PIPE, stderr=subprocess.PIPE )
	except OSError as exc: returnCode, output, error = 'command failed to launch', '', str( exc )
	else: output, error = [ x.decode( 'utf8' ).strip() for x in sp.communicate( stdin ) ]; returnCode = sp.returncode
	if raiseException and returnCode:
		if isinstance( returnCode, int ): returnCode = 'command failed with return code %s' % returnCode
		raise OSError( '%s:\n    %s\n    %s' % ( returnCode, cmd, error ) )
	return returnCode, output, error

def GetRevision():
	rev = '(unknown revision)'
	repoSubdirectories = [ entry for entry in os.listdir( ROOT ) if os.path.isdir( os.path.join( ROOT, entry ) ) ]
	if all( x in repoSubdirectories for x in [ '.git', 'devel', 'release' ] ): # then we're probably in the right place
		dotGitDir = os.path.join( ROOT, '.git' )
		out = ' '.join(
			stdout.strip()
			for cmd in [
				'git log -1 "--format=%h (%ci)"',
				#'git describe --always --all --long --dirty=+ --broken=!',
			] for errorCode, stdout, stderr in [ Bang( cmd, cwd=ROOT ) ] if not errorCode
		)
		if out:
			errorCode, stdout, stderr = Bang( 'git describe --always --all --long --dirty=+ --broken=!', cwd=ROOT )
			inflection = re.match( r'.*?([\+\!]*)$', stdout ).group( 1 ) if stdout else ''
			if inflection: out = re.sub( r'^(\S+)', r'\1' + inflection, out )
			#if inflection: out += ' ' + inflection
			rev = 'git ' + out
	elif all( x in repoSubdirectories for x in [ '.hg', 'devel', 'release' ] ): # then we're probably in the right place
		errorCode, stdout, stderr = Bang( 'hg id -intb -R "%s"' % ROOT )
		if not errorCode: rev = 'hg ' + stdout
	return rev
		
SUBSTITUTIONS = {
	         'API_NAME' : None,  # collectively, the header files must supply 
	    'API_SHORTNAME' : None,  # value for both of these
	
	          'AUTOGEN' : os.path.basename( __file__ ),
	  'BUILD_DATESTAMP' : time.strftime( '%Y-%m-%d %H:%M:%S %z' ),
	     'API_REVISION' : GetRevision(),
}

def Expand( definition, template ):
	txt = template
	def repl( match ):
		name = match.group( 'name' ).upper()
		indent = match.group( 1 )
		r = definition.get( name, match.group( 0 ) )
		if name in definition and indent: return re.sub( '^', indent, r, flags=re.MULTILINE ).rstrip()
		else: return r.strip()
	txt = re.sub( r'(^[ \t]+)?\@\{?(?P<name>[a-zA-Z_]+)\}?', repl, txt, flags=re.MULTILINE )
	txt = txt.strip().replace( SPACE_PLACEHOLDER, ' ' ).replace( NEWLINE_PLACEHOLDER, '\n' )
	return txt

def Report( x, indent=0, sort=True ):
	s = ''
	spaces = ' ' * indent
	if isinstance( x, dict ):
		longest = len( max( x, key=len ) )
		items = x.items()
		if sort: items = sorted( items )
		s += spaces + '{\n' + '\n'.join( '%s%r : %s,' % (  ' ' * ( indent + 1 + longest - len( k ) ), k, Report( v, indent=indent + 1 + longest - len( k ) ).lstrip() ) for k, v in items ) + '\n' + spaces + '}'
	elif isinstance( x, tuple ):
		s += spaces + '(\n' + '\n'.join( Report( v, indent=indent + 4 ) + ',' for v in x ) + '\n' + spaces + ')'
	elif isinstance( x, list ):
		s += spaces + '[\n' + '\n'.join( Report( v, indent=indent + 4 ) + ',' for v in x ) + '\n' + spaces + ']'
	elif isinstance( x, ( float ) ):
		s += spaces + str( x )
	else:
		s += spaces + repr( x )
	return s

def FindFiles( top='.', stem=() ):
	for x in os.listdir( top ):
		joined = os.path.join( top, x )
		partial = stem + ( x, )
		if os.path.isdir( joined ):
			for x in FindFiles( joined, stem=partial ): yield x
		else: yield '/'.join( partial )

	
def ParseHeaderFiles( *headerFiles ):
	headerFiles = [ fileName for item in headerFiles for pattern in ( item if isinstance( item, ( tuple, list ) ) else [ item ] ) for fileName in sorted( glob.glob( pattern ) ) ]
	parsed = {}
	if not headerFiles: headerFiles = [ 'EXAMPLE' ]
	releaseHppContent = ''
	definitions = []
	c_preprocessor_directives = []
	wrappedClasses = {}
	allClasses = {}
	
	substitutionDetectionPattern = re.compile( r"""
		(?P<stuff>[^\r\n]*?)
		[\t ]*
		\/\*
		[\t ]*
		API_VAR[\t ]+
		(?P<key>[a-zA-Z0-9_]+)
		[\t ]*
		(=[\t ]*(?P<optionalValue>.*?)[\t ]*)?
		\*\/
		(([\t ]*)\n)?
	""", re.VERBOSE | re.M )
	
	def ReadSubstitution( match ):
		stuff = match.group( 'stuff' )
		key = match.group( 'key' )
		optionalValue = match.group( 'optionalValue' )
		terms = shlex.split( stuff.rstrip( ';' ) )
		value = optionalValue if optionalValue else terms[ -1 ] if terms else ''
		previous = SUBSTITUTIONS.get( key, None )
		if previous not in [ None, value ]: raise RuntimeError( 'header files define conflicting values for %s: %s and %s' % ( key, previous, value ) )
		SUBSTITUTIONS[ key ] = value
		repl = stuff + ( '\n' if match.group( 0 ).endswith( '\n' ) and stuff else '' )
		#print( '%r %r %r %r %r' % ( key, value, stuff, match.group( 0 ), repl ) )
		return repl
	
	for headerFile in headerFiles:
		if headerFile == 'EXAMPLE': source = EXAMPLE
		else: source = open( headerFile, 'rt' ).read()
		source = source.strip().replace( '\r\n', '\n' ).replace( '\r', '\n' )

		source = re.sub( substitutionDetectionPattern, ReadSubstitution, source )

		if not any( v in source for v in MARKER.values() ): continue
		for marker in MARKER.values():
			source = re.sub( r'(?<=\S)[ \t]+' + re.escape( marker ), '\n' + marker, source )
			source = re.sub( re.escape( marker ) + r'([\t ]+)\n?', marker + r'\n\1', source ).lstrip()
			
		mode = 'copy'
		namespace = []
		prefixLines = []
		
		for line in source.split( '\n' ):
			line = line.rstrip()
			line = re.sub( substitutionDetectionPattern, ReadSubstitution, line )
			lineStripped = line.strip()
			lineWithoutComments = RemoveCppComments( lineStripped ).strip()
			lineWithoutSpaces = lineWithoutComments.replace( '\t', '' ).replace( ' ', '' )
			
			for k, v in MARKER.items():
				if lineStripped == v: mode = k; break
			else:
				action = mode
				if lineWithoutSpaces in [ 'private:', 'protected:' ]: action = 'skip'
				elif lineWithoutSpaces in [ 'public:' ] and action in [ 'wrap' ]: action = 'copy'
				if action != 'skip':
					className = '::'.join( name for type, name in namespace ) if ( namespace and namespace[ -1 ][ 0 ] == 'class' ) else None
					if className and className not in allClasses: allClasses[ className ] = []
					match = re.match( NAMESPACE_PATTERN, lineWithoutComments )
					if match: namespace.append( ( match.group( 'type' ), match.group( 'name' ) ) )
					elif lineWithoutSpaces in [ '}', '};' ] and namespace:
						type, name = namespace.pop( -1 )
					elif action == 'copy' and len( lineWithoutSpaces ):
						if not lineWithoutComments.startswith( '#' ) and lineWithoutSpaces.endswith( ';' ):
							if className: allClasses[ className ].append( lineWithoutComments )
					elif action == 'wrap' and len( lineWithoutSpaces ):
						if lineWithoutComments.startswith( '#' ):
							c_preprocessor_directives.append( line )
						elif lineWithoutComments.endswith( ';' ):
							lineWithoutComments = ' '.join( prefixLines + [ lineWithoutComments ] )
							prefixLines = []
							match = re.match( PROTOTYPE_PATTERN, lineWithoutComments )
							if not match: raise RuntimeError( 'failed to parse %r in %s' % ( lineWithoutComments, headerFile ) )
							d = match.groupdict()
							d[ 'CPP_CLASS' ] = className
							if className: wrappedClasses[ className ] = allClasses[ className ]
							else: d[ 'CPP_NAME' ]  = '::'.join( name for type, name in namespace + [ [ '', d[ 'CPP_NAME' ] ] ] )
							d[ 'cpp_header' ] = headerFile
							for k, v in list( d.items() ): d[ k ] = v.strip() if v else ''
							definitions.append( d )
							#print( 'wrapping: ' + lineWithoutComments + str( d ) )
						else:
							prefixLines.append( lineWithoutComments )
					releaseHppContent += line + '\n'
	def pp( x, indent='' ):
		newIndent = indent + '  '
		if isinstance( x, dict ):  return '{\n' + ''.join( '%s%31s : %s,\n' % ( newIndent, repr(key), pp( value, newIndent ) ) for key, value in x.items() ) + indent + '}'
		if isinstance( x, list ):  return '[\n' + ''.join( '%s%s,\n' % ( newIndent, pp( value, newIndent ) ) for value in x ) + indent + ']'
		if isinstance( x, tuple ): return '(\n' + ''.join( '%s%s,\n' % ( newIndent, pp( value, newIndent ) ) for value in x ) + indent + ')'
		return indent + repr( x )
	
	#print( 'allClasses = ' + pp( allClasses ) )
	#print( 'wrappedClasses = ' + pp( wrappedClasses ) )
	#print( pp( definitions ) )
	#print( releaseHppContent )
	
	for className, members in wrappedClasses.items():
		if members and re.match( r'.*void\s*\*\s*mShadow\s*;\s*$', members[ 0 ] ): continue
		print( '\n%r' % {className: members} )
		raise RuntimeError( '%s class must have `void * mShadow` as its first member (remember also to initialize it to NULL in the constructor definition)' % className )
	
	
	SUBSTITUTIONS.setdefault( 'API_VERSION', '0.0.0' )
	SUBSTITUTIONS[ 'API_VERSION' ] = version = SUBSTITUTIONS[ 'API_VERSION' ].strip( '"' )
	SUBSTITUTIONS[ 'RC_FILEVERSION' ] = SUBSTITUTIONS[ 'RC_PRODUCTVERSION' ] = ', '.join( ( version.split( '.' ) + '0 0 0 0'.split() )[ :4 ] )
	SUBSTITUTIONS[ 'ASSERT_ENDIANNESS' ] = "'%s'" % SUBSTITUTIONS.get( 'ASSERT_ENDIANNESS', "0" ).strip( '\'"' ).upper()[ 0 ] # possible values are "0", "'L'" and "'B'". It's used as a C char type, so we need to ensure single-quotes around L or B:
	
	for k in [ 'API_NAME', 'API_SHORTNAME' ]:
		if not SUBSTITUTIONS[ k ]: raise RuntimeError( '%s was not defined in any header' % k )
		
	namespaceInflectionPattern = re.compile( r"""
	^(%s)\:\:
	""" % '|'.join( re.escape( SUBSTITUTIONS[ k ] ) for k in [ 'API_NAME', 'API_SHORTNAME' ] ), re.VERBOSE )
	
	c_class_names = set()
	c_instance_pointer_types = set()
	def infer_c_type( cpp_type ): # NB: uses `c_instance_pointer_types` from surrounding scope
		note = ''
		cpp_type = re.sub( r'\s+', ' ', cpp_type.replace( '&', ' &' ).replace( '*', ' *' ) )
		if cpp_type == 'bool':
			c_type = 'int'
		elif re.match( r'^(const\s+)?(std\:\:)?string(\s*\&)?', cpp_type ):
			c_type = 'const char *'
			note = 'cpp_string'
		elif '::' in cpp_type and not cpp_type.startswith( ( '::', 'std::' ) ):
			c_type = cpp_type.replace( '::', '_' ).strip()
			if c_type.endswith( '*' ): c_type = c_type[ :-1 ].strip()
			#elif c_type.endswith( '&' ): c_type = c_type[ :-1 ].strip() # TODO - special conversion required?
			core = re.sub( r'^\s*const\s+', '', c_type.rstrip( ' \t&*[]' ) )
			c_instance_pointer_types.add( core )
		else:
			c_type = cpp_type.lstrip( ':' )
		return c_type, note
	
	def grokstars( argdef ): # ensures that * (or &) can be easily split off as part of the type, where it belongs, rather than sticking to, and being misclassified as part of, the argument name
		return argdef.replace( '&', '& ' ).replace( '& &', '&&' ).replace( '*', '* ' ).replace( '* *', '**' ) 
	
	for d in definitions:
		d.setdefault( 'CPP_QUALIFIERS', '' )
		d.setdefault( 'CPP_CLASS', '' )
		d.setdefault( 'CPP_DEFINE_ARGS_WITH_DEFAULTS', '' )	
		d[ 'C_RETURN_TYPE' ], d[ 'RETURNS_CPP_STRING' ] = infer_c_type( d[ 'CPP_RETURN_TYPE' ] )
		args = d[ 'ARGUMENTS' ] = d.get( 'CPP_DEFINE_ARGS_WITH_DEFAULTS', '' ).strip( ' \t()' ).replace( ',', '\n' )
		args = d[ 'ARGUMENTS' ] = [] if args in [ '', 'void' ] else args.split( '\n' )
		for i, argdef in enumerate( args ):
			arg = args[ i ] = {}
			argdef, arg[ 'DEFAULT' ] = [ x.rstrip( '=' ).strip() for x in ( argdef + '=' ).split( '=', 1 ) ]
			arg[ 'CPP_TYPE' ], arg[ 'NAME' ] = grokstars( argdef ).rsplit( None, 1 )
			arg[ 'TYPE_POSTFIX' ] = ''
			if arg[ 'NAME' ].strip().endswith( '[]' ):
				arg[ 'TYPE_POSTFIX' ] = '[]'
				arg[ 'NAME' ] = arg[ 'NAME' ].rstrip( '[] \t' )
			arg[ 'C_TYPE' ], arg[ 'IS_CPP_STRING' ] = infer_c_type( arg[ 'CPP_TYPE' ] )	
			arg[ 'CPP_CAST' ] = cppCast = arg[ 'CPP_TYPE' ] + ( '*' if arg[ 'TYPE_POSTFIX' ] else '' )
			arg[ 'C_CAST'   ] = arg[ 'C_TYPE'   ] + ( '*' if arg[ 'TYPE_POSTFIX' ] else '' )
			if arg[ 'IS_CPP_STRING' ]:
				arg[ 'PASS_C'   ] = '{NAME}.c_str()'
				arg[ 'PASS_CPP' ] = 'INPUT_STRING( {NAME} )'
			else:
				needsShadow = cppCast.endswith( '*' ) and cppCast.split()[ 0 ] in wrappedClasses
				arg[ 'PASS_C'   ] =   '( {C_CAST} )THAT({NAME})' if needsShadow else '( {C_CAST} ){NAME}'
				arg[ 'PASS_CPP' ] = '( {CPP_CAST} ){NAME}'
		# TODO: go through each CPP_PASS_ARGS, see if what we have is a pointer to a class in wrappedClasses
		#       if so, it will have an mShadow member, so do the mShadow dance with it.
		
		d[ 'CPP_DEFINE_ARGS_WITHOUT_DEFAULTS' ] = ', '.join( '{CPP_TYPE} {NAME}{TYPE_POSTFIX}'.format( **arg ) for arg in d[ 'ARGUMENTS' ] )
		d[ 'C_DEFINE_ARGS' ] = ', '.join( '{C_TYPE} {NAME}{TYPE_POSTFIX}'.format( **arg ) for arg in d[ 'ARGUMENTS' ] )
		d[ 'C_PASS_ARGS'   ] = ', '.join( arg[ 'PASS_C'   ].format( **arg ) for arg in d[ 'ARGUMENTS' ] )
		d[ 'CPP_PASS_ARGS' ] = ', '.join( arg[ 'PASS_CPP' ].format( **arg ) for arg in d[ 'ARGUMENTS' ] )
	
		d[ 'CPP_FULL_NAME' ] = '::'.join( d[ x ] for x in 'CPP_NAMESPACE CPP_CLASS CPP_NAME'.split() if d.get( x, '' ) )
		pieces = re.split( '::', d[ 'CPP_FULL_NAME' ] )
		d[ 'CPP_SHORT_NAME' ] = pieces[ -1 ]
		d[ 'CPP_NAMESPACE' ] = '::'.join( pieces[ :-1 ] )
		
		if d[ 'CPP_CLASS' ]:
			d[ 'CPP_CLASS' ] = d[ 'CPP_NAMESPACE' ]
			d[ 'CPP_CLASS_SHORT_NAME' ] = re.split( '::', d[ 'CPP_CLASS' ] )[ -1 ]
			d[ 'C_INSTANCE_POINTER_TYPE' ] = d[ 'CPP_CLASS' ].replace( '::', '_' )
			c_instance_pointer_types.add( d[ 'C_INSTANCE_POINTER_TYPE' ] )
			if   d[ 'CPP_SHORT_NAME' ] ==       d[ 'CPP_CLASS_SHORT_NAME' ]:
				d[ 'SPECIES' ] = 'constructor'
			elif d[ 'CPP_SHORT_NAME' ] == '~' + d[ 'CPP_CLASS_SHORT_NAME' ]:
				d[ 'SPECIES' ] = 'destructor'
			else:
				d[ 'SPECIES' ] = 'method'
			if d[ 'SPECIES' ] not in [ 'constructor' ]:
				d[ 'C_DEFINE_ARGS' ] = d[ 'C_INSTANCE_POINTER_TYPE' ] + ' _this' + ( ', ' if d[ 'C_DEFINE_ARGS' ] else '' ) + d[ 'C_DEFINE_ARGS' ]
				d[ 'C_PASS_ARGS' ] = '( {C_INSTANCE_POINTER_TYPE} )THIS'.format( **d ) + ( ', ' if d[ 'C_PASS_ARGS' ] else '' ) + d[ 'C_PASS_ARGS' ]
		else:
			d[ 'SPECIES' ] = 'function'
		if not d[ 'C_DEFINE_ARGS' ]: d[ 'C_DEFINE_ARGS' ] = 'void'
		if d[ 'SPECIES' ] == 'constructor':
			d[ 'C_NAME' ] = d[ 'CPP_CLASS' ].replace( '::', '_' ) + '_New'
			d[ 'C_RETURN_TYPE' ] = d[ 'C_INSTANCE_POINTER_TYPE' ];
		elif d[ 'SPECIES' ] == 'destructor':
			d[ 'C_NAME' ] = d[ 'CPP_CLASS' ].replace( '::', '_' ) + '_Delete'
			d[ 'C_RETURN_TYPE' ] = 'void';
		else:
			d[ 'C_NAME' ] = d[ 'CPP_FULL_NAME' ].replace( '::', '_' )
			if not args: d[ 'CPP_DEFINE_ARGS_WITH_DEFAULTS' ] = d[ 'CPP_DEFINE_ARGS_WITHOUT_DEFAULTS' ] = 'void'
		# TODO: in upgrading from previous autogen approach, we have lost the ability to set @DOC and (here) @C_FAILURE_VALUE
		d.setdefault( 'C_FAILURE_VALUE', 'NULL' if d[ 'C_RETURN_TYPE' ].strip().endswith( '*' ) else '0' )
		if d[ 'C_RETURN_TYPE' ] == 'void':
			d[ 'LOBBY_EXECUTION_MACRO' ] = 'DO'
			d[ 'PORCH_EXECUTION_MACRO' ] = 'DO'
		elif d[ 'RETURNS_CPP_STRING' ]:
			d[ 'LOBBY_EXECUTION_MACRO' ] = 'RETURN_STRING'
			d[ 'PORCH_EXECUTION_MACRO' ] = 'RETURN_STRING'
		else:
			d[ 'LOBBY_EXECUTION_MACRO' ] = 'RETURN'
			d[ 'PORCH_EXECUTION_MACRO' ] = 'RETURN'

		d[ 'PYTHON_CLASS' ] = re.sub( namespaceInflectionPattern, '', d[ 'CPP_CLASS' ] ).replace( '::', '.' )
		d[ 'PYTHON_NAME' ] = { 'constructor' : '__init__', 'destructor' : '__del__' }.get( d[ 'SPECIES' ], re.sub( namespaceInflectionPattern, '', d[ 'CPP_NAME' ] ).replace( '::', '.' ) )

	max_length_of_c_return_type = max( len( d[ 'C_RETURN_TYPE' ] ) for d in definitions )
	max_length_of_cpp_return_type = max( len( d[ 'CPP_RETURN_TYPE' ] ) for d in definitions )
	for d in definitions:
		d[ 'PADDED_C_RETURN_TYPE' ] = d[ 'C_RETURN_TYPE' ] + SPACE_PLACEHOLDER * ( 1 + max_length_of_c_return_type - len( d[ 'C_RETURN_TYPE' ] ) )
		d[ 'PADDED_CPP_RETURN_TYPE' ] = d[ 'CPP_RETURN_TYPE' ] + SPACE_PLACEHOLDER * ( 1 + max_length_of_cpp_return_type - len( d[ 'CPP_RETURN_TYPE' ] ) )

	generated = collections.defaultdict( lambda: collections.defaultdict( str ) )

	c_type_declarations = ''.join( 'typedef struct _{t} {{void *__{t};}} * {t};\n'.format( t=t ) for t in c_instance_pointer_types )
	# NB: the `void *` member is only there because an empty struct generates a warning from the
	#     C++ compiler (empty structs have size 0 in C and size 1 in C++). `void *` is chosen because
	#     coincidentally, it should always meaningfully point to the `mShadow` member---but at the
	#     moment we do not make any active use of that fact.
	
	###
		
	lobby = 'devel/src/autogen/lobby.cpp'  # equivalent of *_C_Interface.cpp
	lobbyIncludes = { '"' + os.path.basename( d[ 'cpp_header' ] ) + '"' for d in definitions }
	lobbyIncludes.add( '"ThreadGlobals.hpp"' )  # for ThreadGlobals::GetLastError and ThreadGlobals::ClearLastError()
	lobbyIncludes.add( '"DebuggingUtils.hpp"' ) # for DebuggingUtils::SanityCheck(char assertEndianness='\0')
	lobbyIncludes.add( '"Introspection.hpp"' ) # for Introspection::GetVersion(), Introspection::GetRevision() and Introspection::GetBuildDatestamp()
	lobbyIncludes = [ '<string>' ] + sorted( lobbyIncludes )
	generated[ lobby ][ 'INCLUDES' ] = '\n'.join( '#include %s' % inc for inc in lobbyIncludes )
	generated[ lobby ][ 'MACROS' ] = r"""
#undef  SET_ERROR
#define SET_ERROR    ThreadGlobals::SetLastError
#undef  CACHE_STRING
#define CACHE_STRING ThreadGlobals::CacheString
#undef  INPUT_STRING
#define INPUT_STRING( s )  ( s ? s : "" )
#undef  DO
#define DO( TYPE, FAILURE, COMMAND ) try { COMMAND; } catch( std::string e ){ SET_ERROR( e ); } catch( std::exception const & e ) { SET_ERROR( e.what() ); }
#undef  RETURN
#define RETURN( TYPE, FAILURE, CALL ) DO( TYPE, FAILURE, return ( TYPE )( CALL ) ); return ( TYPE )( FAILURE );
#undef  RETURN_STRING
#define RETURN_STRING( TYPE, FAILURE, CALL ) RETURN( TYPE, FAILURE, CACHE_STRING( CALL ) )
""".lstrip()
	generated[ lobby ][ 'CPP_DEFINITIONS' ] = """
namespace @API_SHORTNAME
{
	const char * Error( void )      { return ThreadGlobals::GetLastError();   } // TODO: is CACHE_STRING being called more than once here?
	const char * ClearError( void ) { return ThreadGlobals::ClearLastError(); }             // include declarations for these
	const char * GetVersion( void ) {        return Introspection::GetVersion(); }          // functions, or not, as you wish,
	const char * GetRevision( void ) {       return Introspection::GetRevision(); }         // in your API declaration header
	const char * GetBuildDatestamp( void ) { return Introspection::GetBuildDatestamp(); }
	void         SanityCheck( void ) { DebuggingUtils::SanityCheck( @ASSERT_ENDIANNESS ); }
}
""".lstrip() # you need to /*API_WRAP*/ your *declarations* of these in your header if you want them to be available (and for Python's sake you'd better include ClearError), but you don't need to (and shouldn't) write your own definitions
	generated[ lobby ][ 'DECLARATIONS' ] = c_type_declarations + '\n'
	generated[ lobby ][ 'TEMPLATE' ] = '@INCLUDES\n\n@MACROS\n\n@CPP_DEFINITIONS\n\nextern "C" {\n@DECLARATIONS\n\n@DEFINITIONS\n}\n'

	###
	
	version_info = 'devel/src/autogen/build_info.h'  # this directory should appear before `devel/src` itself on the include path; therefore the autogenned version of this header should overshadow the generic fallback
	generated[ version_info ][ 'TEMPLATE' ] = """
#define BUILD_VERSION   "@API_VERSION"
#define BUILD_REVISION  "@API_REVISION"
#define BUILD_DATESTAMP "@BUILD_DATESTAMP"
"""
	###
	
	door = 'devel/src/autogen/door.def'
	generated[ door ][ 'TEMPLATE' ] = '@EXPORTS\n'

	###
	
	rsrc = 'devel/src/autogen/dll.rc'
	generated[ rsrc ]

	###
	
	release_c_header = 'release/@API_NAME.h'
	generated[ release_c_header ][ 'DECLARATIONS' ] = c_type_declarations + '\n'
	generated[ release_c_header ][ 'C_PREPROCESSOR_DIRECTIVES' ] = '\n'.join( c_preprocessor_directives )
	# template will be read from a template file (TODO: what truly needs to be in that template? can we eliminate it?)

	###
	
	release_cpp_header = 'release/@API_NAME.hpp'
	generated[ release_cpp_header ][ 'DECLARATIONS' ] = releaseHppContent + '\n'
	generated[ release_cpp_header ][ 'TEMPLATE' ] = '@DECLARATIONS\n'

	### 
	
	porch = 'release/@{API_NAME}_API_Loader.c' 
	generated[ porch ][ 'CPP_MACROS' ] = r"""
#undef  THIS
#define THIS ( mShadow ? mShadow : this )
#undef  THAT
#define THAT( ARGNAME )   ( ARGNAME && ARGNAME->mShadow ? ARGNAME->mShadow : ARGNAME )
#undef  CHECK
#define CHECK if( @{API_SHORTNAME}_Error() ) throw( std::string( @{API_SHORTNAME}_ClearError() ) )
#undef  DO
#define DO( CALL ) CALL; CHECK;
#undef  RETURN
#define RETURN( TYPE, CALL ) TYPE _r = ( TYPE )( CALL ); CHECK; return _r;
#undef  RETURN_STRING
#define RETURN_STRING( TYPE, CALL ) TYPE _r( CALL ); CHECK; return _r;
""".lstrip() # TODO: see if we can remove the @{API_SHORTNAME} inflection above?
	# read template from file (can't avoid that---needs skeleton of loader function, etc) 


	python = 'release/@{API_NAME}.py'
	generated[ python ][ 'PYTHON_CLASS_MAPPING' ] = ','.join(
		'%s=%r' % ( d[ 'C_INSTANCE_POINTER_TYPE' ], d[ 'PYTHON_CLASS' ] )
		for d in definitions if d[ 'SPECIES' ] == 'constructor'
	)

	license = 'devel/src/autogen/license_header.txt'
	generated[ license ]

	### 
	
	for d in definitions:
	
		if d[ 'SPECIES' ] == 'constructor':
			cWrapper = Expand( d, "@C_RETURN_TYPE @C_NAME( @C_DEFINE_ARGS ) { RETURN( @C_INSTANCE_POINTER_TYPE, NULL, new @CPP_CLASS( @CPP_PASS_ARGS ) ) }" )
		elif d[ 'SPECIES' ] == 'destructor':
			cWrapper = Expand( d, "@C_RETURN_TYPE @C_NAME( @C_DEFINE_ARGS ) { delete ( @CPP_CLASS * )_this; }"  )
		elif d[ 'SPECIES' ] == 'method':
			cWrapper = Expand( d, "@C_RETURN_TYPE @C_NAME( @C_DEFINE_ARGS ) { @LOBBY_EXECUTION_MACRO( @C_RETURN_TYPE, @C_FAILURE_VALUE, ( ( @CPP_CLASS * )_this )->@CPP_SHORT_NAME( @CPP_PASS_ARGS ) ); }" )
		elif d[ 'SPECIES' ] == 'function':
			cWrapper = Expand( d, "@C_RETURN_TYPE @C_NAME( @C_DEFINE_ARGS ) { @LOBBY_EXECUTION_MACRO( @C_RETURN_TYPE, @C_FAILURE_VALUE, @CPP_FULL_NAME( @CPP_PASS_ARGS ) ); }" )
		else:
			raise ValueError( 'SPECIES=%r' % d[ 'SPECIES' ] )
		generated[ lobby ][ 'DEFINITIONS' ] += cWrapper + '\n'
		
		###

		exportsLine = Expand( d, "EXPORTS @C_NAME" )
		generated[ door ][ 'EXPORTS' ] += exportsLine + '\n'
		
		###
		
		cDeclaration = Expand( d, "DYNAMIC   @PADDED_C_RETURN_TYPE FUNCTION( @C_NAME )( @C_DEFINE_ARGS );" )
		generated[ release_c_header ][ 'DECLARATIONS' ] += cDeclaration + '\n'
			
		###

		cDefinition = Expand( d, "DEFINE_DYNAMIC( @C_RETURN_TYPE, @C_NAME, ( @C_DEFINE_ARGS ) );" )
		cLoader     = Expand( d,   "LOAD_DYNAMIC( @C_RETURN_TYPE, @C_NAME, ( @C_DEFINE_ARGS ) );" )
		generated[ porch ][ 'DEFINITIONS' ] += cDefinition + '\n'
		generated[ porch ][ 'LOADERS'     ] += cLoader + '\n'

		if   d[ 'SPECIES' ] == 'constructor':
			cppWrapper = Expand( d, "@CPP_RETURN_TYPE @CPP_FULL_NAME( @CPP_DEFINE_ARGS_WITHOUT_DEFAULTS ) @CPP_QUALIFIERS { DO( mShadow = ( void * )@C_NAME( @C_PASS_ARGS ) ); }" )
		elif d[ 'SPECIES' ] == 'destructor':
			cppWrapper = Expand( d, "@CPP_RETURN_TYPE @CPP_FULL_NAME( @CPP_DEFINE_ARGS_WITHOUT_DEFAULTS ) @CPP_QUALIFIERS {     @C_NAME( @C_PASS_ARGS );   }" ) # because destructors shouldn't throw exceptions
		elif d[ 'CPP_RETURN_TYPE' ] in [ '', 'void' ]:
			cppWrapper = Expand( d, "@CPP_RETURN_TYPE @CPP_FULL_NAME( @CPP_DEFINE_ARGS_WITHOUT_DEFAULTS ) @CPP_QUALIFIERS { DO( @C_NAME( @C_PASS_ARGS ) ); }" )
		else:
			cppWrapper = Expand( d, "@CPP_RETURN_TYPE @CPP_FULL_NAME( @CPP_DEFINE_ARGS_WITHOUT_DEFAULTS ) @CPP_QUALIFIERS { @PORCH_EXECUTION_MACRO( @CPP_RETURN_TYPE, @C_NAME( @C_PASS_ARGS ) ); }" )
		generated[ porch ][ 'CPP_DEFINITIONS' ] += cppWrapper + '\n'

		defaults = ', '.join( arg[ 'NAME' ] + '=' + dict( true='True', false='False', null='None', nullptr='None' ).get( arg[ 'DEFAULT' ].lower(), arg[ 'DEFAULT' ] ) for arg in d[ 'ARGUMENTS' ] if arg[ 'DEFAULT' ] )
		pythonDefinition = Expand( d, "_Define( className='@PYTHON_CLASS', pyName='@PYTHON_NAME', cReturnType='@C_RETURN_TYPE', cName='@C_NAME', cArgString='@C_DEFINE_ARGS', defaults=dict(%s) )" % defaults )
		generated[ python ][ 'PYTHON_DEFINITIONS' ] += pythonDefinition + '\n'
	
	#print( Report( definitions ) )

	templatesDir = os.path.realpath( os.path.join( ROOT, 'devel', 'src', 'templates' ) )
	for fileName, fileContent in sorted( generated.items() ):
		fileName = Expand( SUBSTITUTIONS, fileName )
		destinationFile = os.path.realpath( os.path.join( ROOT, fileName ) )
		
		if 0:
			print( '\n\n\n// %s\n' % fileName )
			for sectionName, sectionContent in fileContent.items():
				print( '// %s\n%s' % ( sectionName, sectionContent ) )
		
		if 'TEMPLATE' in fileContent: 
			templateFile = ''
			template = fileContent[ 'TEMPLATE' ]
		else:
			templateFile = os.path.realpath( os.path.join( templatesDir, fileName ) )
			templateMatches = [ os.path.realpath( os.path.join( templatesDir, partial ) ) for partial in FindFiles( templatesDir ) if Expand( SUBSTITUTIONS, partial ) == fileName ]
			if not templateMatches:
				print( 'failed to find ' + templateFile )
				continue
			if len( templateMatches ) > 1:
				raise RuntimeError( 'multiple templates match ' + fileName + ':' + ''.join( '\n   ' + x for x in templateMatches ) )
			templateFile = templateMatches[ 0 ]
			template = open( templateFile, 'rt' ).read()
			
		txt = Expand( fileContent, template )
		txt = Expand( SUBSTITUTIONS, txt )
		txt += '\n'
		previousText = open( destinationFile, 'rt' ).read() if os.path.isfile( destinationFile ) else None
		if txt == previousText:
			print( ' no change to ' + destinationFile )
		else:
			if templateFile: print( 'interpolating %s\n         from %s' % ( destinationFile, templateFile ) )
			else: print( '     creating ' + destinationFile )
			destinationDir = os.path.dirname( destinationFile )
			if not os.path.isdir( destinationDir ): os.makedirs( destinationDir )
			open( destinationFile, 'wt' ).write( txt )
	
	return locals()

if __name__ == '__main__':
	result = ParseHeaderFiles( *sys.argv[ 1: ] )
	locals().update( result )
