# TODO: why am I having to do this by hand?? 
#       `cmake -DCMAKE_TOOLCHAIN_FILE=blah` is supposed to take care of this automatically according to
#           https://cmake.org/cmake/help/latest/manual/cmake-toolchains.7.html#cross-compiling-for-linux
#           https://kubasejdak.com/how-to-cross-compile-for-embedded-with-cmake-like-a-champ
#       but my experience on cmake 3.16.3 has been that it doesn't happen unless you do it manually as
#       follows:

IF( NOT "${CMAKE_TOOLCHAIN_FILE}" STREQUAL "" ) 
	MESSAGE( "Including toolchain file '" ${CMAKE_TOOLCHAIN_FILE} "'" )
	INCLUDE( ${CMAKE_TOOLCHAIN_FILE} )
	SET( CMAKE_CROSSCOMPILING TRUE )                                # TODO: doesn't seem to be respected
ENDIF()

