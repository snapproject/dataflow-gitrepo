import os
import sys
import time
import random
import numbers
import inspect
import argparse
import unittest
import DataFlow as df

"""
DataFlowTesting.py contains functions for randomization and 
the Main() function that initializes random-seed logic
"""

# generates a list of all ASCII characters except for '$'
DEFAULT_CHARACTER_POOL = [ chr( x ) for x in range( 33, 127 ) ]

# generates a list of all lowercase, uppercase, and digital characters
FILENAME_CHARACTER_POOL = 'abcdefghijklmopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_'
# NB: do not include both '?' and '!' in this list, otherwise randomly
# generated archive and/or file names may sometimes contain '?!' which
# is used in Summary() strings to mark assertion failures in the C++ code

# generates 10 character string from a random pool
def RandomContent( nCharacters=10, pool=DEFAULT_CHARACTER_POOL, raw=False):
	x = ''.join( random.choice( pool ) for i in range( nCharacters ) )
	return df._IfStringThenRawString( x ) if raw else x

# name of file is random (but without $ prefix, so its type is $f by default)
def RandomFilePath( maxLengthOfEachLevel=10, maxNumberOfLevels=7 ):
	return '/'.join( RandomContent(random.randint(1,maxLengthOfEachLevel), FILENAME_CHARACTER_POOL) for i in range(random.randint(1, maxNumberOfLevels)))

def RandomArchiveName( maxLength=10 ):
	return RandomFilePath(maxLengthOfEachLevel=maxLength, maxNumberOfLevels=1 )

# used to generate random size of SDAs and files
def RandomSDASize():
	return random.choice(range(4098, 70000))

# Returns a small SDA with a number of files that have been written to
def GenerateSmallSDA(totalFiles=10, empty=False, payloadSize=10, sdaSize=RandomSDASize()):
	testSDA = df.SharedDataArchive('ns', sdaSize, numberOfHashTableSlots=11)
	if (not empty): # write content to the files
		for iFile in range(totalFiles):
			try:
				testSDA.Open(RandomFilePath(), 'o', 1).Write(RandomContent(payloadSize))
			except:
				if payloadSize <= 1: break
				payloadSize //= 10
	else:
		for iFile in range(totalFiles):
			try:
				testSDA.Open(RandomFilePath(), 'o', 1)
			except:
				break
	return testSDA

RANDOMSEED = None
def SetRandomSeed( seed=None ):
	if seed is None: seed = int( time.time() )
	random.seed( seed )
	try: import numpy
	except: pass
	else: numpy.random.seed( seed )
	global RANDOMSEED
	if RANDOMSEED != seed:
		sys.stderr.write('--seed=%r\n' % seed )
		RANDOMSEED = seed

OPTS = argparse.Namespace()
def ProcessCommandLine( argv, description=None ):
	parser = argparse.ArgumentParser( prog=argv[ 0 ], description=description, formatter_class=argparse.RawDescriptionHelpFormatter )
	parser.add_argument( '-s', '--seed',       metavar='RANDOMSEED',  action='store', default=None, type=int, help='random seed' )
	parser.add_argument( '-d', '--debug',      metavar='DEBUG_LEVEL', action='store', default=0,    type=int, help='verbosity level for DataFlow.SetDebugLevel()' )
	parser.add_argument( '-v', '--verbosity',  metavar='VERBOSITY',   action='store', default=2,    type=int, help='verbosity level for unittest.main()' )
	parser.add_argument( '-f', '--fragmented',                        action='store_true', default=False,     help='initialize SDAs in fragmented mode' )
	#opts = parser.parse_args( args=None ) # args=sys.argv[ 1: ]
	opts, argv[ 1: ] = parser.parse_known_args( args=argv[ 1: ] )  # remaining positional args should be of unittest's standard form TestClass.testMethod
	SetRandomSeed( opts.seed )
	df.SetDebugLevel( opts.debug )
	global OPTS; OPTS = opts

def Main( **kwargs ):
	testClasses = kwargs.pop( 'testClasses', None )
	if not testClasses: testClasses = []
	elif not isinstance( testClasses, ( tuple, list ) ): testClasses = [ testClasses ]
	tests = '\n\n'.join( '\n'.join( '    %s.%s' % ( testClass.__name__, name ) for name, attr in testClass.__dict__.items() if callable( attr ) and name.startswith( 'test' ) ) for testClass in testClasses )
	description = kwargs.pop( 'description', None )
	if not description: description = ''
	if tests:
		if description: description = description.rstrip() + '\n\n'
		description += 'Optionally limit tests by specifying them by name on the command line:\n\n' + tests
	ProcessCommandLine( sys.argv, description=description )
	kwargs.setdefault( 'verbosity', getattr( OPTS, 'verbosity', 2 ) )
	kwargs.setdefault( 'exit', False )
	df.CheckRevision()
	unittest.main( **kwargs )
	print('{\n%s}' % ''.join('    %r : %r,\n' % item for item in TestingConditions().items()))

def TestingConditions():
	return {
		'seed': RANDOMSEED,
		'fragmented': OPTS.fragmented,
		'dataflow-library': df.GetRevision(),
		'dataflow-gitrepo': df._GetRevisionViaGit() if callable( getattr( df, '_GetRevisionViaGit', None ) ) else '???',
		'sys': sys.version + ' ' + sys.platform,
	}

class FileTestCase( unittest.TestCase ):

	def isEmpty(self, sda):
		return len(sda.AllFileNames()) == 0

	def assertEmpty(self, sda):
		self.assertEqual(sda.AllFileNames(includeDirectories=True), ['$d'])
	
	def clearSDA(self, sda):
		"""
		Ensure that all files have been removed from the SDA
		"""
		for fileName in sda.AllFileNames():
			self.removeFile(sda, fileName)
	
	def expectedContent( self, sda ):
		if not hasattr( self, '_expected' ): self._expected = {}
		return self._expected.setdefault( id( sda ), {} )
			
	def writeExpectedContent(self, sda, fileName, mode, fileContent, debug=False):
		"""
		Creates an entry for the dictionary content, where each key
		is is a file name, and each value is the associated payload

		Each SDA will have an expectedContent dictionary
		
		`mode` may be 'w', or 'a' as in SharedDataArchive.File objects.
		To emulate 'o'-for-overwrite mode, pass an integer value for `mode`:
		that is interpreted as the position at which to start overwriting.
		If `fileContent` is None with an integer `mode`, the file is simply
		truncated at that position.

		If `fileContent` is None in other modes ('w' or 'a'), the associated
		entry is removed.
		"""
		expectedContent = self.expectedContent(sda)
		try: fileName = sda.Open(fileName).GetName() # canonicalize if possible
		except df.FileNotFoundError as err: 
			# print("writeExpectedContent Error")
			# print(err)
			pass
		key = fileName 
		if fileContent is None:
			if isinstance(mode, numbers.Integral): expectedContent[key] = expectedContent[key][:mode]; return # emulates file truncation
			else: expectedContent.pop(key); return # emulates file removal
		if key not in expectedContent.keys() or mode == 'w': expectedContent[key] = df._IfStringThenRawString('')
		fileContent = df._IfStringThenRawString(fileContent)
		if isinstance(mode, numbers.Integral):
			overwritePosition = mode
			expectedContent[key] = expectedContent[key][:overwritePosition] + fileContent + expectedContent[key][overwritePosition+len(fileContent):]
		else:
			expectedContent[key] += fileContent


	def verifyIntegrity(self, sda):
		"""
		Ensure that the contents of the files in the SDA lines up with expected 
		content saved in the expectedContent dictionary
		"""
		expectedContent = self.expectedContent(sda)
		expectedFileNames = [ fileName for fileName in expectedContent.keys() ]
		fileNames = sda.AllFileNames()
		self.assertEqual(sorted(fileNames), sorted(expectedFileNames))
		for fileName in fileNames:
			f = sda.Open( fileName, 'r' )
			availableContentLength = f.AvailableContentLength()
			tellMin, tellMax = f.TellMin(), f.TellMax()
			content = f.Read()
			del f
			self.assertEqual( len( content ), availableContentLength )
			self.assertEqual( len( content ), tellMax - tellMin )
			self.assertEqual( content, expectedContent[ fileName ][ tellMin: ] )
		# print('verified content of %d files' % len(fileNames))


	def fillSDA(self, sda):
		"""
		completely fill an SDA with as many empty files as possible
		"""
		while (True): 
			fileName = RandomFilePath()
			payloadSize = random.randint(1, 256)
			try:
				sda.Open(fileName, 'o', payloadSize)
			except df.DFException as err:
				decreasedPayloadSize = payloadSize // 10
				try: sda.Open(fileName, 'o', decreasedPayloadSize)
				except: break
			self.writeExpectedContent(sda, fileName, 'w', '')

	"""
	The following set of functions are called in randomFileOperations
	"""

	def createFile(self, sda):
		fileName = RandomFilePath()
		filePayload = RandomContent(random.randint(1, 1000), raw=True)
		try:
			sda.Open(fileName, 'w', len(filePayload)).Write(filePayload)
		except df.DFException as err:
			# print(err)
			return False
		self.writeExpectedContent(sda, fileName, 'w', filePayload) 
		return True

	def appendFile(self, sda, fileName):
		toAppend = RandomContent(random.randint(1,1000), raw=True)
		file = sda.Open(fileName, 'a')
		try:
			file.Write(toAppend) 
		except df.DFException as err:
			# nothing to do: as of the ring2 branch, failure to clear enough space for the whole Write() means no data at all will be written
			return False
		else:
			self.writeExpectedContent(sda, fileName, 'a', toAppend) 
			return True
		
	def editFile(self, sda, fileName, minReplacementLength=1, maxReplacementLength=1000, truncate=None):
		toAdd = RandomContent(random.randint(minReplacementLength, maxReplacementLength), raw=True)
		file = sda.Open(fileName, 'o') 
		fileIndex = random.randint(file.TellMin(), file.TellMax())
		file.Seek(fileIndex)
		if truncate is None: truncate = random.randint(0,1)
		if truncate:
			file.Truncate()
			self.writeExpectedContent(sda, fileName, fileIndex, None)
		try:
			file.Write(toAdd) 
		except df.DFException as err:
			# nothing to do: as of the ring2 branch, failure to clear enough space for the whole Write() means no data at all will be written
			return False
		else:
			self.writeExpectedContent(sda, fileName, fileIndex, toAdd)
			return True
			
	def truncateFile(self, sda, fileName):
		return self.editFile(sda, fileName, minReplacementLength=0, maxReplacementLength=0, truncate=True)

	def shrinkFile(self, sda, fileName):
		file = sda.Open(fileName, 'o')
		before = file.PayloadCapacity()
		try:
			file.Shrink()
		except df.DFException as err:
			# print(err)
			return False
		after = file.PayloadCapacity()
		return [ before - after ] # considered truthy because it's a non-empty list; value inside may be 0 (there may easily have been no shrinkage)

	def removeFile(self, sda, fileName):
		try:
			self.writeExpectedContent(sda, fileName, 'a', None)
			sda.Open(fileName, 'o').Remove()
		except df.DFException as err:
			print(err)
			return False
		return True

	# --------------------------- FRAGMENTEDSDA --------------------------------

	def initializeFragmentedSDA(self, sda, empty=False):
		"""
		A fragmented SDA contains vacated lots and random file operations have 
		been performed on them. A fragmented SDA can be full or empty. When an 
		SDA is full, it will contains files have all been written to. 
		"""
		newFilePath = RandomFilePath()
		try:sda.Open(newFilePath, 'w', 1).Write('')
		except: print(sda.Summary()); raise
		self.writeExpectedContent(sda, newFilePath, 'w', '')

		# randomly initialize new files or append to exisiting files until the
		# SDA is full, then break
		while (True): 
			self.verifyIntegrity(sda)
			command = random.choice(['create', 'append'])
		
			if command == 'create':
				fileName = RandomFilePath()
				createdFile = self.createFile(sda)
				if (createdFile == False):
					break

			elif command == 'append':
				fileName = random.choice(sda.AllFileNames())
				appendedFile = self.appendFile(sda, fileName)
				if (appendedFile == False):
					break

		filesAdded = sda.AllFileNames()
		toRemove = random.randint(1, len(filesAdded)-1)
		for iFiles in range(toRemove):
			fileName = random.choice(filesAdded)
			self.removeFile(sda, fileName)
			filesAdded.remove(fileName)

		# add a random number of files to the SA
		for iFiles in range(toRemove//2):
			self.createFile(sda)

		self.randomFileOperations(sda, empty)

		if (empty):
			self.clearSDA(sda)
			

	def randomFileOperations(self, sda, empty=False):
		"""
		Operations are selected from a list at random and performed 
		on the contents of the SDA. This function is meant to fragment
		the files in a given SDA. If a file is meant to be empty, the
		'remove' operation will be added to the list so that files can 
		be taken out at random. 
		postFragmentationCommands is a dictionary that tracks that number 
		of time each command was run. 
		"""
		self.verifyIntegrity(sda)
		fileCommands = [ 'create', 'append', 'edit', 'truncate', 'shrink' ]
		self.postFragmentationCommands = {}

		if (empty==True):
			fileCommands.append('remove')

		while True:
			if (self.isEmpty(sda)):
				break

			command = random.choice(fileCommands)  
			
			commandSuccess = True
			if (command == 'create'):
				commandSuccess = self.createFile(sda)
			
			allFiles = sda.AllFileNames()
			fileName = random.choice(allFiles)

			if (command == 'append'):
				commandSuccess = self.appendFile(sda, fileName)
			
			elif (command == 'edit'):
				if (sda.Open(fileName).AvailableContentLength() == 0):
					continue
				commandSuccess = self.editFile(sda, fileName)

			elif (command == 'truncate'):
				if (sda.Open(fileName).AvailableContentLength() == 0):
					continue
				commandSuccess = self.truncateFile(sda, fileName)
				
			elif (command == 'shrink'):
				commandSuccess = self.shrinkFile(sda, fileName)

			elif (command == 'remove'):
				commandSuccess = self.removeFile(sda, fileName)
			
			if not commandSuccess:
				break
				
			if command == 'shrink':
				commandSuccess = commandSuccess[ 0 ] # shrinkage is only recorded as a success if non-zero amount of capacity was recovered. but a "failure" in this sense should not be fatal
			if commandSuccess:
				self.postFragmentationCommands[ command ] = self.postFragmentationCommands.get( command, 0 ) + 1

		allFiles = sda.AllFileNames()
		if empty:
			# delete all files until sda is empty
			for fileName in allFiles:
				self.removeFile(sda, fileName)
		# print(self.postFragmentationCommands)
				
	# -----------------------------------------------------------------------------

def fakeSubTest( self, *pargs, **kwargs ):
	class UselessContextManager:
		def __enter__( self ): pass
		def __exit__( self, *pargs ): pass
	return UselessContextManager()
if not hasattr( FileTestCase, 'subTest' ): FileTestCase.subTest = fakeSubTest


def db( *pargs, **kwargs ):
	msg = '\n'.join( [ str( x ) for x in pargs ] + [ '%s = %r' % item for item in kwargs.items() ] )
	if '\n' in msg: msg = '\n  ' + msg.replace( '\n', '\n  ' )
	caller = inspect.getframeinfo( inspect.stack()[ 1 ][ 0 ] )
	sys.stderr.write( "\n%s:%d:    %s\n" % ( caller.filename, caller.lineno, msg ) )
	try: sys.stderr.flush()
	except: pass

