#!/usr/bin/env python
__doc__ = """
Run this file to perform all tests (or, optionally, just the
specific tests you list on the command-line).

To drill down on one test interactively (let's say
`testVacatedLots()` fails when it is specified as the
sole test with `--seed=1616417943` and no fragmentation)::

	from TestFiles import df, dft, TestFiles
	
	dft.SetRandomSeed( 1616417943 ) # ...or whichever settings led
	dft.OPTS.fragmented = False     # to the failure originally
	
	t = TestFiles() # make instance of unittest.TestCase subclass
	t.setUp()       # the unittest framework would normally do this
	
	t.testVacatedLots()
	
	# t.tearDown()  # the unittest framework would normally do this
	
"""
import random
import DataFlow as df
import DataFlowTesting as dft

class TestFiles(dft.FileTestCase):

	def setUp(self):
		"""
		Called immediately before the test method + intiates two SDAs
		 * testSDA is an empty SDA that can be filled later
		 * fullSDA is filled with as many files as possible
		   - if not framented, fullSDA is filled with empty files
		   - if fragmented, fullSDA is filled with written files
		"""

		dft.SetRandomSeed( dft.RANDOMSEED )
		self.testSDA = df.SharedDataArchive('testSDA', 25600, forceReformat=True, numberOfHashTableSlots=11)
		self.fullSDA = df.SharedDataArchive('fullSDA', 25600, forceReformat=True, numberOfHashTableSlots=11)
		if getattr( dft.OPTS, 'fragmented', False ):
			self.initializeFragmentedSDA(self.testSDA, empty=True)
			self.initializeFragmentedSDA(self.fullSDA, empty=False)
		else:
			self.fillSDA(self.fullSDA)

		self.assertEmpty(self.testSDA)


	def tearDown(self):
		"""
		Called immediately after the execution of each test case. 
		Asserts testSDA and fullSDA are valid SDAs before deconstruction
		Will only be called if the setUp() succeeds, regardless of the 
		outcome of the test method
		"""
		self.assertTrue('?!' not in self.testSDA.Summary())
		self.assertTrue('?!' not in self.fullSDA.Summary())


	# ----------------------- SHARED DATA ARCHIVE -----------------------------
	def testInitializingSDAs(self):
		"""
		Initialize SDAs with random names
		"""
		randomNames = [ dft.RandomFilePath() for f in range(10) ]
		for name in randomNames:
			with self.subTest(name):
				testSDA = df.SharedDataArchive(dft.RandomArchiveName(), dft.RandomSDASize(), forceReformat=True, numberOfHashTableSlots=11)
				if '?!' in testSDA.Summary(): print(testSDA.Summary())
				self.assertTrue('?!' not in testSDA.Summary())

	
	def testNonAlphaNumericName(self):
		"""
		Initialize SDAs with non-alphabetic names
		"""
		nonAlpha = '@#$%^&*()'
		for n in nonAlpha:
			with self.subTest(n=n):
				testSDA = df.SharedDataArchive(n, 70000, forceReformat=True, numberOfHashTableSlots=11)
				self.assertTrue('?!' not in testSDA.Summary())

	
	def testInitializeMultipleSDAs(self):
		"""
		Initialize multiple SDAs without deleting lock
		"""
		allSDAs = []
		for iSDA in range(30):
			sdaName = dft.RandomArchiveName()
			with self.subTest(sdaName):
				allSDAs.append(df.SharedDataArchive(sdaName, dft.RandomSDASize(), forceReformat=True, numberOfHashTableSlots=11))
		self.assertTrue(len(allSDAs), 30)
	

	def testWritetoTwoSDAs(self):
		"""
		Write to more than one instance pointing to the same SDA,
		to verify that ReadWriteMutex plays nice
		"""
		s1 = df.SharedDataArchive(dft.RandomArchiveName(), dft.RandomSDASize(), forceReformat=True, numberOfHashTableSlots=11)
		s2 = df.SharedDataArchive(s1.GetName())
		self.createFile(s1)
		self.createFile(s2)

		d1 = self.expectedContent(s1)
		d2 = self.expectedContent(s2)
		d1.update(d2)
		d2.update(d1)
		self.verifyIntegrity(s1)
		self.verifyIntegrity(s2)

	
	def testFillLargeSDAwithOneEmptyFile(self):
		"""
		Create an SDA that is one file, until the SDA is entirely filled by this
		file, start with an arbitrarily large capacity and determine how large it
		can possibly get
		"""
		testSDA = df.SharedDataArchive('ns', 2560000, forceReformat=True, numberOfHashTableSlots=11)
		capacity = random.randint(7000, 20000)
		capacityIncrement = 1000
		filePath = dft.RandomFilePath()
		while (True):
			try:
				testSDA.Open(filePath, 'w', capacity)
			except df.DFException as err:
				if capacityIncrement > 1:
					capacity -= capacityIncrement
					capacityIncrement //= 10
					capacity += capacityIncrement
				else: break
			capacity += capacityIncrement

		totalFiles = len(testSDA.AllFileNames())
		self.assertEqual(totalFiles, 1)
		self.assertLess(testSDA.GetSpaceAvailable(0), 256)

				
	# -------------------------- BASIC FILE TESTS -----------------------------
	def testIntegrity(self):
		self.verifyIntegrity(self.testSDA)
		self.verifyIntegrity(self.fullSDA)

	def testDefaultSDAsExist(self):
		"""
		Ensures that testSDA and fullSDA have been created
		"""
		self.assertTrue(self.fullSDA)
		self.assertTrue(self.testSDA)


	def testCreateEmptyFile(self):
		"""
		Tests whether a single empty file can be created in testSDA
		and if that file has correct amount of parameters
		"""
		fileName = dft.RandomFilePath()
		self.testSDA.Open(fileName, 'o', 1)
		self.writeExpectedContent(self.testSDA, fileName, 'w', '')
		self.verifyIntegrity(self.testSDA)


	# ------------------------------ WRITE() ---------------------------------
	def testOverwiteAllFiles(self):
		"""
		Open and write small content to all files in fullSDA
		"""
		vacatedInitial = self.fullSDA.GetSpaceAvailable(True) - self.fullSDA.GetSpaceAvailable(False)
		for file in self.fullSDA.Open('$d', 'o').Walk():
			with self.subTest(file=file.GetName()):
				previousSize = file.PayloadCapacityUsed()
				payload = dft.RandomContent(nCharacters=previousSize // 2, raw=True)
				file.Truncate().Write(payload)
				self.writeExpectedContent(self.fullSDA, file.GetName(), 'w', payload)
		vacatedFinal = self.fullSDA.GetSpaceAvailable(True) - self.fullSDA.GetSpaceAvailable(False)
		self.assertLessEqual(vacatedInitial, vacatedFinal)
		self.verifyIntegrity(self.fullSDA)
	

	def testWriteLargePayload(self):
		"""
		Initialize one file and write a large amounts of text in one line (i.e. >5000 chars)
		"""
		fileName = dft.RandomFilePath()
		content = dft.RandomContent(random.randint(5000, 20000), raw=True) # NB: hardcoded relationship between this and the size of the empty SDA (segment size 25600). We're allowing enough slack here for a few LotHeaders, which will be used if we're writing this in an empty-but-fragmented SDA.
		self.testSDA.Open(fileName, 'w', 1).Write(content)
		self.writeExpectedContent(self.testSDA, fileName, 'w', content)
		self.verifyIntegrity(self.testSDA)


	# ------------------------------ APPEND() -------------------------------
	def testAppendSmallText(self):
		"""
		Initialize one file with a payload of 10 bytes, and append 10 bytes of
		content until the SDA has been completely filled  
		"""
		content = dft.RandomContent(10, raw=True)
		self.testSDA.Open('nf', 'w', 1).Write(content) 
		self.writeExpectedContent(self.testSDA, 'nf', 'w', content)
		while (True):
			file = self.testSDA.Open('nf', 'a')
			content = dft.RandomContent(10, raw=True)
			sizeBefore = file.PayloadCapacityUsed()
			try:
				file.Write(content)
			except df.DFException as err:
				bytesWritten = file.PayloadCapacityUsed() - sizeBefore # assumes non-wrapped
				break
			else:
				bytesWritten = len( content )
			finally:
				self.writeExpectedContent(self.testSDA, 'nf', 'a', content[:bytesWritten])
		self.assertLess(self.testSDA.GetSpaceAvailable(includeVacatedLots=True), 256)
		self.verifyIntegrity(self.testSDA)


	def testAppendLargeText(self):
		"""
		Initialize one file in testSDA and write a large amount to the file. 
		As there is less space available, smaller amounts are added to the file 
		until the SDA is completely full.
		"""
		payloadSizeIncrement = 1000
		self.testSDA.Open('n','w', 1)
		self.writeExpectedContent(self.testSDA, 'n', 'w', '')
		content = ''
		while (True):
			file = self.testSDA.Open('n', 'a')
			content = dft.RandomContent(payloadSizeIncrement, raw=True)
			sizeBefore = file.PayloadCapacityUsed()
			try:
				file.Write(content)
			except df.DFException as err:
				bytesWritten = file.PayloadCapacityUsed() - sizeBefore # assumes non-wrapped
				payloadSizeIncrement //= 10
				if payloadSizeIncrement < 1: break
			else:
				bytesWritten = len( content )
			finally:
				self.writeExpectedContent(self.testSDA, 'n', 'a', content[:bytesWritten])
		self.verifyIntegrity(self.testSDA)
		self.assertLess(self.testSDA.GetSpaceAvailable(includeVacatedLots=False), 256)


	# --------------------------- REMOVE() ----------------------------
	def testCreateAndRemoveOneFile(self):
		"""
		Initialize and remove one file in one line
		"""
		initialAvailable = self.testSDA.GetSpaceAvailable(includeVacatedLots=True)
		self.testSDA.Open('nf', 'w', 1).Remove()
		currentlyAvailable = self.testSDA.GetSpaceAvailable(includeVacatedLots=True)
		self.assertEqual(initialAvailable, currentlyAvailable) 


	def testRemoveRandomFiles(self):
		"""
		Create a list of all file names in fullSDA, then randomly select files 
		to remove until the SDA is empty
		"""
		fileNames = self.fullSDA.AllFileNames()
		while fileNames:
			currentFile = random.choice(fileNames)
			self.removeFile(self.fullSDA, currentFile)
			fileNames.remove(currentFile)
		self.assertEmpty(self.fullSDA)


	def testRemoveAllFiles(self):
		"""
		Remove all files from fullSDA
		"""
		initialAvailable = self.fullSDA.GetSpaceAvailable(includeVacatedLots=True)
		fileNames = self.fullSDA.AllFileNames()
		self.assertGreater( len(fileNames), 0 )
		for fileName in fileNames:
			self.fullSDA.Open(fileName, 'o').Remove()
		fileNames = self.fullSDA.AllFileNames()
		self.assertEqual( len(fileNames), 0 )
		finalAvailable = self.fullSDA.GetSpaceAvailable(includeVacatedLots=True)
		self.assertLess(initialAvailable, finalAvailable)


	def testVacatedLots(self):
		"""
		Create vacated lots by removing a set of random files from fullSDA and 
		create a new file, and append to the file until the SDA is full. 
		The content of the SDA will be verified and there should be vacated lots 
		present in fullSDA.
		"""
		# remove a random number of files from the SDA
		allFileNames = self.fullSDA.AllFileNames()

		numRemoved = random.randint(1, len(allFileNames))
		for file in range(numRemoved):
			fileName = random.choice(allFileNames)
			self.removeFile(self.fullSDA, fileName)
			allFileNames.remove(fileName)
		vacatedSpaceBefore = self.fullSDA.GetSpaceAvailable(includeVacatedLots=True) - self.fullSDA.GetSpaceAvailable(includeVacatedLots=False)
		
		payload = dft.RandomContent(0, raw=True)
		payloadSize = 0
		payloadSizeIncrement = 1000
		while True:
			newFileName = dft.RandomFilePath(maxNumberOfLevels=1)
			# create the new file in the root directory so that we
			# can be sure we only need to create the file (not the
			# file AND its directory ancestry). We know at least one
			# Lot will have been vacated (so ONE file should definitely
			# be able to be created) but we can't guarantee more than that.
			if newFileName not in allFileNames: break
		try:
			self.fullSDA.Open(newFileName, 'w', 1)
		except df.DFException as err:
			print('FAILED to create new file %r after removing %r' % (newFileName, numRemoved))
		#completeContent = '\n'.join( '%04d'%i for i in range( 10000 ) ).encode( 'utf8' )
		while True:
			toAppend = dft.RandomContent(payloadSizeIncrement, raw=True)
			#toAppend = completeContent[ payloadSize : payloadSize + payloadSizeIncrement ]
			fileHandle = self.fullSDA.Open(newFileName, 'a')
			#with open('before_%s' % payloadSize, 'wt') as fh: fh.write(str(fileHandle))
			try:
				fileHandle.Write(toAppend)
			except df.DFException as err:
				# print(err)
				if payloadSizeIncrement <= 1: break
				payloadSizeIncrement //= 10
			else:
				self.writeExpectedContent(self.fullSDA, newFileName, 'a', toAppend)
				payloadSize += payloadSizeIncrement
				payload += toAppend
			del fileHandle
		vacatedSpaceAfter = self.fullSDA.GetSpaceAvailable(includeVacatedLots=True) - self.fullSDA.GetSpaceAvailable(includeVacatedLots=False)
		self.assertGreaterEqual(vacatedSpaceBefore, 0)
		self.assertEqual(vacatedSpaceAfter, 0)
		self.verifyIntegrity(self.fullSDA)


	# ---------------------- TRUNCATE() & SHRINK() ---------------------------
	# Truncate() cuts the payload from the current position onward, and it 
	# shouldn't effect the underyling infrastructure of the file, while
	# Shrink() does not affect the payload, but rather reclaims *unoccupied*
	# space in a file

	def testTruncateSmallEmptyFile(self, targetCapacity=1 ):
		"""
		Truncate a single, empty file
		"""
		f = self.testSDA.Open('nf', 'w', targetCapacity)
		usageBefore = f.PayloadCapacityUsed()
		capacityBefore = f.PayloadCapacity()
		del f
		f = self.testSDA.Open('nf', 'o').Truncate()
		usageAfter = f.PayloadCapacityUsed()
		capacityAfter = f.PayloadCapacity()
		del f
		self.assertGreaterEqual( capacityBefore, targetCapacity )
		self.assertEqual( capacityBefore, capacityAfter )
		self.assertEqual( usageBefore, 0 )
		self.assertEqual( usageAfter,  0 )
		
	
	def testTruncateLargeEmptyFile(self):
		"""
		Truncate a single, larger empty file
		"""
		self.testTruncateSmallEmptyFile( targetCapacity=10000 )


	def testTruncateRandomFiles(self):
		"""
		Truncate a random number of files from fullSDA if the files are not 
		already empty
		"""
		numFiles = random.randint( 1, len(self.fullSDA.AllFileNames()) )
		for iFiles in range(numFiles):
			fileName = random.choice(self.fullSDA.AllFileNames())
			f = self.fullSDA.Open( fileName, 'o' )
			position = random.randint( f.TellMin(), f.TellMax() )
			f.Seek( position ).Truncate()
			self.writeExpectedContent(self.fullSDA, fileName, position, None)
			del f
		self.verifyIntegrity(self.fullSDA)


	def testShrinkAllFiles(self):
		"""
		Test whether all files can be shrunk, see that files have been 
		properly shrunk by asserting that content has remained the same
		"""
		availableBefore = self.fullSDA.GetSpaceAvailable(includeVacatedLots=True)
		for file in self.fullSDA.Open('$d', 'o').Walk():
			with self.subTest(file=file.GetName()):
				originalPayload = file.Read()
				file.Shrink()
				currentPayload = file.Seek(0).Read()
				self.assertEqual(originalPayload, currentPayload)
		availableAfter = self.fullSDA.GetSpaceAvailable(includeVacatedLots=True)
		self.assertGreaterEqual(availableAfter, availableBefore)


	def testTruncateAndShrinkSmallFiles(self):
		"""
		Truncating and shrinking files that are already minimum-size (256 bytes)
		should have no effect. A single 512-byte file, when truncated
		and shrunk, should allow us to recoup 256 bytes.
		"""
		fileNames = list( dft.FILENAME_CHARACTER_POOL ) # all possible smallest-possible file paths
		for fileName in fileNames:
			self.testSDA.Open(fileName, 'w', 1) 
		available0 = self.testSDA.GetSpaceAvailable(includeVacatedLots=True)
		for fileName in fileNames:
			self.testSDA.Open(fileName, 'o').Shrink() 
		available1 = self.testSDA.GetSpaceAvailable(includeVacatedLots=True)
		self.assertEqual(available0,available1)
		
		# add single 512 byte Lot file
		self.testSDA.Open('nf', 'w', 256).Write(dft.RandomContent(256, raw=True))
		self.testSDA.Open('nf', 'o').Truncate()
		available2 = self.testSDA.GetSpaceAvailable(includeVacatedLots=True)
		self.assertEqual(available2, available0 - 512)
		self.testSDA.Open('nf', 'o').Shrink()
		available3 = self.testSDA.GetSpaceAvailable(includeVacatedLots=True)
		self.assertEqual(available3, available0 - 256) # should have reclaimed half of the 512-byte Lot

		
	def testSuccessfulDeallocationShrink(self):
		"""
		Shrink a large file and determine whether it will produce vacated lots
		"""
		initialAvailable = self.testSDA.GetSpaceAvailable(includeVacatedLots=True)
		self.testSDA.Open('f1', 'w', 5000)
		content = dft.RandomContent(1024, raw=True)
		self.testSDA.Open('f1', 'w').Write(content)
		self.writeExpectedContent(self.testSDA, 'f1', 'w', content)
		self.testSDA.Open('f2', 'w', 1) # presence of this file ensures freed space will be shelved as vacated lots rather than deallocated 
		self.writeExpectedContent(self.testSDA, 'f2', 'w', '')
		availableBeforeShrink = self.testSDA.GetSpaceAvailable(includeVacatedLots=True)
		self.testSDA.Open('f1', 'o').Shrink()
		availableAfterShrink = self.testSDA.GetSpaceAvailable(includeVacatedLots=True)
		vacatedLots = availableAfterShrink - self.testSDA.GetSpaceAvailable(includeVacatedLots=False)
		self.assertGreater(initialAvailable, availableAfterShrink)
		self.assertGreater(availableAfterShrink, availableBeforeShrink)
		self.assertGreater(vacatedLots, 0)
		self.verifyIntegrity(self.testSDA)


	def testAppendUsingSeek(self):
		"""
		Seek and append to the end of a long file
		"""   
		# remove half of files to make space for more content to be written
		totalFiles = len(self.fullSDA.AllFileNames())
		for iFile in range(totalFiles//2):
			fileNames = self.fullSDA.AllFileNames()
			randomFile = random.choice(fileNames)
			self.removeFile(self.fullSDA, randomFile)

		fileNames = self.fullSDA.AllFileNames()
		randomFile = random.choice(fileNames)
		content = dft.RandomContent(10000, raw=True)
		initialPayload = self.fullSDA.Open(randomFile).PayloadCapacityUsed()
		backtrack = random.randint(0, initialPayload) # assumes non-wrapped
		self.fullSDA.Open(randomFile, 'a').Seek(-backtrack, 1).Write(content)
		self.writeExpectedContent(self.fullSDA, randomFile, initialPayload - backtrack, None)
		self.writeExpectedContent(self.fullSDA, randomFile, 'a', content)
		finalPayload = self.fullSDA.Open(randomFile).PayloadCapacityUsed()
		self.assertLess(initialPayload,finalPayload) # assumes non-wrapped
		self.verifyIntegrity(self.fullSDA)
		

	def testEditUsingSeek(self):
		"""
		Edit the payload of a file using Seek() and Truncate()
		"""
		originalContent = 'abcdXYZefg'
		s = self.testSDA
		s.Open('test', 'w', 1).Write(originalContent)
		f = s.Open('test', 'o').Seek(7)
		tail = f.Read()
		f.Seek(4).Truncate().Write(tail)
		editedContent = s.Open('test').Read().decode("utf-8") 
		self.assertEqual(editedContent, 'abcdefg')

	 
	def testRelativeSeeking(self):
		"""
		Seek() and Truncate() without hardcoding the length of the file's payload
		"""
		originalContent = 'abcdXYZefg'
		self.testSDA.Open('test', 'w',1).Write(originalContent) 
		f = self.testSDA.Open('test', 'o').Seek(4)
		secondPart = f.Read()
		f.Seek(-len(secondPart), 1).Truncate().Write(secondPart[3:])
		editedContent = self.testSDA.Open('test').Read().decode("utf-8")
		self.assertEqual(editedContent, 'abcdefg')

	
	def testOneCharacterSeek(self):
		"""
		Seek() when there is one character in the file
		"""
		f = self.testSDA.Open('test', 'o', 1)
		f.Write('e')
		firstCharacter = self.testSDA.Open('test','r').Seek(0).Read()
		lastCharacter = self.testSDA.Open('test','r').Seek(-1).Read()
		self.assertEqual(firstCharacter,lastCharacter)


if __name__ == '__main__':
	#df.SetDebugLevel(3)
	dft.Main( description=__doc__, testClasses=[ TestFiles ] )
