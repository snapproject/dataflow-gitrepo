#!/usr/bin/env python
# $BEGIN_DATAFLOW_LICENSE$
# 
# This file is part of the DataFlow project, a software library for
# efficient asynchronous multi-stream inter-process communication.
# 
# Copyright (c) 2014-2021 Jeremy Hill
# 
# DataFlow is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program. If not, see http://www.gnu.org/licenses/ .
# 
# Some third-party source code is compiled and linked into the
# DataFlow binary, as follows: devel/src/sem_timedwait_apple.cpp
# is adapted from sem_timedwait.cpp by Keith Shortridge, copyright
# (c) 2013 Australian Astronomical Observatory (AAO), released under
# the MIT License; devel/src/dirent_win.h is copyright (c) 2006
# Toni Ronkko, and released under the MIT License. The DataFlow build
# system also uses some third-party code:
# devel/build/cmake-recipes/DefineFunction_TARGET_ARCHITECTURE.cmake
# which is copyright (c) 2012 Petroules Corporation, released under
# the 2-Clause BSD License. For more details, see the respective
# copyright, licensing and disclaimer information in each of these
# files.
# 
# $END_DATAFLOW_LICENSE$

r"""
If you're a user and you're seeing this file, it's presumably because
you checked out a bleeding-edge copy of DataFlow in the form of a
git repository.  In that case, the best way to use this file is
to install the included working copy of the Python package,
`./release`, as an "editable" package::

    python -m pip install -e .

(where it is assumed that the current working directory `.` is the
directory that contains this `setup.py` file).


The other usage of this file is to package DataFlow up for release,
which only the DataFlow project maintainers will need to do. So, if
that's not you, you can stop reading now.  If it is you, then here's
how it works:

Your Python distro will need the following prerequisites installed::

    python -m pip install wheel    # only necessary for older Python distros
    python -m pip install twine

As always, make sure `python` invokes the correct Python! That also applies
during the following release procedure:

1. Verify/ensure that the version number in `release/DataFlow.py` is
   new, i.e. it exceeds the largest version number available on PyPI at
   https://pypi.org/project/dataflow-rt/#files
   To change it, you should change it at its source: `devel/src/DataFlow_API.hpp`
   in the comment /*API_VAR   API_VERSION      = x.y.z             */
   and then rebuild the library.

2. Commit everything to the `master` branch.

3. Update the content of the `release` branch::

       git checkout release
       git merge master -m "release X.Y.Z"
       git checkout master
       # do not post-bump the version numbers yet: setup.py will need them
   
4. Make and upload the release::

       python setup.py bdist_wheel --universal
       python -m twine upload dist/*
       rm -rf build dist python/dataflow_rt.egg-info
       
       # To test, using a distro that has a previous pip-installed version:
       python -m pip install --upgrade DataFlow --no-deps
   
5. As soon as the upload is done, maybe *already* bump the version number
   in `MASTER_META`, and commit those changes (but only to the `master`
   branch). The two advantages to that are (i) that any unreleased changes you
   now make will be associated with a version that is distinct from all pippable
   releases, and (ii) that you won't have to take any action in step 1 next time
   around.

6. Push everything to the server repo::

       git push --all --follow-tags

7. After a short while, check https://dataflow.readthedocs.io (for the release
   version) and https://dataflow.readthedocs.io/en/latest (for the master
   branch). Refresh both pages in the browser and verify the new version
   numbers in each.
"""
import os
import sys
import glob
import inspect
import setuptools

package_dir = 'release'

# import DataFlow itself - but make sure it's the to-be-installed version and not some legacy version hanging over
try: __file__
except: __file__ = inspect.getfile( inspect.currentframe() )
sys.path.insert( 0, os.path.join( os.path.dirname( __file__ ), package_dir ) )
import DataFlow as df
sys.path.pop( 0 )

meta = df.__meta__
#meta[ 'indent' ] = df.Meta.Indenter( meta ) # allows things like long_description='...\n* {indent[  citation]}\n...'.format( **meta )

# https://packaging.python.org/tutorials/distributing-packages/#setup-args
setup_args = dict(
	name = 'dataflow-rt',
	description = 'A package for flexible efficient multi-channel real-time data exchange.',
	long_description = """\
TODO
""".format( **meta ),
	long_description_content_type='text/x-rst',
	license = 'GPL v3+',
	url = meta.get( 'homepage', None ),
	author = meta.get( 'author', None ),
	author_email = meta.get( 'email', None ),
	version = meta[ 'version' ],
	python_requires = '>=2.7, !=3.0.*, !=3.1.*, !=3.2.*, <4',
	install_requires = [
		# 'numpy',   # maybe?
	],
	extras_require = { 
		#,
	},
	package_dir = {
		'' : package_dir,  # NB: can't be absolute, can't go upwards
	},
	py_modules = [ 'DataFlow' ],
	package_data = {
		'' : [  # should key be '' or 'DataFlow' ?
			'libDF-Darwin-64bit.dylib',  # TODO: doesn't get included.  Also, need other platforms
		]
	},
	include_package_data = True,
	#data_files = [], # NB: when pip-installed, items listed here get installed *outside* the package directory
	#**df.Manifest( 'setup' )
)

if __name__ == '__main__':
	if len( sys.argv ) < 2:
		subcmd = 'bdist_wheel --universal'
		sys.argv += subcmd.split()
		print( 'Assuming subcommand: ' + subcmd )
	setuptools.setup( **setup_args )
