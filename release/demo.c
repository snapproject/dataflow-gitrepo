/*
# $BEGIN_DATAFLOW_LICENSE$
# 
# This file is part of the DataFlow project, a software library for
# efficient asynchronous multi-stream inter-process communication.
# 
# Copyright (c) 2014-2021 Jeremy Hill
# 
# DataFlow is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program. If not, see http://www.gnu.org/licenses/ .
# 
# Some third-party source code is compiled and linked into the
# DataFlow binary, as follows: devel/src/sem_timedwait_apple.cpp
# is adapted from sem_timedwait.cpp by Keith Shortridge, copyright
# (c) 2013 Australian Astronomical Observatory (AAO), released under
# the MIT License; devel/src/dirent_win.h is copyright (c) 2006
# Toni Ronkko, and released under the MIT License. The DataFlow build
# system also uses some third-party code:
# devel/build/cmake-recipes/DefineFunction_TARGET_ARCHITECTURE.cmake
# which is copyright (c) 2012 Petroules Corporation, released under
# the 2-Clause BSD License. For more details, see the respective
# copyright, licensing and disclaimer information in each of these
# files.
# 
# $END_DATAFLOW_LICENSE$



This file illustrates some of the functionality of the DataFlow C/C++ API.
It is intended to be read as documentation, with examples. It can be compiled.


1. Getting started

To create a DataFlow application:

(1) Add the file DataFlow_API_Loader.c to your project (can be compiled as C or C++)

(2) #include "DataFlow.h" in your own C or C++ code

(3) Call Load_DF_API() in your code before calling any of the other functions.
    The argument to Load_DF_API may be a string that explicitly specifies the
    name of the dynamic library file to load. Alternatively, if you pass NULL
    then the default name will be used.  This default can be manipulated at
    compile time by defining the DF_PLATFORM symbol. For example, using gcc on
    Mac OSX on a 64-bit intel machine:

       gcc -DDF_PLATFORM=-Darwin-x86_64  demo.c DF_API_Loader.c

    will cause Load_DF_API(NULL) to look for "libDF-Darwin-x86_64.dylib"
    instead of just "libDF.dylib". As another example, if you're using Visual
    C++ to compile a 32-bit binary on a Windows machine, you could say:

       cl /DDF_PLATFORM=-Windows-i386  demo.c DataFlow_API_Loader.c

    which would cause Load_DF_API(NULL) to look for "libDF-Windows-i386.dll"
    instead of just "libDF.dll" .

(4) Check the return value of Load_DF_API before proceeding. It will be zero on
    success. Any non-zero value means the API failed to load and other API calls
    (function names starting with DF_) should not be called.


This file may be compiled. In it, we will build documented example applications
for .....
                                                                              */

#include "DataFlow.h"

#include <stdio.h>
#include <string.h>

    int Demo( int argc, const char * argv[] ); /* to be defined later */
    
    int main( int argc, const char * argv[] )
    {

        int i, argMover, autoSearch = 1;
        const char *dllname = NULL;
        /* NULL (or empty string) causes the loader to look for the dynamic 
           library under its default name (incorporating the DF_PLATFORM macro,
           if defined). Let's also allow the default to be overridden from the
           command line using the --library=/path/to/dynamic-library option:
        */
        for( i = 1; i < argc; i++ )
        {
            if( strncmp( argv[ i ], "--library=", 10 ) == 0 )
            {
                dllname = argv[ i ] + 10;
                for( argMover = i + 1; argMover < argc; argMover++ )
                    argv[ argMover - 1 ] = argv[ argMover ];
                i--; argc--; continue;
            }
            if( strncmp( argv[ i ], "--auto-search=", 14 ) == 0 )
            {
                const char *pref = argv[ i ] + 14;
                if( !strcmp( pref, "0" ) || !strcmp( pref, "false" ) || !strcmp( pref, "off" ) )
                    autoSearch = 0;
                for( argMover = i + 1; argMover < argc; argMover++ )
                    argv[ argMover - 1 ] = argv[ argMover ];
                i--; argc--; continue;
            }
        }
        
        if( Load_DF_API( dllname, autoSearch ) != 0 )
            return fprintf( stderr, "%s\n", Get_DF_Loader_Error() );
        
        return Demo( argc, argv );
    }
                                                                              /*

2. Handling API errors

There are two ways to handle errors in the DataFlow API. One is to use the C++
bindings of `DataFlow.hpp` which throw string exceptions when an API call fails.
The other is in plain C: whenever a DataFlow API call throws an error, it will
store a string pointer that contains the error message.  Both `DF_Error()` and
`DF_ClearError()` can be used to check for errors: they both return `NULL` if
no error message has been issued since the last call to `DF_ClearError()`.
If there has been an error, they return a `const char *`  to the error message.
Note that all `const char *`  string pointers returned by DataFlow API functions
like this are semi-persistent: only a finite number of strings will be stored
per thread. If you need to keep the string, allocate memory and copy it (the
easy way is to assign it to a `std::string` in C++) before calling any more
DataFlow API functions. The string storage is thread-specific and thread-safe,
(you will only be seeing errors from your own thread, this way).

You should check for errors after any DataFlow API call.  One way to do this is
illustrated in the following example, where a macro `CHECK` is defined to allow
easy return from `main()` or from any other function that announces failure
using a non-zero return value:
                                                                              */
    int CheckError( void )
    {
        if( DF_Error() ) return fprintf( stderr, "%s\n", DF_ClearError() );
        else return 0;
    }
    #define CHECK     if( CheckError() != 0 ) return -1;
                                                                              /*
We will use the `CHECK` macro to make our subsequent examples more readable, but
it is up to you to define it, or something similar, if you choose to handle
errors this way in your own code.

For most purposes, it will probably suffice that each thread only has access
to errors generated in the same thread. If the design requires more flexibility,
an alternative method is to register a `DF_MessageCallback` function using the
global function `DF_SetErrorCallback( func )`. This approach makes use of global
memory to store the function pointer---so separate threads should not attempt
to set *different* error callbacks---but the error string itself will not be
global when passed to the callback, and will not conflict with errors issued by
other threads. Your implementation of the callback can then implement mutexing
as necessary,  and redirect the message however is appropriate for your design.


3.  Application structure
                                                                              */
    int Demo( int argc, const char * argv[] )
    {
        DF_SanityCheck(); CHECK
        fprintf( stderr, "Loaded DataFlow API:\n" );
        fprintf( stderr, "        Version: %s\n", DF_GetVersion() );
        fprintf( stderr, "       Revision: %s\n", DF_GetRevision() );
        fprintf( stderr, "     Build Date: %s\n", DF_GetBuildDatestamp() );
        fprintf( stderr, "   Library File: %s\n", DF_LibraryPath( "", NULL ) );
        fprintf( stderr, "Executable File: %s\n", DF_ExecutablePath( "" ) );
        return 0;
    }
