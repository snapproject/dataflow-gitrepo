# TODO: Disclaimer
# 
# Copyright (c) @{YEARS} @{COPYRIGHT_HOLDER}


The DataFlow API dynamic libraries in this directory have version @{VERSION}.

To create a DataFlow application:

(1) Add the file DataFlow_API_Loader.c to your project (can be compiled as C or C++)

(2) #include "DataFlow.h" in your own C or C++ code

(3) Call Load_DF_API() in your code before calling any of the other functions.
    The argument to Load_DF_API may be a string that explicitly specifies the
    name of the dynamic library file to load. Alternatively, if you pass NULL
    then the default name will be used.  This default can be manipulated at
    compile time by defining the DF_PLATFORM symbol. For example, using gcc on
    Mac OSX on a 64-bit intel machine:
    
       gcc -DDF_PLATFORM=-Darwin-x86_64  demo.c DF_API_Loader.c
    
    will cause Load_DF_API(NULL) to look for "libDF-Darwin-x86_64.dylib"
    instead of just "libDF.dylib". As another example, if you're using Visual C++
    to compile a 32-bit binary on a Windows machine, you could say:
    
       cl /DDF_PLATFORM=-Windows-i386  demo.c DF_API_Loader.c
    
    which would cause Load_DF_API(NULL) to look for "libDF-Windows-i386.dll"
    instead of just "libDF.dll" .

(4) Check the return value of Load_DF_API before proceeding. It will be zero on
    success. Any non-zero value means the API failed to load and other API calls
    (function names starting with DF_) should not be called.


Consult demo.c for commented example code.


