import os
import re
import sys
import pickle

import DataFlow

Unspecified = object()
UPDIR = re.compile( r'(?<=/)[^/]+/\.\.(/|$)' )

def dbreport(**k): print( '\n'.join( '%s = %r' % item for item in k.items() ) )

class Wormhole( object ):
	def __init__( self, name='wormhole', minSize=1024*1024*10, protocol=None, prefix=None ):
		self._prefix = ''
		if prefix: prefix = self._joinpath( prefix )
		if isinstance( name, Wormhole ):
			other = name
			self.__archive = other.__archive
			self._prefix   = other._prefix   if prefix   is None else prefix
			self._protocol = other._protocol if protocol is None else protocol
		else:
			self.__archive = DataFlow.SharedDataArchive( name, minSize )
			self._prefix   = '/' if prefix   is None else prefix
			self._protocol =  2  if protocol is None else protocol

	def _joinpath( self, *parts ):
		path = '/' + '/'.join( x for part in [ self._prefix ] + list( parts ) for x in ( part.replace( '\\', '/' ).split( '/' ) if part else [] )  if x and x != '.' )
		while re.findall( UPDIR, path ): path = re.sub( UPDIR, '', path )
		return path

	def _del( self, name ):
		try: f = self.__archive.Open( self._prefix + '/' + name, 'o' )
		except IOError as error: pass
		else: f.Remove()
		del f
		
	def _get( self, name, defaultValue=Unspecified ):
		lock = self.__archive.Open( '$a/lock', 'w', 1 )
		try:
			f = self.__archive.Open( self._prefix + '/' + name )
		except IOError as error:
			if defaultValue is Unspecified: raise( error )
			content = pickle.dumps( defaultValue, protocol=self._protocol )
			f = self.__archive.Open( self._prefix + '/' + name, 'w', len( content ) )
			f.Write( content )
		else:
			content = f.Read()
		finally:
			del lock # must be within a finally clause, because otherwise it will get preserved together with all the other local variables for post-mortem debugging if an exception is raised
		del f
		return pickle.loads( content )	
		
	def _set( self, *pargs, **kwargs ):
		try: dict( pargs )
		except: raise ValueError( 'bad *pargs' )
		lock = self.__archive.Open( '$a/lock', 'w', 1 )
		try:
			for name, value in list( pargs ) + list( kwargs.items() ):
				content = pickle.dumps( value, protocol=self._protocol )
				f = self.__archive.Open( self._prefix + '/' + name, 'w', len( content ) )
				f.Write( content )
		finally:
			del lock # must be within a finally clause, because otherwise it will get preserved together with all the other local variables for post-mortem debugging if an exception is raised
		return self
	
	def _subtree( self, *relativePath ):
		return self.__class__( self, prefix=self._joinpath( *relativePath ) )
	
	def _getdict( self, pattern='*', hierarchical=False, keysOnly=False ):
		pattern = pattern.replace( '\\', '/' )
		pattern = '/' + pattern.lstrip( '/' )
		if '*' not in pattern: pattern = pattern.rstrip( '/' ) + '/*'
		pattern = pattern.strip('/').split( '/' )
		firstStar = min( i for i, piece in enumerate( pattern + [ '*' ] ) if '*' in piece )
		stem = '$d' + self._joinpath( *pattern[ :firstStar ] ).rstrip( '/' )
		pattern = '/'.join( pattern[ firstStar: ] )
		#dbreport(stem=stem, patt=pattern)
		pattern = pattern.replace( '(', r'\(' ).replace( ')', r'\)' ) # 2 (must be before 3 because 2 escapes existing parentheses, and 3 may introduce unescaped parentheses)
		pattern = re.sub( r'\[[^\[\]]+\|[^\[\]]+\]', lambda match: '(' + match.group( 0 )[ 1 : -1 ] + ')', pattern ) # 3
		pattern = pattern.replace( '.', '\\.' ).replace( '?', '[^/]' )
		pattern = pattern.replace( '**', '\x00' )
		pattern = pattern.replace( '*', '[^/]*' )
		pattern = pattern.replace( '\x00', '.*' )
		pattern = '^' + pattern + '$'
		result = {}
		try: root = self.__archive.Open( stem, 'r' )
		except IOError: return result
		for file in root.Walk( restrictToTypes='f' ):
			partialName = file.GetName()[ len( stem ) + 1: ]
			if re.match( pattern, partialName ):
				result[ partialName ] = 1 if keysOnly else pickle.loads( file.Read() )
		if hierarchical:
			backlog = [ result ]
			while backlog:
				parent = backlog.pop( 0 )
				for k, v in list( parent.items() ):
					if '/' in k:
						first, rest = k.split( '/', 1 )
						child = parent.get( first, None )
						if child is None:
							child = parent[ first ] = {}
							backlog.append( child )
						child[ rest ] = parent.pop( k )
		elif keysOnly:
			result = list( result )
		return result
		
	def __delitem__( self, key ): self._del( key )
	def __getitem__( self, key ): return self._get( key )
	def __setitem__( self, key, value ): self._set( ( key, value ) )
	def __delattr__( self, key ): self._del( key )
	def __getattr__( self, attrName ): return self._get( attrName )	
	def __setattr__( self, attrName, value ):
		if attrName.startswith( '_' ): return object.__setattr__( self, attrName, value )
		self._set( ( attrName, value ) )
	#def __getattribute__( self, attrName, defaultValue=Unspecified ):
	#	if attrName.startswith( '_' ): return object.__getattribute__( self, attrName, defaultValue )
	#	return self._get( ( attrName, defaultValue ) )
	def __dir__( self ): return self._getdict( keysOnly=True )
	_getAttributeNames = __dir__
