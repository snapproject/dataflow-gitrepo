.. default-role:: code

What is DataFlow?
=================

DataFlow is a cross-platform software library for inter-process communication,
optimized for neurotechnology applications such as brain-computer interfaces.
Such systems typically consist of multiple modular subsystems that want to share
(a) a large number of small pieces of information that do not change often (what
some applications, for  example `BCI2000 <https://bci2000.org>`_, would call
"parameters") as well as (b) a small number of pieces of information that change
hundreds to thousands of times per second, in  the form of time series
("signals"), and possibly (c) several other pieces of information  that may
change with intermediate frequency, either regularly ("states") or irregularly
("events").  DataFlow does not draw a distinction between these types of data,
but nonetheless aims to allow processes to share them all easily and
efficiently, while imposing as few constraints as possible.

It achieves this using an asynchronous shared memory buffer inspired by the
`FieldTrip Buffer <https://www.fieldtriptoolbox.org/development/realtime/buffer/>`_
(a standalone "realtime" part of the FieldTrip project, not to be confused with
the popular FieldTrip toolbox for Matlab). In contrast to the FieldTrip Buffer,
DataFlow does not center everything around one signal source at a single data
rate. Instead, inspired by `Labstreaminglayer (LSL)
<https://labstreaminglayer.readthedocs.io>`_, an arbitrary number of separate
data streams are handled—at independent rates, if desired. DataFlow aims to
offer the "best of both worlds" from FieldTrip Buffer and LSL without being
dependent on either.

DataFlow is currently unfinished but offers some useful functionality to
approximate these goals. See the `Status`_ section, below.

Where does DataFlow fit?
========================

* It is a dynamic library, implemented in C++11, with functions exposed in C.

* It includes wrappers for other languages: auto-extending wrappers are written
  for  C++ and for Python 2.x & 3.x.

* Compilation uses no third-party headers/code (no boost, etc). See `How to
  build DataFlow`_ below, for details.

* It contains 10,000 lines over about 60 files.

* The lower levels are undocumented (hey, at least nothing is called `x` or `i`
  or `tmp`).

* It has no third-party dependencies at runtime.

* So far has passed its tests (at each stage of development) using the same
  codebase and build scripts, on:

  - Windows (i386)
  - Windows (amd64/x86_64)
  - macOS (x86_64)
  - Linux (Ubuntu 18.04 LTS and 20.04 LTS on amd64/x86_64; 32-bit Raspbian on
    ARMv6 and ARMv7)
  
  Porting and testing is planned for 64-bit ARMv8. DataFlow has never been
  tested on any big-endian system, and there are no plans to do so.


What kind of data can DataFlow represent?
=========================================

Basic data types
----------------

* Floating point numbers (32-bit or 64-bit)

* Integers (all the usual flavors: 8 to 64 bit, signed or unsigned)

* Strings (raw bytes—assume UTF-8 encoding if you want)

* Packed arrays of any of the above (number of dimensions, extent of each
  dimension, raw data). For packed arrays of strings, a delimiter is used in the
  raw data to allow each string "element" to have a different length.

Anything higher-order: `DataNode` trees
---------------------------------------

* Associate any basic data type with a string key.

* Use the convention that `/` is a hierarchy delimiter in those keys.

* Allows de-facto implementation of lists, dictionaries, classes::

      /Subject/Name      -> "Bob"
      /Subject/Fingers/0 -> "left thumb"
      /Subject/Fingers/1 -> "left index"
      /Subject/Fingers/2 -> "left middle"
      ...
      /EMG/SamplesPerSecond  -> 3200
      /EMG/Signal/Agonist    -> 0.0043445
      ...


Operational details
===================

* DataFlow provides fast shared-memory access to a data hierarchy (one segment, 
  typically tens/hundreds of MB). It manages multiple processes' access to the
  shared memory segment using a readers/writer lock, implemented via kernel
  primitives (semaphores).
  
* It will treat all entries homogeneously as collections of "events" separated
  in time—no distinction is made between what BCI2000 would call "parameters",
  "states", "events" and "signals".  Some (i.e. parameters) may only ever get
  set once. Others (events) may change irregularly. Others (signals and states)
  will be updated regularly. None of that is enforced—it's the clients'
  business, not DataFlow's.

* Multiple threads of the same process can also access DataFlow. To do this,
  they do not do anything different—they simply do not know that they are not
  separate processes. They must have separate instances of the class that
  represents the connection to DataFlow, and they will wait for each others'
  kernel-level signals just like processes do.

* DataFlow will always remember the most recent "event" attached to any name
  (if it runs out of the space necessary to do this, that's fatal).

* Additionally, it will remember as many past events as will fit in the
  allocated space, organized for fast retrieval (e.g. as a packed time series)
  under each key. One exception is that the library's "memory" for a particular
  key will be zeroed if you change the "format" of the data associated with it
  (i.e. you submit an event whose basic type—and/or dimensions, if it's a packed
  array—do not match the previous event submitted under the same key).
  Otherwise, events will be kept as long as memory allows (oldest events
  overwritten when necessary).

* It will allow retrieval primarily on a "look-back" basis (e.g. "the last 250 
  milliseconds", or "the last N events") according to a certain query grammar
  (probably not a "language" parsed from a string, but rather a query object
  built call-by-call in the client's memory space).  A snapshot of data under
  multiple keys can be delivered at once, based on criteria from an independent
  set of keys if desired ("at once" means based on a single, consistent,
  frozen state of the shared-memory segment, between the time the client locks
  it and the first time it unlocks it).

* Transactions with DataFlow hold the readers/writer lock for as little time as 
  possible, to allow multiple processes to interleave their transactions. A 
  successfully-fulfilled request will make a copy of the requested data in local
  (non-shared) memory.

* Processes wait for their requests to be fulfilled without busy-waiting: any 
  not-immediately-fulfillable request is stored as a request-for-notification in
  the shared memory segment itself, and these are checked by any process that
  writes to the archive. When a request is judged to be fulfillable, it is
  removed from the archive and an inter-process signal is sent by semaphore to
  the waiting process.


Status
======

* The "back end" is implemented, including cross-platform management of:

  - shared memory segments
  - semaphores
  - readers-writer locks
  - the `SharedDataArchive` (synthesizes all of the above)
  - `DataNode` trees
  
  2020's big achievement was the `SharedDataArchive` class: it creates/accesses
  a shared-memory-resident "file system" with efficient lookup, tree traversal
  and retrieval even when "files" are fragmented. This was required to keep
  track of events coming in under different keys in unanticipated order and in
  unanticipated amounts, while retaining a structure in which packed time-series
  retrieval is efficient.
  
  The SharedDataArchive requires all of the above elements except `DataNode`
  trees. It has been thoroughly unit-tested at the level of `SharedDataArchive`
  and `SharedDataArchive::File` methods. There is some documentation in the
  header `devel/src/SharedDataArchive.hpp`.

* The "front end" is not yet done. Some source files (`RingBuffer.cpp`) are in 
  indeterminate state, not yet included in the `CPP_LIBRARY_FILES` list in
  `devel/build/CMakeLists.txt` and so do not get compiled. Others (`Request.cpp`
  and `Confluence.cpp`) are included and compiled but are non-working stubs.
  Unfinished functionality includes:

  - Organizing the most-recent-event per key, time-series-of-previous-events per
    key, and global index of events across all keys, into "files" in the
    `SharedDataArchive`.
    
  - Building a request (simultaneously serializing it, so that it can be stored
    in the `SharedDataArchive` if not immediately fulfillable, or sent over
    TCP/IP)
  
  - Checking whether a request is fulfillable.
  
  - Retrieving and packing the requested data into a `DataNode` tree.  
  
  Example requests (plain English versions) might be:
  
  - "When you have 320 samples of `/EMG/Signal/Agonist` occurring after a
    `/EMG/TriggerCopy/RisingEdgeDetected` event more recent than [XYZ], give me
    the events across the matching timespan from the whole `/EMG` tree".
    
  - "When you have samples of `/EEG/Signal` that span at least 1 second,
    and 250 milliseconds' worth of them are more recent than [XYZ], give
    me the last 1 second of everything under `/EEG` (and if there are nodes
    of that tree that haven't been updated during that time, just give
    me their most-recent values again)."

* The "peripherals" are not yet done. This includes:

  - A simple standalone TCP/IP server that receives serialized requests,
    submits them to a local DataFlow instance, waits for fulfillment,
    serializes the resulting `DataNode` tree and sends it back.
    
  - A client that sends a serialized request over TCP/IP to a server at a known
    address and port, waits for the response, and unserializes the result as a
    `DataNode` tree. The client code will be rolled into the library itself—from
    the client application's point of view, there needs to be minimal difference
    between accessing a local DataFlow instance and a remote one.

  - A BCI2000 module that interfaces with a (remote or local) DataFlow instance.
  
  - An LSL inlet and outlet that interface with a (remote or local) DataFlow
    instance.
  
  - A standalone utility that regularly scrapes all new events and saves them to
    file into a generic DataFlow-optimized format.

  - A reader utility for the DataFlow file format.

How to build DataFlow
=====================

Build-time requirements are:
  
* CMake from https://www.cmake.org (for Windows, opt for "add to PATH
  for all users" during installation)
* Python (any version)
* a C compiler (tested with MSVC on Windows, `gcc` everywhere else)
* a C++11 compiler (tested with MSVC on Windows, `g++ -std=c++11` everywhere
  else)
   
The script `devel/build/go.cmd` is a bilingual POSIX/Windows script that wraps
the necessary `cmake` and `make` calls (or `cmake` and `msbuild` on Windows—the
wrapper only supports MSVC). It can be run from any working directory, so for
example you can say::

    .\devel\build\go.cmd
  
on Windows (pass the optional argument `Win32` or `Win64` to specify the target
architecture). Anywhere else, you would say::

    ./devel/build/go.cmd

Set the environment variable `DFSKIPTESTS=1` to build only the dynamic library
and skip all the test executables. Or, conversely, set `DFQUICKTEST=1` to build
only `cpptest` and skip everything else (even the library).

On Linux, you can also cross-compile by defining `CMAKE_TOOLCHAIN_FILE` on the
`go.cmd` command line::

    ./devel/build/go.cmd -DCMAKE_TOOLCHAIN_FILE=devel/build/crosscompile/piZero-on-linux64.cmake

Each toolchain file will expect the `$TOOLCHAINS` environment variable to be
defined, specifying a directory where you keep toolchains in general. It will
also expect a `gcc`/`g++` toolchain to be available at a specifically-named
subdirectory of that directory. The toolchain file in the above example,
`piZero-on-linux64.cmake`, expects a toolchain to be located at
`raspberrypi-tools/arm-bcm2708/arm-linux-gnueabihf` relative to `$TOOLCHAINS`.
This can be set up as follows::

    export TOOLCHAINS=$HOME/crosscompile   # or wherever you like
    mkdir -p "$TOOLCHAINS"
    cd "$TOOLCHAINS"
    git clone https://github.com/raspberrypi/tools  raspberrypi-tools

Other toolchains will have other equally-specific requirements, so you will have
to examine the file you intend to use, to determine exactly what it wants.


What does the build script build?
=================================

Here's what you get from running `devel/bin/go.cmd`:

* In `release`: everything needed to release the DataFlow library in its current
  (albeit unfinished) state, including the dynamic library itself, headers and
  wrappers.  In particular, `DataFlow.py` is a wrapper that can be imported as a
  module in Python 2.7 or 3.x, allowing rapid testing.   In
  `devel/test/minimaltest.c` and  `devel/test/minimaltest.cpp` you can see
  templates for how a client might write code, in C or C++ respectively, to link
  dynamically to DataFlow.

* In `devel/bin`: a few binaries for testing:

  - a copy of the dynamic library, identical to the one in `release`
  
  - `ctest`:  based on `release/demo.c` (a minimal, unfinished placeholder
    demo), this console program is built a test of static linking from C source
    to the (wrapped) DataFlow C++ sources.
    
  - `dlltest`:  also based on `release/demo.c`, this console program is a test
    of *dynamic* linking from C source to the DataFlow library.

  - `cpptest`: based on the separate `devel/test/test.cpp` source, this C++
    console program statically links to the DataFlow C++ sources and allows
    testing of multiple aspects of the underlying framework. The
    `cpptest --help` output provides an overview. The most sophisticated
    function is an interactive realtime SharedDataArchive viewer:
    `cpptest --SharedDataArchive ARCHIVE_NAME summary --period=1` (see the
    output of `cpptest --SharedDataArchive --help` for details).

  - `speedtest`: based on the separate `devel/test/speedtest.cpp` source, this
    is a C++ console program. Although it uses a number of other source files
    from the DataFlow codebase as statically-linked helpers for its own purposes
    (mundane things like timing, string and exception handling) this program
    otherwise links dynamically to the DataFlow library the same way that a
    client would. For details, see the `Performance testing`_ section below.

The dynamically-linked test programs (`dlltest` and `speedtest`) first look for
the dynamic library in the current working directory, then look in their own
directory (next to the executable), then finally search the library path. This
unusual ordering was implemented to ensure common behavior across Windows,
Darwin and Linux. It is controlled by the second argument, `autoSearch`, that
you pass to `Load_DF_API()`. If you pass 0 here (or if you pass an explicit
dynamic-library path as the first argument and it contains slashes), then the
different platforms will follow their own OS-specific conventions (Windows:
executable-directory, path, working-directory; Darwin: working-directory, path;
Linux: path only, so you'd better set the `LD_LIBRARY_PATH` environment
variable).


Stability testing
=================

Unit tests that try to break the `SharedDataArchive` with random-but-replicable
sequences of "file system" operations are currently in a separate repository:: 

    git clone git@github.com:cmlino/dataflow-tests  test

TODO: fork the `dataflow-tests` repository; document it more.


Performance testing
===================

The following low-level tests give an idea of the precision and overhead
involved in basic operations:

* `cpptest --TimeUtils`
* `cpptest --NamedSemaphore`  (run it from more than one console concurrently)
* `cpptest --ReadWriteMutex`  (run it from more than one console concurrently)

A more-integrated performance estimate can be obtained via `speedtest`. Multiple
instances of `speedtest` are designed to be run simultaneously, using each
other's inputs as outputs in whatever configuration you like. In the absence of
the finished DataFlow "front end", `speedtest` does a simple test of data
throughput, simply writing junk data to, and/or reading it back from, named
"files" in the virtual "file system" of a `SharedDataArchive` and using a
rudimentary request-for-notification system also implemented in the archive.
Positional arguments, in order, are:

    `--period=MSEC`
        
        A numerical value dictating the number of milliseconds per cycle.  It
        doesn't have to be the same across instances that feed each other, but
        that will have efficiency implications. It can be 0, meaning "work as
        fast as possible". The default value is 50.
        
    `--in=INPUT_BYTES_PER_CYCLE`
    
        An integer number of bytes to read per cycle (0 means "no input
        expected"). The default value is 0.
        
    `--out=OUTPUT_BYTES_PER_CYCLE`
        
        An integer number of bytes to write per cycle. Default value is 0.
        
    `--stride=NEW_INPUT_BYTES_PER_CYCLE`
            
        An integer indicating the number of bytes to move forward on each
        cycle in the input file. It defaults to be the same as
        `INPUT_BYTES_PER_CYCLE` but can be made different, e.g. to emulate
        demand for data in overlapping sliding windows.
        
    `--from=INPUT_FILENAME`
    
        A string dictating the name of the "file" in the `SharedDataArchive`
        from which data should be read. It cannot be the same as the `--to`
        filename if there is both non-zero input and non-zero output. The
        default value is "default".
        
    `--to=OUTPUT_FILENAME`
    
        A string dictating the name of the "file" in the `SharedDataArchive`
        to which data should be written. It cannot be the same as the `--from`
        filename if there is both non-zero input and non-zero output. The
        default value is "default".
    
    `--report=REPORT_FILENAME`
    
        A string dictating the name of the file to which to write the report
        when the program terminates. Blank means stdout. Default is blank
        Substrings `$YYYYMMDD` and `$HHMMSS` will automatically get
		interpolated to make date/time stamps within the filename.

    `--verbosity=SPEEDTEST_VERBOSITY_LEVEL`
    
        An integer, dictating the speedtest code's own level of verbosity. At
        level 1, print each cycle timestamps to the console. At level 2,
        print a bit more, etc.  The default is 0 unless there's zero input
        and zero output, in which case the default is 1.
        
    `--debug=DATAFLOW_LIBRARY_DEBUG_LEVEL`
        
        An integer, indicating the verbosity/debug level of the DataFlow library
        itself. The default value is 0.
    

Note that non-zero values of either `--debug` or `--verbosity` will impact
performance  considerably.

Each speedtest instance runs until it is interrupted with ctrl-C. The `results`
field of the Python-/JSON- formatted output is one line per cycle, reporting the
cycle duration in seconds followed by the amount of idle time (waiting for
request-for-notification, but *could* have been used for other processing).
